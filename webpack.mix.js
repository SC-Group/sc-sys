let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
let rootsrc = `resources/assets`;
let scjsroot = `${rootsrc}/js/sccomponents`;
let scsassroot = `${rootsrc}/sass/sccomponents`;

mix.js(`${scjsroot}/main/app.js`, 'public/js/app.js');
mix.js(`${scjsroot}/credenciamentoScripts/StartCredenciamento.js`, 'public/js/credenciamento.js');
mix.js(`${scjsroot}/camisasScripts/StartCamisa.js`,`public/js/camisas.js`);
mix.js(`${scjsroot}/eventScripts/ConfigEventoVariables.js`,`public/js/evento.js`);
mix.js(`${scjsroot}/formScripts/form.js`,`public/js/form.js`);
mix.js(`${scjsroot}/atividadesScripts/StartAtividades.js`,`public/js/atividades.js`);
mix.js(`${scjsroot}/patrociniosScripts/StartPatrocinios.js`,`public/js/patrocinios.js`);
mix.js(`${scjsroot}/apoiosScripts/StartApoios.js`,`public/js/apoios.js`);
mix.js(`${scjsroot}/staffsScripts/StartStaffs.js`,`public/js/staffs.js`);
mix.js(`${scjsroot}/profileScripts/StartProfile.js`,`public/js/profile.js`);
mix.js(`${scjsroot}/cronogramaScripts/StartCronogramas.js`,`public/js/cronogramas.js`);


//********************Sass**************************//

mix.sass(`${scsassroot}/main.scss`,`public/css/sc.css`);
// mix.sass(`${scsassroot}/form/main.scss`,`public/css/sc-form.css`);
// mix.sass(`${scsassroot}/comissao/main.scss`,`public/css/sc-comissao.css`);
// mix.sass(`${scsassroot}/comissao/main.scss`,`public/css/sc-comissao.css`);