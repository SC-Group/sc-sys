# Instalação do sistema da Semana da Computação usando Docker

-Utilizando Docker e Docker Compose, precisamo apenas fazer o seguinte

> cp .env.example .env

Agora, precisamos fazer algumas modificações no nosso arquivo **.env**.
Todos esses valores foram definidos no arquivo **docker-compose.yml**.

```bash
DB_CONNECTION=pgsql
DB_HOST=postgres
DB_PORT=5432
DB_DATABASE=evento
DB_USERNAME=evento
DB_PASSWORD=evento
```

Agora, vamos seguir.
Primeiro, vamos subimos os containers que foram definidos no **docker-compose.yml**

> $ docker-compose up -d

Agora, instalamos o projeto

> $ docker-compose run app composer install

Aplicamos as migrações
> $ docker-compose run app php artisan migrate

Criamos uma nova chave para o projeto que ficará no arquivo **.env**

> $ docker-compose run app php artisan key:generate

Ao acessar o endereço **localhost**, será possível visualizar o projeto.

Sempre que for necessário executar algum comando (seja do Composer ou do próprio Laravel), basta digitar no terminal

> $ docker-compose run [app|postgres] [command]


Mais informações sobre o docker e docker-compose [aqui](https://docs.docker.com/) e [aqui](https://docs.docker.com/compose/)

