# Instalação do sistema da Semana da Computação

> OBS: Neste Passo a passo não está sendo considerado instalação com https.

## Requisitos mínimos
* PHP >= 7.2
* BCMath PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Apache IIS ou Nginx com suporte a php e módulo rewrite habilitado (o apache precisa ter overwriting de .htaccess habilitado).
* Composer package manager
* node.js 12.16+
* Banco de dados (mysql ou pgsql) configurado com 1 schema sendo usado exclusivamente para o sistema. (Com usuário e senha definidos com plenos poderes para este schema).
* \[**Opcional**: Ver [configurando node project](#configurando-node-project-não-obrigatório) \] screen (aplicativo multi-instância para terminal)

* \[**Opcional**: Ver [configurando node project](#configurando-node-project-não-obrigatório) \] Redis

## Instalação

> Neste passo-a-passo será usado o servidor Apache em um computador debian based. Supondo que seu projeto estará em `/home/usuario/Projetos/sc-sys`. Usaremos o vim como editor de texto.

* Fazer clone do repositório em uma pasta de sua preferência (no nosso caso, em `/home/usuario/Projetos`)
_____
### Configurando o servidor para apontar para o diretório público de seu projeto...

* Vá para o diretório de configuração do apache
```bash
    cd /etc/apache2/
```

* Abra o apache2.conf 
```bash
    sudo vim apache2.conf
```

* Cole o texto abaixo no apache.conf, abaixo da configuração de diretório do `/var/www/`. Se sua pasta do projeto não for `/home/usuario/Projeto`, troque-a adequadamente. Se sua pasta do projeto for a própria `/var/www/`, apenas substitua o conteúdo da configuração pelo conteúdo dentro das tags abaixo.

```apacheconf
<Directory /home/usuario/Projetos>
    Options Indexes FollowSymLinks
    AllowOverride all
    Require all granted
</Directory>
```

* Vá para a pasta de configuração do site e copie o arquivo 000-default.conf atribuindo o nome do seu site ao novo arquivo. (Aqui escolhemos `sys.scufrj.com`). E abra o novo arquivo como root.

```bash
    cd /etc/apache2/sites-available
    sudo cp 000-default.conf sys.scufrj.com.conf
    sudo vim sys.scufrj.com.conf
```

* Configure o novo arquivo como abaixo, fazendo as substituições necessárias dependendo de como e onde seu projeto foi configurado. Após alterações, salve.
> OBS: Se você precisar configurar o projeto com https, será necessário incluir as credenciais neste arquivo, bem como modificar a porta para 443. Aqui está um link que pode ser útil: [https://www.aprendendolinux.com/configurando-o-apache-com-com-certificado-ssl-livre-da-lets-encrypt/](https://www.aprendendolinux.com/configurando-o-apache-com-com-certificado-ssl-livre-da-lets-encrypt/)


``` apacheconf
<VirtualHost *:80>
    ServerAdmin admin@sys.scufrj.com
    ServerName sys.scufrj.com
    ServerAlias sys.scufrj.com
    DocumentRoot /home/usuario/Projetos/sc-sys/public
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

* Reinicie o servidor

```bash
    sudo service apache2 restart
```
____
### Configurando o projeto

* Vá até o diretório raíz do projeto

```bash
    cd /home/usuario/Projetos/sc-sys
```

* Copie o arquivo .env.example para .env e edite este arquivo com as informações necessárias.
```bash
    sudo cp .env.example .env
```

* É imprescindível preencher estes campos do .env salvando logo após.

```env
APP_DEBUG= "<<se for local, true, senao false>>"
APP_URL="<<url raiz do sistema ex: sys.scufrj.com>>"


DB_CONNECTION= "<<mysql ou pgsql>>" 
DB_HOST= "<<localhost>>"
DB_PORT= "<<geralmente 3306>>"
DB_DATABASE="<<nome do schema mysql ou database pgsql>>"
DB_USERNAME="<<db_user>>"
DB_PASSWORD="<<db_password>>"

MAIL_DRIVER=smtp
MAIL_HOST="<<host de email, adquirido do provedor>>"
MAIL_PORT="<<porta padrão do host>>"
MAIL_USERNAME=<<email>>
MAIL_PASSWORD=<<Senha (no caso do google, senha de aplicativo, conseguida nas configurações da conta)>>
MAIL_ENCRYPTION="<<geralmente tls>>"

```

* instalando pacotes (supondo que já esteja no diretório raiz so projeto):

```bash
composer install
```

* configurando as permissões do projeto (supondo que esteja no raiz do projeto).

```bash
sudo chmod 775 -R .
sudo chmod 777 -R public
sudo chmod 777 -R storage
```

* compilando js e sass:
```bash
   npm run prod
```
* Caso precise usar um bot de envio de mensagens, preencha os espaços necessários no arquivo .env e troque `BOT_ENABLED` para true.

* Rode os comandos um por vez:
```bash
    php artisan key:generate
    php artisan migrate
    php artisan db:seed
```

**Seu projeto deverá funcionar em localhost (se local) ou no domínio definido**

**Para primeiro uso, o usuário é o definido no .env e a senha é `scadmin`. troque esta senha na sessão de recuperação de senha**

______
### configurando node project (não obrigatório)

Para habilitar a atualização automática dos itens de credenciamento, é necessário habilitar o servidor node.

* Configure o Redis no arquivo .env com host, port, etc. e garanta que o serviço do redis esteja funcionando.

* Configure o servidor node nos campos apropriados do .env

* Na pasta raiz do projeto, execute:
```bash
npm install
``` 

* Crie uma nova instância do screen.
* Dentro da instancia criada do screen, execute:
```bash
npm run start
``` 