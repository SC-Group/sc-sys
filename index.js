require('dotenv').config();
const fs = require('fs');
const https = require('https');
const http = require('http');
let socket_io = require('socket.io');
let Redis = require('ioredis');

let redis = new Redis({
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST,
    password: process.env.REDIS_PASSWORD,
});

function onRequest(req,res){
    res.writeHead(200, {
        'Access-Control-Allow-Origin' : process.env.APP_URL,
    });
}

let io;

if(process.env.APP_ENV!=='local'){
    const privateKey = fs.readFileSync(process.env.NODESERVER_CERTIFICATE_PATH+'/privkey.pem', 'utf8');
    const certificate = fs.readFileSync(process.env.NODESERVER_CERTIFICATE_PATH+'/cert.pem', 'utf8');
    const ca = fs.readFileSync(process.env.NODESERVER_CERTIFICATE_PATH+'/chain.pem', 'utf8');

    const credentials = {
        key: privateKey,
        cert: certificate,
        ca: ca
    };
    const httpsServer = https.createServer(credentials, onRequest);
    io = socket_io(httpsServer);
    httpsServer.listen(process.env.NODESERVER_PORT, function(){
        console.log(`Listening on Port ${process.env.NODESERVER_PORT}`);
    });
}else{
    const httpServer = http.createServer(onRequest);
    io = socket_io(httpServer);
    httpServer.listen(process.env.NODESERVER_PORT, function(){
        console.log(`Listening on Port ${process.env.NODESERVER_PORT}`);
    });
}

//--------------------------------------------------------------------

redis.subscribe('sc-data-channel', function(err, count) {});

redis.on('message', function(channel, data) {
    data = JSON.parse(data);
    io.emit(""+data.key, JSON.stringify(data.dataset));
});