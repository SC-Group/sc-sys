<?php
/**
 * Created by PhpStorm.
 * User: fjmrs
 * Date: 03/03/2019
 * Time: 18:27
 */

namespace App\Helpers;


class RegexPatternJSHelper
{
    public static function _default($pattern,$msg=null, $options=''){
        $msg = $msg??__("messages.FormValidation.InvalidField");
        return[
            "pattern"=>$pattern,
            "patternOptions"=>$options,
            "msg"=>$msg
        ];
    }
    public static function email($options='',$msg=null){
        $msg = $msg??__("validation.email",["attribute"=>"de E-mail"]);
        $pattern = '(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))';
        return RegexPatternJSHelper::_default($pattern,$msg,$options);
    }

    public static function url($options='',$msg=null){
        $msg = $msg??__("validation.url");
        $pattern =  '^((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$';
        return RegexPatternJSHelper::_default($pattern,$msg,$options);
    }

    public static function name($options='',$msg=null){
        $pattern = "^([A-ZÉÚÍÓÁÇÈÙÌÒÀÕÃÑÊÛÎÔÂËYÜÏÖÄ](\'?[a-zéúíóáèùìòàçõãñêûîôâëÿüïöä])+(-[A-ZÉÚÍÓÁÇÈÙÌÒÀÕÃÑÊÛÎÔÂËYÜÏÖÄ](\'?[a-zéúíóáèùìòàçõãñêûîôâëÿüïöä])+)*( ((e|y|de( lo| los| la| las)?|do|dos|da|das|del|van|von|bin|le) )?(d\'|O\'|Mc|Mac|al\-)?[A-ZÉÚÍÓÁÇÈÙÌÒÀÕÃÑÊÛÎÔÂËYÜÏÖÄ](\'?[a-zéúíóáèùìòàçõãñêûîôâëÿüïöä])+(-[A-ZÉÚÍÓÁÇÈÙÌÒÀÕÃÑÊÛÎÔÂËYÜÏÖÄ](\'?[a-zéúíóáèùìòàçõãñêûîôâëÿüïöä])+)*)+( Jr\.|II|III|IV)?)$";
        $msg = $msg??__("messages.FormValidation.ValidName");
        return RegexPatternJSHelper::_default($pattern,$msg,$options);
    }

    public static function password($options='',$msg=null){
        $pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})";//(?=.*[\[\].? =+¨\-_\\!@#$%\^&\*])";
        $msg = $msg??__("messages.FormValidation.PasswordRegex");
        return RegexPatternJSHelper::_default($pattern,$msg,$options);
    }

}
