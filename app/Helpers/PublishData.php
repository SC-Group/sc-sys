<?php


namespace App\Helpers;
use Illuminate\Support\Facades\Redis;

class PublishData
{
    public static function publish($key,$data){
        Redis::publish("sc-data-channel",json_encode(["key"=>$key,'dataset'=>$data]));
    }
}