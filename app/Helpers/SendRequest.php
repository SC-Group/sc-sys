<?php


namespace App\Helpers;


class SendRequest
{

    public static function send($url,$data,$dataType){
        switch($dataType){
            case "json":$content=['application/json',json_encode($data)];break;
            case "form":$content=['application/x-www-form-urlencoded',http_build_query($data)];break;
            case "html":$content=['text/html',$data];break;
            case "string":$content=['text/plain',$data];break;
        }
        $options = array(
            'http' => array(
                'header'  => "Content-type: $content[0]\r\n",
                'method'  => 'POST',
                'content' => $content[1]
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) {
            throw new \HttpException(__("messages.UnexpectedError"));
        }
        return $result;
    }
}