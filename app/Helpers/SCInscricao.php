<?php


namespace App\Helpers;


class SCInscricao
{
    public static function get($inscricaoId,$numZeros=4){
        $inscStr = "$inscricaoId";
        $inscLen = strlen($inscStr);
        $numZeros = $numZeros>=$inscLen?$numZeros-$inscLen:0;
        $out ="SC-";
        for($i=0;$i<$numZeros;$i++) $out.="0";
        return $out.$inscStr;
    }
}