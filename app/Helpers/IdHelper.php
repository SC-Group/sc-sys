<?php
/**
 * Created by PhpStorm.
 * User: filipe
 * Date: 01/03/18
 * Time: 15:01
 */

namespace App\Helpers;
use Vinkla\Hashids\Facades\Hashids;
class IdHelper
{
    public static function enc(Array $arr){
        array_push($arr,(int)rand(0,100));
        return Hashids::encode($arr);
    }
    public static function dec($str){
        $arr=Hashids::decode($str);
        array_pop($arr);
        return $arr;
    }
}