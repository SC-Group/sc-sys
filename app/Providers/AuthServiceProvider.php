<?php

namespace App\Providers;

use App\Models\Comissao;
use App\Models\Evento;
use App\Models\Palestrante;
use App\Models\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Aluno as Aluno;
use App\Models\Professor as Professor;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes(function ($router){
            $router->forTransientTokens();
        });
        Passport::tokensExpireIn(now()->addDays(20));
        Passport::refreshTokensExpireIn(now()->addDays(1));
        Passport::personalAccessTokensExpireIn(now()->addDays(20));

        Passport::tokensCan([
            'be-staff' => 'Permissões de staff',
            'be-comissao' => 'Permissões de comissão',
            'be-aplicacao' => "Permissões para aplicativos externos ou extensões"
        ]);

        Gate::define('be-applicacao', function ($user) {
            $aluno = Aluno::find($user->id);
            if($aluno!==null){
                return $aluno->comissao()->exists();
            }
            return false;
        });

        Gate::define('be-comissao', function ($user) {
            $aluno = Aluno::find($user->id);
            $superadmin = $user->superadmin;
            if($superadmin) return true;
            if($aluno!==null){
                return $aluno->comissao()->exists();
            }
            return false;
        });

        Gate::define('be-staff', function ($user) {
            $aluno=$user->aluno();
            $ret=false;
            if($user->superadmin) return true;
            if($aluno->exists()){
                $evento=Evento::whereDate('fim', '>=', Carbon::today()->subDays(7)->toDateString());
                if($evento->exists()){
                    $ret = $aluno->first()->staff()->where('evento_id',$evento->first()->id)->where("status","aprovado")->exists();
                }
                return Gate::check("be-comissao")||$ret;
            }
            return false;
        });

        Gate::define('be-aluno', function ($user) {
            return Aluno::where("id","=",$user->id)->exists();
        });

        Gate::define('be-professor', function ($user) {
            return Professor::where("id","=",$user->id)->exists();
        });

        Gate::define('be-palestrante', function ($user) {
            return Palestrante::where("id","=",$user->id)->exists();
        });
    }
}
