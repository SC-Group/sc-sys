<?php


namespace App\Exceptions;


use Throwable;

class NotImplementedException extends \Exception
{
    public function __construct($message = "NotImplementedException", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}