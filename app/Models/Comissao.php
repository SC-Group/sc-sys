<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comissao extends Model
{
    public function aluno(){
        return $this->hasMany("App\Models\Aluno");
    }
}
