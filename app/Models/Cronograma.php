<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cronograma extends Model
{
    protected $fillable=["atividade_id", "dia_id", "hora_id", "local_id"];
    public function atividade(){
        return $this->belongsTo("App\Models\Atividade");
    }

    public function palestrante(){
        return $this->belongsTo("App\Models\Palestrante");
    }

    public function staff(){
        return $this->belongsTo("App\Models\Staff");
    }

    public function hora(){
        return $this->belongsTo("App\Models\Hora");
    }

    public function dia(){
        return $this->belongsTo("App\Models\Dia");
    }

}
