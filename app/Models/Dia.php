<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AuthMail as ResetPasswordNotification;

class Dia extends Model
{
    protected $fillable=["data"];
   public function evento(){
       $this->belongsTo("\App\Models\Evento");
   }


    public function inscricoes(){
        return $this->belongsToMany("App\Models\Inscricao","inscricao_dia","dia_id","inscricao_id")->withPivot('presenca');
    }

    public function cronogramas(){
       return $this->hasMany("App\Models\Cronograma");
    }

}
