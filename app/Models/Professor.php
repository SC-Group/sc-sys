<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    public function user(){
        return $this->belongsTo("App\Models\User",'id','id');
    }

    public function instituicao(){
        return $this->belongsTo("App\Models\Instituicao");
    }

    public function curso(){
        return $this->belongsTo("App\Models\Curso");
    }
}
