<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disponibilidade extends Model
{
    protected $fillable=["dia_id", "palestrante_id", "hora_id", "staff_id"];
    
    public function palestrante(){
        return $this->belongsTo("App\Models\Palestrante");
    }

    public function staff(){
        return $this->belongsTo("App\Models\Staff");
    }

    public function hora(){
        return $this->belongsTo("App\Models\Hora");
    }

    public function dia(){
        return $this->belongsTo("App\Models\Dia");
    }

}
