<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Apoio extends Model
{
    public function eventos(){
        return $this->belongsToMany("App\Models\Evento","evento_apoio","apoio_id","evento_id");
    }
}
