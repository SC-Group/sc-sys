<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    protected $fillable=["nome","vagas","descricao"];
    public function evento(){
        return $this->belongsTo("App\Models\Evento");
    }

    public function cronogramas(){
        return $this->hasMany("App\Models\Cronograma");
    }
}
