<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = ['code'];

    public function evento(){
        return $this->belongsTo("App\Models\Evento");
    }

    public function camisas(){
        return $this->belongsToMany("App\Models\Camisa","card_camisa","card_id","camisa_id")->withPivot('pgantes','pgdepois','entregues','reservadas');
    }

}
