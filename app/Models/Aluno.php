<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    public function user(){
        return $this->belongsTo("App\Models\User",'id','id');
    }

    public function staff(){
        return $this->hasMany("App\Models\Staff");
    }

    public function comissao(){
        return $this->belongsTo("App\Models\Comissao");
    }

    public function instituicao(){
        return $this->belongsTo("App\Models\Instituicao");
    }

    public function curso(){
        return $this->belongsTo("App\Models\Curso");
    }

}
