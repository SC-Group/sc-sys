<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instituicao extends Model
{
    public function scopeUfrj($query){
        return $query->where("nome","Universidade Federal do Rio de Janeiro")->where("tipo","Universidade")->firstOrFail();
    }
    public function cursos(){
        return $this->belongsToMany("App\Models\Curso","instituicao_curso");
    }
}
