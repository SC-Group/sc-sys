<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hora extends Model
{
    protected $fillable=["inicio","fim"];

    public function cronogramas(){
        return $this->hasMany("App\Models\Cronograma");
    }

    public function evento(){
        return $this->belongsTo("App\Models\Evento","evento_id","id");
    }

}
