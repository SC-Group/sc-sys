<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inscricao extends Model
{
    protected $fillable=["kit"];
    public function user(){
        return $this->belongsTo("App\Models\User");
    }

    public function evento(){
        return $this->belongsTo("App\Models\Evento");
    }

    public function atividades(){
        return $this->belongsToMany("App\Models\Atividade");
    }



    public function dias(){
        return $this->belongsToMany("App\Models\Dia","inscricao_dia","inscricao_id","dia_id")->withPivot('presenca');
    }

    public function egg(){
        return $this->hasOne("App\Models\Egg");
    }
}
