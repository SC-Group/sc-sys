<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{

    public function scopeCc($query){
        return $query->firstOrCreate(["nome"=>"Ciência da Computação"]);
    }

    public function instituicoes(){
        return $this->belongsToMany("App\Models\Instituicao","instituicao_curso");
    }

    public function ex_ufrj(){
            return $this->hasMany("App\Models\Curso","curso_id_ex_ufrj","id");
    }

    public function aluno(){
        return $this->hasMany("App\Models\Aluno");
    }
}
