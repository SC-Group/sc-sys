<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Atividade extends Model
{
    protected $fillable=["nome","tipo","especial","descricao"];
    public function inscricoes(){
        return $this->belongsToMany("App\Models\Inscricao");
    }

    public function trilha(){
        return $this->belongsTo("App\Models\Trilha");
    }

    public function local(){
        return $this->belongsTo("App\Models\Local");
    }

    public function cronogramas(){
        return $this->hasMany("App\Models\Cronograma");
    }


    public function palestrantes(){
        return $this->belongsToMany("App\Models\Palestrante",
            "atividade_palestrante",
            "atividade_id",
            "palestrante_id")->withPivot(["presente"]);
    }

    public function evento(){
        return $this->belongsTo("App\Models\Evento");
    }

}
