<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Camisa extends Model
{   protected $fillable = ['tamanho', 'tipo', 'qtd_total','cor'];

    public function evento(){
        $this->belongsTo("App\Models\Evento");
    }

    public function users(){
        return $this->belongsToMany("App\Models\User", "user_camisa", "camisa_id","user_id")->withPivot('pgantes','pgdepois','entregues','reservadas');
    }

    public function cards(){
        return $this->belongsToMany("App\Models\Card", "card_camisa", "camisa_id","card_id")->withPivot('pgantes','pgdepois','entregues','reservadas');
    }

}
