<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patrocinador extends Model
{
    public function nivelPatrocinio(){
        return $this->belongsToMany("App\Models\NivelPatrocinio","nivel_patrocinio_patrocinador","patrocinador_id","nivel_patrocinio_id");
    }

    public function evento(){
        return $this->belongsToMany("App\Models\Evento","evento_patrocinador","patrocinador_id","evento_id");
    }

}
