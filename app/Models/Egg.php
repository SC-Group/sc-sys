<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Egg extends Model
{
    protected $fillable=["cod","premio","url_img"];
    public function evento(){
        return $this->belongsTo('App\Models\Evento');
    }

    public  function inscricao(){
        return $this->belongsTo("App\Models\Inscricao");
    }

}