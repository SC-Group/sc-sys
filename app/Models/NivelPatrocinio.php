<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NivelPatrocinio extends Model
{
    protected $fillable = ["nome","nivel"];
    public function patrocinadores(){
        return $this->belongsToMany("App\Models\Patrocinador","nivel_patrocinio_patrocinador","nivel_patrocinio_id","patrocinador_id");
    }

    public function evento(){
        return $this->belongsTo("App\Models\Evento");
    }

}
