<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Palestrante extends Model
{

    public function user(){
        return $this->belongsTo("App\Models\User",'user_id','id');
    }

    public function disponibilidades(){
        return $this->hasMany("App\Models\Disponibilidade");
    }

    public function evento(){
        return $this->belongsTo("App\Models\Evento",'evento_id','id');
    }

    public function atividades(){
        return $this->belongsToMany("App\Models\Atividade",
            "atividade_palestrante",
            "palestrante_id",
            "atividade_id")->withPivot(["presente"]);
    }

}
