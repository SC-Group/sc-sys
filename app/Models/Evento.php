<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    public function camisas(){
        return $this->hasMany("App\Models\Camisa");
    }

    public function inscricao(){
        return $this->hasMany("App\Models\Inscricao");
    }

    public function trilhas(){
        return $this->hasMany("App\Models\Trilha");
    }

    public function cards(){
        return $this->hasMany("App\Models\Card");
    }

    public function dias(){
        return $this->hasMany("App\Models\Dia");
    }

    public function horas(){
        return $this->hasMany("App\Models\Hora");
    }

    public function locais(){
        return $this->hasMany("App\Models\Local");
    }

    public function eggs(){
        return $this->hasMany("App\Models\Egg");
    }

    public function palestrantes(){
        return $this->hasMany("App\Models\Palestrante");
    }

    public function staffs(){
        return $this->hasMany("App\Models\Staff");
    }

    public function atividades(){
        return $this->hasMany('App\Models\Atividade');
    }

    public function niveisPatrocinio(){
        return $this->hasMany("App\Models\NivelPatrocinio");
    }

    public function apoios(){
        return $this->belongsToMany("App\Models\Apoio","evento_apoio","evento_id", "apoio_id");
    }
}
