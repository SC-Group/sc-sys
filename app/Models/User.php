<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AuthMail as ResetPasswordNotification;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   public function sendPasswordResetNotification($token)
   {
       $this->notify(new ResetPasswordNotification($token));
   }

    protected $fillable = [
        'name', 'email', 'password','categoria'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function inscricao(){
        return $this->hasMany("App\Models\Inscricao");
    }

    public function professor(){
        return $this->hasOne("App\Models\Professor",'id','id');
    }

    public function aluno(){
        return $this->hasOne("App\Models\Aluno",'id','id');
    }

    public function palestrante(){
        return $this->hasMany("App\Models\Palestrante","user_id","id");
    }

    public function ex_ufrj_curso(){
        return $this->belongsTo("App\Models\Curso","curso_id_ex_ufrj","id");
    }

    public function camisas(){
        return $this->belongsToMany("App\Models\Camisa","user_camisa","user_id","camisa_id")->withPivot('pgantes','pgdepois','entregues','reservadas');
    }

}
