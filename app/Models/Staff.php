<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staffs';

    public function aluno(){
        return $this->belongsTo("App\Models\Aluno");
    }

    public function evento(){
        return $this->belongsTo("App\Models\Evento");
    }

    public function comissao(){
        return $this->belongsTo("App\Models\Comissao");
    }

    public function disponibilidades(){
        return $this->hasMany("App\Models\Disponibilidade");
    }

}
