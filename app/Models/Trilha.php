<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trilha extends Model
{
    protected $fillable=[
        "nome",
        "cor",
        "descricao"
    ];
     //     string"nome"; string"cor"; text"descricao"->nullable;
    public function evento(){
        return $this->belongsTo('App\Models\Evento');
    }

}
