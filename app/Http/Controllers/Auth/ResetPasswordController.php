<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\RegexPatternJSHelper;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $pass;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->pass = RegexPatternJSHelper::password();
        $this->middleware('guest');
    }

    protected function rules()
    {
        $pass = RegexPatternJSHelper::password();
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|regex:/'.$this->pass['pattern'].'/',
        ];

    }
    protected function validationErrorMessages()
    {
        return ["password.regex"=>$this->pass["msg"]];
    }
}
