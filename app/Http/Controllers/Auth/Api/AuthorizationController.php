<?php


namespace App\Http\Controllers\Auth\Api;


use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Laravel\Passport\Client;
use Laravel\Passport\Passport;
use Laravel\Passport\PersonalAccessClient;

class AuthorizationController extends Controller
{

    public function getUserClient($request){
        $clientId = $request->get('clientId');
        $secret = $request->get('clientSecret');

        $client = Client::find($clientId);
        if(isset($client) && $client->secret === $secret){
            $user = $client->user();
            if($user->exists()) return $user->first();
        }
        return null;
    }

    public function login(Request $request){
        $user = $this->getUserClient($request);
        if (isset($user)) {
            Auth::login($user);
            $grant = ['be-aplicacao'];
            if(Gate::allows('be-comissao')){
                $grant = ['be-comissao','be-staff','be-aplicacao'];
            }
            else if (Gate::allows('be-staff')) {
                $grant = ['be-staff'];
            }

            Auth::logout();

            $tokenFactory = $user->createToken('token',$grant);
            return response()->json([
                "token_type"=> "Bearer",
                "expires_in"=>now()->diffInSeconds(Carbon::parse($tokenFactory->token->expires_at)),
                "access_token"=>$tokenFactory->accessToken
            ]);

        }
        return response()->json(["error"=>__("messages.LoginOrPasswordIncorrectOrClientInexistent")],401);
    }
}