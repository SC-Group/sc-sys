<?php


namespace App\Http\Controllers;


use App\Helpers\IdHelper;
use App\Helpers\RegexPatternJSHelper;
use App\Helpers\RegexPatternJSHelper as RegexHelper;
use App\Helpers\SCInscricao;
use App\Http\Services\SCService;
use App\Http\Services\SendToBotService;
use App\Models\Aluno;
use App\Models\Evento;
use App\Models\Inscricao;
use App\Models\Instituicao;
use App\Models\Palestrante;
use App\Models\Professor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Validator;

class ProfileController
{
    public function getProfile($year){
        $data = ["year"=>$year];
        if(Session::has("msginfo")){
            $data["msginfo"] = Session::get("msginfo");
        }
        if(Session::has("inscnum")){
            $data["inscnum"] = Session::get("inscnum");
        }
        if(Session::has("botmsg")){
            $data["msgBot"] = Session::get("botmsg");
        }
        if(Session::has("status")){
            $data["status"] = Session::get("status");
        }

        return SCService::view("regularpages.profile.main",$data);
    }

    private function validateProfile(Request $request){
        $ufrj = Instituicao::where("sigla","UFRJ")->first();

        $validateName = RegexHelper::name();
        $validatePass = RegexHelper::password();
        $validator =  Validator::make($request->all(),[
            "year"=>"required",
            "nome"=>array("required","regex:/".$validateName["pattern"]."/"),
            "email"=>"required | email",
            "password"=>array("nullable" , "confirmed" , "regex:/".$validatePass["pattern"]."/"),
            "tipouser"=>"required | in:participante,aluno,professor,exufrj",

            "instituicao-Aluno-id"=>"required_if:tipouser,aluno",
            "instituicao-Aluno-curso"=>"required_if:tipouser,aluno",

            "ufrj-dre"=>'nullable|regex:/^[1-9][0-9]{8}/',
            "estagio"=>"nullable | boolean",

            "instituicao-Professor-id"=>"required_if:tipouser,professor",
            "instituicao-Professor-curso"=>"required_if:tipouser,professor",

            "tiposalvamento"=>"required | in:inscrever,inscrever_com_camisas,salvar",

            "instituicao-Participante-curso"=>"required_if:tipouser,exufrj",

            "camisas.*.*.*"=>"nullable | digits:1",
            "bot"=>"sometimes | nullable | boolean"

        ],[
            "nome.regex"=>$validateName["msg"],
            "password.regex"=>$validatePass["msg"]
        ]);

        $validator->sometimes("instituicao-Aluno-id", "exists:instituicaos,id", function ($input) {
            return $input->tipouser == "aluno";
        });
        $validator->sometimes("instituicao-Aluno-curso", "exists:cursos,id", function ($input) {
            return $input->tipouser == "aluno";
        });
        $validator->sometimes("instituicao-Professor-id", "exists:instituicaos,id", function ($input) {
            return $input->tipouser == "professor";
        });
        $validator->sometimes("instituicao-Professor-curso", "exists:cursos,id", function ($input) {
            return $input->tipouser == "professor";
        });
        $validator->sometimes("instituicao-Participante-curso", "exists:cursos,id", function ($input) {
            return $input->tipouser == "exufrj";
        });

        $tipoSalvamento = $request->get("tiposalvamento");
        $evento = Evento::where("ano",$request->get("year"))->first();

        if(($tipoSalvamento=="inscrever" || $tipoSalvamento=="inscrever_com_camisas")){
            if($evento!==null){
                $inicioInsc = \Carbon\Carbon::parse($evento->inicio_insc);
                $fimEvt = \Carbon\Carbon::parse($evento->fim);
                $isInscTime = now()->gte($inicioInsc) && now()->lte($fimEvt);
                if($isInscTime){
                    $validator->getMessageBag()->add('inscricao', 'Não é possível concluir operação, requisição inválida');
                }
            }else{
                $validator->getMessageBag()->add('inscricao', 'Não é possível concluir operação, requisição inválida');
            }
        }
        return $validator;
    }

    private function formatCamisas($evento, Request $request){
        $camisas = [];
        $cam = $request->get('camisas');
        $temTipo = [];
        $temTipo["Feminina"] = $evento->camisas()->where('tipo','Feminina')->exists();
        $temTipo["Masculina"] = $evento->camisas()->where('tipo','Masculina')->exists();

        foreach ($cam as $tipo=>$tamanhos){
            if($temTipo[$tipo]){
                foreach ($tamanhos as $tam=>$cores){
                    foreach ($cores as $cor=>$qtd){
                        if($qtd>0){
                            $searchFor = [['tipo',$tipo], ['tamanho',$tam], ['cor',$cor]];
                            $camisaId = $evento->camisas()->where($searchFor)->first()->id;
                            $camisas[$camisaId] = ["reservadas"=>$qtd];
                        }
                    }
                }
            }
        }

        return $camisas;

    }

    public function saveProfile(Request $request){
        $validation = $this->validateProfile($request);
        $ufrj = Instituicao::ufrj();

        $DRE = $request->get('ufrj-dre');
        $instituicao = $request->get('instituicao-Aluno-id');

        if ($validation->fails() || ($instituicao==$ufrj->id && $DRE==null)) {
            if ($instituicao == $ufrj->id && $DRE == null)
                $validation->getMessageBag()->add('DRE', __("messages.FormValidation.ValidDRE"));
            return back()
                ->withErrors($validation)
                ->withInput()->with(["msg"=>__("messages.ValidationErrors"), "status"=>"error"]);
        }


        try {
            $sendToBot = $request->get("bot")==="1";
            $tipoSalvamento = $request->get("tiposalvamento");
            $evento = Evento::where("ano",$request->get("year"))->first();
            if($evento!==null && $evento->camisas()->exists() && $tipoSalvamento=="inscrever_com_camisas" )
            {
                $formatedCamisas = $this->formatCamisas($evento, $request);
            }

            $sendToBotData=[];

            DB::transaction(function ()use(&$evento, &$request, &$ufrj, &$formatedCamisas, &$sendToBotData, &$sendToBot) {
                $user = auth()->user();
                $tipo = $request->get("tipouser");
                if($tipo!=="aluno"){
                    $user->aluno()->delete();
                }
                if($tipo!=="professor"){
                    $user->professor()->delete();
                }
                if($tipo!=="exufrj"){
                    $user->curso_id_ex_ufrj = null;
                }
                if($tipo === "aluno"){
                    $aluno = $user->aluno()->first();
                    if($aluno==null){
                        $aluno=new Aluno();
                        $aluno->user()->associate($user);
                    }
                    $instituicao = Instituicao::find($request->get("instituicao-Aluno-id"));
                    $curso = $instituicao->cursos()->find($request->get("instituicao-Aluno-curso"));
                    $aluno->ufrj_dre = ($instituicao->id === $ufrj->id? $request->get("ufrj-dre"):null);
                    $aluno->estagio = $request->get("estagio")??false;
                    $aluno->curso()->associate($curso);
                    $aluno->instituicao()->associate($instituicao);
                    $aluno->save();
                }elseif($tipo === "professor"){
                    $professor = $user->professor()->first();
                    if($professor==null){
                        $professor=new Professor();
                        $professor->user()->associate($user);
                    }
                    $instituicao = Instituicao::find($request->get("instituicao-Professor-id"));
                    $curso = $instituicao->cursos()->find($request->get("instituicao-Professor-curso"));
                    $professor->curso()->associate($curso);
                    $professor->instituicao()->associate($instituicao);
                    $professor->save();
                }elseif($tipo==="exufrj"){

                    $curso = $ufrj->cursos()->findOrFail($request->get("instituicao-Participante-curso"));
                    $user->curso_id_ex_ufrj = $curso->id;
                }

                $password = $request->get("password");

                if($password!=null){
                    $user->password = Hash::make($password);
                }
                $user->tel=$request->get("tel");
                $user->name=$request->get("nome");
                $user->email=$request->get("email");
                $tipoSalvamento = $request->get("tiposalvamento");
                if(($tipoSalvamento=="inscrever" || $tipoSalvamento=="inscrever_com_camisas")) {
                    $inscricao = new Inscricao();
                    $inscricao->evento()->associate($evento);
                    $inscricao->user()->associate($user);
                    $inscricao->send_to_bot=$sendToBot;
                }

                $user->save();
                if(isset($inscricao) && $inscricao!=null) {
                    if ($formatedCamisas != null) {
                        auth()->user()->camisas()->attach($formatedCamisas);
                    }
                    $inscricao->save();

                }
            });

        }catch (\Exception $e){
            if(env("APP_DEBUG")===true){
                return back()->with(["msg"=>__("messages.InternalErrorOnSaving")."<br>".$e->getMessage(), "status"=>"error"]);
            }
            return back()->with(["msg"=>__("messages.InternalErrorOnSaving"), "status"=>"error"]);
        }

        if(($tipoSalvamento=="inscrever" || $tipoSalvamento=="inscrever_com_camisas")) {
            $user = auth()->user();
            $inscricao = $user->inscricao()->where("evento_id",$evento->id)->first();
            $sendToBotData["sc"] = $inscricaoNum =SCInscricao::get($inscricao->id);
            $botMsg = null;
            if($sendToBot&&boolval(env("BOT_ENABLED",false))){
                try{
                    SendToBotService::Send($sendToBotData);
                }catch (\Exception $e){
                    $botMsg = __("messages.BotError",["bot"=>env("BOT_NAME","Bot")]);
                    return back()->with(["botmsg"=>$botMsg, "status"=>"warning", "inscnum"=>$inscricaoNum]);
                }
                return back()->with(["botmsg"=>"Dados enviados com sucesso para ".env("BOT_NAME","Bot"), "status"=>"success", "inscnum"=>$inscricaoNum]);
            }
            return back()->with(["status"=>"success", "inscnum"=>$inscricaoNum]);
        }
        return back()->with(["msg"=>__("messages.SavedWithSuccess"), "status"=>"success"]);
    }

    public function savePalestranteProfile(Request $request){

        $validation = Validator::make($request->all(),[
            "disponibilidade"=>"min:1 | required",
            "disponibilidade.*"=>"min:1",
            "disponibilidade.*.*"=>"boolean | in:1",
            "year"=>"exists:eventos,ano"
        ]);
        if($validation->fails()){
            return back()->with(["msg"=>$validation->messages()->first(),"status"=>"error"]);
        }
        $year = $request->get("year");
        $evento = Evento::where("ano",$year)->first();
        $palestrante = auth()->user()->palestrante()->where("evento_id",$evento->id)->first();
        if($palestrante===null){
            return back()->with(["msg"=>"Você não é palestrante do evento de $year","status"=>"error"]);
        }
        $cronogramas = [];
        $disponibilidade = $request->get("disponibilidade");
        foreach ($disponibilidade as $encdiaid=>$horas) {
            $diaId = IdHelper::dec($encdiaid)[0];
            foreach ($horas as $enchoraid => $_marcado) {
                $horaId = IdHelper::dec($enchoraid)[0];
                $marcado = boolval($_marcado);
                if ($marcado) {
                    array_push($cronogramas, [
                        "dia_id" => $diaId,
                        "hora_id" => $horaId
                    ]);
                }
            }
        }

        DB::transaction(function () use (&$palestrante,&$cronogramas) {
            $palestrante->disponibilidades()->delete();
            $palestrante->disponibilidades()->createMany($cronogramas);
        });

        return back()->with(["msg"=>__("messages.SavedWithSuccess"), "status"=>"success"]);

    }

    public function saveStaffProfile(Request $request){

        $validation = Validator::make($request->all(),[
            "disponibilidade"=>"min:1 | required",
            "disponibilidade.*"=>"min:1",
            "disponibilidade.*.*"=>"boolean | in:1",
            "year"=>"exists:eventos,ano"
        ]);
        if($validation->fails()){
            return back()->with(["msg"=>$validation->messages()->first(),"status"=>"error"]);
        }
        $year = $request->get("year");
        $evento = Evento::where("ano",$year)->first();
        $aluno = auth()->user()->aluno();
        if(!$aluno->exists()){
            return back()->with(["msg"=>"Você não é um aluno","status"=>"error"]);
        }
        $staff = $aluno->first()->staff()->where("evento_id",$evento->id);
        if(!$staff->exists()){
            return back()->with(["msg"=>"Você não é staff e nem um candidato a staff do evento de $year","status"=>"error"]);
        }
        $staff=$staff->first();

        $cronogramas = [];
        $disponibilidade = $request->get("disponibilidade");
        foreach ($disponibilidade as $encdiaid=>$horas) {
            $diaId = IdHelper::dec($encdiaid)[0];
            foreach ($horas as $enchoraid => $_marcado) {
                $horaId = IdHelper::dec($enchoraid)[0];
                $marcado = boolval($_marcado);
                if ($marcado) {
                    array_push($cronogramas, [
                        "dia_id" => $diaId,
                        "hora_id" => $horaId
                    ]);
                }
            }
        }

        DB::transaction(function () use (&$staff,&$cronogramas) {
            $staff->disponibilidades()->delete();
            $staff->disponibilidades()->createMany($cronogramas);
        });

        return back()->with(["msg"=>__("messages.SavedWithSuccess"), "status"=>"success"]);

    }


}