<?php

namespace App\Http\Controllers\Staff\Camisas;

use App\Models\Camisa;
use App\Models\Card;
use App\Models\Evento;
use App\Models\Inscricao;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller as Controller;
use App\Http\Services\SCService as SC;
use Validator;

class DevolverCamisasController extends CamisasController
{

    private function devolve($entidade,$evento,$lines){
        try {
            $precoAntes =  $evento->preco_camisa_antes;
            $precoDepois = $evento->preco_camisa_depois;
            $qtdTotalAntes=0;
            $qtdTotalDepois=0;
            $totalDevolvidas = [];
            $errors=[];

            DB::transaction(function () use (&$errors,&$qtdTotalAntes,&$qtdTotalDepois,&$totalDevolvidas,$lines, $evento, $entidade) {
                foreach ($lines as $key=>$line) {
                    $id = $line["camisaid"];
                    $qtd = $line["qtd"];
                    $camisa = $entidade->camisas()->where("id",$id);
                    if($camisa->exists()){
                        $camisa = $camisa->first();
                        $pgAntes = $camisa->pivot->pgantes;
                        $pgDepois = $camisa->pivot->pgdepois;

                        if(($pgAntes+$pgDepois) >= $qtd){
                            if($qtd <= $pgAntes){
                                $qtdAntes = $qtd;
                                $qtdDepois = 0;
                            }else{
                                $qtdAntes = $pgAntes;
                                $qtdDepois = $qtd-$qtdAntes;
                            }

                            if(!array_key_exists("$id",$totalDevolvidas)){
                                $totalDevolvidas["$id"]=0;
                            }

                            if($camisa->pivot->entregues >= $qtd){
                                $camisa->pivot->entregues -= $qtd;
                                $totalDevolvidas["$id"] += $qtd;
                            }else{
                                $totalDevolvidas["$id"] += $camisa->pivot->entregues;
                                $camisa->pivot->entregues=0;
                            }

                            $camisa->pivot->pgantes -= $qtdAntes;
                            $camisa->pivot->pgdepois -= $qtdDepois;
                            $camisa->qtd_comprada -= $qtd;

                            if( $camisa->pivot->pgantes==0&&
                                $camisa->pivot->pgdepois==0&&
                                $camisa->pivot->reservadas==0&&
                                $camisa->pivot->entregues==0){
                                $entidade->camisas()->detach($id);
                            }else{
                                $camisa->pivot->save();
                            }

                            $camisa->save();

                            $qtdTotalAntes+=$qtdAntes;
                            $qtdTotalDepois+=$qtdDepois;
                        }else{
                            $errors["$id"]=__("messages.QtdReturnGreaterThanBought",["qtd"=>$pgAntes+$pgDepois]);
                        }
                    }else{
                        $errors["$id"]=__("messages.TShirtNotPresentOnBD");
                    }
                }
                $entidade->save();
            }, 5);


            return [
                [
                    "errors"=>$errors,
                    "msg"=>(count($errors)>0?__("messages.NotAllTShirtsReturned"):__("messages.TShirtReturnedSuccessfully")),
                    "status"=>(count($errors)>0?"warning":"success"),
                    "camisasDevolvidas"=>$totalDevolvidas,
                    "precoTotal"=>($qtdTotalAntes*$precoAntes+$qtdTotalDepois*$precoDepois)
                ],
                200
            ];
        }catch (Exception $e){
            return [
                [
                    "msg"=>__("messages.CriticalErrorOperationWasReverted"),
                    "status"=>"error"
                ],
                500
            ];
        }
    }


    public function devolverCamisas(Request $request){
        $validator = Validator::make($request->all(), [
            "entidadeId" => 'required',
            "entidadeTipo" => 'required',
            'year'=>'required',
            'l'=>"required",
            'l.*'=>"required",
            'l.*.camisaid'=>'required|numeric',
            'l.*.qtd'=>'required|numeric|min:1'
        ], [
            'l.required'=>__("messages.NoTShirtToReturn"),
            'l.*.required'=>__("messages.NoTShirtToReturn"),
            'l.*.camisaid.required'=>__("messages.NeedChooseValidTShirt"),
            "entidadeId.required"=>__("messages.InvalidChoiceOfUser"),
            "entidadeTipo.required"=>__("messages.InvalidChoiceOfUser"),
            'year.required'=>__("messages.EventYearRequired")
        ]);



        if($request->has("entidadeId")&&$request->has("entidadeTipo")){
            $entidadeCamisas = parent::entidadeCamisas($request)[0];
            $camisas = $entidadeCamisas["camisas"];
            $entidadeCamisas = $entidadeCamisas["entidadeCamisas"];
        }else{
            return response()->json(["msg"=>__("messages.CantIdentifyUser"),"status"=>"error"],400);
        }


        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = __("messages.SomeErrorsOnSaving");
            if($errors->has("l")){
                $errorMessage = $errors->first("l");
            }elseif($errors->has("l.*")){
                $errorMessage = $errors->first("l.*");
            }

            return response()->json(["camisas"=>$camisas,"entidadeCamisas"=>$entidadeCamisas,"msg"=>$errorMessage,"errors"=>$validator->errors(),"status"=>"error"],400);

        }else{

            $lines = $request->get("l");
            $year = $request->get("year");
            $evento = Evento::where("ano", $year)->firstOrFail();


            $entidadeId = $request->get("entidadeId");
            $entidadeTipo = $request->get("entidadeTipo");
            if($entidadeTipo==="card"){
                $entidade = Card::findOrFail($entidadeId);
            }else{
                $entidade = User::findOrFail($entidadeId);
            }

            $responseArray = $this->devolve($entidade, $evento, $lines);

            $entidadeCamisas = parent::entidadeCamisas($request)[0];
            $camisas = $entidadeCamisas["camisas"];
            $entidadeCamisas = $entidadeCamisas["entidadeCamisas"];


            $responseArray[0]["camisas"] = $camisas;
            $responseArray[0]["entidadeCamisas"] = $entidadeCamisas;
            return response()->json($responseArray[0],$responseArray[1]);
        }
    }

}