<?php

namespace App\Http\Controllers\Staff\Camisas;

use App\Models\Camisa;
use App\Models\Card;
use App\Models\Evento;
use App\Models\Inscricao;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller as Controller;
use App\Http\Services\SCService as SC;
use Validator;

class StatusCamisasController extends CamisasController
{
    private function status($entidade,$evento,$lines){
        try {
            $errors = [];
            DB::transaction(function () use ($lines, $evento, $entidade) {

                foreach ($lines as $key=>$line) {
                    $id = $line["camisaid"];
                    $qtd = $line["entregues"];
                    $camisa = $entidade->camisas()->where("id",$id);
                    if($camisa->exists()){
                        $camisa = $camisa->first();
                        $camisa->pivot->entregues = $qtd;
                        $camisa->pivot->save();
                        $camisa->save();
                    }else{
                        $errors[$id]=__("message.TShirtNotPresentOnBD");
                    }
                }
                $entidade->save();
            }, 5);

            return [
                [
                    "errors"=>$errors,
                    "msg"=>__("messages.UpdatedWithSuccess"),
                    "status"=>"success"
                ],
                200
            ];
        }catch (Exception $e){
            return [
                [
                "msg"=>__("messages.CriticalErrorOperationWasReverted"),
                "status"=>"error"
                ],
                500
            ];
        }
    }


    public function statusCamisas(Request $request){
        $validator = Validator::make($request->all(), [
            "entidadeId" => 'required',
            "entidadeTipo" => 'required',
            'year'=>'required',
            'l'=>"required",
            'l.*'=>"required",
            'l.*.camisaid'=>'required|numeric',
            'l.*.entregues'=>'required|numeric'
        ], [
            'l.required'=>__("messages.NoTShirtToAlterStatus"),
            'l.*.required'=>__("messages.NoTShirtToAlterStatus"),
            'l.*.camisaid.required'=>__("messages.NeedChooseValidTShirt"),
            'l.*.entregues.required'=>__("messages.RequiredDeliveredTShirtNumber"),
            'l.*.entregues.numeric'=>__("messages.QuantityNeedsToBeANumber"),
            "entidadeId.required"=>__("messages.InvalidChoiceOfUser"),
            "entidadeTipo.required"=>__("messages.InvalidChoiceOfUser"),
            'year.required'=>__("messages.EventYearRequired"),
        ]);


        if($request->has("entidadeId")&&$request->has("entidadeTipo")){
            $entidadeCamisas = parent::entidadeCamisas($request)[0];
            $camisas = $entidadeCamisas["camisas"];
            $entidadeCamisas = $entidadeCamisas["entidadeCamisas"];
        }else{
            return response()->json(["msg"=>__("messages.CantIdentifyUser"),"status"=>"error"],400);
        }


        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = __("messages.SomeErrorsOnSaving");
            if($errors->has("l")){
                $errorMessage = $errors->first("l");
            }elseif($errors->has("l.*")){
                $errorMessage = $errors->first("l.*");
            }

            return response()->json(["camisas"=>$camisas,"entidadeCamisas"=>$entidadeCamisas,"msg"=>$errorMessage,"errors"=>$validator->errors(),"status"=>"error"],400);

        }else{

            $lines = $request->get("l");
            $year = $request->get("year");
            $evento = Evento::where("ano", $year)->firstOrFail();


            $entidadeId = $request->get("entidadeId");
            $entidadeTipo = $request->get("entidadeTipo");
            if($entidadeTipo==="card"){
                $entidade = Card::findOrFail($entidadeId);
            }else{
                $entidade = User::findOrFail($entidadeId);
            }

            $responseArray = $this->status($entidade, $evento, $lines);

            $entidadeCamisas = parent::entidadeCamisas($request)[0];
            $camisas = $entidadeCamisas["camisas"];
            $entidadeCamisas = $entidadeCamisas["entidadeCamisas"];


            $responseArray[0]["camisas"] = $camisas;
            $responseArray[0]["entidadeCamisas"] = $entidadeCamisas;
            return response()->json($responseArray[0],$responseArray[1]);
        }
    }

}