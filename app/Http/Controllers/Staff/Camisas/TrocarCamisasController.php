<?php

namespace App\Http\Controllers\Staff\Camisas;

use App\Models\Camisa;
use App\Models\Card;
use App\Models\Evento;
use App\Models\Inscricao;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller as Controller;
use App\Http\Services\SCService as SC;
use Validator;

class TrocarCamisasController extends CamisasController
{



    private function troca($entidade,$evento,$lines){
        try {
            $errors=[];
            $totalFisico = [];
            DB::transaction(function () use (&$totalFisico,&$errors,$lines, $evento, $entidade) {

                foreach ($lines as $key=>$line) {
                    $idTroca = $line['lineTroca']["camisaid"];
                    $idPor = $line['linePor']["camisaid"];
                    $qtd = $line["qtd"];
                    $camisaTroca = $entidade->camisas()->where("id",$idTroca);
                    $camisaPor = $entidade->camisas()->where("id",$idPor);


                    if($idTroca==$idPor){
                        $errors["$idTroca-$idPor"] = __("messages.TShirtsAreEquals");
                        continue;
                    }

                    if($camisaTroca->exists()){
                        $camisaTroca = $camisaTroca->first();
                        $pgAntes = $camisaTroca->pivot->pgantes;
                        $pgDepois = $camisaTroca->pivot->pgdepois;

                        if(($pgAntes+$pgDepois) >= $qtd){
                            if($camisaTroca->pivot->entregues>=$qtd){
                                $qtdAEntregar = $qtd;
                            }else{
                                $qtdAEntregar = $camisaTroca->pivot->entregues;
                            }

                            if($qtd <= $pgAntes){
                                $qtdAntes = $qtd;
                                $qtdDepois = 0;
                            }else{
                                $qtdAntes = $pgAntes;
                                $qtdDepois = $qtd-$qtdAntes;
                            }

                            $camisaTroca->pivot->entregues -= $qtdAEntregar;
                            $camisaTroca->pivot->pgantes -= $qtdAntes;
                            $camisaTroca->pivot->pgdepois -= $qtdDepois;

                            if($camisaPor->exists()){
                                $camisaPor = $camisaPor->first();
                                if($camisaPor->qtd_comprada+$qtd <= $camisaPor->qtd_total){
                                    $camisaPor->pivot->entregues += $qtdAEntregar;
                                    $camisaPor->pivot->pgantes += $qtdAntes;
                                    $camisaPor->pivot->pgdepois += $qtdDepois;
                                    if(!array_key_exists("$idTroca-$idPor",$totalFisico)){
                                        $totalFisico["$idTroca-$idPor"]=$qtdAEntregar;
                                    }
                                    $camisaPor->qtd_comprada += $qtd;
                                    $camisaTroca->qtd_comprada -= $qtd;
                                    $camisaPor->pivot->save();
                                }else{
                                    $errors["$idTroca-$idPor"]=__("messages.NoTShirtToChange",["qtd"=>$qtd]);
                                }

                            }else{
                                $camisaPor = $evento->camisas()->where("id",$idPor);
                                if($camisaPor->exists()){//Adicionar nova camisa
                                    $camisaPor = $camisaPor->first();
                                    if($camisaPor->qtd_comprada+$qtd <= $camisaPor->qtd_total){
                                        $pivotPor["entregues"]= $qtdAEntregar;
                                        $pivotPor['pgantes'] = $qtdAntes;
                                        $pivotPor['pgdepois'] = $qtdDepois;
                                        if(!array_key_exists("$idTroca-$idPor",$totalFisico)){
                                            $totalFisico["$idTroca-$idPor"]=$qtdAEntregar;
                                        }
                                        $camisaPor->qtd_comprada += $qtd;
                                        $camisaTroca->qtd_comprada -= $qtd;
                                        $entidade->camisas()->attach($camisaPor,$pivotPor);
                                    }else{
                                        $errors["$idTroca-$idPor"]=__("messages.NoEnoughTShirtToChange",["qtd"=>$qtd]);
                                    }
                                }else{
                                    $errors["$idTroca-$idPor"]=__("messages.TShirtNotPresentOnBD");
                                }
                            }


                            if( $camisaTroca->pivot->pgantes==0&&
                                $camisaTroca->pivot->pgdepois==0&&
                                $camisaTroca->pivot->reservadas==0&&
                                $camisaTroca->pivot->entregues==0){
                                $entidade->camisas()->detach($idTroca);
                            }else{
                                $camisaTroca->pivot->save();
                            }

                            $camisaPor->save();
                            $camisaTroca->save();

                        }else{
                            $errors["$idTroca-$idPor"]=__("messages.QuantityToChangeGreaterThanBought");
                        }

                    }else{
                        $errors["$idTroca-$idPor"]=__("messages.ClientDoesntHaveTShirt");
                    }
                }
                $entidade->save();
            }, 5);


            return [
                [
                    "errors"=>$errors,
                    "msg"=>(count($errors)>0?__("messages.NotAllTShirtsChanged"):__("messages.TShirtChangedSuccessfully")),
                    "status"=>(count($errors)>0?"warning":"success"),
                    "camisasTrocadas"=>$totalFisico

                ],
                200
            ];
        }catch (Exception $e){
            return [
                [
                "msg"=>__("messages.CriticalErrorOperationWasReverted"),
                "status"=>"error"
                ],
                500
            ];
        }
    }


    public function trocarCamisas(Request $request){

        $validator = Validator::make($request->all(), [
            "entidadeId" => 'required',
            "entidadeTipo" => 'required',
            'year'=>'required',
            'l'=>"required",
            'l.*'=>"required",
            'l.*.lineTroca.camisaid'=>'required|numeric',
            'l.*.linePor.camisaid'=>'required|numeric',
            'l.*.qtd'=>'required|numeric|min:1'
        ], [
            'l.required'=>__("messages.NoTShirtToChange"),
            'l.*.required'=>__("messages.NoTShirtToChange"),
            'l.*.*.camisaid.required'=>__("messages.NeedChooseValidTShirt"),
            'l.*.*.camisaid.numeric'=>__("messages.NeedChooseValidTShirt"),
            "entidadeId.required"=>__("messages.InvalidChoiceOfUser"),
            "entidadeTipo.required"=>__("messages.InvalidChoiceOfUser"),
            'year.required'=>__("messages.EventYearRequired"),
        ]);



        if($request->has("entidadeId")&&$request->has("entidadeTipo")){
            $entidadeCamisas = $this->entidadeCamisas($request)[0];
            $camisas = $entidadeCamisas["camisas"];
            $entidadeCamisas = $entidadeCamisas["entidadeCamisas"];
        }else{
            return response()->json(["msg"=>__("messages.CantIdentifyUser"),"status"=>"error"],400);
        }

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = __("messages.SomeErrorsOnSaving");
            if($errors->has("l")){
                $errorMessage = $errors->first("l");
            }elseif($errors->has("l.*.*")){
                $errorMessage = $errors->first("l.*.*");
            }

            return response()->json(["camisas"=>$camisas,"entidadeCamisas"=>$entidadeCamisas,"msg"=>$errorMessage,"errors"=>$validator->errors(),"status"=>"error"],400);

        }else{

            $lines = $request->get("l");
            $year = $request->get("year");
            $evento = Evento::where("ano", $year)->firstOrFail();


            $entidadeId = $request->get("entidadeId");
            $entidadeTipo = $request->get("entidadeTipo");
            if($entidadeTipo==="card"){
                $entidade = Card::findOrFail($entidadeId);
            }else{
                $entidade = User::findOrFail($entidadeId);
            }

            $responseArray = $this->troca($entidade, $evento, $lines);
            $responseArray[0]["camisas"] = $camisas;
            $responseArray[0]["entidadeCamisas"] = $entidadeCamisas;
            return response()->json($responseArray[0],$responseArray[1]);
        }
    }

}