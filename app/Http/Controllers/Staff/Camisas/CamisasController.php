<?php

namespace App\Http\Controllers\Staff\Camisas;

use App\Models\Camisa;
use App\Models\Card;
use App\Models\Evento;
use App\Models\Inscricao;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller as Controller;
use App\Http\Services\SCService as SC;
use Validator;

class CamisasController extends Controller
{
    function getCamisas($year,Request $request){
        $evento = Evento::where("ano",$year);

        if($evento->exists()){
            $camisas = $evento->first()->camisas();

            if($camisas->exists()){
                $camisas = $camisas->get(["id","tamanho","qtd_total","qtd_comprada","tipo","cor"]);
                return SC::view("managepages.camisas.main",['year'=>$year,'camisas'=>$camisas]);
            }else{
                return redirect()->route("home")->with([
                    "msg"=>__("messages.DoesntHaveTShirtsOnEvent"),
                    "status"=>"error"
                ]);
            }
        }

        return redirect()->route("home")->with(["msg"=>__("messages.EventDoesntExists"),"status"=>"error"]);
    }

    public function getCamisasDaEntidade(Request $request){
        $entidadeCamisas = $this->entidadeCamisas($request);
        return response()->json($entidadeCamisas[0],$entidadeCamisas[1]);
    }

    protected function entidadeCamisas($request){
        $entidadeId = $request->get("entidadeId");
        $entidadeTipo = $request->get("entidadeTipo");
        $year = $request->get("year");

        $evento = Evento::where("ano",$year);
        if($evento->exists()){
            $evento=$evento->first();
            if($entidadeTipo==="card"){
                $entidade = Card::find($entidadeId);
            }else{
                $entidade = User::find($entidadeId);
            }
            $camisasDaEntidade = $entidade->camisas()->where("evento_id",$evento->id)->get();
            $out = [];
            $camisas = $evento->camisas()->get(["id","tamanho","qtd_total","qtd_comprada","tipo","cor"]);
            foreach ($camisasDaEntidade as $camisa){
                $id = $camisa->id;
                $pgAntes = $camisa->pivot->pgantes;
                $pgDepois = $camisa->pivot->pgdepois;
                $entregues = $camisa->pivot->entregues;
                $reservadas = $camisa->pivot->reservadas;
                array_push($out,["id"=>$id,"pgAntes"=>$pgAntes,"pgDepois"=>$pgDepois,"entregues"=>$entregues,"reservadas"=>$reservadas]);
            }
            return [["camisas"=>$camisas,"entidadeCamisas"=>$out],200];
        }

        return [["msg"=>__("messages.EventDoesntExists"),"status"=>"error"],500];
    }



}