<?php

namespace App\Http\Controllers\Staff\Camisas;

use App\Models\Camisa;
use App\Models\Card;
use App\Models\Evento;
use App\Models\Inscricao;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller as Controller;
use App\Http\Services\SCService as SC;
use Validator;

class VenderCamisasController extends CamisasController
{

    private function venda($entidade,$evento,$entregar,$lines){
        try {
            $inicioEvento = $evento->inicio;
            $qtdTotal=0;
            $errors=[];
            if(Carbon::today()->lessThan(new Carbon($inicioEvento))){
                $preco = $evento->preco_camisa_antes;
                $columnUpdate = "pgantes";
            }else{
                $columnUpdate="pgdepois";
                $preco = $evento->preco_camisa_depois;
            }

            DB::transaction(function () use (&$errors,&$qtdTotal,$columnUpdate, $lines, $evento, $entidade,$entregar) {

                foreach ($lines as $key=>$line) {
                    $id = $line["camisaid"];
                    $qtd = $line["qtd"];
                    $camisa = $entidade->camisas()->where("id",$id);
                    if($camisa->exists()){
                        $camisa = $camisa->first();
                        if($camisa->qtd_comprada+$qtd <= $camisa->qtd_total){
                            $camisa->pivot->$columnUpdate +=  $qtd;
                            if($entregar){
                                $camisa->pivot->entregues += $qtd;
                            }
                            $camisa->qtd_comprada += $qtd;
                            $qtdTotal+=$qtd;
                            $camisa->pivot->save();
                            $camisa->save();
                        }else{
                            $errors[$id] = __("messages.NoEnoughTShirtToSell",["qtd"=>$qtd]);
                        }
                    }else{
                        $camisa = $evento->camisas()->where("id",$id);
                        if($camisa->exists()){
                            $camisa = $camisa->first();
                            if($camisa->qtd_comprada+$qtd <= $camisa->qtd_total){
                                $pivotData = [$columnUpdate=>$qtd];
                                if($entregar){
                                    $pivotData["entregues"]=$qtd;
                                }
                                $camisa->qtd_comprada += $qtd;
                                $entidade->camisas()->attach($camisa,$pivotData);
                                $qtdTotal += $qtd;
                                $camisa->save();
                            }else{
                                $errors[$id]= __("messages.NoEnoughTShirtToSell",["qtd"=>$qtd]);
                            }
                        }else{
                            $errors[$id]=__("messages.TShirtNotPresentOnBD");
                        }
                    }
                }
                $entidade->save();
            }, 5);


            return [
                [
                    "errors"=>$errors,
                    "msg"=>
                        (count($errors)>0?
                            __("messages.NotAllTShirtsSold"):
                            (
                                $entregar?
                                    __("messages.TShirtSoldAndDeliveredSuccessfully"):
                                    __("messages.TShirtSoldSuccessfully")
                            )
                        ),
                    "status"=>(count($errors)>0?"warning":"success"),
                    "precoTotal"=>$qtdTotal*$preco
                ],
                200
            ];
        }catch (Exception $e){
            return [
                [
                "msg"=>__("messages.CriticalErrorOperationWasReverted"),
                "status"=>"error"
                ],
                500
            ];
        }
    }


    public function venderCamisas(Request $request){
        $validator = Validator::make($request->all(), [
            "entidadeId" => 'required',
            "entidadeTipo" => 'required',
            'year'=>'required',
            'l'=>"required",
            'l.*'=>"required",
            'l.*.camisaid'=>'required|numeric',
            'l.*.qtd'=>'required|numeric|min:1'
        ], [
            'l.required'=>__("messages.NoTShirtToSell"),
            'l.*.required'=>__("messages.NoTShirtToSell"),
            'l.*.camisaid.required'=>__("messages.NeedChooseValidTShirt"),
            "entidadeId.required"=>__("messages.InvalidChoiceOfUser"),
            "entidadeTipo.required"=>__("messages.InvalidChoiceOfUser"),
            'year.required'=>__("messages.EventYearRequired"),
        ]);



        if($request->has("entidadeId")&&$request->has("entidadeTipo")){
            $entidadeCamisas = parent::entidadeCamisas($request)[0];
            $camisas = $entidadeCamisas["camisas"];
            $entidadeCamisas = $entidadeCamisas["entidadeCamisas"];
        }else{
            return response()->json(["msg"=>__("messages.CantIdentifyUser"),"status"=>"error"],400);
        }


        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = __("messages.SomeErrorsOnSaving");
            if($errors->has("l")){
                $errorMessage = $errors->first("l");
            }elseif($errors->has("l.*")){
                $errorMessage = $errors->first("l.*");
            }

            return response()->json(["camisas"=>$camisas,"entidadeCamisas"=>$entidadeCamisas,"msg"=>$errorMessage,"errors"=>$validator->errors(),"status"=>"error"],400);

        }else{

            $lines = $request->get("l");
            $year = $request->get("year");
            $evento = Evento::where("ano", $year)->firstOrFail();
            $entregar = $request->get("vender")=="venderEentregar";


            $entidadeId = $request->get("entidadeId");
            $entidadeTipo = $request->get("entidadeTipo");
            if($entidadeTipo==="card"){
                $entidade = Card::findOrFail($entidadeId);
            }else{
                $entidade = User::findOrFail($entidadeId);
            }

            $responseArray = $this->venda($entidade, $evento, $entregar, $lines);

            $entidadeCamisas = parent::entidadeCamisas($request)[0];
            $camisas = $entidadeCamisas["camisas"];
            $entidadeCamisas = $entidadeCamisas["entidadeCamisas"];


            $responseArray[0]["camisas"] = $camisas;
            $responseArray[0]["entidadeCamisas"] = $entidadeCamisas;
            return response()->json($responseArray[0],$responseArray[1]);
        }
    }

}