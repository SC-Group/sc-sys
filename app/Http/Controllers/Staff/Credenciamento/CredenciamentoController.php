<?php


namespace App\Http\Controllers\Staff\Credenciamento;


use App\Helpers\PublishData;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SearchUserController;
use App\Http\Services\SCService as SC;
use App\Models\Dia;
use App\Models\Evento;
use App\Models\Inscricao;
use App\Models\Staff;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use mysql_xdevapi\Exception;
use Validator;
class CredenciamentoController extends Controller
{
    public function getCredenciamento($year,Request $request){
        $evento = Evento::where("ano",$year);

        if($evento->exists()){
            return SC::view("managepages.credenciamento.main",['year'=>$year]);
        }
        return redirect()->route("home")->with(["msg"=>__("messages.EventDoesntExists"),"status"=>"error"]);
    }

    public function getUsers(Request $request){
        $userController = new SearchUserController();
        $users = $userController->searchUserModel($request);

        if($users instanceof JsonResponse){
            return $users;
        }
        $evento = $users[1];
        $users = $users[0];
        $data = $users->has('inscricao')->paginate(10,["id","name","email"]);
        $users = $data->load(['inscricao' => function ($query) use ($evento) {
            $query->where("evento_id", $evento->id)->select(['id','user_id','kit'])->with(['dias'=>function($query){
                $query->select("data");
            }]);
        }]);
        $pages = [
            "current"=>$data->currentPage(),
            "last"=>$data->lastPage(),
            "urls"=>$data->getUrlRange(1,$data->lastPage())
        ];
        return response()->json(["users"=>$users,"pagesData"=>$pages]);
    }

    public function alterStatusUser(Request $request){
        $validator = Validator::make($request->all(), [
            "inscid" => 'required',
            "status" => 'required|in:Presente,Faltou,Selecione',
            "data" => 'required|date',
            "year"=>'required|numeric'
        ], [
            'inscid.required'=>__("messages.UserNeedToBeInformed")
        ]);

        if($validator->fails()){
            return response()->json(["msg"=>$validator->errors()->first(),"status"=>"error"],400);
        }else{
            $eventoId = Evento::where("ano",$request->get("year"))->first()->id;
            try{
                $kit = $request->get("kit")==="true";
                $inscId = $request->get("inscid");
                $dateString = $request->get("data");
                $date = Carbon::parse($dateString);
                $status = $request->get("status");
                $status = $status==="Selecione"?null:$status;
                $inscricao = Inscricao::find($inscId);

                if($inscricao == null) return response()->json(["msg"=>__("messages.SubscriptionNotFound"), "status"=>"error"],400);

                if(! Auth::user()->can('be-comissao')) {//se usuario autenticado é staff
                    $target = $inscricao
                        ->user()->first()
                        ->aluno()->first();
                    $targetIsStaff = $target
                        ->staff()->where('evento_id',$eventoId)->where("status","aprovado")->exists();
                    $targetIsComissao = $target
                        ->comissao()->exists();

                    if($targetIsComissao) throw new \Exception(__("StaffsCannotAccreditCommission"));
                    if($targetIsStaff) throw new \Exception(__("StaffsCannotAccreditStaffs"));
                }

                $dia = $inscricao->dias()->where('data',$date);
                if($dia->exists()){
                    $dia = $dia->first();
                    $dia->pivot->presenca = $status;
                    $dia->pivot->save();
                    $inscricao->kit = $kit;
                    $inscricao->save();
                    $this->updateUserStatus($inscId,$status,$kit,$dateString);
                    return response()->json([
                        "user"=>["inscid"=>$inscId,"status"=>$status,"kit"=>$kit,"data"=>$dateString],
                        "msg"=>__("messages.UpdatedWithSuccess"),
                        "status"=>"success"
                    ]);

                }else{

                    $dia = Dia::where('data',$date);
                    if($dia->exists()){
                        $inscricao->dias()->attach($dia->first()->id,["presenca"=>$status]);
                        $this->updateUserStatus($inscId,$status,$kit,$dateString);

                        $inscricao->kit = $kit;
                        $inscricao->save();

                        return response()->json([
                            "user"=>["inscid"=>$inscId,"status"=>$status,"kit"=>$kit,"data"=>$dateString],
                            "msg"=>__("messages.UpdatedWithSuccess"),
                            "status"=>"success"
                        ]);
                    }else{
                        throw new \Exception(__("messages.DayDoesntExists"));
                    }
                }

            }catch (\Exception $e){
                return response()->json(["msg"=>$e->getMessage(),"status"=>"error"],500);
            }

        }
    }

    public function updateUserStatus($inscId,$status,$kit,$day){
        $userStatus = ["inscid"=>$inscId,"status"=>$status,"kit"=>$kit, "data"=>$day];
        PublishData::publish("userStatus",$userStatus);
    }

}