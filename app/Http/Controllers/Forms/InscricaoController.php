<?php
/**
 * Created by PhpStorm.
 * User: fjmrs
 * Date: 03/03/2019
 * Time: 21:20
 */

namespace App\Http\Controllers\Forms;
use App\Http\Controllers\Controller;
use App\Models\Instituicao;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Evento;
use App\Models\User;
use App\Helpers\IdHelper;
use App\Helpers\RegexPatternJSHelper as RegexHelper;
use App\Http\Services\SCService as SC;
use App\Http\Services\SaveInscricaoParticipanteService as Save;
use Validator;


class InscricaoController extends FormController
{
    protected $sessionSequenceName = "_sequenceFormInsc";
    protected $correctSequence=[[0],[0],[0],[2],[2],[2],[3],[3,4,5,6],[7]];
    private $viewpath="forms.inscricao.inscricaogeral.";

    protected function saveData($year, Request $request)
    {
        return Save::saveData($year,$request);
    }

    public function getInscricao(Request $request, $year){

        $evento = Evento::where('ano','=',$year);

        if(!$evento->exists()) return response()->view('errors.main',['evt'=>$evento->first(),'msg'=>__("messages.FormEventNotCreatedYet",["year"=>$year])],500);

        $evento = $evento->first();
        $inscdate = Carbon::parse($evento->inicio_insc);
        $fimdate = Carbon::parse($evento->fim);
        $now = Carbon::now();

        if($now->greaterThan($fimdate)) return SC::view('errors.main',['year'=>$year,'msg'=>__("messages.FormEventAlreadyFinished",["year"=>$year]),'status'=>'error']);
        if($now->lessThan($inscdate))return SC::view('forms.inscricao.clockcount',['year'=>$year,'now'=>$now]);

        $section = IdHelper::dec($request->old('sectionid'))[0]??0;
        $arrvar = ["year"=>$year];

        switch($section){
            case 0: return SC::view($this->viewpath.'inscricaoID',$arrvar);
            case 1: return SC::view($this->viewpath.'inscricaoLogin',$arrvar);
            case 2: return SC::view($this->viewpath.'inscricaoDPess',$arrvar);
            case 3: return SC::view($this->viewpath.'inscricaoAluno',$arrvar);
            case 4: return SC::view($this->viewpath.'inscricaoProfessor',$arrvar);
            case 5: return SC::view($this->viewpath.'inscricaoParticipante',$arrvar);
            case 6: return SC::view($this->viewpath."inscricaoEstagio",$arrvar);
            case 7: return SC::view($this->viewpath."inscricaoCamisas",$arrvar);
            default: return SC::view($this->viewpath.'inscricaoID',$arrvar);
        }

    }



    function  executeProcess($id,$year,$request){
        $response = null;
        switch($id){
            case 0: $response = $this->postSectionID($year,$request);break;
            case 1: $response = $this->postSectionLogin($year,$request);break;
            case 2: $response = $this->postSectionDPess($year,$request);break;
            case 3: $response = $this->postSectionAluno($year,$request);break;
            case 4: $response = $this->postSectionProfessor($year,$request);break;
            case 5: $response = $this->postSectionParticipante($year,$request);break;
            case 6: $response = $this->postSectionEstagio($year,$request);break;
            case 7: $response = $this->postSectionCamisas($year,$request);break;
            default: $response = back()->with(["msg"=>__("messages.WrongFormPage"), "status"=>"error"]);
        }
        return $response;
    }


    protected function sendDataSuccess($data, $year){
        $dataSession = ['inscricao'=>$data["inscricao"]->id];
        $dataSession["status"] = "success";
        if($data["botStatus"]==="error"){
            $dataSession["msg"]=__("messages.FormSubscriptionSuccess").__("messages.BotError",["bot"=>env("BOT_NAME","Bot")]);
            $dataSession["status"]="warning";
        }
        return redirect()
            ->route('inscsuccess',['year'=>$year])
            ->with($dataSession);
    }

    function postSectionID($year,Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $email = $request->get('email');
            $user = User::where('email',$email);
            if($user->exists()){
                $user=$user->first();
                $evento = Evento::where("ano",$year)->first();
                $inscricao = $user->inscricao()->where("evento_id",$evento->id);
                if($inscricao->exists()){
                    return redirect("/")->with(["msg"=>"Você já está inscrito no evento de $year, faça login no sistema!","status"=>"error"]);
                }
                $arrvar = ['year'=>$year, 'emailValue'=>$email];
                $this->next = 1;
                return SC::view($this->viewpath.'inscricaoLogin',$arrvar);
            }else{
                $this->next=2;
                $arrvar = ['year'=>$year];
                return SC::view($this->viewpath.'inscricaoDPess',$arrvar);
            }
        }
    }

    function postSectionLogin($year,Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()->with(["emailValue"=>$request->get("email")]);
        }else{

            $credentials = $request->only('email', 'password');
            if(Auth::attempt($credentials)){
                $arrvar = [
                    'year'=>$year,

                ];
                return redirect()->route('profile',$arrvar)->with([
                    "msginfo"=>[
                        "msg"=>"Para se inscrever, atualize seus dados e escolha o botão de inscrição no evento de $year",
                        "status"=>"info"
                    ]
                ]);
            }else{
                return back()->withInput()->withErrors(['password'=>__("messages.IncorrectPassword")])->with(["emailValue"=>$request->get("email")]);
            }
        }
    }


    function postSectionDPess($year,Request $request){
        $regexname = RegexHelper::name();
        $regexPass = RegexHelper::password();
        $validator = Validator::make($request->all(), [
            'name'=>array("required","regex:/".$regexname['pattern']."/"),
            'tel'=>'nullable | digits: 11',
            'password' => 'required|confirmed|regex:/'.$regexPass['pattern'].'/',
            'category' => 'required'
        ], [
            'name.regex'=>$regexname['msg'],
            'password.regex'=>$regexPass['msg']
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $cat = $request->get('category');
            $arrvar = ['year'=>$year];
            if($cat=='Professor'){
                $this->next=4;
                return SC::view($this->viewpath.'.inscricaoProfessor',$arrvar);
            }else if($cat=='Estudante (fundamental, médio, técnico ou graduação)'){
                $this->next=3;
                return SC::view($this->viewpath.'.inscricaoAluno',$arrvar);
            }else if($cat=='Outro'){
                $this->next=5;
                return SC::view($this->viewpath.'.inscricaoParticipante',$arrvar);
            }
            return response(__("messages.UnexpectedError"),400);
        }
    }

    private function postSectionParticipante($year, Request $request)
    {
        $ufrj_id = Instituicao::where('sigla',"UFRJ")->first()->id;
        $validator = Validator::make($request->all(), [
            'instituicao'=>"required|in:".$ufrj_id,
            'exAluno' => 'required|boolean',
            'curso'=>'required_if:exAluno,1',
        ], [
            'instituicao.in'=>__("messages.FormValidation.NeedToBeUFRJ"),
            'exAluno.required'=>__("messages.FormValidation.RequiresUFRJExStudent"),
            'exAluno.boolean'=>__("messages.NeedOnlyYesOrNo"),
            'curso.required_if'=>__("messages.FormValidation.NeedCourse"),
        ]);

        if($request->get('exAluno')==1){
            $isCurso = $request->get('curso')==null;
            if($isCurso){
                $validator->after(function ($validator) {
                    $validator->errors()->add('curso', __("messages.FormValidation.NeedCourse"));
                });
            }
        }

        if ($validator->fails()) {
             return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $this->next=7;
            $arrvar = ['year'=>$year];
            return SC::view($this->viewpath.'.inscricaoCamisas',$arrvar);
        }
    }

    private function postSectionAluno($year, Request $request)
    {
        $ufrj_id = Instituicao::where('sigla',"UFRJ")->first()->id;
        $validator = Validator::make($request->all(), [
            'instituicaoTipo'=>'required',
            'instituicaoUF'=>'required',
            'instituicaoID' => 'required',
            'DRE'=>'nullable|regex:/^[1-9][0-9]{8}/'
        ], [
            'instituicaoID.required'=>__("messages.FormValidation.RequiredInstituicaoIDAluno"),
            'instituicaoUF.required'=>__("messages.FormValidation.RequiredInstituicaoUFAluno"),
            'instituicaoTipo.required'=>__("messages.FormValidation.RequiredInstituicaoTipoAluno"),
            'instituicaoCursoGraduacao.required_if:instituicaoTipo,Universidade'=>__("messages.FormValidation.NeedCourse"),
            'instituicaoCursoTecnico.required_if:instituicaoTipo,Escola de Ensino Técnico'=>__("messages.FormValidation.NeedCourse"),
            'DRE.regex'=>__("messages.FormValidation.ValidDRE"),
        ]);
        $DRE = $request->get('DRE');
        $instituicao = $request->get('instituicaoID');

        if ($validator->fails() || ($instituicao==$ufrj_id && $DRE==null)) {
            if($instituicao==$ufrj_id && $DRE==null)
                $validator->getMessageBag()->add('DRE', __("messages.FormValidation.ValidDRE"));
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $arrvar = ['year'=>$year];
            $instTipo = $request->get('instituicaoTipo');
            if($instTipo=='Escola de Ensino Técnico'|| $instTipo=='Universidade'){
                $this->next=6;
                return SC::view($this->viewpath.'.inscricaoEstagio',$arrvar);
            }
            $this->next=7;
            return SC::view($this->viewpath.'.inscricaoCamisas',$arrvar);
        }
    }

    private  function postSectionProfessor($year, Request $request){

        $validator = Validator::make($request->all(), [
            'instituicaoTipo'=>'required',
            'instituicaoUF'=>'required',
            'instituicaoID' => 'required',
            'instituicaoCursoTecnico'=>'required_if:instituicaoTipo,Escola de Ensino Técnico',
            'instituicaoCursoGraduacao'=>'required_if:instituicaoTipo,Universidade',

        ], [
            'instituicaoTipo.required'=>__("messages.FormValidation.RequiredInstituicaoTipoProf"),
            'instituicaoUF.required'=>__("messages.FormValidation.RequiredInstituicaoUFProf"),
            'instituicaoID.required'=>__("messages.FormValidation.RequiredInstituicaoIDProf"),
            'instituicaoCursoGraduacao.required_if:instituicaoTipo,Universidade'=>__("messages.FormValidation.NeedCourse"),
            'instituicaoCursoTecnico.required_if:instituicaoTipo,Escola de Ensino Técnico'=>__("messages.FormValidation.NeedCourse"),
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $arrvar = ['year'=>$year];
            $this->next=7;
            return SC::view($this->viewpath.'.inscricaoCamisas',$arrvar);
        }
    }

    private function postSectionEstagio($year,Request $request){
        $validator = Validator::make($request->all(), [
            'estagio'=>'required|boolean'
        ], [
            'estagio.required'=>__("messages.FormValidation.NeedEstagio"),
            'estagio.in'=>__("messages.FormValidation.NeedEstagio"),
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $arrvar = ['year'=>$year];
            $this->next=7;
            return SC::view($this->viewpath.'.inscricaoCamisas',$arrvar);
        }
    }

    private  function postSectionCamisas($year, Request $request){
        $validator = Validator::make($request->all(), [
            'camisas_masc'=>'array',
            'camisas_fem'=>'array',
            'camisas_masc.*.*'=>'required|numeric',
            'camisas_fem.*.*'=>'numeric',
        ], [
            'camisas_masc.array'=>__("messages.FormValidation.NeedChooseTShirts"),
            'camisas_fem.array'=>__("messages.FormValidation.NeedChooseTShirts")
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $this->next=8;
            return self::FIM;
        }
    }

    function getInscricaoEnd($year){
        $inscricao = session()->get('inscricao');
        session()->reflash();
        $arrvar = ['year'=>$year,"inscricao"=>$inscricao];
        return SC::view($this->viewpath.'.inscricaoEnd',$arrvar);
    }

}