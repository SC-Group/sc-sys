<?php

namespace App\Http\Controllers\Forms;
use App\Helpers\IdHelper;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;

abstract class FormController extends Controller{
    const FIM = -10;
    protected $next=null;
    protected $correctSequence = [];
    protected $sessionSequenceName = "_sequenceForm";

    protected function checkSequence($sequence){//Dada a sequencia da session, retorna ultimo item correto;
        $lastcorrect = 0;
        $lenseq = count($sequence);
        for ($i = 1; $i < $lenseq; $i++) {
            $secAtual = $sequence[$i];
            $secAnterior = $sequence[$i-1];
            if (in_array($secAnterior, $this->correctSequence[$secAtual])) $lastcorrect = $i;
        }
        return $lastcorrect;
    }

    protected function getSectionBack($request){
        if( $request->session()->has($this->sessionSequenceName)){
            $sequence = $request->session()->get($this->sessionSequenceName);
            array_pop($sequence);
            $inputs = $request->session()->get('section'.$sequence[count($sequence)-1]);
            $request->session()->put($this->sessionSequenceName,$sequence);
            $request->session()->put("_old_input",$inputs);
        }
        return back();
    }

    protected function  checkSession($id,$response, Request $request){
        $sequence = $request->session()->get($this->sessionSequenceName)??[0];
        array_push($sequence,$this->next);
        $nextIndex = array_search($this->next,$sequence);
        $lastIndex = $this->checkSequence($sequence);
        $correct=true;
        if($nextIndex!==false){
            if($lastIndex<$nextIndex){
                $nextIndex = $lastIndex;
                $correct=false;
            }elseif($sequence[$nextIndex-1]!=$id){
                $correct=false;
                $nextIndex = $nextIndex-1;
            }
            if(!$correct&&
                ($this->correctSequence[$this->next]===$this->correctSequence[$sequence[$nextIndex]])&&
                ($this->next!==$sequence[$nextIndex])
            ){
                $sequence[$nextIndex] = $this->next;
                $correct=true;
            }
            $sequence = array_slice($sequence,0,$nextIndex+1);
        }

        $request->session()->put($this->sessionSequenceName,$sequence);

        if(!$correct){
            $inputs = $request->session()->get('section'.$sequence[count($sequence)-1]);
            $request->session()->put("_old_input",$inputs);
            return back()->with(["msg"=>__("messages.FormFlowError").__("messages.FormLastValidPosition"), "status"=>"warning"]);
        }

        $request->session()->put("section".$id,$request->all());

        return $response;
    }

    public function postInscricao($year,Request $request){
        $id = IdHelper::dec($request->get('sectionid'))[0];
        $buttonBack = $request->get('buttonSend')=='Anterior';
        if($buttonBack){
            return $this->getSectionBack($request);
        }else{
            $response = $this->executeProcess($id,$year,$request);

            if(isset($response)&&$response!==self::FIM&&$response->status()==200){

                $response = $this->checkSession($id,$response,$request);

            }elseif($response===self::FIM){
                $response = $this->checkSession($id,$response,$request);

                if($response===self::FIM){
                    $data = $this->saveData($year,$request);
                    $request->session()->flush();
                    if($data["status"] == "success"){
                        return $this->sendDataSuccess($data,$year);
                    }else{
                        return redirect()->route("errorpage")->with(["msg"=>$data["msg"],"status"=>"error"]);
                    }
                }
            }
            return $response;
        }
    }

    protected abstract function saveData($year,Request $request);
    protected abstract function getInscricaoEnd($year);
    protected abstract function getInscricao(Request $request, $year);
    protected abstract function executeProcess($id,$year,$request);
    protected abstract function sendDataSuccess($data, $year);

}