<?php


namespace App\Http\Controllers\Forms;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Evento;

class ClockController extends Controller
{
    public function getInscTime(Request $request){
        $year = $request->get("year");
        if($year!=null){
            return response()->json(["timeInsc"=>Evento::where("ano",$year)->first()->inicio_insc, "timeNow"=>now()]);
        }else{
            return response()->json(["msg"=>__("messages.EventNotFound"),"status"=>"error"],400);
        }
    }
}