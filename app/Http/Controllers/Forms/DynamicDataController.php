<?php


namespace App\Http\Controllers\Forms;


use App\Http\Controllers\Controller;
use App\Models\Instituicao;
use Illuminate\Http\Request;

class DynamicDataController extends Controller
{
    function getInstituicaoEstados(Request $request){
        $tipo = $request->get("tipo");
        $uf = Instituicao::distinct()->where("tipo","=",$tipo)->orderBy('uf_nome','desc')->get(['uf_nome','uf_sigla']);
        return response()->json($uf);
    }

    function getInstituicoes(Request $request){
        $instituicoes = Instituicao::where("tipo",$request->get("tipo"))->where("uf_sigla",$request->get("uf_sigla"))->orderBy('nome','desc')->get(['id','nome','sigla']);
        return response()->json($instituicoes);
    }

    function getInstituicaoTipos(){
        $tipos = Instituicao::distinct()->orderBy('tipo','desc')->get(['tipo']);
        return response()->json($tipos);
    }

    function getInstituicaoCursos(Request $request){
        $id = null;
        try{
            $id = intval($request->get("id"));
        }catch (Exception $e){
            return response()->json(["msg"=>__("messages.IdentifierIsNotAValidNumber"),'status'=>'error'],400);
        }
        $instituicao = Instituicao::where("id","=",$id)->first();
        if($instituicao==null){
            return response()->json(["msg"=>"","status"=>"error"],406);
        }
        $cursos = $instituicao->cursos()->orderBy('nome','desc')->get(["id","nome"]);
        return response()->json($cursos);
    }
}