<?php

namespace App\Http\Controllers\Forms;
use App\Http\Controllers\Controller;
use App\Models\Instituicao;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Evento;
use App\Models\User;
use App\Helpers\IdHelper;
use App\Helpers\RegexPatternJSHelper as RegexHelper;
use App\Http\Services\SCService as SC;
use App\Http\Services\SaveInscricaoStaffService as Save;
use Validator;

class StaffController extends FormController {

    protected $sessionSequenceName = "_sequenceFormStaff";
    protected $correctSequence=[[0],[0],[1],[0],[2,3],[4],[5],[6]];
    private $viewpath="forms.staffs.";

    protected function saveData($year, Request $request)
    {
        return Save::saveData($year,$request);
    }

    public function getInscricao(Request $request, $year){

        $evento = Evento::where('ano','=',$year);

        if(!$evento->exists()) return response()->view('errors.main',['evt'=>$evento->first(),'msg'=>__("messages.FormEventNotCreatedYet",["year"=>$year])],500);

        $evento = $evento->first();
        $inscdate = Carbon::parse($evento->inicio_insc);
        $now = Carbon::now();

        if($now->greaterThan($inscdate->subDays(30)))
            return SC::view('errors.main',['year'=>$year,'msg'=>__("messages.FormStaffSubscriptionAlreadyFinished",["year"=>$year]),'status'=>'error'],400);

        $section = IdHelper::dec($request->old('sectionid'))[0]??0;
        $arrvar = ["year"=>$year];

        switch($section){
            case 0: return SC::view($this->viewpath.'ID',$arrvar);
            case 1: return SC::view($this->viewpath.'Login',$arrvar);
            case 2: return SC::view($this->viewpath."ConfirmLogin",$arrvar);
            case 3: return SC::view($this->viewpath.'DPess',$arrvar);
            case 4: return SC::view($this->viewpath.'Disponibilidade',$arrvar);
            case 5: return SC::view($this->viewpath."Estagio",$arrvar);
            case 6: return SC::view($this->viewpath."Camisas",$arrvar);
            default: return SC::view($this->viewpath.'ID',$arrvar);
        }

    }

    protected function sendDataSuccess($data, $year){
        $dataSession = ['inscricao'=>$data["inscricao"]->id];
        $dataSession["status"] = "success";

        if($data["botStatus"]==="error"){
            $dataSession["msg"]=__("messages.FormSubscriptionSuccess").__("messages.BotError",["bot"=>env("BOT_NAME","Bot")]);
            $dataSession["status"]="warning";
        }
        return redirect()
            ->route('inscstaffsuccess',['year'=>$year])
            ->with($dataSession);
    }

    function  executeProcess($id,$year,$request){
        $response = null;

        switch($id){
            case 0: return $this->postSectionID($year,$request);break;
            case 1: return $this->postSectionLogin($year,$request);break;
            case 2: return $this->postSectionConfirmLogin($year,$request);break;
            case 3: return $this->postSectionDPess($year,$request);break;
            case 4: return $this->postSectionDisponibilidade($year,$request);break;
            case 5: $response = $this->postSectionEstagio($year,$request);break;
            case 6: $response = $this->postSectionCamisas($year,$request);break;
            default: $response = back()->with(["msg"=>__("messages.WrongFormPage"), "status"=>"error"]);
        }

        return $response;
    }

    function postSectionID($year,Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $email = $request->get('email');
            if(User::where('email',$email)->exists()){
                $arrvar = ['year'=>$year, 'emailValue'=>$email];
                $this->next = 1;
                return SC::view($this->viewpath.'Login',$arrvar);
            }else{
                $this->next=3;
                $arrvar = ['year'=>$year];
                return SC::view($this->viewpath.'DPess',$arrvar);
            }
        }
    }


    protected function postSectionLogin($year,Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {

            return back()
                ->withErrors($validator)
                ->withInput()->with(["emailValue"=>$request->get("email")]);
        }else{

            $credentials = $request->only('email', 'password');
            if($request->session()->has("_auth")){
                $request->session()->forget("_auth");
            }
            $oldAuth = null;
            if(Auth::check()){
                $oldAuth = Auth::id();
                Auth::logout();
            }
            if(Auth::attempt($credentials)){
                $request->session()->put("_oldAuth",$oldAuth);
                $request->session()->put("_auth",Auth::id());
                $this->next=2;
                $arrvar = [
                    'year'=>$year
                ];
                return SC::view($this->viewpath.'ConfirmLogin',$arrvar);
            }else{
                return back()->withInput()->withErrors(['password'=>__("messages.IncorrectPassword")])->with(["emailValue"=>$request->get("email")]);
            }
        }
    }

    protected function postSectionConfirmLogin($year, Request $request){
        $validator = Validator::make($request->all(), [
            'confirm' => 'required|in:1',
        ], [
            'confirm.required'=>__("messages.FormValidation.ConfirmDataRequired"),
            'confirm.in'=>__("messages.FormValidation.ConfirmDataRequired")
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            Auth::logout();
           if($request->session()->has("_oldAuth")){
               $oldAuth = $request->session()->get("_oldAuth");
               Auth::loginUsingId($oldAuth);
           }

            $this->next=4;
            $arrvar = ['year'=>$year];
            return SC::view($this->viewpath.'Disponibilidade',$arrvar);
        }
    }


    function postSectionDPess($year,Request $request){
        $regexname = RegexHelper::name();
        $regexPass = RegexHelper::password();
        $validator = Validator::make($request->all(), [
            'name'=>array("required","regex:/".$regexname['pattern']."/"),
            'password' => 'required|confirmed|regex:/'.$regexPass['pattern'].'/',
            'tel'=>'nullable | digits:11',
            "dre"=>"required | digits:9",
            "curso"=>"required | exists:cursos,id",
            "confirm"=>"required | boolean | in:1"
        ], [
            'name.regex'=>$regexname['msg'],
            'password.regex'=>$regexPass['msg'],
            'confirm'=>'Para seguir, você precisa confirmar que é estudante de graduação da ufrj'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $arrvar = ['year'=>$year];
            $this->next=4;
            return SC::view($this->viewpath.'Disponibilidade',$arrvar);
        }
    }


    protected function postSectionDisponibilidade($year, Request $request){

        $validator = Validator::make($request->all(), [
            'disponibilidade'=>'required|min:1',
        ], [
            'disponibilidade.required'=>__("messages.FormValidation.RequiredAvailability"),
            'disponibilidade.min'=>__("messages.FormValidation.RequiredAvailability"),
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $arrvar = ['year'=>$year];
            $this->next=5;
            return SC::view($this->viewpath.'Estagio',$arrvar);
        }
    }


    private function postSectionEstagio($year,Request $request){
        $validator = Validator::make($request->all(), [
            'estagio'=>'required|boolean'
        ], [
            'estagio.required'=>__("messages.FormValidation.NeedEstagio"),
            'estagio.in'=>__("messages.FormValidation.NeedEstagio"),
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $arrvar = ['year'=>$year];
            $this->next=6;
            return SC::view($this->viewpath.'Camisas',$arrvar);
        }
    }

    private  function postSectionCamisas($year, Request $request){
        $validator = Validator::make($request->all(), [
            'camisas_masc'=>'array',
            'camisas_fem'=>'array',
            'camisas_masc.*.*'=>'required|numeric',
            'camisas_fem.*.*'=>'numeric',
        ], [
            'camisas_masc.array'=>__("messages.FormValidation.NeedChooseTShirts"),
            'camisas_fem.array'=>__("messages.FormValidation.NeedChooseTShirts")
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $this->next=7;
            return self::FIM;
        }
    }

    function getInscricaoEnd($year){
        $inscricao = session()->get('inscricao');
        session()->reflash();
        $arrvar = ['year'=>$year,"inscricao"=>$inscricao];
        return SC::view($this->viewpath.'End',$arrvar);
    }

}