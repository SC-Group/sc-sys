<?php
/**
 * Created by PhpStorm.
 * User: fjmrs
 * Date: 03/03/2019
 * Time: 21:20
 */

namespace App\Http\Controllers\Forms;

use App\Http\Services\SCService;
use App\Models\Atividade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Evento;
use App\Models\User;
use App\Helpers\IdHelper;
use App\Helpers\RegexPatternJSHelper as RegexHelper;
use App\Http\Services\SCService as SC;
use App\Http\Services\SaveInscricaoAtividadesService as Save;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;


class SubmissaoAtividadesController extends FormController
{

    protected $sessionSequenceName = "_sequenceFormAtividades";
    protected $correctSequence=[[0],[0],[1],[0],[2,3],[4],[5],[5],[6],[7,8],[7,8,9],[7,8,10]];
    protected $viewpath="forms.atividades.";

    protected function saveData($year, Request $request)
    {
        return Save::saveData($year,$request);
    }

    public function getInscricao(Request $request, $year){
        $evento = Evento::where('ano','=',$year);

        if(!$evento->exists())
            return response()->view('errors.main',['evt'=>$evento->first(),'msg'=>__("messages.FormEventNotCreatedYet",["year"=>$year])],500);

        $evento = $evento->first();
        $inscdate = Carbon::parse($evento->inicio_insc);
        $now = Carbon::now();

        if($now->greaterThan($inscdate->subDays(30)))
            return SC::view('errors.main',['year'=>$year,'msg'=>__("messages.FormActivitySubscriptionAlreadyFinished",["year"=>$year]),'status'=>'error'],400);

        $section = IdHelper::dec($request->old('sectionid'))[0]??0;

        $arrvar = ["year"=>$year];
        if(Auth::check())
            $arrvar["emailValue"]=Auth::user()->email;

        switch($section){
            case 0: return SC::view($this->viewpath.'ID',$arrvar);
            case 1: return SC::view($this->viewpath.'Login',$arrvar);
            case 2: return SC::view($this->viewpath."ConfirmLogin",$arrvar);
            case 3: return SC::view($this->viewpath.'DPess',$arrvar);
            case 4: return SC::view($this->viewpath.'Disponibilidade',$arrvar);
            case 5: return SC::view($this->viewpath.'Atividade',$arrvar);
            case 6: return SC::view($this->viewpath.'AtividadeExiste',$arrvar);
            case 7: return SC::view($this->viewpath."NovaAtividade",$arrvar);
            case 8: return SC::view($this->viewpath."ConfirmaAtividade",$arrvar);
            case 9: return SC::view($this->viewpath."Estagio",$arrvar);
            case 10: return SC::view($this->viewpath."Camisas",$arrvar);
            default: return SC::view($this->viewpath.'ID',$arrvar);
        }

    }


    protected function executeProcess($id,$year,$request){
        $response = null;
        switch($id){
            case 0: $response = $this->postSectionID($year,$request);break;
            case 1: $response = $this->postSectionLogin($year,$request);break;
            case 2: $response = $this->postSectionConfirmLogin($year,$request);break;
            case 3: $response = $this->postSectionDPess($year,$request);break;
            case 4: $response = $this->postSectionDisponibilidade($year,$request);break;
            case 5: $response = $this->postSectionAtividade($year,$request);break;
            case 6: $response = $this->postSectionAtividadeExiste($year,$request);break;
            case 7: $response = $this->postSectionNovaAtividade($year,$request);break;
            case 8: $response = $this->postSectionConfirmaAtividade($year,$request);break;
            case 9: $response = $this->postSectionEstagio($year,$request);break;
            case 10: $response = $this->postSectionCamisas($year,$request);break;
            default: $response = back()->with(["msg"=>__("messages.WrongFormPage"), "status"=>"error"]);
        }
        return $response;
    }


    protected function sendDataSuccess($data, $year){
        $dataSession = [
            'user_exist'=>$data['palestrante_exist'],
            'palestrante'=>$data["palestrante"]->id,
            'ativCode' => $data["ativCode"]
        ];
        $dataSession["status"] = "success";

        return redirect()
            ->route('inscatividadesuccess',['year'=>$year])
            ->with($dataSession);
    }


    protected function postSectionID($year,Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $email = $request->get('email');
            if(User::where('email',$email)->exists()){
                $arrvar = ['year'=>$year, 'emailValue'=>$email];
                $this->next = 1;
                return SC::view($this->viewpath.'Login',$arrvar);
            }else{
                $this->next=3;

                $arrvar = ['year'=>$year];
                return SC::view($this->viewpath.'DPess',$arrvar);
            }
        }
    }


    protected function postSectionLogin($year,Request $request){
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            
            return back()
                ->withErrors($validator)
                ->withInput()->with(["emailValue"=>$request->get("email")]);
        }else{
            
            $credentials = $request->only('email', 'password');
            if($request->session()->has("_auth")){
                $request->session()->forget("_auth");
            }
            if(Auth::check()){
                Auth::logout();
            }
            if(Auth::attempt($credentials)){
                $request->session()->put("_auth",Auth::id());
                $this->next=2;
                $arrvar = [
                    'year'=>$year
                ];
                return SC::view($this->viewpath.'ConfirmLogin',$arrvar);
            }else{
                return back()->withInput()->withErrors(['password'=>__("messages.IncorrectPassword")])->with(["emailValue"=>$request->get("email")]);
            }
        }
    }


    protected function postSectionConfirmLogin($year, Request $request){
        $validator = Validator::make($request->all(), [
            'confirm' => 'required|in:1',
        ], [
            'confirm.required'=>__("messages.FormValidation.ConfirmDataRequired"),
            'confirm.in'=>__("messages.FormValidation.ConfirmDataRequired")
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            Auth::logout();
            $this->next=4;
            $arrvar = ['year'=>$year];
            return SC::view($this->viewpath.'Disponibilidade',$arrvar);
        }
    }


    protected function postSectionDPess($year,Request $request){
        $regexname = RegexHelper::name();

        $regexPass = RegexHelper::password();
        $validator = Validator::make($request->all(), [
            'name'=>array("required","regex:/".$regexname['pattern']."/"),
            'password' => 'required|confirmed|regex:/'.$regexPass['pattern'].'/',
            'tel'=>"nullable | digits:11",
            'category' => 'required',
            'dre'=>'required_if:category,aluno | digits:9',
            'curso'=>Rule::requiredIf(function () use($request){
                $category= $request->get('category');
                return $category=="aluno"||$category=="ex_aluno";
            })
        ], [
            'dre.size'=>__("messages.FormValidation.ValidDRE"),
            'dre.required_if'=>__("messages.FormValidation.RequiredDRE"),
            'name.required'=>'O nome é obrigatório',
            'name.regex'=>$regexname['msg'],
            'category.required'=>'Você precisa informar que tipo de palestrante você é',
            'password.regex'=>$regexPass['msg']
        ]);

        $validator->sometimes('dre','size:9',function($input){
            return $input->category=='aluno';
        });

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $arrvar = ['year'=>$year];
            $this->next=4;
            return SC::view($this->viewpath.'.Disponibilidade',$arrvar);
        }
    }


    protected function postSectionDisponibilidade($year, Request $request){

        $validator = Validator::make($request->all(), [
            'disponibilidade'=>'required|min:1',
        ], [
            'disponibilidade.required'=>__("messages.FormValidation.RequiredAvailability"),
            'disponibilidade.min'=>__("messages.FormValidation.RequiredAvailability"),
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $arrvar = ['year'=>$year];
            $this->next=5;
            return SC::view($this->viewpath.'Atividade',$arrvar);
        }
    }


    protected function postSectionAtividade($year, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cadastro_atividade'=>'required',
        ], [
            'cadastro_atividade.required'=>__("messages.FormValidation.ChooseSubscriptionMode"),
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $arrvar = ['year'=>$year];
            $atividade = $request->get('cadastro_atividade');
            if($atividade=="nova"){
                $this->next=7;
                return SC::view($this->viewpath.'.NovaAtividade',$arrvar);
            }

            $this->next=6;
            return SC::view($this->viewpath.'.AtividadeExiste',$arrvar);
        }
    }


    protected function postSectionNovaAtividade($year, Request $request){
        $validator = Validator::make($request->all(), [
            'tipo' => 'required|in:Workshop,Palestra',
            'titulo' => 'required|max:100',
            'vinculada' => 'required|in:true,false',
            'descricao' => "required|max:400",
            'recursos' => 'required_if:tipo,Workshop|max:400',
            'organizacao' => 'required_if:vinculada,true|max:100',
            'tipo_organizacao' => 'required_if:vinculada,true'
        ], [
            'tipo.required' => __("messages.FormValidation.RequiredActivityType"),
            'tipo.in' => __("messages.FormValidation.ActivityWorkshopOrPalestra"),
            'titulo.required' => __("messages.FormValidation.RequiredActivityTitle"),
            'titulo.max' => __("messages.FormValidation.ActivityTitleMaxSize"),
            'vinculada.*' => __("messages.FormValidation.ActivityIsAttached"),
            'tipo_organizacao.in' => __("messages.FormValidation.CorrectActivityTypeOrganization"),
            'tipo_organizacao.required_if' => __("messages.FormValidation.RequiredActivityTypeOrganization"),
            'organizacao.required' => __("messages.FormValidation.RequiredOrganization"),
            'organizacao.max' => __("messages.FormValidation.OrganizationMaxSize"),
            'descricao.required' => __("messages.FormValidation.RequiredActivityDescription"),
            'descricao.max' => __("messages.FormValidation.ActivityDescriptionMaxSize"),
            'recursos.required' => __("messages.FormValidation.RequiredActivityResources"),
            'recursos.max' => __("messages.FormValidation.ActivityResourcesMaxSize"),
        ]);
        $validator->sometimes(['tipo_organizacao'],Rule::in(['Grupo de Interesse', 'Empresa']),function ($input){
            return $input->vinculada=="true";
        });

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            return $this->nextStepView($year,$request);
        }
    }


    protected  function postSectionAtividadeExiste($year, Request $request){
        $validator = Validator::make($request->all(), [
            'codigo'=>'required'
        ], [
            'codigo.required'=>__("messages.FormValidation.RequiredActivityCode")
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $evt = Evento::where("ano",$year)->first();
            $atividade = Atividade::where("codigo",$request->get("codigo"))->where("evento_id",$evt->id);

            if($atividade->exists()){
                $atividade=$atividade->first();
                if(session()->has('_auth')) {
                    $user = User::find(session()->get('_auth'));
                    $palestrante = $user->palestrante()->where("evento_id", $evt->id)->first();
                    if($palestrante!=null) {
                        $atividades = $palestrante->atividades();
                        if ($atividades->exists()) {
                            $alreadyAssociated = $atividades->where("id", $atividade->id)->exists();
                            if ($alreadyAssociated) {
                                return back()
                                    ->withInput()
                                    ->with(["msg" => __("messages.ActivityAlreadyAttachedToSpeaker"), "status" => "error"]);
                            }
                        }
                    }
                }
                $arrvar = ['year'=>$year,'atividade'=>$atividade];
                $this->next=8;
                return SC::view($this->viewpath.'ConfirmaAtividade',$arrvar);
            }else{
                return back()
                    ->withInput()
                    ->with(["msg"=>__("messages.ActivityDoesntExistsOnEvent",["year"=>$year]), "status"=>"error"]);
            }
        }
    }


    protected function authenticatedShouldGoToEstagio(User $user){
        $aluno = $user->aluno();
        if($aluno->exists()){
            $aluno=$aluno->first();
            if(!$aluno->estagio){
                $instituicao = $aluno->instituicao();
                if($instituicao->exists()){
                    $instituicaoTipo = $instituicao->first()->tipo;
                    if ($instituicaoTipo == "Escola de Ensino Técnico" || $instituicaoTipo == "Universidade") {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    protected function notAuthenticatedShouldGoToEstagio(Request $request){
        $category = $request->session()->get('section3.category');
        return $category=="aluno";
    }


    protected function authenticatedShouldGoToCamisas(User $user,$year){
        $evento = Evento::where("ano",$year)->first();
        $camisas = $user->camisas()->where("evento_id",$evento->id);
        return !$camisas->exists();
    }


    protected function nextStepView($year, $request){
        $arrvar=["year"=>$year];
        if($request->session()->has("_auth")){
            $user = User::find($request->session()->get("_auth"));
            if($this->authenticatedShouldGoToEstagio($user)){
                $this->next=9;
                return SC::view($this->viewpath."Estagio",$arrvar);
            }else if($this->authenticatedShouldGoToCamisas($user,$year)){
                $this->next=10;
                return SC::view($this->viewpath."Camisas",$arrvar);
            }
        }else{
            if($this->notAuthenticatedShouldGoToEstagio($request)){
                $this->next=9;
                return SC::view($this->viewpath."Estagio",$arrvar);
            }else{
                $this->next=10;
                return SC::view($this->viewpath."Camisas",$arrvar);
            }
        }

        $this->next = 11;
        return self::FIM;
    }


    protected function postSectionConfirmaAtividade($year,$request){
        return $this->nextStepView($year,$request);
    }


    protected function postSectionEstagio($year,Request $request){
        $validator = Validator::make($request->all(), [
            'estagio'=>'required|in:1,0'
        ], [
            'estagio.required'=>__("messages.FormValidation.NeedEstagio"),
            'estagio.in'=>__("messages.FormValidation.NeedEstagio"),
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $arrvar = ['year'=>$year];
            $this->next=10;
            return SC::view($this->viewpath.'Camisas',$arrvar);
        }
    }


    protected function postSectionCamisas($year, Request $request){
        $validator = Validator::make($request->all(), [
            'camisas_masc'=>'array',
            'camisas_fem'=>'array',
            'camisas_masc.*.*'=>'numeric',
            'camisas_fem.*.*'=>'numeric',
        ], [
            'camisas_masc.array'=>__("messages.FormValidation.NeedChooseTShirts"),
            'camisas_fem.array'=>__("messages.FormValidation.NeedChooseTShirts"),
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $this->next=11;
            return self::FIM;
        }
    }


    protected function getInscricaoEnd($year){
        $palestrante = session()->get('palestrante');
        $ativCode = session()->get('ativCode');
        $user_exist = session()->get("user_exist");
        session()->reflash();
        $arrvar = ["userExist"=>$user_exist,'year'=>$year,"palestrante"=>$palestrante,"ativCode"=>$ativCode];
        return SC::view($this->viewpath.'End',$arrvar);
    }

}