<?php


namespace App\Http\Controllers\Cronogramas;

use App\Http\Services\SCService;
use App\Models\Evento;
use App\Models\Cronograma;
use Carbon\Carbon;

class CronogramasController
{
    public function getCronogramasOverview($year){
        $evento = Evento::where("ano",$year);
        if(!$evento->exists()){
            return redirect("/")->with(["msg"=>__("messages.EventNotFound"), "status"=>"error"]);
        }
        $evento=$evento->first();

        $dias = $evento->dias();
        if (!$dias->exists()) {
            return back()->with(["msg"=>__("messages.DayDoesntExists"), "status"=>"error"]);
        }

        $horas = $evento->horas();
        if (!$horas->exists()) {
            return back()->with(["msg"=>__("messages.DayDoesntExists"), "status"=>"error"]);
        }

        $locais = $evento->locais();
        if(!$locais->exists()){
            return back()->with(["msg"=>__("messages.LocalDoesntExistsOnEvent")]);
        }
        return SCService::view("regularpages.cronogramas.main",["page"=>"overview","year"=>$year, "dias"=>$dias->get(),"horas"=>$horas->get(), "locais"=>$locais->get()]);
    }

    public function getCronograma($year,$dia,$mes){
        $evento = Evento::where("ano",$year);
        if(!$evento->exists()){
            return redirect("/")->with(["msg"=>__("messages.EventNotFound"), "status"=>"error"]);
        }
        $evento=$evento->first();
        $dia = $evento->dias()->whereDate("data",new Carbon("$year-$mes-$dia"));
        if (!$dia->exists()) {
            return back()->with(["msg"=>__("messages.DayDoesntExists"), "status"=>"error"]);
        }
        $dia=$dia->first();

        $horas = $evento->horas();
        if (!$horas->exists()) {
            return back()->with(["msg"=>__("messages.DayDoesntExists"), "status"=>"error"]);
        }

        $locais = $evento->locais();
        if(!$locais->exists()){
            return back()->with(["msg"=>__("messages.LocalDoesntExistsOnEvent")]);
        }
        return SCService::view("regularpages.cronogramas.main",["page"=>"","year"=>$year, "dia"=>$dia,"horas"=>$horas->get(), "locais"=>$locais->get()]);
    }

}