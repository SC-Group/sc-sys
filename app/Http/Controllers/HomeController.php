<?php

namespace App\Http\Controllers;

use App\Models\Staff;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Models\Evento;
use Carbon\Carbon;
use Vinkla\Hashids\Facades\Hashids;
use App\Http\Services\SCService as SC;
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($year=null){

        return SC::view("regularpages.resumo.main");
    }

    protected function isValidUserForEgg($user, $insc, $evento){
        $isDateValid = Carbon::create($evento->fim)->gt(now());
        $isStaff = Staff::where("aluno_id",$user->id)->where("status","aprovado")->where("evento_id",$evento->id)->exists();
        $isComissao = $user->aluno()->first()!==null && $user->aluno()->first()->comissao_id !== null;
        return $insc!==null  && !$isStaff && !$isComissao && !$insc->egg()->exists() && $isDateValid;
    }

    public function validateEgg(Request $request){
        $validator = Validator::make($request->all(),[
            "eggcod"=>"required | max: 255",
            "year"=>"required"
        ]);
        if($validator->fails()){
            return back()->withInput()->withErrors($validator->messages());
        }

        $year = $request->get('year');
        $evento = Evento::where("ano",$year);
        if(!$evento->exists()){
            return back()->with(["msg"=>__("messages.EventNotFound"),"status"=>"error"]);
        }
        $evento=$evento->first();
        $egg = $evento->eggs()->where("cod",$request->get("eggcod"))->first();
        $user = auth()->user();
        $insc = $user->inscricao()->where("evento_id",$evento->id)->first();
        if($egg!==null && $this->isValidUserForEgg($user,$insc,$evento) && !$egg->inscricao()->exists()){
            $egg->inscricao()->associate($insc);
            $egg->save();
            return back();
        }
        return back()->with(["msg"=>"Código inválido","status"=>"info"]);
    }

}
