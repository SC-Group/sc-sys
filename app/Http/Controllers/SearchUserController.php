<?php


namespace App\Http\Controllers;

use App\Models\Evento;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Validator;

class SearchUserController extends Controller
{

    public function replaceRegexAccent($string)
    {
        $replacement = ["(a|á|à|ã|â|ä)", "(A|Á|À|Ã|Â|Ä)", "(e|é|è|ê|ë)", "(E|É|È|Ê|Ë)", "(i|í|ì|î|ï)", "(I|Í|Ì|Î|Ï)", "(y|ỳ|ý|ŷ|ÿ)", "(Y|Ỳ|Ý|Ŷ|Ÿ)", "(o|ó|ò|õ|ô|ö)", "(O|Ó|Ò|Õ|Ô|Ö)", "(u|ú|ù|û|ü)", "(U|Ú|Ù|Û|Ü)", "(n|ñ)", "(N|Ñ)"];
        $regex = [];
        foreach ($replacement as $r) {
            array_push($regex, "/" . $r . "/");
        }
        return preg_replace($replacement, $replacement, $string);
    }

    public function searchUserByEmail($evento, $input)
    {
        $user = User::where("email", $input);
        if (!$user->exists())
            throw new Exception(__("messages.UserNotFound"), 400);
        return $user;
    }

    public function searchUserByInscId($evento, $input)
    {
        $inscricao = $evento->inscricao()->find($input);
        if (!$inscricao!=null)
            throw new Exception(__("messages.SubscriptionNotFound"), 400);

        $user = $inscricao->user();
        if (!$user->exists())
            throw new Exception(__("messages.SubscriptionExistsButHasNoUser"), 400);

        return $user;
    }

    public function searchUserByNome($evento, $input)
    {   if(empty($input))
            throw new Exception("Input vazio",400);
        $input = $this->replaceRegexAccent($input);
        $user = User::where("name", "regexp", $input);
        if (!$user->exists())
            throw new Exception(__("messages.UserNotFound"), 400);
        return $user;
    }

    public function searchUserById($evento, $input)
    {
        $user = User::where("id",$input);

        if (!$user->exists())
            throw new Exception(__("messages.UserNotFound"), 400);

        return $user;
    }


    public function getUserAttributes($users, $eventoId)
    {
        $users = $users->load(['inscricao' => function ($query) use ($eventoId) {
            $query->where("evento_id", $eventoId)->select("id", "user_id");
        }]);
        if (is_a($users, "\Illuminate\Database\Eloquent\Collection")) {
            foreach ($users as $user) {
                $inscricao = $user->inscricao->makeHidden("user_id")->first();
                if ($inscricao != null) {
                    $user->inscId = $inscricao->id;
                }
                $user->tipo="user";
                unset($user->inscricao);
            }
        } else {
            $inscricao = $users->inscricao->makeHidden("user_id")->first();
            if ($inscricao != null) {
                $users->inscId = $inscricao->id;
            }
            $users->tipo="user";
            unset($users->inscricao);
        }
        return $users;
    }

    function searchUserModel(Request $request){
        $validator = Validator::make($request->all(), [
            'year' => 'required|digits:4',
        ], ['year.required'=>__("messages.EventYearRequired")]);
        if($validator->fails()){
            return response()->json(["msg"=>$validator->messages(),"status"=>"error"],400);
        }
        $evento = Evento::where("ano", $request->get("year"));
        if (!$evento->exists())
            return response()->json(["msg" => __("messages.EventDoesntExists"), "status" => "error"], 500);
        $evento = $evento->first();
        $user = null;
        $inscId = null;
        $typeSearch = $request->get("search-user-type");
        $input = $request->get("search-user-input");

        switch ($typeSearch) {
            case "email" :
                $user = $this->searchUserByEmail($evento, $input);
                break;
            case "inscid" :
                $user = $this->searchUserByInscId($evento, $input);
                break;
            case "userid" :
                $user = $this->searchUserById($evento, $input);
                break;
            case "nome" :
                $user = $this->searchUserByNome($evento, $input);
                break;
            case "ninsc":
            default:
                return response()->json(["msg" => __("messages.ErrorTryingLocate"), "status" => "error"],500);
        }

        return [$user,$evento];
    }

    function searchUser(Request $request){
        try {
            $user = $this->searchUserModel($request);
            $evento = $user[1];
            $user = $user[0];
        } catch (Exception $e) {
            return response()->json(["msg" => $e->getMessage(), "status" => "error"], 400);
        }
        $data = $user->paginate(4,["id", "name", "email","tel"]);
        $pages = [
            "current"=>$data->currentPage(),
            "last"=>$data->lastPage(),
            "urls"=>$data->getUrlRange(1,$data->lastPage())
        ];
        return response()->json(["user"=>$this->getUserAttributes($data, $evento->id),"pages"=>$pages]);
    }

}