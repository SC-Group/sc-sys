<?php


namespace App\Http\Controllers;

use App\Models\Card;
use App\Models\Evento;
use Illuminate\Http\Request;
use Validator;

class SearchNaoInscritoController extends Controller
{

    private function searchCardRegistry($code,$evento){
        $card = $evento->cards()->where("code",$code);
        if(!$card->exists()){
            $card = $evento->cards()->create([
                'code' => $code,
            ]);
            return $card;
        }
        return $card->first();
    }



    function searchCard(Request $request){
        $validator = Validator::make($request->all(), [
            'search-user-input'=>'required',
            'year' => 'required|digits:4',
        ],
        [
            'search-user-input.required'=>__("messages.CodeCardNeeded"),
            'year.required'=>__("messages.EventYearRequired"),
        ]);
        if($validator->fails()){
            return response()->json(["msg"=>$validator->messages(),"status"=>"error"],400);
        }
        $evento = Evento::where("ano", $request->get("year"));
        if (!$evento->exists())
            return response()->json(["msg" => __("messages.EventDoesntExists"), "status" => "error"], 500);
        $evento = $evento->first();
        $input = $request->get("search-user-input");

        $card = $this->searchCardRegistry($input,$evento);

        $data=[];
        $data["id"] = $card->id;
        $data["name"] = "Não inscrito";
        $data["email"] = "Sem Email";
        $data["inscId"] = $card->code;
        $data["tipo"] = "card";

        $pages = [
            "current"=>1,
            "last"=>1
        ];
        return response()->json(["user"=>[$data],"pages"=>$pages]);
    }

}