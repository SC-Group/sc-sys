<?php


namespace App\Http\Controllers\Comissao\Atividades;


use App\Http\Controllers\Controller;
use App\Http\Services\SCService as SC;
use App\Models\Atividade;
use App\Models\Cronograma;
use App\Models\Evento;
use App\Models\Local;
use App\Models\Trilha;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Array_;
use phpDocumentor\Reflection\Types\Boolean;
use Validator;

class AtividadesController extends Controller
{
    public function getAtividadesConfig($year){
        $evento = Evento::where("ano",$year);
        if($evento->exists()){
            return SC::view("managepages.atividades.main",["year"=>$year]);
        }
        return back()->with(["msg"=>__("messages.EventDoesntExists"),'status'=>'error'],400);
    }

    public function getAtividades($year){
        $evento = Evento::where("ano",$year);
        if(!$evento->exists())
            return response()->json(["msg"=>__("messages.EventDoesntExists"),'status'=>'error'],400);
        $evento = $evento->first();
        $atividades = $evento->atividades()->where("especial", false);
        if(!$atividades->exists())
            return response()->json(["msg"=>__("messages.NoSubmittedActivity",["year"=>$year]),'status'=>'error'],400);
        $atividades = $atividades->with(['trilha:id,nome,cor',"palestrantes.disponibilidades", "palestrantes.user"])
            ->paginate(20,['id', 'nome', 'descricao', 'tipo', 'precisainscrever', 'gera_certificado','aprovada', 'trilha_id', 'codigo', 'organizacao_vinculada', 'recursos', "capacidade"]);

        return response()->json($atividades);
    }

    public function  getTrilhas($year){
        $evento = Evento::where("ano",$year);
        if(!$evento->exists())
            return response()->json(["msg"=>__("messages.EventDoesntExists"),'status'=>'error'],400);
        $evento = $evento->first();

        $trilhas = $evento->trilhas();
        if(!$trilhas->exists())
            return response()->json(["msg"=>__("messages.NoTrailInEvent"), "status"=>"error"],400);

        $trilhas = $trilhas->get(['id',"nome","cor"]);
        return response()->json($trilhas);

    }

    function getDisponibilidades($year){
        $evento = Evento::where("ano",$year);
        if(!$evento->exists())
            return response()->json(["msg"=>__("messages.EventDoesntExists"),'status'=>'error'],400);
        $evento = $evento->first();

        $dias = $evento->dias();
        $horas = $evento->horas();

        if(!$dias->exists())
            return response()->json(["msg"=>__("messages.NoDaysInEvent"), "status"=>"error"],400);
        if(!$horas->exists())
            return response()->json(["msg"=>__("messages.NoHoursInEvent"), "status"=>"error"],400);

        $disponibilidades["dias"] = $dias->get();
        $disponibilidades["horas"] = $horas->get();
        $disponibilidades["disabled"] = Cronograma::whereHas('dia', function (Builder $query) use(&$evento){
            $query->where('evento_id', $evento->id);
        })->where("atividade_id","<>",null)->get();



        return response()->json($disponibilidades);
    }


    public function saveAtividade(Request $request, $year){

        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:atividades,id|max:255',
            'nome' => 'required|string|max:100',
            'descricao' => 'required|string|max:400',
            'recursos' => 'nullable|string|max:400',
            'tipo' => 'required|in:Palestra,Minicurso,Workshop',
            'aprovacao' => 'boolean',
            'trilha' => 'present|nullable|exists:trilhas,id',
            'certificado' => 'required|boolean',
            'inscricao' => 'required|boolean',
            'year' => 'required|digits:4',
            "capacidade"=>"required | numeric"
        ]);

        if ($validator->fails()) {
            return response()->json(["status"=>"error","msg"=>__("messages.ValidationErrors"),"errors"=>$validator->messages()],400);
        }
        $evento=Evento::where("ano",$request->get("year"));

        if(!$evento->exists()){
            return response()->json(["status"=>"error","msg"=>__("messages.EventNotFound")]);
        }
        $evento = $evento->first();

        $atividade = $evento->atividades()->find(intval($request->get("id")));

        if($atividade==null){
            return response()->json(["msg"=>__("messages.ActivityDoesntExistsOnEvent",["year"=>$year]),"status"=>"error"],400);
        }

        $local = null;
        $trilha = null;

        $formLocal = $request->get("local");
        $formTrilha = $request->get("trilha");
        
        if($formTrilha!==null){
            $trilha = $evento->trilhas()->find(intval($formTrilha));
            if($trilha==null){
                return response()->json(["status"=>"error","msg"=>__("messages.TrailDoesntExistsOnEvent")],400);
            }
        }

        $cronogramasDays = json_decode($request->get("cronogramas_selecionados"));


        $atividade->nome = $request->get("nome");
        $atividade->descricao = $request->get("descricao");
        $atividade->recursos = $request->get("recursos");
        $atividade->tipo = $request->get("tipo");
        $atividade->aprovada = boolval($request->get("aprovacao"));
        $atividade->trilha()->associate($trilha);
        $atividade->gera_certificado = boolval($request->get("certificado"));
        $atividade->precisainscrever = boolval($request->get("inscricao"));
        $atividade->capacidade = intval($request->get("capacidade"));
        $atividade->save();


        return response()->json(["msg"=>"Sucesso","status"=>"success"]);
    }

}