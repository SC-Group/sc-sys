<?php


namespace App\Http\Controllers\Comissao\Apoios;


use App\Exceptions\NotImplementedException;
use App\Http\Services\SCService as SC;
use App\Models\Evento;
use App\Models\Apoio;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Validator;

class ApoiosController
{
    public function getApoiosConfig($year){
        $evento = Evento::where("ano",$year);
        $apoios = Apoio::all();

        if($evento->exists()){
            $evento=$evento->first();
            $escolhidos = $evento->apoios()->get();

            return SC::view("managepages.apoios.main",["year"=>$year,"apoios"=>$apoios,"apoiosEscolhidos"=>$escolhidos]);
        }
        return back()->with(["msg"=>__("messages.EventDoesntExists"),'status'=>'error'],400);
    }

    public function getOrganizacaoDataEdit(Request $request){
        $id=$request->get("id");
        $apoio = Apoio::find($id);
        if($apoio==null){
            return response()->json(["msg"=>__("messages.ErrorTryingLocate"),"status"=>"error"],400);
        }
        return response()->json(["data"=>$apoio]);
    }

    public function getApoioDataEdit(Request $request){
        $year = $request->get("year");
        $evento = Evento::where("ano",$year);
        if(!$evento->exists()){
            return response()->json(["msg"=>__("messages.EventNotFound"),"status"=>"error"],400);
        }
        $evento=$evento->first();
        $id=$request->get("id");
        $apoio = $evento->apoios()->find($id);
        if($apoio==null){
            return response()->json(["msg"=>__("messages.ErrorTryingLocate"),"status"=>"error"],400);
        }

        return response()->json(["data"=>$apoio]);
    }


    public function saveFile(string $directory, Apoio $apoio, Request $request){
        $storage = Storage::disk('uploadimage');
        if($storage->exists($directory)) {
            $url_atual = $apoio->logo_url;
            if($url_atual!=null){
                $url_atual = str_replace("/images/EventsImages","",$url_atual);
                $storage->delete($url_atual);
            }
        }else{
            $storage->makeDirectory($directory, 0775, true);
        }

        $imgFile = $request->file("organizacaoLogoFile");
        $pathFileSave= $storage->putFile($directory,$imgFile);
        $url = $storage->url($pathFileSave);
        $apoio->logo_url = $url;
        return $url;
    }


    public function saveOrganizacaoDataEdit(Request $request){
        $validator = Validator::make($request->all(),[
            "tipoCadastro"=>"required | in:novo,edicao",
            "organizacaoId"=>"required_if:tipoCadastro,edicao | nullable | exists:apoios,id",
            "organizacaoNome"=>"required | max:255",
            "organizacaoLogoFile"=>"required_if:tipoCadastro,novo | nullable | file | image",
            "organizacaoResponsavel"=>"required | max:255",
            "organizacaoTelefone"=>"max:13",
            "organizacaoEmail"=>"required | email",
            "organizacaoCargo"=>"nullable | max:255",
            "organizacaoSite"=>"nullable | url",
        ]);


        $logoFails = ($request->get("organizacaoId")===null && !$request->hasFile("organizacaoLogoFile"));


        if($validator->fails() || $logoFails){
            $msg = $validator->messages();
            if($logoFails) {
                $msg->add("organizacaoLogo", __("validation.required", ["attribute" => "organizacao logo file"]));
            }
            $msg->add("modalopen",true);
            return back()->withInput()->withErrors($msg);
        }

        if($request->get("organizacaoId")!==null){
            $organizacao = Apoio::find($request->get("organizacaoId"));
        }else{
            $organizacao = new Apoio();
        }


        if($request->hasFile("organizacaoLogoFile")){
           $this->saveFile("apoios",$organizacao,$request);
        }

        $organizacao->nome = $request->get("organizacaoNome");
        $organizacao->responsavel = $request->get("organizacaoResponsavel");
        $organizacao->tel_responsavel = $request->get("organizacaoTelefone");
        $organizacao->email_responsavel = $request->get("organizacaoEmail");
        $organizacao->cargo_responsavel = $request->get("organizacaoCargo");
        $organizacao->site = $request->get("organizacaoSite");

        $organizacao->save();

        return back()->with(["msg"=>__("messages.Success")]);

    }

    public function changeStatusApoio(Request $request){
        $validation = Validator::make($request->all(),[
            "year"=>"required",
            "sendForm"=>"required",
            "selecionado"=>"required | min:1",
        ]);



        if($validation->fails()){

           return back()->withInput()->withErrors($validation->messages());
        }

        $evento = Evento::where("ano",$request->get("year"));
        if(!$evento->exists()){
            return back()->withErrors(["msg"=>__("messages.EventNotFound"),"status"=>"error"]);
        }
        $evento = $evento->first();

        foreach ($request->get("selecionado") as $id=>$value){
            if(boolval($value)){
                $evento->apoios()->detach($id);
            }

        }

        return back()->with(["msg"=>__("messages.Success")]);
    }

    private function adicionarAEvento(Request $request){
        $year = $request->get("year");
        $evento = Evento::where("ano",$year);
        if(!$evento->exists()){
            throw new \Exception(__("messages.EventNotFound"));
        }
        $evento=$evento->first();


        $selecionados = $request->get("selecionado");
        $errorFindingSupport = false;
        foreach ($selecionados as $id => $status){
            if(boolval($status)===true){
                $apoio = Apoio::find($id);
                if($apoio!=null && !$evento->apoios->contains($apoio->id)){
                    $evento->apoios()->attach($apoio->id);
                }else{
                    $errorFindingSupport=true;
                }
            }
        }
        if($errorFindingSupport){
            throw new \Exception(__("messages.SomeErrorsOnSaving"));
        }
    }

    private function excluirOrganizacao(Request $request){

        $selecionados = $request->get("selecionado");
        $supports = [];
        foreach ($selecionados as $id => $status){
            if($status!=null){
                array_push($supports,$id);
            }
        }
        if(sizeof($supports)>0){
            Apoio::destroy($supports);
        }else{
            throw new \Exception(__("messages.SomeErrorsOnSaving"));
        }

    }

    public function changeStatusOrganizacao(Request $request){
        $validation = Validator::make($request->all(),[
            "year"=>"required",
            "sendForm"=>"required",
            "selecionado"=>"required | min:1",
            //"nivelId"=>"required"
        ]);

        if($validation->fails()){
            return back()->withInput()->withErrors($validation->messages());
        }

        $actionType = $request->get("sendForm");
        try {
            switch ($actionType) {
                case "adicionar":
                    $this->adicionarAEvento($request);
                    break;
                case "excluir":
                    $this->excluirOrganizacao($request);
                    break;
            }
        }catch (\Exception $e){
            return back()->with(["msg"=>$e->getMessage(), "status"=>"error"]);
        }

        return back()->with(["msg"=>__("messages.Success")]);
    }




}