<?php

namespace App\Http\Controllers\Comissao\Staffs;

use App\Http\Services\SCService;
use App\Models\Comissao;
use App\Models\Evento;
use App\Models\Staff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class StaffsController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth');
    }

    public function getStaffsConfig($year){
        $evento = Evento::where("ano",$year);
        if(!$evento->exists()){
            return redirect("/")->with(["msg"=>__("messages.EventNotFound"), "status"=>"error"]);
        }
        $evento = $evento->first();
        $staffs = $evento->staffs()->orderBy("status","asc")->orderBy("comissao_id","asc")->get();
        $dias = $evento->dias()->get(["id","data"]);
        $horas = $evento->horas()->get(["id","inicio","fim"]);
        $comissoes = Comissao::get(["nome","id"]);
        return SCService::view("managepages.staffs.main",["year"=>$year, "staffs"=>$staffs, "comissoes"=>$comissoes,"dias"=>$dias,"horas"=>$horas]);
    }

    public function getStaffData(Request $request){
        $staff = $request->get("staff_id");
        $staff = Staff::find($staff)->with(["aluno.user","comissao", "aluno.curso", "disponibilidades:id,staff_id,dia_id,hora_id"]);
        return $staff->first();
    }

    public function saveStaff(Request $request){
        $validation = Validator::make($request->all(),[
            "staffid"=>"required | exists:staffs,id",
           "status"=>"nullable | in:Em processo,Aprovado,Dispensado",
           "comissao"=>"nullable | exists:comissaos,id"
        ]);
        if($validation->fails()){
            return back()->with(["msg"=>$validation->messages()->first(), "status"=>"error"]);
        }
        $staffId = $request->get("staffid");
        $status = $request->get("status");
        $comissaoId=$request->get("comissao");
        $staff = Staff::find($staffId);
        $staff->status = $status;
        $staff->comissao_id = $comissaoId;
        $staff->save();
        return back()->with(["msg"=>"Sucesso", "status"=>"success"]);
    }

}
