<?php
/**
 * Created by PhpStorm.
 * User: filipe
 * Date: 01/03/18
 * Time: 18:09
 */

namespace App\Http\Controllers\Comissao\Evento;
use App\Http\Controllers\Controller;
use App\Models\Evento;
use App\Models\Egg;
use Illuminate\Http\Request;
use App\Helpers\IdHelper;
use App\Http\Services\SCService as SC;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Validator;

class EggsController extends Controller{
    public function getEggsConfig($year){
        $event = Evento::where("ano","=",$year);
        if($event->exists()) {
            $event = $event->first();
            $eggs = $event->eggs()->get();
            $view = SC::view("managepages.evento.configeggs", ["year" => $year, "eggs" => $eggs]);
            return response()->json(["view" => $view->content()]);
        }
        return response()->json(["msg"=>__("messages.EventUndefined"),'status'=>'info']);
    }

    public function deleteEggs($deleted){
        $errors=[];
        $deletados=0;
        if(count($deleted)>0){
            $toBeDeleted=[];
            foreach ($deleted as $key=>$elm) {
                try {
                    if (strpos($key, "new") === false) {
                        $eggId = IdHelper::dec($elm)[0];
                        array_push($toBeDeleted, $eggId);
                        $deletados++;
                    } else {
                        throw new Exception(__("messages.CantDeleteIfNotCreated"),333);
                    }
                }catch (\Exception $e){

                    if($e->getCode()===333){
                        $errors[$key]=$e->getMessage();
                    }else{
                        $errors[$key]=__("messages.UnexpectedError").__("messages.ReloadPage");
                    }
                }
            }
            Egg::destroy($toBeDeleted);
        }
        return ["regDeletados"=>$deletados,"errors"=>$errors];
    }


    public function saveFile(string $directory,$egg, $imgFile){
        $storage = Storage::disk('local');
        if($storage->exists($directory)) {
            if($egg!=null){
                $url_atual = $egg->url_img;
                if($url_atual!=null){
                    $storage->delete($url_atual);
                }
            }
        }else{
            $storage->makeDirectory($directory, 0775, true);
        }

        if($imgFile==null && $egg->url_img==null){
            throw new \Exception(__("messages.EditWithNoImage"));
        }

        if($imgFile!=null) {
            $urlFile = $directory . "/" . uniqid(rand(), true) . ".dat";
            $data = "data:" . $imgFile->getMimeType() . ";base64," . base64_encode(file_get_contents($imgFile->getRealPath()));
            $pathFileSave = $storage->put($urlFile, $data);
            $storage->url($pathFileSave);
            return $urlFile;
        }
        return null;
    }

    public function addEggs($evento,$egg){
        $year = $evento->ano;
        if($egg["cod"]==='') throw new \Exception(__("messages.CodeCantBeEmpty"));
        $cod = $egg["cod"];
        $fileUrl = $this->saveFile($year."/eggs",null,$egg["img"]);

        $egg = $evento->eggs()->create([
            "cod"=>$cod,
            "premio"=>$egg["premio"],
            "url_img"=>$fileUrl
        ]);

        return IdHelper::enc([$egg->id]);
    }

    public function updateEggs($evento,$eggId,$egg){
        $eggDB = $evento->eggs()->find($eggId);
        $alterou = false;
        if($egg["cod"]==='') throw new \Exception(__("messages.CodeCantBeEmpty"));
        if($eggDB!==null){
            if($eggDB->cod != $egg["cod"]){
                $eggDB->cod=$egg["cod"];
                $alterou=true;
            }
            if($eggDB->premio!=$egg["premio"]){
                $eggDB->premio=$egg["premio"];
                $alterou=true;
            }
            if(isset($egg["img"])&&$egg["img"]!==null){
                $fileUrl = $this->saveFile($evento->ano."/eggs",$eggDB,$egg["img"]);
                $eggDB->url_img=$fileUrl;
                $alterou=true;
            }
        }
        if($alterou) $eggDB->save();
        return $alterou;
    }

    public function saveEggs($evento,$eggs){
        $regCriados=0;
        $regAtualizados=0;
        $regNewIds = [];
        $errors = [];
        foreach ($eggs as $key=>$egg){
            if(strpos($key,"new")!==false){
                try{
                    $id = $this->addEggs($evento,$egg);
                    $regNewIds[$key]=$id;
                    $regCriados++;
                }catch (\Exception $e){
                    throw $e;
                    $errors[$key]=__("messages.CantAdd");
                }
            }else{
                $eggId = IdHelper::dec($key)[0];
                try{
                    $alterou = $this->updateEggs($evento,$eggId,$egg);
                    if($alterou)$regAtualizados++;
                }catch (\Exception $e){
                    throw $e;
                    $errors[$key]=__("messages.CantUpdate");
                }
            }
        }
        return ["regNewIds"=>$regNewIds,"regCriados"=>$regCriados,"regAtualizados"=>$regAtualizados,"errors"=>$errors];
    }

    public function saveEggsConfig(Request $request){

        $validator = Validator::make($request->all(),[
            "year"=>"required|numeric",
            "egg"=>"required_without:deleted",
            "deleted"=>"required_without:egg",
        ],[
            "year.required"=>__("messages.EventYearRequired"),
            "year.numeric"=>__("messages.EventYearNumeric"),
            "egg.required_without"=>__("messages.NothingTODO"),
            "deleted.required_without"=>__("messages.NothingTODO"),
        ]);

        if($validator->fails()){
            return response()->json([
                "msg"=>$validator->errors(),
                "status"=>"error"
            ],400);
        }else{
            $year = $request->get("year");
            $deleted = json_decode($request->get("deleted"));
            $evento = Evento::where("ano",$year)->first();
            $eggs = $request->all("egg")["egg"];
            $deletedErrors=[];
            $savedErrors=[];
            $newIds = [];

            if(count($deleted)==0&&!isset($eggs)){
                return response()->json(["msg"=>__("messages.NothingTODO"),"status"=>"info"]);
            }

            if(isset($deleted)&&count($deleted)>0) {
                $delResp = $this->deleteEggs($deleted);
                $deletedErrors = $delResp["errors"];
            }
            if(isset($eggs)&&count($eggs)>0){
                $saveResp = $this->saveEggs($evento,$eggs);
                $savedErrors = $saveResp["errors"];
                $newIds = $saveResp["regNewIds"];
            }

            if(count($deletedErrors)>0||count($savedErrors)>0){
                $msg=__("messages.SomeErrorsOnSaving");
                $msgStatus="warning";
            }else{
                $msg="Sucesso!";
                $msgStatus="success";
            }

            if(isset($delResp) && isset($saveResp)&& ($delResp["regDeletados"]>0 ||
                $saveResp["regCriados"]>0 ||
                $saveResp["regAtualizados"]>0)
            ){
                $msg.="\n";
            }

            if(isset($delResp)&&$delResp["regDeletados"]>0){
                $msg.=__("messages.DeletedRegisters").$delResp["regDeletados"];
            }

            if(isset($saveResp)&&$saveResp["regCriados"]>0){
                $msg.=__("messages.AddedRegisters").$saveResp["regCriados"];
            }

            if(isset($saveResp)&&$saveResp["regAtualizados"]>0){
                $msg.=__("messages.UpdatedRegisters").$saveResp["regAtualizados"];
            }


            return response()->json([
                "ids"=>$newIds,
                "msg"=>$msg,"status"=>$msgStatus,
                "deletedErrors"=>$deletedErrors,
                "savedErrors"=>$savedErrors
            ]);

        }
    }
}