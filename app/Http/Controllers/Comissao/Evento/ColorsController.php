<?php
/**
 * Created by PhpStorm.
 * User: filipe
 * Date: 01/03/18
 * Time: 17:58
 */

namespace App\Http\Controllers\Comissao\Evento;
use App\Http\Controllers\Controller;
use Faker\Provider\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;
use App\Models\Evento;
use App\Http\Services\SCService as SC;
use Validator;
class ColorsController extends Controller
{
    /*******************************Cores********************************/
    public function getColorsConfig($year){

        $event = Evento::where("ano","=",$year);
        if($event->exists()){
            $event = $event->first();
            if($event->url_img_logo!=null&&$event->url_img_bg!==null){
                $view = SC::view("managepages.evento.configcolor",["year"=>$year]);
                return response()->json(["view"=>$view->content()]);
            }
            return response()->json(["msg"=>__("messages.NoLogoAndBg"),"status"=>"info"],500);
        }
        return response()->json(["msg"=>__("messages.EventUndefined"),'status'=>'error']);
    }
    public function setPallete($colors,$evento,$fileUrl=null){
        $evento->primary_color=json_encode($colors["primary"]);
        $evento->secondary_color=json_encode($colors["secondary"]);
        $evento->stress_color=json_encode($colors["stress"]);
        $evento->bg1_color=json_encode($colors["bg1"]);
        $evento->bg2_color=json_encode($colors["bg2"]);
        $evento->save();
        if($fileUrl!=null){
            return response()->json(["msg"=>__("messages.SavedWithSuccess"),"colors"=>$colors,"css"=>$fileUrl,'status'=>'success']);
        }else{
            return response()->json(["msg"=>__("messages.SavedWithSuccess"),"colors"=>$colors,'status'=>'success']);
        }
    }
    public function getPallete(Request $request){
        $year = $request->get("year");
        $evento = Evento::where("ano","=",$year);
        $colors=[];
        $colors["primary"] = json_decode($evento->primary_color);
        $colors["secondary"] = json_decode($evento->secondary_color);
        $colors["stress"] = json_decode($evento->stress_color);
        $colors["bg1"] = json_decode($evento->bg1_color);
        $colors["bg2"] = json_decode($evento->bg2_color);
        return response()->json(["colors"=>$colors,'status'=>'success']);
    }
    public function setCssColors(Request $request){
        $year = $request->get("year");
        $colors = $request->get("colors");
        $evento = Evento::where("ano","=",$year)->first();
        if(empty($colors)) return response()->json(["error"=>__("messages.NoColorsToSave")],500);
        if(empty($evento)) return response()->json(["error"=>__("messages.EventNotFound")],404);

        $f = new Filesystem();
        $rootPath = "css/EventsColors/";
        $sysRootPath = public_path($rootPath);

        if($f->exists($sysRootPath.$year)){
            $success = $f->cleanDirectory($sysRootPath.$year);
            if(!$success){
                return response()->json(["error"=>__("messages.CantUpdateColors")],500);
            }
            $evento->css_url=null;
        }

        $fileName = uniqid().".css";
        $content=view("layouts.cores",["colors"=>$colors]);

        if(Storage::disk('uploadcss')->put($year."/".$fileName,$content)){
            $evento->css_url=$rootPath.$year."/".$fileName;
            return $this->setPallete($colors,$evento,$rootPath.$year."/".$fileName);
        }

        return response()->json(["error"=>__("messages.CantUpdateColors")],500);
    }
}