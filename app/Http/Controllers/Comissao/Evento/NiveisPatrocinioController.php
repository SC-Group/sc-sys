<?php


namespace App\Http\Controllers\Comissao\Evento;


use App\Helpers\IdHelper;
use App\Http\Services\SCService as SC;
use App\Models\Evento;
use App\Models\Local;
use App\Models\NivelPatrocinio;
use Illuminate\Http\Request;
use \Exception;
use Validator;

class NiveisPatrocinioController
{

    public function getConfig($year)
    {
        $view = SC::view('managepages.evento.configniveispatrocinio', ["year" => $year, "niveispatrocinio" => NivelPatrocinio::all()]);
        return response()->json(["view" => $view->content()]);
    }

    public function deleteNiveisPatrocinio($deleted)
    {
        $errors = [];
        $deletados = 0;
        if (count($deleted) > 0) {
            $toBeDeleted = [];
            foreach ($deleted as $key => $elm) {
                try {
                    if (strpos($key, "new") === false) {
                        $nivelpatrocinioId = IdHelper::dec($elm)[0];
                        array_push($toBeDeleted, $nivelpatrocinioId);
                        $deletados++;
                    } else {
                        throw new Exception(__("messages.CantDeleteIfNotCreated"), 333);
                    }
                } catch (Exception $e) {
                    if ($e->getCode() === 333) {
                        $errors[$key] = $e->getMessage();
                    } else {
                        $errors[$key] = __("messages.UnexpectedError") . __("messages.ReloadPage");
                    }
                }
            }
            NivelPatrocinio::destroy($toBeDeleted);
        }
        return ["regDeletados" => $deletados, "errors" => $errors];
    }


    public function addNiveisPatrocinio($evento, $nivelpatrocinio)
    {
        $nivelpatrocinio = $evento->niveisPatrocinio()->create([
            "nome" => $nivelpatrocinio["nome"],
            "nivel" => $nivelpatrocinio["nivel"]
        ]);
        return IdHelper::enc([$nivelpatrocinio->id]);
    }


    public function updateNivelPatrocinio($evento, $nivelpatrocinioId, $nivelpatrocinio)
    {
        $nivelpatrocinioDB = $evento->niveisPatrocinio()->find($nivelpatrocinioId);
        $alterou = false;
        if ($nivelpatrocinioDB !== null) {
            if ($nivelpatrocinioDB->nome != $nivelpatrocinio["nome"]) {
                $nivelpatrocinioDB->nome = $nivelpatrocinio["nome"];
                $alterou = true;
            }
            if ($nivelpatrocinioDB->nivel != $nivelpatrocinio["nivel"]) {
                $nivelpatrocinioDB->nivel = $nivelpatrocinio["nivel"];
                $alterou = true;
            }
        }
        if ($alterou) $nivelpatrocinioDB->save();
        return $alterou;
    }

    public function saveNiveisPatrocinio($evento, $niveispatrocinio)
    {
        $regCriados = 0;
        $regAtualizados = 0;
        $regNewIds = [];
        $errors = [];
        foreach ($niveispatrocinio as $key => $nivelpatrocinio) {
            if (strpos($key, "new") !== false) {
                try {
                    $id = $this->addNiveisPatrocinio($evento, $nivelpatrocinio);
                    $regNewIds[$key] = $id;
                    $regCriados++;
                } catch (\Exception $e) {
                    $errors[$key] = __("messages.CantAdd");
                }
            } else {
                $nivelpatrocinioId = IdHelper::dec($key)[0];
                try {
                    $alterou = $this->updateNivelPatrocinio($evento, $nivelpatrocinioId, $nivelpatrocinio);
                    if ($alterou) $regAtualizados++;
                } catch (\Exception $e) {
                    $errors[$key] = __("messages.CantUpdate");
                }
            }
        }
        return ["regNewIds" => $regNewIds, "regCriados" => $regCriados, "regAtualizados" => $regAtualizados, "errors" => $errors];
    }


    public function saveConfig(Request $request)
    {

        $validator = Validator::make($request->all(), [
            "year" => "required|numeric",
            "nivelpatrocinio" => "required_without:deleted",
            "deleted" => "required_without:nivelpatrocinio"
        ], [
            "year.required" => __("messages.EventYearRequired"),
            "year.numeric" => __("messages.EventYearNumeric"),
            "nivelpatrocinio.required_without" => __("messages.NothingTODO"),
            "deleted.required_without" => __("messages.NothingTODO")
        ]);

        if ($validator->fails()) {
            return response()->json([
                "msg" => $validator->errors(),
                "status" => "error"
            ], 400);
        } else {
            $year = $request->get("year");
            $deleted = json_decode($request->get("deleted"));
            $evento = Evento::where("ano", $year)->first();
            $niveispatrocinio = $request->get("nivelpatrocinio");

            $deletedErrors = [];
            $savedErrors = [];
            $newIds = [];

            if (count($deleted) == 0 && !isset($niveispatrocinio)) {
                return response()->json(["msg" => __("messages.NothingTODO"), "status" => "info"]);
            }

            if (isset($deleted) && count($deleted) > 0) {
                $delResp = $this->deleteNiveisPatrocinio($deleted);
                $deletedErrors = $delResp["errors"];
            }

            if (isset($niveispatrocinio) && count($niveispatrocinio) > 0) {
                $saveResp = $this->saveNiveisPatrocinio($evento, $niveispatrocinio);
                $savedErrors = $saveResp["errors"];
                $newIds = $saveResp["regNewIds"];
            }


            if (count($deletedErrors) > 0 || count($savedErrors) > 0) {
                $msg = __("messages.SomeErrorsOnSaving");
                $msgStatus = "warning";
            } else {
                $msg = "Sucesso!";
                $msgStatus = "success";
            }

            if (isset($delResp) && isset($saveResp) && ($delResp["regDeletados"] > 0 ||
                    $saveResp["regCriados"] > 0 ||
                    $saveResp["regAtualizados"] > 0)
            ) {
                $msg .= "\n";
            }

            if (isset($delResp) && $delResp["regDeletados"] > 0) {
                $msg .= __("messages.DeletedRegisters") . $delResp["regDeletados"];
            }

            if (isset($saveResp) && $saveResp["regCriados"] > 0) {
                $msg .= __("messages.AddedRegisters") . $saveResp["regCriados"];
            }

            if (isset($saveResp) && $saveResp["regAtualizados"] > 0) {
                $msg .= __("messages.UpdatedRegisters") . $saveResp["regAtualizados"];
            }


            return response()->json([
                "ids" => $newIds,
                "msg" => $msg, "status" => $msgStatus,
                "deletedErrors" => $deletedErrors,
                "savedErrors" => $savedErrors
            ]);


        }
    }

}