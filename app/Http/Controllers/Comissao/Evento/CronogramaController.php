<?php
/**
 * Created by PhpStorm.
 * User: filipe
 * Date: 01/03/18
 * Time: 17:56
 */

namespace App\Http\Controllers\Comissao\Evento;
use App\Models\Atividade;
use App\Models\Camisa;
use App\Models\Cronograma;
use App\Models\Dia;
use App\Models\Evento;
use App\Models\Hora;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\IdHelper;
use App\Http\Services\SCService as SC;
use Validator;
class CronogramaController extends Controller
{

    public function getConfig($year){
        $evento = Evento::where("ano",$year);
        if($evento->exists()){
            $evento = $evento->first();
            $evtInit = \Carbon\Carbon::create($evento->inicio);
            $evtEnd = \Carbon\Carbon::create($evento->fim);
            $evtInitTime = \Carbon\Carbon::createFromFormat('H:i:s',$evtInit->format('H:i:s'));
            $evtEndTime = \Carbon\Carbon::createFromFormat('H:i:s',$evtEnd->format('H:i:s'));
            $period =  \Carbon\CarbonPeriod::create($evento->inicio,$evento->fim);
            $period->filter(function ($date){return !$date->isWeekend();});

            $view = SC::view("managepages.evento.configcronograma",[
                "evtInit"=>$evtInit,
                "evtEnd"=>$evtEnd,
                "evtInitTime"=>$evtInitTime,
                "evtEndTime"=>$evtEndTime,
                "period"=>$period,
                "year"=>$year,
                "horas"=> $evento->horas()->get()
            ]);
            return response()->json(["view"=>$view->content()]);
        }
        return response()->json(["msg"=>__("messages.EventDoesntExists"),'status'=>'error'],500);
    }

    private function mergeLines($edited,$new){
        $array=[];

        if($new!=null && sizeof($new)>0){
            $array = array_merge($array, $new);
        }
        if($edited!=null && sizeof($edited)>0){
            $array = array_merge($array, $edited);
        }
        return $array;

    }

    private function deleteChronos(Evento $evento, Request $request){
        if($request->has("chronos-line.deleted")) {
            $lines = json_decode($request->input("chronos-line.deleted"));
            $decodedIds = [];
            foreach ($lines as $line) {
                $id = IdHelper::dec($line);
                if(sizeof($id)>0)
                    array_push($decodedIds,$id[0]);
            }

            try {
                Hora::destroy($decodedIds);
            }catch (\Exception $e){
                throw new \Exception(__("messages.ErrorTryingDelete"));
            }

        }
    }

    private function editChronos(Evento $evento, Request $request, $coffee, $almoco){

        if( $request->has("chronos-line.edit")){
            $linesEdit = $request->input("chronos-line.edit");
            $errors = [];
            foreach ($linesEdit as $encId=>$line){
                $id = IdHelper::dec($encId)[0];
                $hora = $evento->horas()->where("id",$id);
                if(!$hora->exists()){
                    array_push($errors,$encId);
                    continue;
                }
                $hora = $hora->first();
                $hora->inicio = $line["inicio"];
                $hora->fim = $line["fim"];
                $this->changeStatus($line, $evento, $hora,$coffee, $almoco);
                $hora->save();
            }
            return $errors;
        }

       return null;
    }

    private function createChronos(Evento $evento, Request $request, $coffee, $almoco){
        if($request->has("chronos-line.new")) {
            $linesEdit = $request->input("chronos-line.new");
            $newIds = [];
            foreach ($linesEdit as $index => $line) {
                $hora = $evento->horas()->firstOrCreate(["inicio" => $line["inicio"], "fim" => $line["fim"]]);
                $this->changeStatus($line, $evento, $hora, $coffee, $almoco);
                $hora->save();
                $newIds[$index] = IdHelper::enc([$hora->id]);
            }
            return $newIds;
        }
        return null;
    }

    private function changeStatus($line, Evento $evento, $hora, $coffee, $almoco){
        foreach ($line["status"] as $date=>$status){
            $dia = $evento->dias()->firstOrCreate(["data"=>$date]);
            if($status==="Atividade") {
                $hora->cronogramas()->where("dia_id", $dia->id)->whereHas('atividade', function (Builder $query) {
                    $query->where('tipo',"Alimentacao");
                })->delete();
            }elseif($status==="Coffee-Break"){
                $cronograma = $hora->cronogramas()->where("dia_id",$dia->id)->has("atividade");
                if($cronograma->exists()){
                    $cronograma = $cronograma->first();
                    $cronograma->atividade_id=$coffee->id;
                    $cronograma->save();
                }else{

                    $cronograma->create(["atividade_id"=>$coffee->id, "dia_id"=>$dia->id]);
                }

            }elseif($status==="Almoço"){
                $cronograma = $hora->cronogramas()->where("dia_id",$dia->id)->has("atividade");
                if($cronograma->exists()){
                    $cronograma = $cronograma->first();
                    $cronograma->atividade_id=$almoco->id;
                    $cronograma->save();
                }else{
                    $cronograma->create(["atividade_id"=>$almoco->id,  "dia_id"=>$dia->id]);
                }
            }
        }
    }

    private function saveChronos(Evento $evento, Request $request){

        if($request->has("chronos-line.new") || $request->has("chronos-line.edit")) {

            $coffee = $evento->atividades()->firstOrCreate(
                [ 'nome'=>'Coffee-Break', 'tipo'=>"Alimentacao", "especial"=>true],
                [ 'descricao' => "Coffee-break para os inscritos da Semana da Computação de $evento->ano"  ]
            );

            $almoco = $evento->atividades()->firstOrCreate(
                ['nome'=>'Almoço', 'tipo' => 'Alimentacao', "especial"=>true],
                ['descricao' => "Almoço para os inscritos da Semana da Computação de $evento->ano"]
            );

            $errors = $this->editChronos($evento,$request,$coffee,$almoco);
            $newIds = $this->createChronos($evento, $request, $coffee, $almoco);

            return ["newIds"=>$newIds, "errors"=>$errors];
        }
        return null;
    }

    private function startEqualsEnd(Request $request){
        $array=$this->mergeLines( $request->input("chronos-line.edit"), $request->input("chronos-line.new") );
        $result = [];
        foreach ($array as $i=>$ivalue){
           if($ivalue["inicio"] === $ivalue["fim"]){
               array_push($result,$i);
           }
        }

        return $result;
    }

    private function overlapsAny(Request $request){
        $array=$this->mergeLines( $request->input("chronos-line.edit"), $request->input("chronos-line.new") );
        foreach ($array as $i=>$ivalue){
            foreach ($array as $j=>$jvalue){
                if($j==$i)continue;

                $p1 = CarbonPeriod::create($ivalue["inicio"], $ivalue["fim"]);

                if($p1->overlaps($jvalue["inicio"], $jvalue["fim"])){
                    return [$i,$j];
                }

            }
        }

        return false;
    }

    private function overLimits(Request $request, Evento $event){

        $start = Carbon::create(Carbon::create($event->inicio)->format("H:i:s"));
        $end = Carbon::create(Carbon::create($event->fim)->format("H:i:s"));

        $array=$this->mergeLines( $request->input("chronos-line.edit"), $request->input("chronos-line.new") );

        foreach ($array as $i=>$ivalue){
            $t1 = Carbon::create($ivalue["inicio"]);
            $t2 = Carbon::create($ivalue["fim"]);
            $t1ok = $t1->isBetween($start,$end);
            $t2ok = $t2->isBetween($start,$end);
            if(!($t1ok && $t2ok)){
                return $i;
            }

        }

        return false;
    }

    public function sendConfig(Request $request){

        $validation = Validator::make($request->all(),
            [
                "chronos-line"=>"required",
                "chronos-line.*"=>"required",
                "year"=>"required"
            ],[
                "chronos-line.required"=>__("messages.NothingTODO"),
                "chronos-line.*.required"=>__("messages.NothingTODO"),
            ]);

        if($validation->fails()){
            return response()->json(["status"=>"error","msg"=>$validation->messages()->first()],400);
        }
        if(sizeof($this->startEqualsEnd($request))>0){
            return response()->json(["status"=>"error","msg"=>__("messages.TimesBeginAndEndAreEquals")],400);
        }
        $overlaps = $this->overlapsAny($request);
        if($overlaps!==false){
            return response()->json(["status"=>"error","msg"=>__("messages.TimesOverlapping"),"errors"=>$overlaps],400);
        }

        $evento = Evento::where("ano",$request->get("year"))->first();

        $overLimits = $this->overLimits($request,$evento);
        if($overLimits!==false){
            return response()->json(["status"=>"error","msg"=>__("messages.TimesOverLimits"), "errors"=>$overLimits]);
        }

        try {
            $this->deleteChronos($evento, $request);
            $saveRet = $this->saveChronos($evento, $request);
            return response()->json(["msg"=>__("messages.SavedWithSuccess"),"status"=>"success", "data"=>$saveRet]);
        }catch (\Exception $e){
            return response()->json([
                "msg"=>__("messages.InternalErrorOnSaving"),
                "exception"=>$e->getMessage(),
                "stackTrace"=>$e->getTrace(),
                "status"=>"error"],500);
        }

    }


}