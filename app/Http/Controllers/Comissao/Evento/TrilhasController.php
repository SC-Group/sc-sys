<?php
/**
 * Created by PhpStorm.
 * User: filipe
 * Date: 01/03/18
 * Time: 18:09
 */

namespace App\Http\Controllers\Comissao\Evento;
use App\Http\Controllers\Controller;
use App\Models\Evento;
use App\Models\Trilha;
use Illuminate\Http\Request;
use App\Helpers\IdHelper;
use App\Http\Services\SCService as SC;

class TrilhasController extends Controller{
    public function getTrilhasConfig($year){
        $event = Evento::where("ano","=",$year);
        if($event->exists()) {
            $event = $event->first();
            $trilhas = $event->trilhas()->get();
            $view = SC::view("managepages.evento.configtrilha", ["year" => $year, "trilhas" => $trilhas]);
            return response()->json(["view" => $view->content()]);
        }
        return response()->json(["msg"=>__("messages.EventUndefined"),'status'=>'info']);
    }

    public function saveTrilhasConfig(Request $request){
       
        $ano = $request->input("ano");
       
        $evento = Evento::where("ano",(int)$ano)->first();
        if(empty($evento))return response()->json(["msg"=>__("messages.EventDoesntExists"),'status'=>'error'],500);
        $txt="";
        $deletedTrilhas = collect($request->input("deletedTrilhasIds"));
        $trilhas = $request->input("trilhas");
        $changed=false;
       
        $sizeDeleted=$deletedTrilhas->count();
        $countCreated=0;
        $countDeleted=0;
        $countAltered=0;

        if($sizeDeleted>0){
            for($countDeleted=0;$countDeleted<$sizeDeleted;$countDeleted++)$deletedTrilhas[$countDeleted] = IdHelper::dec($deletedTrilhas[$countDeleted])[0];
            Trilha::destroy($deletedTrilhas->toArray());
        }
        if($countDeleted>0){
            $txt.=__("messages.DeletedRegisters").$countDeleted.";";
            $changed=true;
        }
        $validqtd=false;
        if(isset($trilhas)){
            foreach ($trilhas as $trilha=>$values){
                $values['cor']=json_encode($values['cor']);
                if(filled($values["nome"])){
                    if(starts_with($trilha,"new")){
                        $dbTrilha=$evento->trilhas()->where("nome","=",$values["nome"])->where("cor","=",$values["cor"])->first();
                        if(empty($dbTrilha)){
                            $dbTrilha = new Trilha([
                                "nome"=>$values["nome"],
                                "cor"=>$values["cor"],
                                "descricao"=>$values["desc"]
                            ]);
                            $evento->trilhas()->save($dbTrilha);
                            $changed=true;
                            $validName=true;
                            $countCreated++;
                            $trilhas[$trilha]=["newid"=>IdHelper::enc([$dbTrilha->id])];
                        }else{
                            $trilhas[$trilha]=["msg"=>__("messages.DuplicatedRegisters"),'status'=>'error'];
                        }
                    }else{
                        $changed2=false;
                        $dbTrilha2=$evento->trilhas()->where("nome","=",$values["nome"])->where("cor","=",$values["cor"])->first();
                        $dbTrilha = Trilha::findOrFail(IdHelper::dec($trilha)[0]);
                        if(($dbTrilha2==null)||($dbTrilha2!=null&&($dbTrilha2->id==$dbTrilha->id))){
                            $validName=true;
                            if($dbTrilha->nome != $values["nome"]){
                                $dbTrilha->nome = $values["nome"];
                                $changed2=true;
                            }
                            if($dbTrilha->cor != $values["cor"]){
                                $dbTrilha->cor=$values["cor"];
                                $changed2=true;
                            }
                            
                            if($dbTrilha->descricao != $values["desc"]){
                                $dbTrilha->descricao=$values["desc"];
                                $changed2=true;
                            }
                           
                            if($changed2){
                                $dbTrilha->save();
                                $countAltered++;
                                $trilhas[$trilha]=["newid"=>IdHelper::enc([$dbTrilha->id])];
                            }
                            
                        }else{
                            $validName=false;
                            $trilhas[$trilha]=["error"=>__("messages.DuplicatedRegisters"),'status'=>'error'];
                        }
                        
                    }
                }else{
                    $validName=false;
                    $trilhas[$trilha]=["error"=>__("messages.TrailNameCantBeEmpty")];
                }        
            }
            if($countCreated>0)$txt.="Registros criados:".$countCreated.";";
            if($countAltered>0)$txt.="Registros alterados:".$countAltered.";";
            if(($changed)||($countAltered>0)){
                return response()->json(["trilhas"=>$trilhas,'msg'=>__("messages.Success")." ".$txt,'status'=>'success']);
            }else{
                if($validName){
                    return response()->json(["trilhas"=>$trilhas,'msg'=>__("messages.NothingTODO"),'status'=>'warning']);
                }else{
                    return response()->json(["trilhas"=>$trilhas,'msg'=>__("messages.ErrorsInRows"),'status'=>'error']);
                }
            }
        }
        if(($changed)){
            return response()->json(["trilhas"=>null,'msg'=>__("messages.Success")." ".$txt,'status'=>'success']);
        }else{
            return response()->json(["trilhas"=>null,'msg'=>__("messages.NothingTODO"),'status'=>'warning']);
        }
    }
}