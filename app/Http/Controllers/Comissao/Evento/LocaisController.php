<?php
/**
 * Created by PhpStorm.
 * User: filipe
 * Date: 01/03/18
 * Time: 18:09
 */

namespace App\Http\Controllers\Comissao\Evento;
use App\Http\Controllers\Controller;
use App\Models\Evento;
use App\Models\Local;
use App\Models\Trilha;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Helpers\IdHelper;
use App\Http\Services\SCService as SC;
use mysql_xdevapi\Exception;
use Validator;

class LocaisController extends Controller{
    public function getLocaisConfig($year){
        $event = Evento::where("ano","=",$year);
        if($event->exists()) {
            $event = $event->first();
            $locais = $event->locais()->get();
            $view = SC::view("managepages.evento.configlocais", ["year" => $year, "locais" => $locais]);
            return response()->json(["view" => $view->content()]);
        }
        return response()->json(["msg"=>__("messages.EventUndefined"),'status'=>'info']);
    }

    public function deleteLocais($deleted){
        $errors=[];
        $deletados=0;
        if(count($deleted)>0){
            $toBeDeleted=[];
            foreach ($deleted as $key=>$elm) {
                try {
                    if (strpos($key, "new") === false) {
                        $localId = IdHelper::dec($elm)[0];
                        array_push($toBeDeleted, $localId);
                        $deletados++;
                    } else {
                        throw new Exception(__("messages.CantDeleteIfNotCreated"),333);
                    }
                }catch (\Exception $e){
                    if($e->getCode()===333){
                        $errors[$key]=$e->getMessage();
                    }else{
                        $errors[$key]=__("messages.UnexpectedError").__("messages.ReloadPage");
                    }
                }
            }
            Local::destroy($toBeDeleted);
        }
        return ["regDeletados"=>$deletados,"errors"=>$errors];
    }

    public function addLocais($evento,$local){
        $local = $evento->locais()->create([
            "nome"=>$local["nome"],
            "vagas"=>$local["vagas"],
            "descricao"=>$local["descricao"],
        ]);
        return IdHelper::enc([$local->id]);
    }

    public function updateLocais($evento,$localId,$local){
        $localDB = $evento->locais()->find($localId);
        $alterou = false;
        if($localDB!==null){
            if($localDB->nome!=$local["nome"]){
                $localDB->nome=$local["nome"];
                $alterou=true;
            }
            if($localDB->vagas!=$local["vagas"]){
                $localDB->vagas=$local["vagas"];
                $alterou=true;
            }
            if($localDB->descricao!=$local["descricao"]){
                $localDB->descricao=$local["descricao"];
                $alterou=true;
            }
        }
        if($alterou) $localDB->save();
        return $alterou;
    }

    public function saveLocais($evento,$locais){
        $regCriados=0;
        $regAtualizados=0;
        $regNewIds = [];
        $errors = [];
        foreach ($locais as $key=>$local){
            if(strpos($key,"new")!==false){
                try{
                    $id = $this->addLocais($evento,$local);
                    $regNewIds[$key]=$id;
                    $regCriados++;
                }catch (\Exception $e){
                    $errors[$key]=__("messages.CantAdd");
                }
            }else{
                $localId = IdHelper::dec($key)[0];
                try{
                    $alterou = $this->updateLocais($evento,$localId,$local);
                    if($alterou)$regAtualizados++;
                }catch (\Exception $e){
                    $errors[$key]=__("messages.CantUpdate");
                }
            }
        }
        return ["regNewIds"=>$regNewIds,"regCriados"=>$regCriados,"regAtualizados"=>$regAtualizados,"errors"=>$errors];
    }

    public function saveLocaisConfig(Request $request){

        $validator = Validator::make($request->all(),[
            "year"=>"required|numeric",
            "local"=>"required_without:deleted",
            "deleted"=>"required_without:local"
        ],[
            "year.required"=>__("messages.EventYearRequired"),
            "year.numeric"=>__("messages.EventYearNumeric"),
            "local.required_without"=>__("messages.NothingTODO"),
            "deleted.required_without"=>__("messages.NothingTODO")
        ]);

        if($validator->fails()){
            return response()->json([
                "msg"=>$validator->errors(),
                "status"=>"error"
            ],400);
        }else{
            $year = $request->get("year");
            $deleted = json_decode($request->get("deleted"));
            $evento = Evento::where("ano",$year)->first();
            $locais = $request->get("local");

            $deletedErrors=[];
            $savedErrors=[];
            $newIds = [];

            if(count($deleted)==0&&!isset($locais)){
                return response()->json(["msg"=>__("messages.NothingTODO"),"status"=>"info"]);
            }

            if(isset($deleted)&&count($deleted)>0) {
                $delResp = $this->deleteLocais($deleted);
                $deletedErrors = $delResp["errors"];
            }
            if(isset($locais)&&count($locais)>0){
                $saveResp = $this->saveLocais($evento,$locais);
                $savedErrors = $saveResp["errors"];
                $newIds = $saveResp["regNewIds"];
            }

            if(count($deletedErrors)>0||count($savedErrors)>0){
                $msg=__("messages.SomeErrorsOnSaving");
                $msgStatus="warning";
            }else{
                $msg="Sucesso!";
                $msgStatus="success";
            }

            if(isset($delResp) && isset($saveResp)&& ($delResp["regDeletados"]>0 ||
                $saveResp["regCriados"]>0 ||
                $saveResp["regAtualizados"]>0)
            ){
                $msg.="\n";
            }

            if(isset($delResp)&&$delResp["regDeletados"]>0){
                $msg.=__("messages.DeletedRegisters").$delResp["regDeletados"];
            }

            if(isset($saveResp)&&$saveResp["regCriados"]>0){
                $msg.=__("messages.AddedRegisters").$saveResp["regCriados"];
            }

            if(isset($saveResp)&&$saveResp["regAtualizados"]>0){
                $msg.=__("messages.UpdatedRegisters").$saveResp["regAtualizados"];
            }


            return response()->json([
                "ids"=>$newIds,
                "msg"=>$msg,"status"=>$msgStatus,
                "deletedErrors"=>$deletedErrors,
                "savedErrors"=>$savedErrors
            ]);

        }
    }
}