<?php
/**
 * Created by PhpStorm.
 * User: filipe
 * Date: 01/03/18
 * Time: 17:56
 */

namespace App\Http\Controllers\Comissao\Evento;
use App\Models\Camisa;
use App\Models\Evento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\IdHelper;
use App\Http\Services\SCService as SC;

class CamisasController extends Controller
{
    public function getCamisasConfig($year){
        $evento = Evento::where("ano","=",$year)->first();
        if($evento!==null){
            $pa=((float)$evento->preco_camisa_antes);
            $pd=((float)$evento->preco_camisa_depois);
            $restoantes = (($pa*100)%100);
            $restodepois = (($pd*100)%100);
            $ndigAntes = 2;//supoe num inteiro
            if(($restoantes!=0)){//se não numero inteiro
                $ndigAntes = 1;//supoe número com uma casa decimal
                $restoantes = $restoantes%10;
                if($restoantes!=0){//se numero com 2 casas decimais
                    $ndigAntes = 1;
                }
            }
            $ndigDepois = 2;//supoe num inteiro
            if(($restodepois!=0)){//se não numero inteiro
                $ndigDepois = 1;//supoe número com uma casa decimal
                $restodepois = $restodepois%10;
                if($restodepois!=0){//se numero com 2 casas decimais
                    $ndigDepois = 1;
                }
            }
            for($i=0;$i<$ndigAntes;$i++)$pa=$pa."0";
            for($i=0;$i<$ndigDepois;$i++)$pd=$pd."0";
            $camisas = $evento->camisas()->get();

            $view = SC::view("managepages.evento.configcamisa",["precoAntes"=>$pa,"precoDepois"=>$pd,"year"=>$year,"camisas"=>$camisas]);
            return response()->json(["view"=>$view->content()]);
        }
        return response()->json(["msg"=>__("messages.EventDoesntExists"),'status'=>'error'],500);
    }
    public function sendCamisasConfig(Request $request){
        $ano = $request->input("ano");
        $evento = Evento::where("ano",(int)$ano)->first();
        if(empty($evento))return response()->json(["msg"=>__("messages.EventDoesntExists"),'status'=>'error'],500);
        $txt="";
        $deletedRows = collect($request->input("deletedRowsIds"));
        $rows = $request->input("rows");
        $precos = $request->input("precos");
        $changed=false;
        if(isset($precos)){
            
            if($evento->preco_camisa_antes != $precos["antes"]){
                $evento->preco_camisa_antes = $precos["antes"];
                $changed=true;
            }
            if($evento->preco_camisa_depois != $precos["depois"]){
                $evento->preco_camisa_depois = $precos["depois"];
                $changed=true;
            }
            if($changed){
                $evento->save();
                $txt.="Preços alterados!";
            }
            
        }
        
        $sizeDeleted=$deletedRows->count();
        $countCreated=0;
        $countDeleted=0;
        $countAltered=0;

        if($sizeDeleted>0){
            for($countDeleted=0;$countDeleted<$sizeDeleted;$countDeleted++)$deletedRows[$countDeleted] = IdHelper::dec($deletedRows[$countDeleted])[0];
            Camisa::destroy($deletedRows);
        }
        if($countDeleted>0){
            $txt.=__("messages.DeletedRegisters").$countDeleted.";";
            $changed=true;
        }
        $validqtd=false;
        if(isset($rows)){

            foreach ($rows as $row=>$values){
                if($values["qtd"]>0){
                    if(starts_with($row,"new")){
                        $camisa=Camisa::where("tipo","=",$values["tipo"])
                            ->where("tamanho","=",$values["tam"])
                            ->where("cor","=",$values["cor"])
                            ->first();
                        if(empty($camisa)){
                            $camisa = new Camisa([
                                "tamanho"=>$values["tam"],
                                "tipo"=>$values["tipo"],
                                "qtd_total"=>$values["qtd"],
                                "cor"=>$values['cor']
                            ]);
                            $evento->camisas()->save($camisa);
                            $changed=true;
                            $validqtd=true;
                            $countCreated++;
                            $rows[$row]=["newid"=>IdHelper::enc([$camisa->id])];
                        }else{
                            $rows[$row]=["error"=>__("messages.DuplicatedRegisters")];
                        }
                    }else{
                        $changed2=false;
                        $camisa2=Camisa::where("tipo","=",$values["tipo"])
                            ->where("tamanho","=",$values["tam"])
                            ->where("cor","=",$values["cor"])
                            ->first();
                        $camisa = Camisa::findOrFail(IdHelper::dec($row)[0]);
                        if(($camisa2==null)||($camisa2!=null&&($camisa2->id==$camisa->id))){
                            $validqtd=true;
                            if($camisa->tamanho != $values["tam"]){
                                $camisa->tamanho = $values["tam"];
                                $changed2=true;
                            }
                            if($camisa->tipo!=$values["tipo"]){
                                $camisa->tipo=$values["tipo"];
                                $changed2=true;
                            }
                            if($camisa->qtd_total!=$values["qtd"]){
                                $camisa->qtd_total=$values["qtd"];
                                $changed2=true;
                            }

                            if($camisa->cor!=$values["cor"]){
                                $camisa->cor=$values["cor"];
                                $changed2=true;
                            }
                           
                            if($changed2){
                                $camisa->save();
                                $countAltered++;
                                $rows[$row]=["newid"=>IdHelper::enc([$camisa->id])];
                            }
                           
                        }else{
                            $validqtd=false;
                            $rows[$row]=["error"=>__("messages.DuplicatedRegisters")];
                        }
                        
                    }
                }else{
                    $validqtd=false;
                    $rows[$row]=["error"=>__("messages.NumberHasToBeGreaterThan",["value"=>0])];
                }        
            }
            if($countCreated>0)$txt.="Registros criados:".$countCreated.";";
            if($countAltered>0)$txt.="Registros alterados:".$countAltered.";";
            if(($changed)||($countAltered>0)){
                return response()->json(["rows"=>$rows,'msg'=>"sucesso! ".$txt,'status'=>'success']);
            }else{
                if($validqtd){
                    return response()->json(["rows"=>$rows,'msg'=>__("messages.NothingTODO"),'status'=>'warning']);
                }else{
                    return response()->json(["rows"=>$rows,'msg'=>__("messages.ErrorsInRows"),'status'=>'errorLog']);
                }
            }
        }
        if(($changed)){
            return response()->json(["rows"=>null,'msg'=>$txt,'status'=>'success']);
        }else{
            return response()->json(["rows"=>null,'msg'=>__("messages.NothingTODO"),'status'=>'warning']);
        }
    }
}