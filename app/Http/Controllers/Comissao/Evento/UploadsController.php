<?php
/**
 * Created by PhpStorm.
 * User: filipe
 * Date: 01/03/18
 * Time: 17:58
 */

namespace App\Http\Controllers\Comissao\Evento;
use App\Http\Controllers\Controller;
use Faker\Provider\File;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Models\Evento;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;
use App\Http\Services\SCService as SC;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Validator;

class UploadsController extends Controller
{
    /*************************Upload de Imagens****************************/
    public function getUploadsConfig($year){
        $event = Evento::where("ano","=",$year);
        if($event->exists()){
            $view = SC::view("managepages.evento.configuploads",["year"=>$year]);
            return response()->json(["view"=>$view->content()]);
        }
        return response()->json(["msg"=>__("messages.EventUndefined"),'status'=>'error']);
    }

    public function saveFile(string $directory,string $fileName,Evento $evento,Request $request){
        $storage = Storage::disk('uploadimage');
        $var_url_file = "url_img_".$fileName;
        if($storage->exists($directory)) {
            $url_atual = $evento->$var_url_file;
            if($url_atual!=null){
                $storage->delete($url_atual);
            }
        }else{
            $storage->makeDirectory($directory, 0775, true);
        }

        $imgFile = $request->file("file.".$fileName);
        $pathFileSave= $storage->putFile($directory,$imgFile);
        $url = $storage->url($pathFileSave);
        $evento->$var_url_file = $url;
        return $url;
    }

    public function sendFiles(Request $request){

        $validator = Validator::make($request->all(),[
            "file"=>"required",
            "file.*"=>"image",
            "year"=>"required|digits:4|numeric"
        ]);
       
        $fileNames=["logo","bg","comercial","camisas_masc","camisas_fem"];
        if($validator->fails()){
            return response()->json($validator->errors(),500);
        }
        $year = $request->get("year");
        $rootPath = $year."/main";
        $evento = Evento::where("ano","=",$year)->first();


        $msg = [];
        $hasfile = false;
        foreach ($fileNames as $name){
            if($request->hasFile("file.".$name)) {
                $hasfile=true;
                try {
                    $msg[$name] = $this->saveFile($rootPath, $name, $evento, $request);
                } catch (FileException $e) {
                    return response()->json(["msg" => $e->getMessage(), "status" => "error"], 500);
                }
            }
        }
        if($hasfile){
            $evento->save();
            return response()->json(['urls'=>$msg,'msg'=>__("messages.UploadedSuccessfully"),'status'=>'success']);
        }else{
            return response()->json(["msg" => __("messages.FilesCantMatchEvent"), "status" => "error"], 500);
        }

    }

}