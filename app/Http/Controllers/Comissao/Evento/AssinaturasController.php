<?php
/**
 * Created by PhpStorm.
 * User: filipe
 * Date: 01/03/18
 * Time: 17:58
 */

namespace App\Http\Controllers\Comissao\Evento;
use App\Http\Controllers\Controller;
use Faker\Provider\File;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Models\Evento;
use Illuminate\Support\Facades\Storage;
use \Illuminate\Contracts\Filesystem\Filesystem;
use App\Http\Services\SCService as SC;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Validator;

class AssinaturasController extends Controller
{
    private function loadImages(Filesystem $disk,$imagePath){
        if($disk->exists($imagePath)){
            return $disk->get($imagePath);
        }
        return null;
    }

    public function getAssinaturasConfig($year){
        $event = Evento::where("ano","=",$year);
        if($event->exists()){
            $event=$event->first();
            $signCoordenadorPath = $event->coordenador_sign;
            $coordenadorName = $event->coordenador_name;
            $signCoordenadorPos = $event->coordenador_sign_pos;

            $signChefePath = $event->chefe_dept_sign;
            $chefeName = $event->chefe_dept_name;
            $signChefePos = $event->chefe_dept_sign_pos;

            $localImagesDisk = Storage::disk("local");
            $signCoordenadorBase64 = $this->loadImages($localImagesDisk, $signCoordenadorPath);
            $signChefeBase64 = $this->loadImages($localImagesDisk, $signChefePath);

            $view = SC::view("managepages.evento.configassinaturas",[
                "year"=>$year,
                "assinaturas"=>[
                    "chefe"=>[
                        "sign"=>$signChefeBase64,
                        "name"=>$chefeName,
                        "pos"=>$signChefePos
                    ],
                    "coordenador"=>[
                        "sign"=>$signCoordenadorBase64,
                        "name"=>$coordenadorName,
                        "pos"=>$signCoordenadorPos
                    ]
                ]
            ]);
            return response()->json(["view"=>$view->content()]);
        }
        return response()->json(["msg"=>__("messages.EventUndefined"),'status'=>'error'],400);
    }




    public function saveFile($nome,$pos,string $directory,string $tablecol,Evento $evento, $imgFile){
        $storage = Storage::disk('local');
        if($storage->exists($directory)) {
            $url_atual = $evento->$tablecol;
            if($url_atual!=null){
                $storage->delete($url_atual);
            }
        }else{
            $storage->makeDirectory($directory, 0775, true);
        }

        $urlcol = $tablecol . "_sign";

        if($imgFile==null && $evento->$urlcol==null){
            throw new \Exception(__("messages.EditWithNoImage"));
        }

        if($imgFile!=null) {
            $data = "data:" . $imgFile->getMimeType() . ";base64," . base64_encode(file_get_contents($imgFile->getRealPath()));
            $pathFileSave = $storage->put($directory . "/" . $tablecol . ".dat", $data);
            $storage->url($pathFileSave);
            $evento->$urlcol = $directory . "/" . $tablecol . ".dat";
        }

        $namecol = $tablecol."_name";
        $poscol = $tablecol."_sign_pos";

        $evento->$namecol = $nome;
        $evento->$poscol = $pos;
    }

    public function saveAssinaturasConfig(Request $request){
        $validator = Validator::make($request->all(),[
            "coordenador.pos"=>"numeric|required_with:coordenador.sign",
            "chefe.pos"=>"numeric|required_with:chefe.sign",
            "coordenador.name"=>"string|required_with:coordenador.sign",
            "chefe.name"=>"string|required_with:coordenador.sign",
            "coordenador.sign"=>"mimes:png,jpeg|file|max:500",
            "chefe.sign"=>"mimes:png,jpeg|file|max:500",
        ]);


        if($validator->fails()){
            return response()->json($validator->errors(),500);
        }
        $year = $request->get("year");
        $rootPath = $year."/main";
        $evento = Evento::where("ano","=",$year)->first();

        $files=$request->only(["coordenador","chefe"]);
        $files['chefe_dept'] = $files['chefe'];
        unset($files['chefe']);

        foreach ($files as $col=>$data){
            try {
                $file = isset($data["sign"])?$data["sign"]:null;
               $this->saveFile($data["name"],$data["pos"],$rootPath, $col, $evento, $file);
            } catch (FileException $e) {
                return response()->json(["msg" => $e->getMessage(), "status" => "error"], 500);
            } catch (\Exception $e){
                return response()->json(["msg" => $e->getMessage(), "status" => "error"], 500);
            }
        }

        $evento->save();
        return response()->json(['msg'=>__("messages.SignsSaved"),'status'=>'success']);

    }


}