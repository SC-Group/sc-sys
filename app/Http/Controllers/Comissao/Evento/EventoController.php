<?php

namespace App\Http\Controllers\Comissao\Evento;
use App\Models\Dia;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Models\Evento;
use App\Http\Services\SCService as SC;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Validator;

class EventoController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }
    /******************************Evento**********************************/
    protected function validatorEventDates(Request $request){

        $year = $request->get("year");
        $evt = Evento::where('ano',$year);
        $evtId=null;
        if($evt->exists()){
            $evtId = $evt->first()->id;
        }

        $validator = Validator::make($request->all(), [
            'edicaoEvt' => ['required','numeric','min:1',Rule::unique('eventos','edicao')->ignore($evtId)],
            'evtInit' => 'required|date_format:d/m/Y H:i:s|before:evtEnd',
            'evtEnd' => 'required|date_format:d/m/Y H:i:s|after:evtInit',
            'inscEvtInit' => 'required|date_format:d/m/Y H:i:s|before:evtInit',
        ]);
        return $validator;
    }
    public function getConfig($year){
        return SC::view('managepages.evento.config',["year"=>$year]);
    }
    public function createEvent(Request $request){
        $validator = $this->validatorEventDates($request);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator,"evento")
                ->withInput();
        }
        $inscInit = Carbon::createFromFormat('d/m/Y H:i:s', $request->get("inscEvtInit"));
        $evInit = Carbon::createFromFormat('d/m/Y H:i:s', $request->get("evtInit"));
        $evEnd = Carbon::createFromFormat('d/m/Y H:i:s', $request->get("evtEnd"));
        $evSameYear = Evento::where("ano","=",$evInit->year)->first();
        if (isset($evSameYear)) {
            return response()->redirectTo(route("comissao.evento.config"),["year"=>$evInit->year])->with("error",["message"=>__("messages.EventAlreadyInYear",["year"=>$evInit->year])]);
        }
        $evento = new Evento();
        $evento->inicio = $evInit->toDateTimeString();
        $evento->fim = $evEnd->toDateTimeString();
        $evento->inicio_insc = $inscInit->toDateTimeString();
        $evento->ano = $evInit->year;
        $evento->edicao = $request->get("edicaoEvt");
        $evento->save();
        $this->resetUploadedFiles($evento->ano);
        return response()->redirectTo(route("comissao.evento",["year"=>$evInit->year]))->with("success",["message"=>__("messages.EventCreatedWithSuccess")]);
    }

    public function resetUploadedFiles($year){
        $storage = Storage::disk('uploadimage');
        $storage->deleteDirectory($year);

        $storage = Storage::disk('uploadcss');
        $storage->deleteDirectory($year);

    }


    private function deleteUselessDays(Evento $evt,Carbon $init,Carbon $end){

        $evt->dias()->whereDate("data","<",$init)->delete();
        $evt->dias()->whereDate("data",">",$end)->delete();

        $timeInit = $init->format("H:i:s");
        $timeEnd = $end->format("H:i:s");

        //inicio e fim antes ou depois do evento -> exclui
        $evt->horas()->whereTime("fim","<",$timeInit)->whereTime("inicio","<",$timeInit)->delete();
        $evt->horas()->whereTime("fim",">",$timeEnd)->whereTime("inicio",">",$timeEnd)->delete();
        //inicio dentro e fim depois -> ajusta fim
        $hora = $evt->horas()->whereTime("fim",">",$timeEnd)->whereTime("inicio",">=",$timeInit)->whereTime("inicio","<=",$timeEnd);
        if($hora->exists()){
            $hora = $hora->first();
            if($hora->inicio == $timeEnd){
                $hora->delete();
            }else {
                $hora->fim=$timeEnd;
                $hora->save();
            }

        }
        //inicio antes e fim dentro -> ajusta inicio
        $hora = $evt->horas()->whereTime("inicio","<",$timeInit)->whereTime("fim",">=",$timeInit)->whereTime("fim","<=",$timeEnd);
        if($hora->exists()){
            $hora = $hora->first();
            if($hora->fim == $timeInit){
                $hora->delete();
            }else {
                $hora->inicio = $timeInit;
                $hora->save();
            }
        }

        //inicio antes e fim depois -> ajusta inicio e fim
        $hora = $evt->horas()->whereTime("inicio","<",$timeInit)->whereTime("fim",">",$timeInit);
        if($hora->exists()){
            $hora = $hora->first();
            if($timeInit===$timeEnd){
                $hora->delete();
            }else {
                $hora->inicio = $timeInit;
                $hora->fim = $timeEnd;
                $hora->save();
            }
        }
    }

    public function alterEvent($year=null,Request $request){
        $validator = $this->validatorEventDates($request);
        if ($validator->fails()) {
            return response()->json(["error"=>$validator->messages()],500);
        }
        $inscInit = Carbon::createFromFormat('d/m/Y H:i:s', $request->get("inscEvtInit"));
        $evInit = Carbon::createFromFormat('d/m/Y H:i:s', $request->get("evtInit"));
        $evEnd = Carbon::createFromFormat('d/m/Y H:i:s', $request->get("evtEnd"));
        $edicao = $request->get("edicaoEvt");

        $evento = Evento::where("ano","=",$year)->first();

        if($evento==null) return response()->json(["msg"=>__("messages.EventNotFound"), "status"=>"error"],400);

        $this->deleteUselessDays($evento, $evInit,$evEnd);

        $evento->inicio = $evInit->toDateTimeString();
        $evento->fim = $evEnd->toDateTimeString();
        $evento->inicio_insc = $inscInit->toDateTimeString();
        $evento->ano = $evInit->year;
        $evento->edicao = $edicao;

        $evento->save();
        return response()->json(["msg"=>__("messages.EventUpdatedWithSuccess"),'status'=>'success']);
    }
}