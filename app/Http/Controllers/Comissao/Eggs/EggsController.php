<?php


namespace App\Http\Controllers\Comissao\Eggs;


use App\Http\Controllers\Controller;
use App\Http\Services\SCService as SC;
use App\Models\Evento;

class EggsController extends Controller
{
    public function getEggsConfig($year){
        $evento = Evento::where("ano",$year);
        if($evento->exists()){
            $evento = $evento->first();
            $eggs = $evento->eggs()->whereNotNull("inscricao_id");
            return SC::view("managepages.eggs.main",["eggs"=>$eggs,"year"=>$year]);;
        }
        return back()->with(["msg"=>__("messages.EventDoesntExists"),'status'=>'error'],500);
    }

}