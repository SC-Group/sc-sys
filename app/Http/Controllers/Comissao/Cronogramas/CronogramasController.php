<?php


namespace App\Http\Controllers\Comissao\Cronogramas;

use App\Helpers\IdHelper;
use App\Http\Services\SCService;
use App\Models\Evento;
use App\Models\Cronograma;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
class CronogramasController
{
    public function getCronogramasOverview($year){
        $evento = Evento::where("ano",$year);
        if(!$evento->exists()){
            return redirect("/")->with(["msg"=>__("messages.EventNotFound"), "status"=>"error"]);
        }
        $evento=$evento->first();

        $dias = $evento->dias();
        if (!$dias->exists()) {
            return back()->with(["msg"=>__("messages.DayDoesntExists"), "status"=>"error"]);
        }

        $horas = $evento->horas();
        if (!$horas->exists()) {
            return back()->with(["msg"=>__("messages.DayDoesntExists"), "status"=>"error"]);
        }

        $locais = $evento->locais();
        if(!$locais->exists()){
            return back()->with(["msg"=>__("messages.LocalDoesntExistsOnEvent")]);
        }
        return SCService::view("managepages.cronogramas.main",["editEnabled"=>true, "page"=>"overview","year"=>$year, "dias"=>$dias->get(),"horas"=>$horas->get(), "locais"=>$locais->get()]);
    }

    public function getCronograma($year,$dia,$mes){
        $evento = Evento::where("ano",$year);
        if(!$evento->exists()){
            return redirect("/")->with(["msg"=>__("messages.EventNotFound"), "status"=>"error"]);
        }
        $evento=$evento->first();
        $dia = $evento->dias()->whereDate("data",new Carbon("$year-$mes-$dia"));
        if (!$dia->exists()) {
            return back()->with(["msg"=>__("messages.DayDoesntExists"), "status"=>"error"]);
        }
        $dia=$dia->first();

        $horas = $evento->horas();
        if (!$horas->exists()) {
            return back()->with(["msg"=>__("messages.DayDoesntExists"), "status"=>"error"]);
        }

        $locais = $evento->locais();
        if(!$locais->exists()){
            return back()->with(["msg"=>__("messages.LocalDoesntExistsOnEvent")]);
        }
        return SCService::view("managepages.cronogramas.main",["editEnabled"=>true,"page"=>"","year"=>$year, "dia"=>$dia,"horas"=>$horas->get(), "locais"=>$locais->get()]);
    }


    public function getAtividade(Request $request){
        $validation = Validator::make($request->all(),[
            "year" => "required | exists:eventos,ano",
            "dataid"=>"required"
        ]);
        $year = $request->get("year");
        $evento = Evento::where("ano", $year)->first();
        $ids = IdHelper::dec($request->get("dataid"));

        $atividade = $evento->dias()->find($ids[0])->cronogramas()->where("hora_id",$ids[1])->where("local_id",$ids[2])->first()->atividade()->with(["trilha:id,nome,cor","palestrantes.user:id,name"])->first(["id","nome","trilha_id"]);

        return response()->json(["data"=>$atividade]);
    }

    public function saveCronograma(Request $request){
        $validation = Validator::make($request->all(),[
            "year" => "required | exists:eventos,ano",
            "dataid"=>"required"
        ]);

        if($validation->fails()){
            return back()->with(["msg"=>__("messages.InternalErrorOnSaving")]);
        }

        $year = $request->get("year");
        $evento = Evento::where("ano", $year)->first();
        $ids = IdHelper::dec($request->get("dataid"));

        $atividade = $request->get("atividade");
        $atividade = ($atividade!=null) ? IdHelper::dec($atividade)[0]:null;

        $cronograma = $evento->dias()->find($ids[0])->cronogramas()->where("hora_id", $ids[1])->where("local_id", $ids[2]);
        if($atividade==null) {
            $cronograma->delete();
        }else if($cronograma->exists()){
            $cronograma = $cronograma->first();
            $cronograma->atividade_id = $atividade;
            $cronograma->save();
        }else{
            Cronograma::updateOrCreate(["hora_id"=>$ids[1], "dia_id"=> $ids[0], "local_id"=>$ids[2]], ["atividade_id"=>$atividade]);
        }
        return back()->with(["msg"=>__("messages.SavedWithSuccess")]);
    }


}