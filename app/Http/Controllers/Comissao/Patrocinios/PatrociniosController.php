<?php


namespace App\Http\Controllers\Comissao\Patrocinios;


use App\Exceptions\NotImplementedException;
use App\Http\Services\SCService as SC;
use App\Models\Evento;
use App\Models\NivelPatrocinio;
use App\Models\Patrocinador;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Validator;

class PatrociniosController
{
    public function getPatrociniosConfig($year){
        $evento = Evento::where("ano",$year);
        $patrocinios = Patrocinador::all();

        if($evento->exists()){
            $evento=$evento->first();

            $escolhidos = NivelPatrocinio::where("evento_id",$evento->id)->with("patrocinadores")->get();
            $niveis = NivelPatrocinio::where("evento_id",$evento->id)->get();

            return SC::view("managepages.patrocinios.main",["year"=>$year,"patrocinadores"=>$patrocinios,"patrocinadoresEscolhidos"=>$escolhidos, "niveis"=>$niveis]);
        }
        return back()->with(["msg"=>__("messages.EventDoesntExists"),'status'=>'error'],400);
    }

    public function getEmpresaDataEdit(Request $request){
        $id=$request->get("id");
        $patrocinador = Patrocinador::find($id)->load("nivelPatrocinio");
        if($patrocinador==null){
            return response()->json(["msg"=>__("messages.ErrorTryingLocate"),"status"=>"error"],400);
        }
        return response()->json(["data"=>$patrocinador]);
    }

    public function getNiveis($year){
        Evento::where("ano",$year);
    }

    public function getPatrocinadorDataEdit(Request $request){
        $id=$request->get("id");
        $year = $request->get("year");
        $evento = Evento::where("ano",$year);
        if(!$evento->exists()){
            return response()->json(["msg"=>__("messages.EventNotFound"),"status"=>"error"],400);
        }
        $evento=$evento->first();
        $niveis = $evento->niveisPatrocinio();
        if(!$niveis->exists()){
            return response()->json(["msg"=>__("messages.EventHasNoSponsorshipLevels"),"status"=>"error"],400);
        }
        $niveis=$niveis->get(["id","nome","nivel"]);
        $patrocinador = Patrocinador::find($id)->load("nivelPatrocinio:id");
        if($patrocinador==null){
            return response()->json(["msg"=>__("messages.ErrorTryingLocate"),"status"=>"error"],400);
        }
        return response()->json(["data"=>$patrocinador,"niveis"=>$niveis]);
    }


    public function saveFile(string $directory, Patrocinador $patrocinador, Request $request){
        $storage = Storage::disk('uploadimage');
        if($storage->exists($directory)) {
            $url_atual = $patrocinador->url_logo;
            if($url_atual!=null){
                $url_atual = str_replace("/images/EventsImages","",$url_atual);
                $storage->delete($url_atual);
            }
        }else{
            $storage->makeDirectory($directory, 0775, true);
        }

        $imgFile = $request->file("empresaLogoFile");
        $pathFileSave= $storage->putFile($directory,$imgFile);
        $url = $storage->url($pathFileSave);
        $patrocinador->url_logo = $url;
        return $url;
    }


    public function saveEmpresaDataEdit(Request $request){
        $validator = Validator::make($request->all(),[
            "tipoCadastro"=>"required | in:novo,edicao",
            "empresaId"=>"required_if:tipoCadastro,edicao | nullable | exists:patrocinadors,id",
            "empresaNome"=>"required | max:255",
            "empresaLogo"=>"required_if:tipoCadastro,novo | nullable | file | image",
            "empresaRazaosocial"=>"required | max:255",
            "empresaResponsavel"=>"required | max:255",
            "empresaTelefone"=>"required | max:13",
            "empresaEmail"=>"required | email",
            "empresaCnpj"=>"nullable | max:255",
            "empresaNivelPatrocinioId"=>"required | integer | min:0",
            "empresaInscMun"=>"nullable | max: 255",
            "empresaInscEst"=>"nullable | max: 255",
            "empresaSite"=>"nullable | url",
            "year"=>"required"
        ]);


        $logoFails = ($request->get("empresaId")===null && !$request->hasFile("empresaLogoFile"));

        if($validator->fails() || $logoFails){
            $msg = $validator->messages();
            if($logoFails) {
                $msg->add("empresaLogo", __("validation.required", ["attribute" => "empresa logo"]));
            }
            $msg->add("modalopen",true);
            return back()->withInput()->withErrors($msg);
        }

        if($request->get("empresaId")!==null){
            $empresa = Patrocinador::find($request->get("empresaId"));
        }else{
            $empresa = new Patrocinador();
        }

        if($request->hasFile("empresaLogoFile")){
           $this->saveFile("patrocinios",$empresa,$request);
        }

        $nivel = intval($request->get("empresaNivelPatrocinioId"));

        if($nivel > 0 ){
            $evento = Evento::where("ano",$request->get("year"));
            if(!$evento->exists()){
                return back()->with(["msg"=>__("messages.EventNotFound")]);
            }
            $evento = $evento->first();
            $nivelAtual = $empresa->nivelPatrocinio()->where("evento_id",$evento->id)->first();

            if($nivelAtual!==null){
                $empresa->nivelPatrocinio()->detach($nivelAtual->id);
            }
            $empresa->nivelPatrocinio()->attach($nivel);

        }

        $empresa->nome = $request->get("empresaNome");
        $empresa->razaosocial = $request->get("empresaRazaosocial");
        $empresa->responsavel = $request->get("empresaResponsavel");
        $empresa->telefone = $request->get("empresaTelefone");
        $empresa->email = $request->get("empresaEmail");
        $empresa->cnpj = $request->get("empresaCnpj");
        $empresa->inscmun = $request->get("empresaInscMun");
        $empresa->inscest = $request->get("empresaInscEst");
        $empresa->site = $request->get("empresaSite");



        $empresa->save();

        return back()->with(["msg"=>__("messages.Success")]);

    }

    public function changeStatusPatrocinador(Request $request){
        $validation = Validator::make($request->all(),[
            "year"=>"required",
            "sendForm"=>"required",
            "selecionado"=>"required | min:1",
        ]);



        if($validation->fails()){

           return back()->withInput()->withErrors($validation->messages());
        }

        $evento = Evento::where("ano",$request->get("year"));
        if(!$evento->exists()){
            return back()->withErrors(["msg"=>__("messages.EventNotFound"),"status"=>"error"]);
        }
        $evento = $evento->first();
        $niveisPatrocinio = $evento->niveisPatrocinio()->pluck('id')->toArray();

        foreach ($request->get("selecionado") as $id=>$value){
            if(boolval($value)===true){
                $patrocinador = Patrocinador::find($id);
                if($patrocinador!=null) {
                    $patrocinador->nivelPatrocinio()->detach($niveisPatrocinio);
                }
            }

        }

        return back()->with(["msg"=>__("messages.Success")]);
    }

    private function adicionarAEvento(Request $request){
        $year = $request->get("year");
        $evento = Evento::where("ano",$year);
        if(!$evento->exists()){
            throw new \Exception(__("messages.EventNotFound"));
        }
        $evento=$evento->first();
        $nivelId = $request->get("nivelId");
        $nivel = $evento->niveisPatrocinio()->find($nivelId);
        if($nivel===null){
            throw new \Exception(__("messages.SponsorshipLevelNotFoundOnEvent"));
        }
        $selecionados = $request->get("selecionado");
        $errorFindingSponsor = false;
        foreach ($selecionados as $id => $status){
            if(boolval($status)===true){
                $patrocinador = Patrocinador::find($id);
                if($patrocinador!=null && !$patrocinador->nivelPatrocinio->contains($nivel->id)){
                    $patrocinador->nivelPatrocinio()->attach($nivel->id);
                }else{
                    $errorFindingSponsor=true;
                }
            }
        }
        if($errorFindingSponsor){
            throw new \Exception(__("messages.SomeErrorsOnSaving"));
        }
    }

    private function excluirEmpresa(Request $request){

        $selecionados = $request->get("selecionado");
        $sponsors = [];
        foreach ($selecionados as $id => $status){
            if($status!=null){
                array_push($sponsors,$id);
            }
        }
        if(sizeof($sponsors)>0){
            Patrocinador::destroy($sponsors);
        }else{
            throw new \Exception(__("messages.SomeErrorsOnSaving"));
        }

    }

    public function changeStatusEmpresa(Request $request){
        $validation = Validator::make($request->all(),[
            "year"=>"required",
            "sendForm"=>"required",
            "selecionado"=>"required | min:1",
            //"nivelId"=>"required"
        ]);

        if($validation->fails()){
            return back()->withInput()->withErrors($validation->messages());
        }

        $actionType = $request->get("sendForm");
        try {
            switch ($actionType) {
                case "adicionar":
                    $this->adicionarAEvento($request);
                    break;
                case "excluir":
                    $this->excluirEmpresa($request);
                    break;
            }
        }catch (\Exception $e){
            return back()->with(["msg"=>$e->getMessage(), "status"=>"error"]);
        }

        return back()->with(["msg"=>__("messages.Success")]);
    }




}