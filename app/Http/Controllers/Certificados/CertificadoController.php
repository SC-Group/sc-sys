<?php


namespace App\Http\Controllers\Certificados;


use App\Http\Controllers\Controller;
use App\Models\Evento;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage;
use App\Models\User;

class CertificadoController extends Controller
{

    public function __construct()
    {
        $this->localDisk = Storage::disk("local");
        $this->disk = Storage::disk("public");
    }


    private function getEventDays($dias){
        $meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
        $mes = -1;
        $ano = -1;
        $anos=[];
        for ($i=0;$i<$dias->count();$i++) {
            $diaCarbon = Carbon::create($dias[$i]->data);
            if($ano!==$diaCarbon->year) {
                $ano=$diaCarbon->year;
                $anos["$ano"]=[];
            }
            if($mes!==$diaCarbon->month) {
                $mes = $diaCarbon->month;
                $anos["$ano"]["".$meses[$mes-1]]=[];
            }
            array_push($anos["$ano"]["".$meses[$mes-1]],$diaCarbon->day);
        }

        $anoString = "";
        $numAnos=sizeof($anos);
        $anoIndex = 0;
        foreach ($anos as $ano=>$meses){
            $numMeses=sizeof($meses);
            $mesString = "";
            $mesIndex = 0;
            foreach ($meses as $mes=>$dias ){
                $numDias=sizeof($dias);
                $diasString = "";
                foreach ($dias as $diaIndex=>$dia){
                    if($diaIndex==0){//primeiro
                        $diasString.="$dia";
                    }elseif($diaIndex+1==$numDias){//ultimo
                        $diasString.=" e $dia";
                    }else{
                        $diasString.=", $dia";
                    }
                }
                if($mesIndex==0){//primeiro
                    $mesString.="$diasString de $mes";
                }elseif($mesIndex+1==$numMeses){//ultimo
                    $mesString.=" e $diasString de $mes";
                }else{
                    $mesString.=", $diasString de $mes";
                }

                $mesIndex++;
            }
            if($anoIndex==0){//primeiro
                $anoString.="$mesString de $ano";
            }elseif($anoIndex+1==$numAnos){//ultimo
                $anoString.=" e $mesString de $ano";
            }else{
                $anoString.=", $mesString de $ano";
            }
            $anoIndex++;
        }
        return $anoString;
    }

    protected function getOptions(Evento $evt,$extra_options=null){
        $fileChefe = null;
        if($this->localDisk->exists($evt->chefe_dept_sign)){
            $fileChefe = $this->localDisk->get($evt->chefe_dept_sign);
        }
        $fileCoord = null;
        if($this->localDisk->exists($evt->coordenador_sign)){
            $fileCoord = $this->localDisk->get($evt->coordenador_sign);
        }

        $certificadosNiveis = $evt->niveisPatrocinio()->has("patrocinadores")->orderBy("nivel","desc");
        $apoios = $evt->apoios()->orderBy("id","desc");

        $dias = $evt->dias()->orderBy("data","asc")->get();
        $diasString = $this->getEventDays($dias);
        return array_merge([
            "evt"=>$evt,
            "dias_da_semana_plural"=>$dias->count()>1,
            "dias_da_semana"=>$diasString,
            "coordenador"=>[
                'name'=>$evt->coordenador_name,
                'sign'=>$fileCoord,
                'linepos'=>$evt->coordenador_sign_pos
            ],
            "chefe"=>[
                'name'=>$evt->chefe_dept_name,
                'sign'=>$fileChefe,
                'linepos'=>$evt->chefe_dept_sign_pos
            ],
            'edicao'=>$evt->edicao,
            'patrociniosniveis'=>$certificadosNiveis->get(),
            'apoios'=>$apoios->get()
        ],$extra_options);
    }




}