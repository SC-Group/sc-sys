<?php


namespace App\Http\Controllers\Certificados;
use App\Http\Services\SCService;
use App\Models\Evento;
use App\Models\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Carbon;

class CertificadoParticipanteController extends CertificadoController
{
    public function getDaysPresence(User $user, Evento $evento){
        $inscrito = $user->inscricao()->where("evento_id",$evento->id)->first();
        if($inscrito===null){
            throw new \Exception("Não foi possível achar inscrição no evento de ".$evento->ano);
        }

        $dias = $inscrito->dias()->wherePivot("presenca","Presente");
        if(!$dias->exists()){
            throw new \Exception("Não foi possível recuperar presenças no evento de ".$evento->ano);
        }
        $dias = $dias->get();

        $diasString = [];
        foreach ($dias as $dia){
            array_push($diasString,Carbon::create($dia->data)->format("d/m/Y"));
        }

        return $diasString;
    }

    public function getCertificado($year){

        $evento = Evento::where("ano",$year);
        if(!$evento->exists()){
            return SCService::view("errors.certificado",["year"=>$year,"msg"=>__("messages.EventNotFound")]);
        }
        $evento=$evento->first();

        if(Carbon::create($evento->fim)->gt(now())){
            return SCService::view("errors.certificado",["year"=>$year,"msg"=>"Certificado só pode ser gerado após o fim do evento"]);
        }

        $user = auth()->user();
        $dre = $user->aluno()->first()->dre;

        try {
            $diasString = $this->getDaysPresence($user, $evento);
        }catch (\Exception $e){
            return SCService::view("errors.certificado",["year"=>$year,"msg"=>$e->getMessage()]);
        }
        $options = $this->getOptions($evento,[
            "nome"=>auth()->user()->name,
            "tipo"=>"participante",
            "dre"=>$dre,
            'dias'=>$diasString,
        ]);

        if($options["coordenador"]["sign"]===null || $options["chefe"]["sign"]===null){
            return SCService::view("errors.certificado",["year"=>$year,"msg"=>"Não há todos os componentes para gerar certificado"]);
        }

        $pdf = PDF::loadView('documents/certificates/certificado',$options);
        $pdf->setPaper('a4','landscape');

        return $pdf->stream();
    }
}