<?php


namespace App\Http\Controllers\Certificados;
use App\Http\Services\SCService;
use App\Models\Evento;
use App\Models\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Carbon;

class CertificadoPalestranteController extends CertificadoController
{
    public function getCertificado($year, $cod){

        $evento = Evento::where("ano",$year);
        if(!$evento->exists()){
            return SCService::view("errors.certificado",["year"=>$year,"msg"=>__("messages.EventNotFound")]);
        }
        $evento=$evento->first();

        if(Carbon::create($evento->fim)->gt(now())){
            return SCService::view("errors.certificado",["year"=>$year,"msg"=>"Certificados só podem ser gerados após o fim do evento", "status"=>"error"]);
        }

        $user = auth()->user();

        $palestrante = $user->palestrante()->where("evento_id",$evento->id);
        if(!$palestrante->exists()){
            return SCService::view("errors.certificado",["year"=>$year,"msg"=>"Você não consta como palestrante do evento de ".$year]);
        }
        $palestrante=$palestrante->first();

        $atividade = $palestrante->atividades()->where("aprovada",true)->where("codigo",$cod)->wherePivot("presente",true);
        if(!$atividade->exists()){
            return SCService::view("errors.certificado",["year"=>$year,"msg"=>"Houve um problema ao tentar localizar atividade com este código."]);
        }

        $atividade=$atividade->first();

        $tipoOrganizacao = null;
        switch($atividade->tipo_organizacao_vinculada) {
            case  "Grupo de Interesse": $tipoOrganizacao = "do grupo de interesse/extensão"; break;
            case "Empresa": $tipoOrganizacao = "da empresa"; break;
            default: $tipoOrganizacao = "da organização"; break;
        }

        $tipoAtividade = null;
        switch($atividade->tipo) {
            case  "Palestra": $tipoAtividade = "a palestra"; break;
            case "Workshop": $tipoAtividade = "o workshop"; break;
            case "Minicurso": $tipoAtividade = "o minicurso"; break;
            default: $tipoAtividade = "a atividade"; break;
        }

        $options = $this->getOptions($evento,[
            "nome"=>$user->name,
            "tipo"=>"palestrante",
            'atividade'=>$atividade->nome,
            'tipoAtividade'=>$tipoAtividade,
            'tipoOrganizacao'=>$tipoOrganizacao,
            'organizacao'=>$atividade->organizacao_vinculada
        ]);

        if($options["coordenador"]["sign"]===null || $options["chefe"]["sign"]===null){
            return SCService::view("errors.certificado",["year"=>$year,"msg"=>"Não há todos os componentes para gerar certificado"]);
        }

        $pdf = PDF::loadView('documents/certificates/certificado',$options);
        $pdf->setPaper('a4','landscape');

        return $pdf->stream();
    }
}