<?php
/**
 * Created by PhpStorm.
 * User: fjmrs
 * Date: 16/03/2019
 * Time: 16:49
 */

namespace App\Http\Services;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Models\Evento;
use Illuminate\Support\Facades\Response;
use Carbon\Carbon;

class SCService
{
    public static function view($view,$variables=[],$status=200,$headers=null){
        $needRedirect=true;
        $route = Route::current();
        $CountEventos = Evento::count();
        $currentYear = Carbon::now()->year;
        if(($CountEventos>0)){//Tem Algum Evento no BD
            if(empty($variables['year'])){//Não tem ano definido
                $evento = Evento::where("ano","<=",$currentYear)->orderBy("ano","desc");
                if($evento->exists()){
                    $evento = $evento->first();
                    $variables['year'] = Carbon::parse($evento->fim)->year;
                }else{
                    $evento = Evento::where("ano",">",$currentYear)->orderBy("ano","asc")->first();
                    $variables['year'] = Carbon::parse($evento->fim)->year;
                }
            }else{//Tem ano definido
                $needRedirect=false;
                $evento = Evento::where("ano","=",$variables['year'])->first();
                if(empty($evento)){//Não achou evento requisitado
                    return redirect()->route("home")->with(["error"=>__("messages.EventNotFound")]);
                }

                $eventos = DB::table('eventos')->orderBy('ano', 'desc')->get();
                $variables["eventos"]=$eventos;
            }
            $variables["cssUrl"]=$evento->css_url;
            $variables["evt"]=["obj"=>$evento,
                "evtInit"=>Carbon::parse($evento->inicio)->format("d/m/Y H:i:s"),
                "evtEnd"=>Carbon::parse($evento->fim)->format("d/m/Y H:i:s"),
                "inscInit"=>Carbon::parse($evento->inicio_insc)->format("d/m/Y H:i:s"),
                "edicao"=>$evento->edicao
            ];
        }else{//Não existe evento no BD
            $variables["evt"]=null;
            if((isset($variables['year']))&&($variables['year']==$currentYear)){
                $needRedirect=false;
            }else{
                $variables['year'] = $currentYear;
            }
        }

        $exploded = explode('{year}',"".$route->uri());
        if((count($exploded)>1)&&$needRedirect){
            $rota = implode($variables['year'],$exploded);
            return response()->redirectTo($rota);
        }else {
             return response()->view($view,$variables,$status);
        }
    }
}