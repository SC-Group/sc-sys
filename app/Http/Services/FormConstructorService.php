<?php
/**
 * Created by PhpStorm.
 * User: fjmrs
 * Date: 04/03/2019
 * Time: 10:59
 */

namespace App\Http\Services;
use App\Helpers\RegexPatternJSHelper;

class FormConstructorService
{
    public function constructForm($id,$title,$fields,$description='',$lastSection=false,$oldData=null){
        if($description==null)$description='';
        $section = $this->createSection($id,$title,$description,$fields);
        return ['filledData'=>$oldData,'section'=>$section,'lastSection'=>$lastSection];
    }

    public function createSection($id,$title,$description='',$fields=[]){
        return
            [   'id' => $id,
                'title' => $title,
                'description' => $description,
                'fields' => $fields,
            ];
    }

    public function createHiddenField($id,$value){
        return [
            'fieldid'=>$id,
            'value'=>$value,
            'type'=>'hidden'
        ];
    }

    public function createField($id,$title,$component=['fieldtype'=>'shortans'],$mandatory=false,$showwhen=null,$loadOptions=null){
        return   [
                'title'=>$title,
                'fieldid'=>$id,
                'mandatory'=>$mandatory,
                'type'=>$component['fieldtype'],
                'components'=>$component,
                'showwhen'=>$showwhen,
                'loadOptions'=>$loadOptions
            ];

    }

    public function createValidation($pattern,$options='',$msg=null){
        switch ($pattern){
            case 'email':
                return RegexPatternJSHelper::email($options,$msg);

            case 'url':
                return RegexPatternJSHelper::url($options,$msg);

            case 'name':
                return RegexPatternJSHelper::name($options,$msg);

            case 'password':
                return RegexPatternJSHelper::password($options,$msg);

            default:
                return RegexPatternJSHelper::_default($pattern,$msg,$options);
        }
    }
    public function image($src,$class = 'w-100',$classcontainer="col-sm-12"){
        return [
            'fieldtype'=>'image',
            'src'=>$src,
            'class'=>$class,
            'classcontainer'=>$classcontainer
        ];
    }

    public function info($txt){
        return [
            'fieldtype'=>'info',
            'txt'=>$txt
        ];
    }

    public function shortAns($type,$mask,$value=null,$validation=null,$maxlen=200){
        return
            [   'fieldtype'=>'shortans',
                'type'=>$type,
                'mask'=>$mask,
                'value'=>$value,
                'validation'=>$validation,
                'maxlen'=>$maxlen
            ];
    }

    public function option($label,$checked=false,String $value=null){
        return ['label'=>$label,'checked'=>$checked, 'value'=>$value];
    }

    public function checkBoxes($options){
        return
            [
                'fieldtype'=>'checkboxes',
                'options'=>$options
            ];
    }

    public function choices($options, $value=null){
        return
            [
                'fieldtype'=>'choices',
                'options' =>$options,
            ];
    }

    public function dynamicSelect($jsScript,$dependency=null){
        return [
          'fieldtype'=>'dynamicselect',
          'api'=>"",
          'js'=>$jsScript,
          'dependency'=>$dependency
        ];
    }

    public function paragraph($value=null,$maxlen=1000){
        return [
            'fieldtype'=>'paragraph',
            'value'=>$value,
            'maxlen'=>$maxlen
        ];
    }

    public function select($options){
        return [
            'fieldtype'=>'select',
            'options' => $options,
        ];
    }

    public function grid($component,$rowlabels,$collabels,$values=null){
        return[
            'values'=>$values,
            'fieldtype'=>'grid',
            'type'=>$component['fieldtype'],
            'components'=>$component,
            'rowlabels'=>$rowlabels,
            'collabels'=>$collabels,
        ];
    }

}