<?php


namespace App\Http\Services;


use App\Exceptions\NotImplementedException;
use App\Helpers\SendRequest;

class SendToBotService
{
    public static function SendHTTPJSON($content){
        SendRequest::send(env("BOT_URL"),$content,"json");
    }

    public static function SendFAKE($content){
        throw new \Exception("ERRO");
    }

    public static function SendRESTJSON($content){
        throw new NotImplementedException();
    }

    public static function SendTELEGRAM($message){
        $key = env("BOT_KEY");
        throw new NotImplementedException();
    }

    public static function SendWHATSAPP($message){
        $key = env("BOT_KEY");
        throw new NotImplementedException();
    }

    public static function Send($content){

        try {
            $type = env("BOT_TYPE","HTTPJSON");
            forward_static_call(array("App\\Http\\Services\\SendToBotService","Send".$type),$content);
        }catch (\Exception $e){
            throw $e;
        }

    }



}