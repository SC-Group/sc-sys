<?php


namespace App\Http\Services;


use App\Helpers\SCInscricao;
use App\Helpers\SendRequest;
use App\Models\Aluno;
use App\Models\Atividade;
use App\Models\Curso;
use App\Models\Evento;
use App\Models\Inscricao;
use App\Models\Instituicao;
use App\Models\Palestrante;
use App\Models\Professor;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class SaveInscricaoAtividadesService
{
    public static function trySaveData($year,Request $request){
        $sessval = $request->session()->all();
        $sequence = $sessval["_sequenceFormAtividades"];
        $evt = Evento::where("ano",$year)->first();
        $evtInitInsc = Carbon::parse($evt->inicio_insc);

        if(now()->greaterThan($evtInitInsc->subDays(30))){
            throw  new Exception(__("messages.NotInActivitySubscriptionPeriod"),334);
        }

        $camisas=null;
        $user = null;
        $aluno = null;
        $user_email = null;
        $user_pass=null;
        $palestrante=null;
        $atividade = null;
        $cronogramas = [];
        $isNewAtiv=false;
        foreach($sequence as $item=>$value){
            if( $value<=10 && filled($sessval['section'.$value])){
                $section = $sessval['section'.$value];
                if($value==0){//ID
                    $user_email = $section['email'];
                }
                if($value==1){//Pass
                    $user_pass = $section["password"];
                }
                if($value==2){
                    if(!Auth::check()){
                        if(!Auth::once(['email'=>$user_email,"password"=>$user_pass])){
                            throw new Exception(__("messages.ErrorRetrivingUserInfo"));
                        }
                    }
                    $user=Auth::user();
                    $aluno = $user->aluno()->first();
                }
                if($value==3){//DPess
                    $user=new User();
                    $user->email=$user_email;
                    $user->password = bcrypt($section["password"]);
                    $user->name=$section["name"];
                    if($section["tel"]!=null){
                        $user->tel=$section["tel"];
                    }
                    if($section["category"]=="aluno"){//Aluno
                        $aluno=new Aluno();
                        $instituicao = Instituicao::where("sigla","UFRJ")->firstOrFail();
                        $curso = $instituicao->cursos()->findOrFail($section['curso']);
                        $aluno->curso()->associate($curso);
                        $aluno->ufrj_dre = $section['dre'];
                        $aluno->instituicao()->associate($instituicao);
                    }
                    if($section["category"]=="ex_aluno"){
                        $curso = Curso::find($section['curso']);
                        $user->ex_ufrj_curso()->associate($curso);
                    }

                }

                if($value==4){//Disponibilidade
                    foreach ($section["disponibilidade"] as $dia=>$horas){
                        $dia = Carbon::createFromFormat("d/m/Y",$dia."/".$year);
                        $dbdia = $evt->dias()->whereDate("data",$dia);
                        if($dbdia->exists()) {
                            $dbdia=$dbdia->first();
                            foreach ($horas as $hora => $value) {
                                $hora = explode(" a ", $hora);
                                $hora[0] = Carbon::createFromFormat("H:i", $hora[0]);
                                $hora[1] = Carbon::createFromFormat("H:i", $hora[1]);
                                $dbhora = $evt->horas()->whereTime("inicio", $hora[0])->whereTime("fim", $hora[1]);

                                if($dbhora->exists()){
                                    $dbhora = $dbhora->first();
                                    array_push($cronogramas, [
                                        "dia_id"=>$dbdia->id,
                                        "hora_id"=>$dbhora->id
                                    ]);
                                }

                            }
                        }
                    }

                }

                if($value==6){//Ativ. Existe
                    $atividade = Atividade::where("codigo",$section["codigo"])->where("evento_id",$evt->id);
                    if(!$atividade->exists()){
                        throw new Exception(__("messages.ActivityCodeDoesntExists",["code"=>$section["atividade"] ]));
                    }else{
                        $atividade = $atividade->first();
                    }
                }

                if($value==7){//Nova Ativ
                    $isNewAtiv=true;
                    $atividade = new Atividade();
                    $atividade->codigo = Str::uuid();
                    $atividade->nome = $section["titulo"];
                    $atividade->tipo = $section["tipo"];
                    if($section["tipo"]="Workshop"){
                        $atividade->recursos = $section["recursos"];
                    }
                    if($section["vinculada"]=="true"){
                        $atividade->tipo_organizacao_vinculada = $section["tipo_organizacao"];
                        $atividade->organizacao_vinculada = $section["organizacao"];
                    }
                    $atividade->descricao = $section["descricao"];

                }

                if($value==9){//Estagio
                    $aluno->estagio = boolval($section['estagio']);
                }

                if($value==10){//Camisas
                    $camisas = [];
                    $temFem = $evt->camisas()->where('tipo','Feminina')->exists();
                    $temMasc = $evt->camisas()->where('tipo','Masculina')->exists();
                    if($temFem&&filled($section['camisas_fem'])){
                        foreach ($section['camisas_fem'] as $tam=>$cores){
                            foreach ($cores as $cor=>$qtd){
                                if($qtd>0){
                                    $searchFor = [['tipo','Feminina'], ['tamanho',$tam]];
                                    if($cor != "Número de Camisas")  array_push($searchFor,['cor',$cor]);
                                    $camisa = $evt->camisas()->where($searchFor)->first();
                                    if($camisa!=null){
                                        $camisaId=$camisa->id;
                                        $camisas[$camisaId] = ["reservadas"=>$qtd];
                                    }
                                }
                            }
                        }
                    }

                    if($temMasc&&filled($section['camisas_masc'])){
                        foreach ($section['camisas_masc'] as $tam=>$cores){
                            foreach ($cores as $cor=>$qtd){
                                if($qtd>0){
                                    $searchFor = [['tipo','Masculina'], ['tamanho',$tam]];
                                    if($cor != "Número de Camisas")  array_push($searchFor,['cor',$cor]);
                                    $camisa = $evt->camisas()->where($searchFor)->first();
                                    if($camisa!=null){
                                        $camisaId=$camisa->id;
                                        $camisas[$camisaId] = ["reservadas"=>$qtd];
                                    }
                                }
                            }
                        }
                    }
                }

            }else{
                if($value>11)
                    throw new Exception(__("messages.UnexpectedError"));
            }
        }


        $user->save();
        $palestrante = $user->palestrante()->where("evento_id",$evt->id);
        if($palestrante->exists()){
            $palestrante=$palestrante->first();
        }else{
            $palestrante=new Palestrante();
        }

        $palestrante_exist=$user->palestrante()->where("evento_id",$evt->id)->exists();
        if($camisas!=null){
            $user->camisas()->attach($camisas);
        }
        if($aluno!=null) {
            $aluno->user()->associate($user);
            $aluno->save();
        }
        $palestrante->user()->associate($user);
        $palestrante->evento()->associate($evt);
        $palestrante->save();
        if($isNewAtiv){
            $atividade->evento()->associate($evt);
        }
        $atividade->save();

        DB::transaction(function () use (&$palestrante,&$cronogramas) {
            $palestrante->disponibilidades()->delete();
            $palestrante->disponibilidades()->createMany($cronogramas);
        });

        $palestrante->atividades()->attach($atividade);
        return ["status"=>"success",
            "user"=>$user,
            'palestrante_exist'=>$palestrante_exist,
            "palestrante"=>$palestrante,
            "ativCode"=>$isNewAtiv?$atividade->codigo:null
        ];
    }

    public static function saveData($year,Request $request){
        try{
            return SaveInscricaoAtividadesService::trySaveData($year,$request);
        }catch (Exception $e){
            if($e->getCode()===334) return ["status"=>"error","msg"=>$e->getMessage()];
            else return ["status"=>"error","msg"=>__("messages.InternalErrorOnSaving")];
        }
    }



}