<?php


namespace App\Http\Services;


use App\Helpers\SCInscricao;
use App\Models\Aluno;
use App\Models\Evento;
use App\Models\Inscricao;
use App\Models\Instituicao;
use App\Models\Staff;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SaveInscricaoStaffService
{
    public static function trySaveData($year,Request $request){
        $sessval = $request->session()->all();
        $sequence = $sessval["_sequenceFormStaff"];
        $evt = Evento::where("ano",$year)->first();
        $evtInitInsc = Carbon::parse($evt->inicio_insc);

        if(now()->greaterThan($evtInitInsc->subDays(30))){
            throw  new Exception("Não está em período de inscrição de staffs",334);
        }

        $camisas=null;
        $user = null;
        $aluno = null;
        $user_email = null;
        $user_pass=null;
        $staff=null;
        $cronogramas = [];
        $sendToBot=false;
        foreach($sequence as $item=>$value){
            if( $value<=6 && filled($sessval['section'.$value])){
                $section = $sessval['section'.$value];
                if($value==0){//ID
                    $user_email = $section['email'];
                }
                if($value==1){//Pass
                    $user_pass = $section["password"];
                }
                if($value==2){//Confirm
                    if(!Auth::check()){
                        if(!Auth::once(['email'=>$user_email,"password"=>$user_pass])){
                            throw new Exception(__("messages.ErrorRetrivingUserInfo"));
                        }
                    }
                    $user=Auth::user();
                    $aluno = $user->aluno()->first();
                }
                if($value==3){//DPess
                    $user=new User();
                    $user->email=$user_email;
                    $user->password = bcrypt($section["password"]);
                    $user->name=$section["name"];
                    if($section["tel"]!=null){
                        $user->tel=$section["tel"];
                        if($section["bot"]!=null){
                            $sendToBot = boolval($section["bot"]);
                        }
                    }

                    $aluno=new Aluno();
                    $instituicao = Instituicao::where("sigla","UFRJ")->firstOrFail();
                    $curso = $instituicao->cursos()->findOrFail($section['curso']);
                    $aluno->curso()->associate($curso);
                    $aluno->ufrj_dre = $section['dre'];
                    $aluno->instituicao()->associate($instituicao);

                }

                if($value==4){//Disponibilidade
                    foreach ($section["disponibilidade"] as $dia=>$horas){
                        $dia = Carbon::createFromFormat("d/m/Y",$dia."/".$year);
                        $dbdia = $evt->dias()->whereDate("data",$dia);
                        if($dbdia->exists()) {
                            $dbdia=$dbdia->first();
                            foreach ($horas as $hora => $value) {
                                $hora = explode(" a ", $hora);
                                $hora[0] = Carbon::createFromFormat("H:i", $hora[0]);
                                $hora[1] = Carbon::createFromFormat("H:i", $hora[1]);
                                $dbhora = $evt->horas()->whereTime("inicio", $hora[0])->whereTime("fim", $hora[1]);

                                if($dbhora->exists()){
                                    $dbhora = $dbhora->first();
                                    array_push($cronogramas, [
                                        "dia_id"=>$dbdia->id,
                                        "hora_id"=>$dbhora->id
                                    ]);
                                }

                            }
                        }
                    }

                }

                if($value==5){//Estagio
                    $aluno->estagio = boolval($section['estagio']);
                }

                if($value==7){//Camisas
                    $camisas = [];
                    $temFem = $evt->camisas()->where('tipo','Feminina')->exists();
                    $temMasc = $evt->camisas()->where('tipo','Masculina')->exists();
                    if($temFem&&filled($section['camisas_fem'])){
                        foreach ($section['camisas_fem'] as $tam=>$cores){
                            foreach ($cores as $cor=>$qtd){
                                if($qtd>0){
                                    $searchFor = [['tipo','Feminina'], ['tamanho',$tam]];
                                    if($cor != "Número de Camisas")  array_push($searchFor,['cor',$cor]);
                                    $camisa = $evt->camisas()->where($searchFor)->first();
                                    if($camisa!=null){
                                        $camisaId=$camisa->id;
                                        $camisas[$camisaId] = ["reservadas"=>$qtd];
                                    }
                                }
                            }
                        }
                    }

                    if($temMasc&&filled($section['camisas_masc'])){
                        foreach ($section['camisas_masc'] as $tam=>$cores){
                            foreach ($cores as $cor=>$qtd){
                                if($qtd>0){
                                    $searchFor = [['tipo','Masculina'], ['tamanho',$tam]];
                                    if($cor != "Número de Camisas")  array_push($searchFor,['cor',$cor]);
                                    $camisa = $evt->camisas()->where($searchFor)->first();
                                    if($camisa!=null){
                                        $camisaId=$camisa->id;
                                        $camisas[$camisaId] = ["reservadas"=>$qtd];
                                    }
                                }
                            }
                        }
                    }
                }

            }else{
                if($value>7)
                    throw new Exception(__("messages.UnexpectedError"));
            }
        }

        $user->save();

        $aluno->user()->associate($user);
        $aluno->save();
        $aluno = Aluno::find($user->id);

        $staff=new Staff();

        if($camisas!=null){
            $user->camisas()->attach($camisas);
        }

        $staff->aluno()->associate($aluno);
        $staff->evento()->associate($evt);
        $staff->save();

        DB::transaction(function () use (&$staff,&$cronogramas) {
            $staff->disponibilidades()->delete();
            $staff->disponibilidades()->createMany($cronogramas);
        });

        $inscricao = new Inscricao();
        $inscricao->send_to_bot=$sendToBot;
        $inscricao->evento()->associate($evt);
        $inscricao->user()->associate($user);
        $inscricao->save();
        $sendToBotData["sc"]=SCInscricao::get($inscricao->id);
        $sendToBotData["tipo"]="Staff";
        $botMsg = null;
        $botStatus = null;
        if($sendToBot&&boolval(env("BOT_ENABLED",false))){
            try{
                SendToBotService::Send($sendToBotData);
                $botStatus = "success";
                $botMsg = __("messages.BotSuccess",["bot"=>env("BOT_NAME","Bot")]);

            }catch (Exception $e){
                $botStatus = "error";
                $botMsg = __("messages.BotError",["bot"=>env("BOT_NAME","Bot")]);
            }
        }

        return ["status"=>"success","staff"=>$staff, "user"=>$user, "inscricao"=>$inscricao, "botMsg"=>$botMsg, "botStatus"=>$botStatus];

    }

    public static function saveData($year,Request $request){
        try{
            return SaveInscricaoStaffService::trySaveData($year,$request);
        }catch (Exception $e){
            if($e->getCode()===334) return ["status"=>"error","msg"=>$e->getMessage()];
            else return ["status"=>"error","msg"=>__("messages.InternalErrorOnSaving")];
        }
    }



}