<?php


namespace App\Http\Services;


use App\Helpers\SCInscricao;
use App\Helpers\SendRequest;
use App\Models\Aluno;
use App\Models\Curso;
use App\Models\Evento;
use App\Models\Inscricao;
use App\Models\Instituicao;
use App\Models\Professor;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SaveInscricaoParticipanteService
{
    public static function trySaveData($year,Request $request){
        $sendToBot = false;
        $sendToBotData=[];
        $sessval = $request->session()->all();
        //$request->session()->flush();
        $sequence = $sessval["_sequenceFormInsc"];
        $evt = Evento::where("ano",$year)->first();
        $evtFim = Carbon::parse($evt->fim);
        $evtInitInsc = Carbon::parse($evt->inicio_insc);
        if($evtInitInsc->greaterThan(Carbon::now()) || $evtFim->lessThan(Carbon::now())){
            throw  new Exception(__("messages.NotInSubscriptionPeriod",["year"=>$year]),334);
        }
        $camisas=null;
        $user = new User();
        $aluno = null;
        $professor=null;
        foreach($sequence as $item=>$value){
            if( $value<=7 && filled($sessval['section'.$value])){
                $section = $sessval['section'.$value];
                if($value==0){//ID
                    $user->email = $section['email'];
                    $sendToBotData["email"]=$section["email"];
                }
                if($value==2){//DPess
                    $user->password = bcrypt($section["password"]);
                    $user->name=$section["name"];
                    if($section["tel"]!=null){
                        $user->tel=$section["tel"];
                        if($section["bot"]!=null){
                            $sendToBot = boolval($section["bot"]);
                        }
                        $sendToBotData["number"]=$user->tel;
                    }
                    $sendToBotData["name"]=$section["name"];
                }
                if($value==3){//Aluno
                    $aluno=new Aluno();
                    $instituicao = Instituicao::findOrFail($section['instituicaoID']);
                    $instTipo = $instituicao->tipo;
                    if($instTipo=='Escola de Ensino Técnico'){
                        $curso = $instituicao->cursos()->findOrFail($section['instituicaoCursoTecnico']);
                        $aluno->curso()->associate($curso);
                    }
                    if($instTipo=='Universidade'){
                        $curso = $instituicao->cursos()->findOrFail($section['instituicaoCursoGraduacao']);
                        $aluno->curso()->associate($curso);
                    }
                    if($instituicao->sigla = "UFRJ"){
                        $aluno->ufrj_dre = $section['DRE'];
                    }
                    $aluno->instituicao()->associate($instituicao);

                }
                if($value==4){//Professor
                    $professor = new Professor();
                    $instituicao = Instituicao::findOrFail($section['instituicaoID']);
                    $instTipo = $instituicao->tipo;
                    if($instTipo=='Escola de Ensino Técnico'){
                        $curso = $instituicao->cursos()->findOrFail($section['instituicaoCursoTecnico']);
                        $professor->curso()->associate($curso);
                    }
                    if($instTipo=='Universidade'){
                        $curso = $instituicao->cursos()->findOrFail($section['instituicaoCursoGraduacao']);
                        $professor->curso()->associate($curso);
                    }
                    $professor->instituicao()->associate($instituicao);

                }
                if($value==5){//Participante
                    if($section['exAluno']){
                        $curso = Curso::find($section['curso']);
                        $user->ex_ufrj_curso()->associate($curso);
                    }
                }

                if($value==6){//Estagio
                    $aluno->estagio = boolval($section['estagio']);
                }

                if($value==7){//Camisas
                    $camisas = [];
                    $temFem = $evt->camisas()->where('tipo','Feminina')->exists();
                    $temMasc = $evt->camisas()->where('tipo','Masculina')->exists();
                    if($temFem&&filled($section['camisas_fem'])){
                        foreach ($section['camisas_fem'] as $tam=>$cores){
                            foreach ($cores as $cor=>$qtd){
                                if($qtd>0){
                                    $searchFor = [['tipo','Feminina'], ['tamanho',$tam]];
                                    if($cor != "Número de Camisas")  array_push($searchFor,['cor',$cor]);
                                    $camisaId = $evt->camisas()->where($searchFor)->first()->id;
                                    $camisas[$camisaId] = ["reservadas"=>$qtd];
                                }
                            }
                        }
                    }

                    if($temMasc&&filled($section['camisas_masc'])){
                        foreach ($section['camisas_masc'] as $tam=>$cores){
                            foreach ($cores as $cor=>$qtd){
                                if($qtd>0){
                                    $searchFor = [['tipo','Masculina'], ['tamanho',$tam]];
                                    if($cor != "Número de Camisas")  array_push($searchFor,['cor',$cor]);
                                    $camisaId = $evt->camisas()->where($searchFor)->first()->id;
                                    $camisas[$camisaId] = ["reservadas"=>$qtd];
                                }
                            }
                        }
                    }
                }

            }else{
                if($value!=8)
                    throw new Exception(__("messages.InternalErrorOnSaving")." ".__("messages.TryAgain"));
            }
        }

        $user->save();
        if($camisas!=null){
            $user->camisas()->attach($camisas);
        }
        if($aluno!=null){
            $aluno->user()->associate($user);
            $aluno->save();
        }else if($professor!=null){
            $professor->user()->associate($user);
            $professor->save();
        }

        $inscricao = new Inscricao();
        $inscricao->send_to_bot=$sendToBot;
        $inscricao->evento()->associate($evt);
        $inscricao->user()->associate($user);
        $inscricao->save();
        $sendToBotData["sc"]=SCInscricao::get($inscricao->id);
        $sendToBotData["tipo"]="Participante";
        $botMsg = null;
        $botStatus = null;
        if($sendToBot&&boolval(env("BOT_ENABLED",false))){
            try{
                SendToBotService::Send($sendToBotData);
                $botStatus = "success";
                $botMsg = __("messages.BotSuccess",["bot"=>env("BOT_NAME","Bot")]);

            }catch (Exception $e){
                $botStatus = "error";
                $botMsg = __("messages.BotError",["bot"=>env("BOT_NAME","Bot")]);
            }
        }
        return ["status"=>"success","user"=>$user,"inscricao"=>$inscricao,"botMsg"=>$botMsg,"botStatus"=>$botStatus];
    }

    public static function saveData($year,Request $request){
        try{
            return SaveInscricaoParticipanteService::trySaveData($year,$request);
        }catch (Exception $e){
            Log::error("Bot Error - ".$e->getMessage());
            if($e->getCode()===334) return ["status"=>"error","msg"=>$e->getMessage()];
            else return ["status"=>"error","msg"=>__("messages.InternalErrorOnSaving")];
        }
    }



}