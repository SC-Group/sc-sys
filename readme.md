# Sistema da Semana da computação

Sistema destinado ao gerenciamento de participantes, patrocinadores, apoioadores, staffs, palestrantes comissão organizadora e todos as atividades relacionadas.

## Documentos

* Instalação usando Docker:  
[./docs/Rodando no docker.md](./docs/Rodando%20no%20docker.md)

* Instalação usando Servidor standard:   
[./docs/Rodando num servidor standard.md](./docs/Rodando%20num%20servidor%20standard.md)

* Licença Pública GNU v3  
[./LICENSE](./LICENSE)

