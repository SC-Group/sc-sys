<div class="input-group">
    <div class="input-group-prepend">
        <label class="input-group-text" for="inputGroupSelect01">{{$label}}</label>
    </div>
    <select data-value="{!! $value??old(''.$fieldid) !!}" {!! isset($dependency)?"data-dependency=".base64_encode(json_encode($dependency)):""!!}
    id="{{$uid}}" class="custom-select formDynamicSelect"  name="{{$fieldid}}" data-api="{{$api}}" data-js="{{$js}}">
    </select>
</div>
@error("$fieldid")
<div style="font-size: 9pt" class="text-danger">
    {{$message}}
</div>
@enderror
<div class="hide formDynamicSelect-option-model">
    <option class="hide" value="">
    </option>
</div>