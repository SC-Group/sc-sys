<div class="row">
    <div class="table-responsive col">
        <table class="table table-sm">
            <thead>
            <tr>
                <td>
                </td>
                @if(count($camCor) <=1 )
                    <td>
                        Número de Camisas
                    </td>
                @else
                    @foreach($camCor as $cor)
                        <td>
                            {{$cor}}
                        </td>
                    @endforeach
                @endif

            </tr>
            </thead>
            <tbody>
            @foreach($camTam as $tam)
                <tr>
                    <td>
                        {{$tam}}
                    </td>
                    @foreach($camCor as $cor)
                        <td>
                            <select name="camisas[{{$tipo}}][{{$tam}}][{{$cor}}]" class="custom-select custom-select-sm">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                            </select>
                        </td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>