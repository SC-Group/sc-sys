
<div class="col-12">
    @php
        $idTipo = "instituicao-$pessoaTipo-tipo";
        $selectTipo = $fc->dynamicselect("SelectInstituicao@tipos",[]);
        $selectTipo["fieldid"]=$idTipo;
        $selectTipo["uid"]=$idTipo;
        $selectTipo["value"]=$values["tipo"];
        $selectTipo['label'] = "Tipo";
    @endphp
    @include("regularpages.profile.components.dynamicSelect",$selectTipo)
</div>
<div class="col-12">
    @php
        $idUF = "instituicao-$pessoaTipo-uf";
        $selectUF = $fc->dynamicselect("SelectInstituicao@ufs",["tipo"=>$idTipo]);
        $selectUF["fieldid"] = $idUF;
        $selectUF["uid"]=$idUF;
        $selectUF["value"]=$values["uf"];
        $selectUF['label'] = "U.F.";
    @endphp
    @include("regularpages.profile.components.dynamicSelect",$selectUF)
</div>
<div class="col-12">
    @php
        $idID = "instituicao-$pessoaTipo-id";
        $selectInst = $fc->dynamicselect("SelectInstituicao@instituicoes",["tipo"=>$idTipo,"uf"=>$idUF]);
        $selectInst["fieldid"] = $idID ;
        $selectInst["uid"]= $idID;
        $selectInst['label'] = "Instituição";
        $selectInst["value"]=$values["id"];
    @endphp
    @include("regularpages.profile.components.dynamicSelect",$selectInst)
</div>
<div class="col-12">
    @php
        $idCurso = "instituicao-$pessoaTipo-curso";
        $selectCurso = $fc->dynamicselect("SelectInstituicao@cursos",["tipo"=>$idTipo,"uf"=>$idUF, "instituicao"=>$idID]);
        $selectCurso["fieldid"] = $idCurso;
        $selectCurso["uid"]= $idCurso;
        $selectCurso['label'] = "Curso";
        $selectCurso["value"]=$values["curso"];
    @endphp
    @include("regularpages.profile.components.dynamicSelect",$selectCurso)
</div>