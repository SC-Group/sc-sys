@php
    $dias = $evt["obj"]->dias()->get();
    $horas = $evt["obj"]->horas()->get();
@endphp
<div class="card">
    <form action="{{$action}}" method="post">
        @csrf
        <input type="hidden" name="year" value="{{$year}}">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h4 class="text-dark-sc-primary">
                        Disponibilidade
                    </h4>
                    <h5>
                        {{$titulo}}
                    </h5>
                </div>
            </div>
            <input type="hidden" name="id" value="{{\App\Helpers\IdHelper::enc([$tipousuario->id])}}">
            <div class="row">
                <div class="col table-responsive">
                    <table class="table table-sm">
                        <thead>
                        <tr>
                            <td></td>
                            @foreach($dias as $dia)
                                <td class="text-center">
                                    {{\Carbon\Carbon::parse($dia->data)->format("d/m")}}
                                </td>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($horas as $hora)
                            <tr>
                                <td style="white-space: nowrap">
                                    {{\Carbon\Carbon::parse($hora->inicio)->format("h:i")}} a {{\Carbon\Carbon::parse($hora->fim)->format("h:i")}}
                                </td>
                                @foreach($dias as $dia)
                                    @php
                                        $uuid = uniqid();
                                    @endphp
                                    <td class="">
                                        <div class="d-flex justify-content-center">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" value="1" name="disponibilidade[{{\App\Helpers\IdHelper::enc([$dia->id])}}][{{\App\Helpers\IdHelper::enc([$hora->id])}}]" {{$tipousuario->disponibilidades()->where("hora_id",$hora->id)->where("dia_id",$dia->id)->exists()?"checked":""}} class="custom-control-input" id="{{$uuid}}">
                                                <label class="custom-control-label"  for="{{$uuid}}"></label>
                                            </div>
                                        </div>
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col justify-content-end d-flex">
                    <button type="submit" class="btn btn-sc-primary">Salvar</button>
                </div>
            </div>
        </div>
    </form>
</div>