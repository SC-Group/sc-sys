@php
    $camMasc = $evt['obj']->camisas()->where('tipo','Masculina');
    $camFem = $evt['obj']->camisas()->where('tipo','Feminina');

    $camMascTam = $camMasc->distinct()->pluck("tamanho")->toArray();
    $camFemTam = $camFem->distinct()->pluck("tamanho")->toArray();
    $camMascCor = $camMasc->distinct()->pluck("cor")->toArray();
    $camFemCor = $camFem->distinct()->pluck("cor")->toArray();
@endphp

<div class="modal fade" id="modal-inscricao" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog   modal-lg modal-dialog-centered" role="document">
        <div class="modal-content bg-dark-sc-primary">
            <div class="modal-header">
                <h3 class="modal-title text-white" id="exampleModalLongTitle">Inscrição</h3>
                <button type="button" class="close  text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body bg-white">
                @if(boolval(env("BOT_ENABLED",false)))
                <div class="row text-dark-sc-primary">
                    <div class="col">
                        <h4>A semana da computação é feita com muito carinho para você.</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col" >
                        <div>
                            Por isso não queremos que você perca nada nesse evento. Iremos enviar para seu telefone, através da aplicação {{env('BOT_NAME',"Bot")}}, atualizações do evento pra você não perder nenhuma palestra. Para isso, basta marcar a caixa abaixo.
                        </div>
                        <div style="font-size: 10pt">
                            * Ao marcar o campo abaixo, você está concordando em compartilhar com a aplicação {{env('BOT_NAME',"Bot")}} seu
                            <strong>número de inscrição</strong>, <strong>nome</strong>, <strong>e-mail</strong> e <strong>número de telefone</strong>.
                        </div>
                    </div>
                </div>
                <div class="row mt-3 mb-5 text-dark-sc-primary">
                    <div class="col" >
                        <form>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" value="1" class="custom-control-input" id="botSet" name="bot">
                                <label class="custom-control-label" for="botSet">Desejo receber atualizações da aplicação {{env("BOT_NAME","Bot")}}</label>
                            </div>
                        </form>
                    </div>
                </div>
                @endif
                @if($evt['obj']->camisas()->exists())
                    <div class="row text-dark-sc-primary">
                        <div class="col">
                            <h4>Camisas:</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            @php
                                $precoantes = $evt["obj"]->preco_camisa_antes;
                                $precodepois = $evt["obj"]->preco_camisa_depois;
                            @endphp
                            <div class="text-dark mb-2">
                                Você gostaria de reservar camisas do evento e garantir o seu tamanho ideal?
                                Elas custarão apenas R${{$precoantes}} cada.
                                Se quiser, escolha o número de camisas a reservar, os tamanhos e o tipo.
                            </div>
                            <div class="mb-3" style="font-size: 10pt">
                                @if($precoantes!==$precodepois)
                                    <div class="mb-1">
                                        *Após o início do evento a camisa custará R${{$precodepois}}
                                    </div>
                                    <div>
                                        *A reserva da camisa a R${{$precoantes}} será confirmada apenas se o pagamento for efetuado antes do início do evento.
                                    </div>
                                @else
                                    <div>
                                        *A reserva da camisa será confirmada apenas se o pagamento for efetuado antes do início do evento.
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if($camFem->exists())
                        <div class="row mt-3">
                            <div class="col">
                                <h5>Modelo de camisas femininas</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col text-center">
                                <img  style="max-width: 100%; height: 300px" src="{{asset($evt["obj"]->url_img_camisas_fem)}}" alt="">
                            </div>
                        </div>
                    @endif
                    <div class="row mt-3 ">
                        <div class="col">
                            <h5>Modelo de camisas {{$camFem->exists()?"masculinas":""}}</h5>
                        </div>
                    </div>
                    <div class="row mb-5">
                        <div class="col text-center">
                            <img style="max-width: 100%; height: 300px" src="{{asset($evt["obj"]->url_img_camisas_masc)}}" alt="">
                        </div>
                    </div>

                    @if($camMasc->exists() || $camFem->exists())
                        <div class="row mb-3">

                            <div class="col">
                                <h5>Camisas {{$camFem->exists()?"masculinas":""}}</h5>
                                @include("regularpages.profile.components.camisasfield",["camTam"=>$camMascTam, "camCor"=>$camMascCor, "camisas"=>$camMasc, "tipo"=>"Masculina"])
                            </div>
                        </div>
                        @if($camFem->exists())
                        <div class="row mb-3">

                            <div class="col">
                                <h5>Camisas femininas</h5>
                                @include("regularpages.profile.components.camisasfield",["camTam"=>$camFemTam, "camCor"=>$camFemCor,"camisas"=>$camFem, "tipo"=>"Feminina"])
                            </div>
                        </div>
                        @endif
                    @endif
                @endif

            </div>
            <div class="modal-footer  bg-white">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" name="inscrever-com-camisas" class="btn btn-sc-primary-dark">Salvar e inscrever</button>
            </div>
        </div>
    </div>
</div>