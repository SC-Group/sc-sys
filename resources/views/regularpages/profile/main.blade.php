@php
    $fc = new \App\Http\Services\FormConstructorService()
@endphp
@extends('layouts.dashboard')
@section("pagename")
    Perfil
@endsection
@push('styles')
    <link rel="stylesheet" href="{{asset('/css/profile.css')}}">
@endpush
@php
    $user = Auth::user();
    $professor = $user->professor()->first();
    $aluno = $user->aluno()->first();
    $exufrj = $user->curso_id_ex_ufrj;
    $palestrante=null;
    $staff=null;
    $inscricao=null;
    $isInscTime=false;

    if($evt!==null && $evt["obj"]!==null){
        $inicioInsc = \Carbon\Carbon::parse($evt["obj"]->inicio_insc);
        $fimEvt = \Carbon\Carbon::parse($evt["obj"]->fim);
        $isInscTime = now()->gte($inicioInsc) && now()->lte($fimEvt);
        $inscricao = $user->inscricao()->where("evento_id",$evt["obj"]->id)->first();
        $palestrante = $user->palestrante()->where("evento_id",$evt["obj"]->id)->first();
        if($aluno!=null){
            $staff = $aluno->staff()->where("evento_id",$evt["obj"]->id)->first();
        }
    }
@endphp
@section("content")
    <div  class="main-container container-fluid px-4 text-muted">
        <div class="card">
            <div class="card-body">
                <form id="profile-form" action="{{route("profile.saveprofile",["year"=>$year])}}" method="post">
                    @csrf
                    <input type="hidden" name="year" value="{{$year}}">
                    <div class="row">
                        <div class="col">
                            <h3 class="card-title text-dark-sc-primary">Perfil de {{explode(" ",$user->name)[0]}}</h3>
                        </div>
                    </div>
                    @if(isset($msginfo))
                    <div class="mb-4 row">
                        <div class="col">
                            <div class="text-{{$msginfo["status"]}}">
                                {{$msginfo["msg"]}}
                            </div>
                        </div>
                    </div>
                    @endif
                    @error("inscricao")
                    <div class="row">
                        <div class="col">

                            <div style="font-size: 9pt" class="text-danger">
                                {{$message}}
                            </div>

                        </div>
                    </div>
                    @enderror
                    <div class="row">
                        <input type="hidden" name="tiposalvamento" value="salvar">
                        <div class="col-12 col-md-6 mb-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" >Nome</label>
                                </div>
                                <input type="text" value="{{old("nome")??$user->name}}" class="form-control" name="nome" placeholder="Nome Completo">
                            </div>
                            @error("nome")
                            <div style="font-size: 9pt" class="text-danger">
                                {{$message}}
                            </div>
                            @enderror
                        </div>

                        <div class="col-12 col-md-6 mb-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" >E-mail</label>
                                </div>
                                <input type="email" value="{{old("email")??$user->email}}" class="form-control" name="email" placeholder="E-mail">
                            </div>
                            @error("email")
                            <div style="font-size: 9pt" class="text-danger">
                                {{$message}}
                            </div>
                            @enderror
                        </div>

                        <div class="col-12 mb-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" >Telefone</label>
                                </div>
                                <input type="text" value="{{old("tel")??$user->tel}}" class="form-control" name="tel" placeholder="Telefone" data-inputmask="'mask':'(99) 99999-9999', 'removeMaskOnSubmit':true">
                            </div>
                            @error("tel")
                            <div style="font-size: 9pt" class="text-danger">
                                {{$message}}
                            </div>
                            @enderror
                        </div>

                        <div class="col-12 col-md-6 mb-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" >Senha</label>
                                </div>
                                <input type="password" class="form-control" name="password" placeholder="Senha">
                            </div>
                            @error("password")
                            <div style="font-size: 9pt" class="text-danger">
                                {{$message}}
                            </div>
                            @enderror
                        </div>

                        <div class="col-12 col-md-6 mb-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" >Confirmação de Senha</label>
                                </div>
                                <input type="password"  class="form-control" name="password_confirmation" placeholder="Confirmação">
                            </div>
                        </div>

                        <div class="col-12  mb-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" >Tipo</label>
                                </div>
                                @php
                                    $tipo = old("tipouser")??(($professor===null)?(($aluno===null)?(($exufrj===null)?"participante":"exufrj"):"aluno"):"professor");
                                @endphp

                                <select name="tipouser" id="tipouser" class="custom-select">
                                    <option {{$tipo==="participante"? "selected":""}} value="participante">Outro</option>
                                    <option {{$tipo==="aluno"? "selected":""}}  value="aluno">Estudante (Fundamental, Médio ou Graduação)</option>
                                    <option {{$tipo==="professor"? "selected":""}}  value="professor">Professor  (Fundamental, Médio ou Graduação)</option>
                                    <option {{$tipo==="exufrj"? "selected":""}}  value="exufrj">Ex aluno de graduação UFRJ</option>
                                </select>
                            </div>
                            @error("tipouser")
                            <div style="font-size: 9pt" class="text-danger">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>



                    {{-- Aluno --}}
                    <div id="aluno-profile"  class="d-none  mt-3">
                        <div class="row">
                            <div class="col">
                                <h4>
                                    Estudante
                                </h4>
                            </div>
                        </div>
                        <div class="row">
                            @php
                                $values=["tipo"=>old("instituicao-Aluno-tipo"),"uf"=>old("instituicao-Aluno-uf"),"id"=>old("instituicao-Aluno-id"),"curso"=>old("instituicao-Aluno-curso")];
                                if($aluno!==null){
                                    $instituicao = $aluno->instituicao()->first();
                                    $curso = $aluno->curso()->first();
                                    $values=["tipo"=>$instituicao->tipo,"uf"=>$instituicao->uf_sigla,"id"=>$instituicao->id,"curso"=>$curso->id];
                                }
                            @endphp
                            @include("regularpages.profile.components.instituicaoSelect",["pessoaTipo"=>"Aluno",
                                "values"=>$values

                            ])

                            <div id="aluno-dre" class="col-12 col-md {{($aluno==null || $instituicao->id!==1)?"d-none":""}}">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" >DRE</label>
                                    </div>
                                <input type="text" value="{{($aluno!==null?$aluno->ufrj_dre:"")}}" class="form-control" name="ufrj-dre" placeholder="DRE">
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" value="1" name="estagio" {{old("estagio")==="1"?"checked":(($aluno!==null && $aluno->estagio)?"checked":"")}} class="custom-control-input" id="customCheckDisabled">
                                    <label class="custom-control-label" for="customCheckDisabled">Estou interessado em participar de oportunidades de estágio</label>
                                </div>
                            </div>


                        </div>
                    </div>

                    {{-- Professor --}}
                    <div id="professor-profile"  class="d-none mt-3">
                        <div class="row">
                            <div class="col">
                                <h4>
                                    Professor
                                </h4>
                            </div>
                        </div>
                        <div class="row">
                            @php
                                $values=["tipo"=>old("instituicao-Professor-tipo"),"uf"=>old("instituicao-Professor-uf"),"id"=>old("instituicao-Professor-id"),"curso"=>old("instituicao-Professor-curso")];
                               if($professor!==null){
                                   $instituicao = $professor->instituicao()->first();
                                   $curso = $professor->curso()->first();
                                   $values=["tipo"=>$instituicao->tipo,"uf"=>$instituicao->uf_sigla,"id"=>$instituicao->id,"curso"=>$curso->id];
                               }
                            @endphp
                            @include("regularpages.profile.components.instituicaoSelect",["pessoaTipo"=>"Professor",
                                "values"=>$values

                            ])
                        </div>
                    </div>

                    {{-- Nada --}}
                    <div id="exufrj-profile" class="d-none  mt-3">
                        <div class="row">
                            <div class="col">
                                <h4>
                                    Ex aluno de graduação da UFRJ
                                </h4>
                            </div>
                        </div>
                        <div class="row">
                            <input type="hidden" name="instituicao-Participante" value="1">
                            <div class="col-12">
                                @php
                                    $idCurso = "instituicao-Participante-curso";
                                    $selectCurso = $fc->dynamicselect("SelectInstituicao@cursos",["instituicao"=>"instituicao-Participante"]);
                                    $selectCurso["fieldid"] = $idCurso;
                                    $selectCurso["uid"]= $idCurso;
                                    $selectCurso['label'] = "Curso feito";
                                    $selectCurso["value"]=$exufrj;
                                @endphp
                                @include("regularpages.profile.components.dynamicSelect",$selectCurso)
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 justify-content-end d-flex flex-wrap mt-3">
                            @if($inscricao===null && $isInscTime )
                                <button id="inscrever" style="white-space: normal" class="m-1 btn-sm text-right btn btn-sc-primary-dark">Salvar dados e me inscrever no evento de {{$year}}</button>
                            @endif
                            <button id="salvar"  style="white-space: normal"  class="m-1 text-right btn {{($inscricao===null && $isInscTime)?"btn-secondary btn-sm ":"btn-sc-primary-dark"}}">{{($inscricao===null && $isInscTime)?"Salvar sem me inscrever":"Salvar dados de usuário"}}</button>
                        </div>
                    </div>

                    @if($inscricao===null && $isInscTime)
                        @include("regularpages.profile.components.inscricao")
                    @endif
                </form>
            </div>
        </div>
        @if($palestrante!==null)
            @include("regularpages.profile.components.disponibilidade",["tipousuario"=>$palestrante, "titulo"=>"Palestrante", "action"=>route("profile.savepalestrante",["year"=>$year])])
        @endif
        @if($staff!==null)
            @include("regularpages.profile.components.disponibilidade",["tipousuario"=>$staff, "titulo"=>"Staff", "action"=>route("profile.savestaff",["year"=>$year])])
        @endif
    </div>


@endsection
@push("scripts")
    <script src="{{asset("/js/profile.js")}}"></script>
    @if(isset($inscnum))
        <script>
            $(document).ready(()=> {
                let postInscricaoInfo = {
                    status: "{{isset($status)?$status:""}}",
                    inscnum: "{{isset($inscnum)?$inscnum:""}}",
                    msgBot: "{{isset($msgBot)?$msgBot:""}}"
                };

                window.loadPostInfo(postInscricaoInfo);
            });
        </script>
    @endif

@endpush
