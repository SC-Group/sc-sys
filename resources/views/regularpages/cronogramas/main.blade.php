@extends('layouts.main',['setBG'=>true])

@push("styles")
    <style>
        body{
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
        ._LOGO{
            height: 130px;
            width: 100%;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center;
        }
    </style>
@endpush

@section('container')
    <div class="container-fluid">
        <div style="min-height: 100vh" class="row justify-content-center align-items-center">
            <div class="col-12" >
                <div class="card text-dark m-md-3" >
                    <div class="card-body">
                        <div class="row justify-content-center mb-4">
                            <div class="_LOGO"></div>
                        </div>
                        <div class="row">
                            <div id="cronogramas-container" class="text-muted container-fluid">
                                <div class="row">
                                    <div class="col text-dark-sc-primary">
                                        @if($page==="overview")
                                            <h3>Cronogramas do evento de {{$year}}</h3>
                                        @else
                                            @php
                                                $d = (new \Carbon\Carbon($dia->data))->format("d/m/Y")
                                            @endphp
                                            <h3>
                                                <a href="{{route("cronogramas",["year"=>$year])}}" class="btn btn-secondary mr-1"><i class="fa fa-arrow-left"></i></a>Cronograma do dia {{$d}}
                                            </h3>
                                        @endif

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        @if($page==="overview")
                                            @include("cronogramascomponents.overview")
                                        @else
                                            @include("cronogramascomponents.cronogramadia")
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('env')
    <meta name="root_event_url" content="{{route("comissao.evento",['year'=>"%%YEAR%%"])}}">
@endpush

@push("scripts")
    <script src="{{asset("/js/cronogramas.js")}}"></script>
@endpush
