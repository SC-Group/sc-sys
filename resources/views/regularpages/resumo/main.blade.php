@extends('layouts.dashboard')
@section("pagename")
    Resumo
@endsection
@push('styles')
    <link rel="stylesheet" href="{{asset('/css/resumo.css')}}">
@endpush
@php
    $user = Auth::user();
    if($evt!==null&&array_key_exists("obj",$evt)){
        $inscricao = $user->inscricao()->where("evento_id",$evt["obj"]->id);
    }

@endphp
@section("content")
    @if(isset($inscricao)&&$inscricao->exists())
        @php $egg = $inscricao->first()->egg(); @endphp
        @if($egg->exists() && !$egg->first()->exibido)
            @include('regularpages.resumo.egg',["egg"=>$egg->first()])
        @endif
    @endif

    <div  class="main-container container-fluid px-4 text-muted">
        <div class="card">
            <div class="card-body">
                <h4 class="text-sc-primary">
                    Olá {{explode(" ",$user->name)[0]}}! Que bom que você está aqui.
                </h4>
                <p>
                    Achamos que você vai adorar a Semana da Computação, nós fizemos com todo carinho pra você.<br>
                    Se você ainda não seguiu nossas redes sociais não perca tempo, corre lá que é bem rápido e você
                    fica por dentro de todas as novidades.
                </p>

                <div class="justify-content-center row mb-3">
                    <div class="mx-2">
                        <a target="_blank" href="http://fb.com/semanadacomputacaoUFRJ">
                            <img width="30" src="{{asset("/images/stdImages/social/facebook.png")}}" alt="">
                        </a>
                    </div>
                    <div class="mx-2">
                        <a target="_blank" href="http://instagram.com/semanadacomputacaoufrj">
                            <img width="30" src="{{asset("/images/stdImages/social/instagram.png")}}" alt="">
                        </a>
                    </div>
                </div>
                @if(\App\Models\Cronograma::whereHas("atividade",function(\Illuminate\Database\Eloquent\Builder $query){return $query->where("especial",false);})->exists())
                    <p>
                        Já estamos também com o nosso cronograma disponível, se quiser dar uma olhada, é só clicar no botão abaixo. <br>
                        <a href="{{route("cronogramas",["year"=>$year])}}" class=" mt-2 btn btn-sm btn-sc-primary" target="_blank">Abrir Cronogramas</a>
                    </p>
                @endif
            </div>
        </div>
        @if(isset($inscricao) && $inscricao->exists())
            @php
                $inscricao = $inscricao->first();
            @endphp
        <div class="card">
            <div class="card-body">
                <h4 class="card-title text-sc-primary">
                    Informações úteis para você:
                </h4>
                <p>
                    <ul>
                        <li>
                            <strong>
                                Número de inscrição no evento de {{$year}}:
                            </strong>
                            {{\App\Helpers\SCInscricao::get($inscricao->id)}}
                        </li>
                        <li>
                            <strong>
                                E-mail cadastrado:
                            </strong>
                            {{$user->email}}
                        </li>
                        @if($user->tel != null)
                            <li>
                                <strong>
                                    Número de telefone (Whatsapp) cadastrado:
                                </strong>
                                <span id="user-tel">{{$user->tel}}</span>
                            </li>
                        @endif
                        @if($inscricao->dias()->wherePivot("presenca","Presente")->exists())
                            <li>
                                <strong>
                                    Dias presentes (credenciamento):
                                </strong>
                                @foreach($inscricao->dias()->wherePivot("presenca","Presente")->get() as $dia)
                                    {{$loop->first?"":($loop->last?" e ":", ")}}{{\Carbon\Carbon::create($dia->data)->format("d/m/Y")}}
                                @endforeach
                            </li>
                        @endif
                    </ul>
                </p>
            </div>
        </div>
        @endif
        @if(isset($evt) && $evt["obj"]!==null && $evt["obj"]->fim!==null && \Carbon\Carbon::create($evt["obj"]->fim)->lt(now()))
            @php
                $participante=null;
                $atividades=null;
                $staff = null;
                $comissao = null;

                $user = auth()->user();
                $palestrante = $user->palestrante()->where("evento_id",$evt["obj"]->id);

                if($palestrante->exists()){
                    $atividades = $palestrante->first()->atividades()->where("aprovada",true)->wherePivot("presente",true);
                    if($atividades->exists()){
                        $atividades = $atividades->get();
                    }else{
                        $atividades=null;
                    }
                }

                $inscricao = $inscricao = $user->inscricao()->where("evento_id",$evt["obj"]->id);
                if($inscricao->exists()){
                    $participante = $inscricao->first()->dias()->wherePivot("presenca","Presente")->exists();
                }

//                $certificadoStaff;
//                $certificadoComissao;
            @endphp
            @if($participante!=null || $atividades!=null || $staff!=null || $comissao!=null)
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Gerar certificados</h4>
                        @if($participante!=null)
                            <div class="row">
                                <div class="col">
                                    <h6>Participante</h6>
                                    <a target="_blank" class="btn btn-outline-dark" href="{{route("certificados.participante",["year"=>$year])}}">Gerar Certificado</a>
                                </div>
                            </div>
                        @endif
                        @if($atividades!=null)
                            <div class="row">
                                <div class="col">
                                    <h6>Palestrante</h6>
                                    @foreach($atividades as $atividade)
                                        <a target="_blank" class="btn btn-outline-dark mx-2" href="{{route("certificados.palestrante",["year"=>$year,"cod"=>$atividade->codigo])}}">{{$atividade->nome}}</a>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endif
        @endif
        @if(isset($evt) && $evt["obj"]!==null && $evt["obj"]->eggs()->exists())
            @include("regularpages.resumo.eggcodinput")
        @endif
    </div>
@endsection

@push("scripts")
    <script !src="">
        $(document).ready(()=>{
            $("#user-tel").inputmask({'mask':'(99) 99999-9999'});
        })
    </script>
@endpush