@php
    use App\Models\Staff;
    use \Carbon\Carbon;
    $user = auth()->user();
    $evento = $evt["obj"];
    $isvalid = false;
    if(($evento!==null && $evento->fim!==null)){
        $isDateValid = Carbon::create($evento->fim)->gt(now());
        $isStaff = Staff::where("aluno_id",$user->id)->where("status","aprovado")->where("evento_id",$evento->id)->exists();
        $isComissao = $user->aluno()->first()!==null && $user->aluno()->first()->comissao_id !== null;
        $insc = $user->inscricao()->where("evento_id",$evento->id)->first();
        $isvalid = $insc!==null  && !$isStaff && !$isComissao && !$insc->egg()->exists() && $isDateValid;
    }

@endphp
@if($isvalid)
<div class="card">
    <form method="post" action="{{route("inserteggcod",["year"=>$year])}}">
        @csrf
        <input type="hidden" name="year" value="{{$year}}">
        <div class="card-body">
            <h4 class="card-title">Código Promocional (Eggs)</h4>
            <div class="row">
                <div class="col">
                    <p class="text-secondary">
                        Abaixo você poderá adicionar códigos promocionais que serão distribuidos durante o evento, através de gincanas, prêmios ou pistas
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    @error('year')
                        <div class="text-danger">Houve um erro ao tentar validar o evento</div>
                    @enderror
                    <div class=" mb-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Código</span>
                            </div>
                            <input type="text" class="form-control" name="eggcod" placeholder="Código" aria-label="Código" aria-describedby="basic-addon1">
                        </div>
                        @error('eggcod')
                            <div class="text-danger mt-1">{{ $message }}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-sc-primary-dark">Enviar</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endif