<div class="table-responsive cronogramas">
    <table>
        <thead>
            <tr>
                @php
                    $locaisString="";
                    $lengthLocais=[];
                    $numCols = \App\Models\Cronograma::whereHas('atividade', function (Illuminate\Database\Eloquent\Builder $query) {
                            $query->where('especial', false);
                        })->distinct()->count();
                @endphp
                <td rowspan="2" width="120"></td>
                @foreach($dias as $dia)
                    @php
                        $lengthLocais["$dia->id"]=0;
                        $numdayHasAtividade = $dia->cronogramas()->whereHas('atividade', function (Illuminate\Database\Eloquent\Builder $query) {
                            $query->where('especial', false);
                        })->count();
                    @endphp
                    @if($numdayHasAtividade>0 || (isset($editEnabled) && $editEnabled===true))
                        @php
                            $diaS =\Carbon\Carbon::parse($dia->data)->format("d/m");
                        @endphp
                        @php
                        foreach ($locais as $local){
                            $numlocalHasAtividade = $local->cronogramas()->where("dia_id",$dia->id)->whereHas('atividade', function (Illuminate\Database\Eloquent\Builder $query) {
                                $query->where('especial', false);
                            })->count();
                            if($numlocalHasAtividade>0 || (isset($editEnabled) && $editEnabled===true)){
                                $size=$numCols>0?100/$numCols:"auto";
                                $locaisString.="<td style=\"width:".$size."%\">{$local->nome}</td>";
                                $lengthLocais["$dia->id"]++;
                            }
                        }
                        @endphp
                        <td style="position: relative" colspan="{{$lengthLocais["$dia->id"]}}">{{$diaS}}<a href="{{url()->current()."/$diaS"}}" class="overCol"></a></td>
                    @endif
                @endforeach
            </tr>
            <tr>
                {!! $locaisString !!}
            </tr>
        </thead>
        <tbody>
            @foreach($horas as $hora)
                @include("cronogramascomponents.components.linecronograma",["dias"=>$dias])
            @endforeach
        </tbody>
    </table>
</div>

