<div class="table-responsive cronogramas cronograma-dia">
    <table>
        <thead>
        <tr>
            @php
                $editEnabled = isset($editEnabled)?$editEnabled:false;
                $locaisString="";

                if($editEnabled){
                    $numCols = sizeof($locais);
                }else{
                    $numCols = \App\Models\Cronograma::where("dia_id",$dia->id)->whereHas('atividade', function (Illuminate\Database\Eloquent\Builder $query) {
                        $query->where('especial', false);
                    })->distinct()->count();
                }
            @endphp
            <td rowspan="2" width="120"></td>

            @php
                $lengthLocais["$dia->id"]=0;
                $dayHasAtividade = $dia->cronogramas()->whereHas('atividade', function (Illuminate\Database\Eloquent\Builder $query) {
                    $query->where('especial', false);
                })->count();
            @endphp
            @if($dayHasAtividade || (isset($editEnabled) && $editEnabled===true))
                @php
                    $diaS =\Carbon\Carbon::parse($dia->data)->format("d/m");
                @endphp
                @php
                    foreach ($locais as $local){
                        $numlocalHasAtividade = $local->cronogramas()->where("dia_id",$dia->id)->whereHas('atividade', function (Illuminate\Database\Eloquent\Builder $query) {
                            $query->where('especial', false);
                        })->count();
                        if($numlocalHasAtividade>0 || (isset($editEnabled) && $editEnabled===true)){
                            $lengthLocais["$dia->id"]++;
                            $size=($numCols>0)?(100/$numCols):"";
                            $locaisString.="<td style=\"width:".$size."%\">{$local->nome}</td>";
                        }
                    }
                @endphp
                <td style="position: relative" colspan="{{$lengthLocais["$dia->id"]}}">{{$diaS}}</td>
            @endif

        </tr>
        <tr>
            {!! $locaisString !!}
        </tr>
        </thead>
        <tbody>
        @php
            $_atividades = [];
        @endphp
        @foreach($horas as $hora)
            @include("cronogramascomponents.components.linecronograma",["dias"=>[$dia]])
        @endforeach
        </tbody>
    </table>
</div>
@if(isset($editEnabled) && $editEnabled)
    @push("scripts")
        @php
            $atividades=[];
            $_atividades = $evt["obj"]->atividades()->where("aprovada",true)->get();
            foreach ($_atividades as $atividade){
                $_palestrantes = $atividade->palestrantes()->get();
                $palestrantes = [];
                foreach ($_palestrantes as $palestrante){
                    array_push($palestrantes,["nome"=>$palestrante->user()->first()->name]);
                }
                $trilha = $atividade->trilha()->first();
                $cor=null;
                if($trilha!=null){
                    $cor = json_decode($trilha->cor);
                }
                array_push($atividades,["id"=>\App\Helpers\IdHelper::enc([$atividade->id]),  "nome"=>$atividade->nome,"palestrantes"=>$palestrantes,"trilha"=>($trilha!=null?["cor"=>$cor,"nome"=>$trilha->nome]:null),"tipo"=>$atividade->tipo]);
            }

        @endphp
    <script>
        window.atividades=JSON.parse(`{!! json_encode($atividades) !!}`);
    </script>
    @endpush
@endif
