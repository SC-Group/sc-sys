<tr>
    @php
        $hi=\Carbon\Carbon::parse($hora->inicio)->format("H:i");
        $hf=\Carbon\Carbon::parse($hora->fim)->format("H:i");
        $previous = null;
        $numSpecial = 0;
        $editEnabled = isset($editEnabled)?$editEnabled:false;
    @endphp
    <td>
        {{$hi}} / {{$hf}}
    </td>
    @foreach($dias as $dia)

        @php
            $dayHasAtividade = $dia->cronogramas()->whereHas('atividade', function (Illuminate\Database\Eloquent\Builder $query) {
                $query->where('especial', false);
            })->count()>0;
            $cronograma = \App\Models\Cronograma::where("dia_id",$dia->id)->where("hora_id",$hora->id);
        @endphp

            @if($cronograma->exists())
                @if($editEnabled || $dayHasAtividade || $loop->last)
                    @php
                        $atividade = $cronograma->first()->atividade()->first();
                    @endphp
                    @if($atividade->especial)
                        @if($previous && $previous->especial)
                            @if($atividade->nome!==$previous->nome)
                                @include("cronogramascomponents.components.cronogramaespecial", ["atividade"=>$previous])
                                @php
                                    $numSpecial=0;
                                @endphp
                            @endif
                        @endif
                        @php
                          $numSpecial+=$lengthLocais["$dia->id"];
                        @endphp

                        @if($loop->last)
                            @include("cronogramascomponents.components.cronogramaespecial")
                            @php
                                $numSpecial=0;
                            @endphp
                        @endif

                    @else
                        @if($previous && $previous->especial)
                            @include("cronogramascomponents.components.cronogramaespecial")
                            @php
                                $numSpecial=0;
                            @endphp
                        @endif
                        @include("cronogramascomponents.components.cronogramaregular")
                    @endif
                    @php
                        $previous=$atividade;
                    @endphp
                @endif
            @else
                @if($previous && $previous->especial)
                    @include("cronogramascomponents.components.cronogramaespecial")
                    @php
                        $numSpecial=0;
                    @endphp
                @endif
                @if($editEnabled || $dayHasAtividade)
                    @include("cronogramascomponents.components.cronogramaregular")
                @endif
                @php
                    $previous = null;
                @endphp
            @endif

    @endforeach
</tr>
