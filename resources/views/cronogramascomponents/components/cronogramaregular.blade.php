@foreach ($locais as $local)

    @php
        $localHasAtividade = $local->cronogramas()->where("dia_id",$dia->id)->whereHas('atividade', function (Illuminate\Database\Eloquent\Builder $query) {
            $query->where('especial', false);
        })->count()>0;
        $cronograma_local = \App\Models\Cronograma::where("dia_id", $dia->id)->where("local_id",$local->id)->where("hora_id",$hora->id);
        $dataId = \App\Helpers\IdHelper::enc([$dia->id,$hora->id,$local->id]);
        $dataString = "data-id=\"$dataId\"
                data-dia=\"".\Carbon\Carbon::parse($dia->data)->format("d/m")."\"
                data-horainicio=\"".\Carbon\Carbon::parse($hora->inicio)->format("h:i")."\"
                data-horafim=\"".\Carbon\Carbon::parse($hora->fim)->format("h:i")."\"
                data-local=\"$local->nome\"";
    @endphp
    @if($localHasAtividade || (isset($editEnabled) && $editEnabled===true))
        @if($cronograma_local->exists())
            @php
                $atividade = $cronograma_local->first()->atividade()->first();
                $trilha = $atividade->trilha()->first();
                if($trilha!==null){
                    $cor = json_decode($trilha->cor);
                    $text = $cor->l>50?"#333":"#fefefe";

                }

                $_palestrantes = $atividade->palestrantes()->get();
                $palestrantes = [];
                foreach ($_palestrantes as $palestrante){
                    array_push($palestrantes,["nome"=>$palestrante->user()->first()->name]);
                }

                $atividade_data = ["id"=>\App\Helpers\IdHelper::enc([$atividade->id]), "nome"=>$atividade->nome,"palestrantes"=>$palestrantes, "trilha"=>($trilha!=null?["cor"=>$cor,"nome"=>$trilha->nome]:null), "tipo"=>$atividade->tipo];



            @endphp
            @push("styles")
                <style>

                    #cronograma-{{$dataId}}{
                        background-color: {{$trilha!==null?"hsl(".$cor->h.",".$cor->s."%,".$cor->l."%)":"white"}};
                        color:{{$trilha!==null?$text:"#777"}};
                        transition: box-shadow 100ms linear;
                        z-index:2;
                        padding: 4px;
                    }

                    #cronograma-{{$dataId}}:hover{
                        box-shadow:inset 0 0 0 4px {{$trilha!==null?"hsla(".$cor->h.",".$cor->s."%,".($cor->l>50?30:80)."%,0.7)":"rgba(0,0,0,0.3)"}};
                    }

                </style>
            @endpush
            @if ($editEnabled)
                <td {!! $dataString !!} data-atividade="{{ json_encode($atividade_data) }}" class='cellEdit' id="cronograma-{{$dataId}}"><div>{{$atividade->nome}}</div></td>
            @else
                <td {!! $dataString !!} data-atividade="{{ json_encode($atividade_data) }}"
                    class='cellShow' id="cronograma-{{$dataId}}"><div>{{$atividade->nome}}</div></td>
            @endif
        @else
            @if ($editEnabled)
                <td {!! $dataString !!} class='cellAdd'><i class="fa fa-plus"></i></td>
            @else
                <td class='cellEmpty'>-</td>
            @endif

        @endif
    @endif
@endforeach
