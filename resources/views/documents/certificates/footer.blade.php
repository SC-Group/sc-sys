@if(isset($apoios))
<div class="apoio cell">

    <div class="footer-item">
        Apoio
    </div>
    <div class="footer-content">
        @php
            $nivelSize = 75;
        @endphp
        @foreach($apoios as $apoio)
            @php
                $size = getimagesize(public_path()."$apoio->logo_url");
                $aspect = $size[0]/$size[1];
                if($aspect<0.28)$size=[$nivelSize*0.2, $nivelSize*0.8];
                if($aspect>=0.28&&$aspect<0.46)$size=[$nivelSize*0.3, $nivelSize*0.7];
                if($aspect>=0.46&&$aspect<0.74)$size=[$nivelSize*0.4, $nivelSize*0.6];
                if($aspect>=0.74&&$aspect<1.19)$size=[$nivelSize*0.5, $nivelSize*0.5];
                if($aspect>=1.19&&$aspect<1.92)$size=[$nivelSize*0.6, $nivelSize*0.4];
                if($aspect>=1.92&&$aspect<3.1)$size=[$nivelSize*0.7, $nivelSize*0.3];
                if($aspect>=3.1&&$aspect<5)$size=[$nivelSize, $nivelSize*0.3];
                if($aspect>=5)$size=[$nivelSize, $nivelSize*0.3];
            @endphp
            <img class="mx-2 footer-logo" style="max-width:{{$size[0]}}px;max-height:{{$size[1]}}px;" src="{{asset($apoio->logo_url)}}">
        @endforeach
    </div>
</div>
@endif
@if(isset($patrociniosniveis))
<div class="patrocinio cell">
    <div class="footer-item">
        Patrocinio
    </div>
    <div  style="border-left: 1px solid #ddd;" class="footer-content">
        @php
            $niveis = $patrociniosniveis;
            $nivelSize = 80;
        @endphp
        @foreach($niveis as $key=>$nivel)
            @foreach($nivel->patrocinadores()->get() as $patrocinador)
                @php
                    $size = getimagesize(public_path()."$patrocinador->url_logo");
                    $aspect = $size[0]/$size[1];

                    if($aspect<0.28)$size=[$nivelSize*0.2, $nivelSize*0.8];
                    if($aspect>=0.28&&$aspect<0.46)$size=[$nivelSize*0.3, $nivelSize*0.7];
                    if($aspect>=0.46&&$aspect<0.74)$size=[$nivelSize*0.4, $nivelSize*0.6];
                    if($aspect>=0.74&&$aspect<1.19)$size=[$nivelSize*0.5, $nivelSize*0.5];
                    if($aspect>=1.19&&$aspect<1.92)$size=[$nivelSize*0.6, $nivelSize*0.4];
                    if($aspect>=1.92&&$aspect<3.1)$size=[$nivelSize*0.7, $nivelSize*0.3];
                    if($aspect>=3.1&&$aspect<5)$size=[$nivelSize, $nivelSize*0.3];
                    if($aspect>=5)$size=[$nivelSize, $nivelSize*0.3];
                @endphp
                <img class="mx-2 footer-logo" style="max-width:{{$size[0]}}px;max-height:{{$size[1]}}px;" src="{{asset($patrocinador->url_logo)}}">
            @endforeach
        @endforeach
    </div>
</div>
@endif
