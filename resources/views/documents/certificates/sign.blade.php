<div class="cell">
    <div style="height: {{(1-$coordenador['linepos'])*70}}px" class="signimg">
        <img src="{{$coordenador['sign']}}" alt="">
    </div>
    <div class="signblock">
        -------------------------------------------------------------------------------------------------------------<br>
        {{$coordenador['name']}}<br>
        Coordenador de Ciência da Computação
    </div>
</div>

<div class="cell">
    <div style="height: {{(1-$chefe['linepos'])*70}}px" class="signimg">
        <img src="{{$chefe['sign']}}" alt="">
    </div>
    <div  class="signblock">
        -------------------------------------------------------------------------------------------------------------<br>
        {{$chefe['name']}}<br>
        Chefe do departamento de Ciência da Computação
    </div>
</div>


