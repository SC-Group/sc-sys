<div class="cell {{$tipo}}">
    @php
        $sc = "<span class='strong'>".($edicao??"1")."ª</span> edição da Semana da Computação da Universidade Federal do Rio de Janeiro, realizada durante o".($dias_da_semana_plural?"s":"")
            ." dia".($dias_da_semana_plural?"s":"")." <span class=\"strong\">$dias_da_semana</span> no Centro de Ciências Matemáticas e da Natureza";
    @endphp

    @if(isset($tipo)&&$tipo === "palestrante")
        <p>
            Certificamos que
            <span class='strong'>{{$nome}}</span>{!!isset($dre)?" DRE <span class='strong'>".$dre."</span>":""!!}{!!isset($organizacao)?", representante $tipoOrganizacao <span class='strong'>".$organizacao."</span>,":""!!}
            ministrou {{$tipoAtividade}} <span class='strong'>{{$atividade}}</span> na {!!$sc!!}.
        </p>
    @elseif(isset($tipo)&&$tipo === "comissao")
        <p>
            Certificamos que
            <span class='strong'>{{$nome}}</span>{!!isset($dre)?" DRE <span class='strong'>".$dre."</span>":""!!}
            participou da Comissão Organizadora da {!!$sc!!}
        </p>
    @elseif(isset($tipo)&&$tipo === "staff")
        <p>
            Certificamos que
            <span class='strong'>{{$nome}}</span>{!!isset($dre)?" DRE <span class='strong'>".$dre."</span>":""!!}
            participou durante os dias <span class='strong'>{{$dias}}</span> da equipe de Staffs da {!!$sc!!}.
        </p>
    @elseif(isset($tipo)&&$tipo === "participante")
        <p>
            Certificamos que
            <span class='strong'>{{$nome}}</span>{!!isset($dre)?" DRE <span class='strong'>".$dre."</span>":""!!}
            participou da {!!$sc!!}, comparecendo no{{$dias_da_semana_plural?"s":""}} dia{{$dias_da_semana_plural?"s":""}} relacionado{{$dias_da_semana_plural?"s":""}} abaixo.
        </p>
        <div class="atividades">
            @foreach($dias as $dia)
                <span class='strong'>•{{$dia}}• </span>
            @endforeach
        </div>
    @endif
</div>