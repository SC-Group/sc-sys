<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="{{public_path('/css/certificado.css')}}">

        <style>

            @font-face {
                font-family: "Roboto Regular";
                font-variant: normal;
                font-weight: normal;
                src: url({{public_path('fonts/roboto.ttf')}}) format('truetype');
            }
            @font-face {
                font-family: "rochester";
                font-variant: normal;
                font-weight: normal;
                src: url({{public_path('fonts/rochester.ttf')}}) format('truetype');
            }
            .etiqueta{
                background-image: url("{{asset($evt->url_img_bg)}}");
            }
        </style>
    </head>
    <body>
        <img  class="background" src="{{asset($evt->url_img_bg)}}" alt="">
        <div class="wrapcontainer">
            <div class="outlayer">
                <img class="inst_logo" src="{{'images/SC-2019-Logos/apoio/Minerva.png'}}" alt="">
                <div class="etiqueta">
                    <div>
                        <img src="{{asset($evt->url_img_logo)}}" alt="">
                    </div>
                    <div class="etiqueta_triangle"></div>
                </div>
            </div>
            <div class="wrapcontent">
                <div class="header table">
                    <div class="title row">
                        <div class="cell">
                            {{$title??'Certificado de Participação'}}
                        </div>
                    </div>
                </div>
                <div class="content table">
                    <div class="row">
                        @include('documents.certificates.content')
                    </div>
                </div>
                <div class="sign table">
                    <div class="row">
                        @include('documents.certificates.sign')
                    </div>
                </div>
                <div class="footer table">
                    <div class="row">
                        @include('documents.certificates.footer')
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>