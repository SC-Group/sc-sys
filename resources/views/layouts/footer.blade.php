<div class="container-fluid">
    <div class="row">
        @php
            $niveis=null;
            $apoios=null;
            if(isset($evt) && $evt["obj"]!=null){
                $niveis = $evt["obj"]->niveisPatrocinio()->has("patrocinadores")->orderBy("nivel","desc");
                $apoios = $evt["obj"]->apoios()->orderBy("id","desc");
            }
        @endphp
        @if($apoios!==null && $apoios->exists())
        <div class="footer-col col-lg-6 mb-5">
            <div class="header d-flex justify-content-center text-dark-sc-primary">
                <h4 class="title">Apoio:</h4>
            </div>
            <div class="body d-flex flex-wrap justify-content-center text-dark align-items-center">
                <div class="row footer-row-content justify-content-center align-items-center flex-wrap">
                        <div class="d-inline-flex col-12 justify-content-center align-items-center flex-wrap">
                            @php
                                $apoios = $apoios->get();
                                $nivelSize = 120;
                            @endphp
                            @foreach($apoios as $apoio)
                                @php
                                    $size = getimagesize(public_path()."$apoio->logo_url");
                                    $aspect = $size[0]/$size[1];

                                    if($aspect<0.28)$size=[$nivelSize*0.2, $nivelSize*0.8];
                                    if($aspect>=0.28&&$aspect<0.46)$size=[$nivelSize*0.3, $nivelSize*0.7];
                                    if($aspect>=0.46&&$aspect<0.74)$size=[$nivelSize*0.4, $nivelSize*0.6];
                                    if($aspect>=0.74&&$aspect<1.19)$size=[$nivelSize*0.5, $nivelSize*0.5];
                                    if($aspect>=1.19&&$aspect<1.92)$size=[$nivelSize*0.6, $nivelSize*0.4];
                                    if($aspect>=1.92&&$aspect<3.1)$size=[$nivelSize*0.7, $nivelSize*0.3];
                                    if($aspect>=3.1&&$aspect<5)$size=[$nivelSize, $nivelSize*0.3];
                                    if($aspect>=5)$size=[$nivelSize, $nivelSize*0.3];
                                @endphp
                                <div class="mx-2 footer-logo" style="width:{{$size[0]}}px;height:{{$size[1]}}px;background-image: url('{{asset($apoio->logo_url)}}')"></div>
                            @endforeach
                        </div>
                </div>
            </div>
        </div>
        @endif
        @if($niveis!==null && $niveis->exists() )
            @php
                $niveis = $niveis->get();
                $count = $niveis->count();
                if($count>0)
                $part = 1/$count;
            @endphp
            <div class="footer-col col-lg-6  mb-5">
                <div class="header d-flex justify-content-center text-dark-sc-primary">
                    <h4 class="title">Patrocínio:</h4>
                </div>
                <div class="body d-flex justify-content-center text-dark align-items-center">
                    <div class="row footer-row-content justify-content-center align-items-center flex-wrap">

                        @foreach($niveis as $key=>$nivel)
                            <div class="d-inline-flex col-12 justify-content-center align-items-center flex-wrap">
                            @php
                                $nivelSize = ($part*($count-$key)*(1-0.2)+0.2)*140;
                            @endphp
                            @foreach($nivel->patrocinadores()->get() as $patrocinador)
                                @php
                                    $size = getimagesize(public_path()."$patrocinador->url_logo");
                                    $aspect = $size[0]/$size[1];

                                    if($aspect<0.28)$size=[$nivelSize*0.2, $nivelSize*0.8];
                                    if($aspect>=0.28&&$aspect<0.46)$size=[$nivelSize*0.3, $nivelSize*0.7];
                                    if($aspect>=0.46&&$aspect<0.74)$size=[$nivelSize*0.4, $nivelSize*0.6];
                                    if($aspect>=0.74&&$aspect<1.19)$size=[$nivelSize*0.5, $nivelSize*0.5];
                                    if($aspect>=1.19&&$aspect<1.92)$size=[$nivelSize*0.6, $nivelSize*0.4];
                                    if($aspect>=1.92&&$aspect<3.1)$size=[$nivelSize*0.7, $nivelSize*0.3];
                                    if($aspect>=3.1&&$aspect<5)$size=[$nivelSize, $nivelSize*0.3];
                                    if($aspect>=5)$size=[$nivelSize, $nivelSize*0.3];
                                @endphp
                                <div class="mx-2 footer-logo" style="width:{{$size[0]}}px;height:{{$size[1]}}px;background-image: url('{{asset($patrocinador->url_logo)}}')"></div>
                            @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="row mt-3">
        <div class="col-lg-12">
            <div class="header d-flex justify-content-center text-dark-sc-primary">
                <h4 class="title">Realização:</h4>
            </div>
            <div class="body d-flex justify-content-center text-dark align-items-center">
                <div class="footer-logo _LOGO" style="width: 120px; height: 120px"></div>
            </div>
        </div>
    </div>
</div>