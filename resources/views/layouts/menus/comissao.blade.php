@php
    $links=[
        'Evento'=>'evento',
        'Eggs'=>'eggs',
        'Staffs'=>'staffs',
        "Cronogramas"=>"cronogramas",
        'Atividades'=>'atividades',
        "Patrocínios"=>"patrocinios",
        "Apoios"=>"apoios"
    ];
@endphp

@foreach($links as $name=>$route)
    <li>
        <a
                {{{ (($currroute=="comissao.".$route) ? 'menu-selected' : '')}}}
                href="{{route("comissao.".$route,["year"=>$year])}}">
            {{$name}}
        </a>
    </li>
@endforeach