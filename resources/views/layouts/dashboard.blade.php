@extends("layouts.main")
@push('styles')
    <link rel="stylesheet" href="{{asset("/frameworks/tempodominus/build/css/tempusdominus-bootstrap-4.min.css")}}" />
@endpush
@section("container")
    <div id="dashboard-wrapper">

        <!-- Sidebar -->
        @include('layouts.sidebar')
        <!-- /#sidebar-wrapper -->

        <div class="page-header-wrapper container-fluid">
            <div class="page-header border-sc-bg2 bg-t99-sc-bg2 row">
                <div class="menu-open-wrapper">
                    <a href="#menu-toggle" id="menu-toggle">
                        <span class="bg-sc-stress"></span>
                        <span class="bg-sc-stress"></span>
                        <span class="bg-sc-stress"></span>
                    </a>
                </div>
                <div class="col-3 p-1 col-sm-2">
                </div>
                <div class="pagename text-sc-stress text-center col">
                    @yield("pagename")
                </div>
                <div data-image="main-logo" class="_LOGO p-1 col-3 col-sm-2">
                </div>
            </div>
        </div>

        <!-- Page Content -->
        <div class="page-wrapper">
            <div class="page-content">

                <div  class="content-body p-0 m-0">
                    @yield('content')

                </div>
                 <div class="footer bg-white" >
                    @include('layouts.footer')
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

@endsection


