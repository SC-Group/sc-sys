@php
    $currroute = Route::currentRouteName();
    $abilities = [
       // "reprorg"=>"Repr. de Org.",
       // 'participante'=>"Participante",
       // "professor"=>"Professor",
       // 'palestrante'=>"Palestrante",
        'staff'=>"Staff",
        'comissao'=>"Comissão"
    ];
@endphp
<div class="sidebar-wrapper bg-sc-primary">
    <div class="sidebar-content">
        <div class="sidebar-brand">
            <a href="#">
                <img data-image="main-logo" class="main-logo"
                     src="
                     @if(isset($evt["obj"])&&(null!==$evt["obj"]->url_img_logo)&&(""!==$evt["obj"]->url_img_logo))
                        {{asset($evt["obj"]->url_img_logo)}}
                     @else
                        {{asset("/images/stdImages/logo.png")}}
                     @endif" alt="Semana da Computação">
            </a>
        </div>

        <div style="border-bottom:1px solid rgba(128,128,128,0.5)" class="sidebar-footer w-100 pt-0 p-2 d-flex bg-t1-sc-primary fg-sc-primary">
            <a class="btn" href="{{route('logout')}}">
                <i class="fa fa-sign-out-alt"></i>&nbsp;Sair
            </a>
        </div>
        <div id="menucontainer" style="margin-top:10px" class="sidebar-nav d-flex fg-sc-primary">
            <ul class="menu-section menu">
                <li>
                    <a {{{ (($currroute=='home') ? 'menu-selected' : '')}}} href="{{route("home",["year"=>$year])}}">Resumo</a>
                </li>
                <li>
                    <a {{{ (($currroute=='profile') ? 'menu-selected' : '')}}} href="{{route("profile",["year"=>$year])}}">Perfil</a>
                </li>
                {{--                        <li>--}}
                {{--                            <a {{{ (($currroute=='palestras') ? 'menu-selected' : '')}}} href="#">Atividades</a>--}}
                {{--                        </li>--}}
                {{--                        <li>--}}
                {{--                            <a {{{ (($currroute=='perfil') ?  'menu-selected' : '')}}} href="#">Perfil</a>--}}
                {{--                        </li>--}}
                {{--                        <li>--}}
                {{--                            <a {{{ (($currroute=='cronograma') ?  'menu-selected' : '')}}} href="#">CronogramaAtividade</a>--}}
                {{--                        </li>--}}
                @foreach ($abilities as $ability=>$name)
                    @can("be-".$ability)
                        <li {{{ ( (strpos($currroute, $ability) !== false) ? 'menu-opened' : '')}}}>
                            <a onclick="toggleMenuSections(this); return false;" href="#"><i style="margin-left: -20px" class="fa fa-caret-right"></i> {{$name}}</a>
                            <ul class="submenu-{{$ability}}">
                                @include('layouts.menus.'.$ability)
                            </ul>
                        </li>
                    @endcan
                @endforeach
            </ul>
        </div>
    </div>
</div>