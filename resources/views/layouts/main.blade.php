<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="local" content="{{asset("/")}}">
    @if(isset($year))<meta name="year" content="{{$year}}">@endif
    @stack("env")
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mukta:200,300,400" rel="stylesheet">
    <link rel="stylesheet" href="{{asset("frameworks/bootstrap/css/bootstrap.css")}}">
    <link rel="stylesheet" href="{{asset("frameworks/bootstrap/css/bootstrap-grid.css")}}">
    <link rel="stylesheet" href="{{asset("frameworks/bootstrap/css/bootstrap-reboot.css")}}">
    <link rel="stylesheet" href="{{asset("css/sc.css")}}">
    <link rel="stylesheet" href="{{asset("css/loader.css")}}">
    <link href="{{asset("fonts/fontawesome-free-5.0.6/web-fonts-with-css/css/fontawesome-all.css")}}" rel="stylesheet">
    @if(isset($cssUrl)&&($cssUrl!==null)&&($cssUrl!==""))
        <link rel="stylesheet" id="customCSS" href="{{asset($cssUrl)}}">
    @else
        <link rel="stylesheet" id="customCSS" href="{{asset("css/stdColors/colors.css")}}">
    @endif

    @stack("styles")
    <style>
        ._LOGO{
            background-image:url( "@if(isset($evt)&&filled($evt["obj"])&& filled($evt["obj"]->url_img_logo)){{asset($evt["obj"]->url_img_logo)}} @else  {{asset("/images/stdImages/logo.png")}} @endif" );
        }

        @if(isset($evt)&&filled($evt["obj"])&& filled($evt["obj"]->url_img_bg))

            ._BG{
                background-image: url("{{asset($evt["obj"]->url_img_bg)}}");
            }
        @endif


        body{

            background-repeat: no-repeat;
            background-position: center;
            background-size: cover !important;
        }
    </style>

</head>
<body class="bg-sc-bg1 @if(isset($setBG)&&$setBG) _BG @endif">
    <div class="modal text-dark fade" id="Modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <div class='loading-box'>
        <div class="loader">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div  class="alert-model d-none m-3 alert alert-dismissible fade" role="alert">
        <strong class="status"><span></span>: </strong><span class="msg"></span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="container-fluid alert-container" style="position:fixed !important;z-index:2000;top:0;width:100%"></div>

    <div class="p-0 m-0">
        @yield('container')
    </div>

    <script src="{{asset("frameworks/jquery-3.3.1.min.js")}}"></script>
    <script src="{{asset("frameworks/popper.min.js")}}"></script>
    <script src="{{asset("frameworks/bootstrap/js/bootstrap.js")}}"></script>
    <script src="{{asset("frameworks/bootstrap/js/bootstrap.bundle.js")}}"></script>
    <script src="{{asset("/frameworks/Inputmask-5.x/dist/jquery.inputmask.min.js")}}"></script>
    <script type="text/javascript" src="{{asset("/frameworks/moment-with-locales.min.js")}}"></script>
    <script type="text/javascript" src="{{asset("/frameworks/tempodominus/build/js/tempusdominus-bootstrap-4.min.js")}}"></script>
    <script src="{{asset('/js/app.js')}}"></script>
    @stack('scripts')
    <script>
        $(document).ready(function(){
            $(":input").inputmask();
            @if (session('msg'))
                Messages["{{session('status')?session('status'):"success"}}"]("{{session('msg')}}");
            @endif
        });
    </script>
    <script src="{{asset("/frameworks/ImageUpload/UploadImage.js")}}"></script>
</body>
</html>



