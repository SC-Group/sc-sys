
@foreach($colors as $name=>$color)
@php
    $name = "sc-$name";
@endphp
  /*Tabelas*/
.table-{{$name}},
.table-{{$name}} > th,
.table-{{$name}} > td {
    background-color: hsl({{$color['h']}},{{$color['s']}}%,90%);
}

.table-hover .table-{{$name}}:hover {
    background-color: hsl({{$color['h']}},{{$color['s']}}%,87%);
}

.table-hover .table-{{$name}}:hover > td,
.table-hover .table-{{$name}}:hover > th {
    background-color: hsl({{$color['h']}},{{$color['s']}}%,87%);
}

/*Botões*/
    @php $colorDark = ['h'=>$color['h'],'s'=>$color['s'],'l'=>30]; @endphp
  .btn-{{$name}}-dark {
  border-color: hsl({{$colorDark['h']}},{{$colorDark['s']}}%,{{($colorDark['l']>90)?30:$colorDark['l']}}%);
  color: hsl({{$colorDark['h']}},{{$colorDark['s']}}%,{{($colorDark['l']>80)?30:100}}%);
  background-color: hsl({{$colorDark['h']}},{{$colorDark['s']}}%,{{$colorDark['l']}}%);
  }

  .btn-{{$name}}-dark:hover {
  border-color: hsl({{$colorDark['h']}},{{$colorDark['s']}}%,{{($colorDark['l']>90)?20:(($colorDark['l']<20)?$colorDark['l']+10:$colorDark['l']-10)}}%);
  color: hsl({{$colorDark['h']}},{{$colorDark['s']}}%,{{($colorDark['l']>80)?30:100}}%);
  background-color: hsl({{$colorDark['h']}},{{$colorDark['s']}}%,{{($colorDark['l']<20)?$colorDark['l']+5:$colorDark['l']-5}}%);
  }

  .btn-{{$name}}-dark:focus, .btn-{{$name}}-dark.focus {
  box-shadow: 0 0 0 0.2rem hsla({{$colorDark['h']}},{{$colorDark['s']}}%,{{($colorDark['l']>80)?50:$colorDark['l']}}%,0.5);
  }

  .btn-{{$name}}-dark.disabled, .btn-{{$name}}-dark:disabled {
  color: hsl({{$colorDark['h']}},{{($colorDark['s']>40)?$colorDark['s']-20:$colorDark['s']}}%,40%);
  background-color: hsl({{$colorDark['h']}},{{($colorDark['s']>40)?$colorDark['s']-20:$colorDark['s']}}%,80%);/*#007bff;*/
  border-color: hsl({{$colorDark['h']}},{{($colorDark['s']>40)?$colorDark['s']-20:$colorDark['s']}}%,75%);
  }

  .btn-{{$name}}-dark:not(:disabled):not(.disabled):active, .btn-{{$name}}-dark:not(:disabled):not(.disabled).active,
  .show > .btn-{{$name}}-dark.dropdown-toggle {
  color: hsl({{$colorDark['h']}},{{$colorDark['s']}}%,{{($colorDark['l']>80)?30:100}}%);
  background-color: hsl({{$colorDark['h']}},{{$colorDark['s']}}%,{{($colorDark['l']<20)?$colorDark['l']+15:$colorDark['l']-15}}%);
  border-color: hsl({{$colorDark['h']}},{{$colorDark['s']}}%,{{($colorDark['l']<20)?$colorDark['l']+20:$colorDark['l']-20}}%);
  }

  .btn-{{$name}}-dark:not(:disabled):not(.disabled):active:focus, .btn-{{$name}}-dark:not(:disabled):not(.disabled).active:focus,
  .show > .btn-{{$name}}-dark.dropdown-toggle:focus {
  box-shadow: 0 0 0 0.2rem hsla({{$colorDark['h']}},{{$colorDark['s']}}%,{{($colorDark['l']>80)?50:$colorDark['l']}}%,0.5);
  }
  /**********/
  
.btn-{{$name}} {
    border-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>90)?30:$color['l']}}%);
    color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?30:100}}%);
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%);
}

.btn-{{$name}}:hover {
    border-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>90)?20:(($color['l']<20)?$color['l']+10:$color['l']-10)}}%);
    color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?30:100}}%);
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']<20)?$color['l']+5:$color['l']-5}}%);
}

.btn-{{$name}}:focus, .btn-{{$name}}.focus {
    box-shadow: 0 0 0 0.2rem hsla({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?50:$color['l']}}%,0.5);
}

.btn-{{$name}}.disabled, .btn-{{$name}}:disabled {
    color: hsl({{$color['h']}},{{($color['s']>40)?$color['s']-20:$color['s']}}%,40%);
    background-color: hsl({{$color['h']}},{{($color['s']>40)?$color['s']-20:$color['s']}}%,80%);/*#007bff;*/
    border-color: hsl({{$color['h']}},{{($color['s']>40)?$color['s']-20:$color['s']}}%,75%);
}

.btn-{{$name}}:not(:disabled):not(.disabled):active, .btn-{{$name}}:not(:disabled):not(.disabled).active,
.show > .btn-{{$name}}.dropdown-toggle {
    color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?30:100}}%);
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']<20)?$color['l']+15:$color['l']-15}}%);
    border-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']<20)?$color['l']+20:$color['l']-20}}%);
}

.btn-{{$name}}:not(:disabled):not(.disabled):active:focus, .btn-{{$name}}:not(:disabled):not(.disabled).active:focus,
.show > .btn-{{$name}}.dropdown-toggle:focus {
  box-shadow: 0 0 0 0.2rem hsla({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?50:$color['l']}}%,0.5);
}

.btn-outline-{{$name}} {
    color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%);
    background-color: transparent;
    background-image: none;
    border-color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%);
}

.btn-outline-{{$name}}:hover {
    color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?30:100}}%);
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%);
    border-color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%);
}

.btn-outline-{{$name}}:focus, .btn-outline-{{$name}}.focus {
    box-shadow: 0 0 0 0.2rem hsla({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%,0.5);
}

.btn-outline-{{$name}}.disabled, .btn-outline-{{$name}}:disabled {
    color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%);
    background-color: transparent;
}

.btn-outline-{{$name}}:not(:disabled):not(.disabled):active, .btn-outline-{{$name}}:not(:disabled):not(.disabled).active,
.show > .btn-outline-{{$name}}.dropdown-toggle {
    color: #fff;
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%);
    border-color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%);
}

.btn-outline-{{$name}}:not(:disabled):not(.disabled):active:focus, .btn-outline-{{$name}}:not(:disabled):not(.disabled).active:focus,
.show > .btn-outline-{{$name}}.dropdown-toggle:focus {
    box-shadow: 0 0 0 0.2rem hsla({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%,0.5);
}

.badge-{{$name}} {
    color: #fff;
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%);
}

.badge-{{$name}}[href]:hover, .badge-{{$name}}[href]:focus {
    color: #fff;
    text-decoration: none;
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']<20)?$color['l']+5:$color['l']-5}}%);
}

.alert-{{$name}} {
    color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']<30)?$color['l']+20:$color['l']-20}}%);
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']<70)?$color['l']+25:$color['l']-25}}%);
    border-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']<80)?$color['l']+15:$color['l']-15}}%);
}

.alert-{{$name}} hr {
    border-top-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']<80)?$color['l']+15:$color['l']-15}}%);
}

.alert-{{$name}} .alert-link {
    color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>50)?$color['l']-50:(($color['l']>30)?$color['l']-30:$color['l']+40)}}%);
}


.list-group-item-{{$name}} {
    color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>50)?$color['l']-50:(($color['l']>30)?$color['l']-30:$color['l']+40)}}%);
    background-color: hsl({{$color['h']}},{{$color['s']}}%,90%);
}

.list-group-item-{{$name}}.list-group-item-action:hover, .list-group-item-{{$name}}.list-group-item-action:focus {
    color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>50)?$color['l']-50:(($color['l']>30)?$color['l']-30:$color['l']+40)}}%);
    background-color: hsl({{$color['h']}},{{$color['s']}}%,87%);
}

.list-group-item-{{$name}}.list-group-item-action.active {
    color: #fff;
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>50)?$color['l']-50:(($color['l']>30)?$color['l']-30:$color['l']+40)}}%);
    border-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>50)?$color['l']-50:(($color['l']>30)?$color['l']-30:$color['l']+40)}}%);
}

.bg-dark-{{$name}}{
  background-color: hsl({{$color['h']}},{{$color['s']}}%,30%);
}
.bg-light-{{$name}}{
    background-color: hsl({{$color['h']}},{{$color['s']}}%,80%);
}
.text-dark-{{$name}}{
    color: hsl({{$color['h']}},{{$color['s']}}%,30%);
}

  a.text-dark-{{$name}}, .btn-link.text-dark-{{$name}}{
    color: hsl({{$color['h']}},{{$color['s']}}%,30%);
  }

  a.text-dark-{{$name}}:hover, a.text-dark-{{$name}}:focus,.btn-link.text-dark-{{$name}}:hover,.btn-link.text-dark-{{$name}}:focus {
    color: hsl({{$color['h']}},{{$color['s']}}%,50%);
  }

.border-light-{{$name}}{
    color: hsl({{$color['h']}},{{$color['s']}}%,80%);
}
.border-dark-{{$name}}{
    border-color: hsl({{$color['h']}},{{$color['s']}}%,30%);
}
.border-light-{{$name}}{
    border-color: hsl({{$color['h']}},{{$color['s']}}%,80%);
}

.bg-{{$name}}{
    border-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>90)?30:$color['l']}}%);
    color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?30:100}}%);
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%) !important;
}

.fg-{{$name}}, .fg-{{$name}} a{
  color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?30:100}}%);
}

.iconbg-{{$name}}{
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?30:100}}%) !important;
}

.fg-{{$name}} a:hover, .fg-{{$name}} a:focus{
  color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?30:100}}%);
  background-color: hsla({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?30:100}}%,0.1);
}

.fg-{{$name}} [menu-selected]{
    color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?30:100}}%);
    background-color: hsla({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?30:100}}%,0.3);
}

  a.bg-{{$name}}:hover, a.bg-{{$name}}:focus,
button.bg-{{$name}}:hover,
button.bg-{{$name}}:focus {
    color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']>80)?30:100}}%);
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']-10}}%) !important;
}

.border-{{$name}} {
    border-color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%) !important;
}


.text-{{$name}} {
    color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%) !important;
}

a.text-{{$name}}:hover, a.text-{{$name}}:focus {
    color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']-15}}%) !important;
}


.btn-link-{{$name}} {
    font-weight: 400;
    color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%);
    background-color: transparent;
}

.btn-link-{{$name}}:hover {
    color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']-15}}%);
    text-decoration: underline;
    background-color: transparent;
    border-color: transparent;
}

.btn-link-{{$name}}:focus, .btn-link-{{$name}}.focus {
    text-decoration: underline;
    border-color: transparent;
    box-shadow: none;
}

.btn-link-{{$name}}:disabled, .btn-link-{{$name}}.disabled {
    color: #6c757d;
}

.custom-control-input-{{$name}}:not(:disabled) ~ .custom-control-label-{{$name}}::before {
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']+20}}%) !important;
}
.custom-control-input-{{$name}}:checked ~ .custom-control-label-{{$name}}::before {
    color: #fff;
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%) !important;
}

.custom-control-input-{{$name}}:focus ~ .custom-control-label-{{$name}}::before {
    box-shadow: 0 0 0 1px #fff, 0 0 0 0.2rem hsla({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%,0.5);
}

.custom-control-input-{{$name}}:active ~ .custom-control-label-{{$name}}::before {
    color: #fff;
    background-color: hsl({{$color['h']}},{{$color['s']}}%,{{($color['l']<80)?$color['l']+15:$color['l']-15}}%);
}


  .custom-control-input-dark-{{$name}}:not(:disabled) ~ .custom-control-label-dark-{{$name}}::before {
    background-color: hsl({{$color['h']}},{{$color['s']}}%,40%) !important;
  }
  .custom-control-input-dark-{{$name}}:checked ~ .custom-control-label-dark-{{$name}}::before {
      color: #fff;
      background-color: hsl({{$color['h']}},{{$color['s']}}%,30%) !important;
  }

  .custom-control-input-dark-{{$name}}:focus ~ .custom-control-label-dark-{{$name}}::before {
    box-shadow: 0 0 0 1px #fff, 0 0 0 0.2rem hsla({{$color['h']}},{{$color['s']}}%,30%,0.5);
  }

  .custom-control-input-dark-{{$name}}:active ~ .custom-control-label-dark-{{$name}}::before {
      color: #fff;
      background-color: hsl({{$color['h']}},{{$color['s']}}%,80%);
  }


  .custom-control-input-{{$name}}:disabled ~ .custom-control-label-{{$name}} {
    color: #6c757d;
}

.custom-control-input-{{$name}}:disabled ~ .custom-control-label-{{$name}}::before {
    background-color: #e9ecef;
}

.bg-t5-{{$name}}{
    background-color:hsla({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%,0.5);
}
  .bg-t4-{{$name}}{
  background-color:hsla({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%,0.4);
  }
  .bg-t3-{{$name}}{
  background-color:hsla({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%,0.3);
  }
  .bg-t2-{{$name}}{
  background-color:hsla({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%,0.2);
  }
  .bg-t1-{{$name}}{
  background-color:hsla({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%,0.1);
  }
  .bg-t01-{{$name}}{
  background-color:hsla({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%,0.01);
  }
  .bg-t7-{{$name}}{
  background-color:hsla({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%,0.7);
  }
  .bg-t9-{{$name}}{
  background-color:hsla({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%,0.9);
  }
  .bg-t95-{{$name}}{
  background-color:hsla({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%,0.95);
  }
  .bg-t99-{{$name}}{
  background-color:hsla({{$color['h']}},{{$color['s']}}%,{{$color['l']}}%,0.99);
  }


@endforeach