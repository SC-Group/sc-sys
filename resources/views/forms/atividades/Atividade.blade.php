@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    $fields=[
            $fc->createField(
                'cadastro_atividade',
                __("view_forms_activity_subscription.Choose"),
                $fc->choices([
                    $fc->option(__("view_forms_activity_subscription.ChoiceNewActivity"),false,"nova"),
                    $fc->option(__("view_forms_activity_subscription.ChoiceExistentActivity"),false,"existente"),
                ]),
                true
            )

        ];
    $form = $fc->constructForm(5,__("view_forms_activity_subscription.ActivityTitle"),$fields);
@endphp
@section('form')
    @include('forms.components.formgroup',$form)
@endsection