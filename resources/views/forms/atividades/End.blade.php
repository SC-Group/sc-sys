@extends('layouts.main')
@section('container')
    <div style="height: 100vh; background-size: cover;background-repeat: no-repeat; background-position: center"
         class="_BG container-fluid d-flex flex-column justify-content-center">
        <div class="row w-100 d-flex align-self-center justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header bg-sc-primary">
                        {{__("messages.Success")}}!
                    </div>
                    <div class="card-body">
                        <div class="col-12 text-center text-dark">
                        @if(!$userExist)
                            <div class="row">
                                <div class="col">
                                    {!! __("view_forms_activity_subscription.End.SpeakerNumberText") !!}
                                </div>
                            </div>
                            <div class="row pt-3">
                                <div class="col">
                                    <span class="badge badge-danger p-2">
                                        <strong>P{{\App\Helpers\SCInscricao::get($palestrante)}}</strong>
                                    </span>
                                </div>
                            </div>
                            @else
                                <div class="row">
                                    <div class="col">
                                        {!! __("view_forms_activity_subscription.End.ActivitySubscriptionSuccess") !!}
                                    </div>
                                </div>
                            @endif
                            @if(isset($ativCode))
                                <div class="row">
                                    <div class="col mt-3">
                                        {!! __("view_forms_activity_subscription.End.ActivityCode") !!}
                                    </div>
                                </div>
                                <div class="row pt-3">
                                    <div class="d-flex col justify-content-center">
                                        <div style="width: 360px" class="input-group input-group-sm mb-3">
                                            <input id="atividade-codigo" readonly type="text" class="border-danger form-control disabled" value="{{$ativCode}}">
                                            <div class="input-group-append">
                                                <button id="atividade-codigo-button-copy" class="btn btn-danger" type="button">{{__("view_forms_activity_subscription.End.CopyButton")}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="row p-4  d-flex justify-content-center">
                        <div class="col-4 d-flex justify-content-center flex-wrap">
                            <a class="m-1 btn btn-sc-primary text-center" href="{{route('formatividade',['year'=>$year])}}">{{__("view_forms_activity_subscription.End.OtherActivitySubscriptionButton")}}</a>
                            <a class="m-1 btn btn-link-sc-primary text-center" href="{{route('home')}}">{{__("view_forms_activity_subscription.End.GoToStartPage")}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("scripts")
<script type="text/javascript">
    function copyInputToClipboard(input) {
        input.select();
        document.execCommand("copy");
        Messages.info("{{__("view_forms_activity_subscription.End.CopiedToClipboard")}}");
    }
    let copy_btn = $("#atividade-codigo-button-copy");
    copy_btn.click(function(){
        copyInputToClipboard($("#atividade-codigo")[0]);
    })
</script>
@endpush