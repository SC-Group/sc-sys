@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    use Carbon\Carbon;
    use App\Models\User;
    $userID = session()->get("_auth");
    $user = User::find($userID);
    $palestrante=null;
    if($user!=null){
        $palestrante = $user->palestrante()->where("evento_id",$evt["obj"]->id)->first();
    }

    $listDays=[];
    $listHoras=[];
    $listValues=[];
    $dias = $evt["obj"]->dias()->orderBy("data");
    $horas = $evt["obj"]->horas()->orderBy("inicio");
    if($dias->exists()){
        $dias = $dias->get();
        if($horas->exists()){
            $horas = $horas->get();
            foreach ($dias as $dkey=>$dia){
                $listDays[$dkey]=Carbon::parse($dia->data)->format("d/m");
                foreach ($horas as $hkey=>$hora){
                    $listHoras[$hkey]=Carbon::parse($hora->inicio)->format("H:i")." a ".Carbon::parse($hora->fim)->format("H:i");
                    if($palestrante!==null && $palestrante->disponibilidades()->where("dia_id",$dia->id)->where("hora_id",$hora->id)->exists()){
                        $listValues[$dkey][$hkey]="true";
                    }
                }
            }
        }

    }

    $fields=[
            $fc->createField(
                'disponibilidade',
                __("view_forms_activity_subscription.Availability.AvailabolityFieldTitle"),
                $fc->grid(
                    $fc->checkBoxes([
                        $fc->option('',false,"true")
                    ]),
                    $listDays,
                    $listHoras,
                    $listValues
                )
            )
        ];

    $form = $fc->constructForm(4,__("view_forms_activity_subscription.Availability.AvailabolityFieldTitle"),$fields,
        __("view_forms_activity_subscription.Availability.TextInfo")
    );
@endphp
@section('form')
    @include('forms.components.formgroup',$form)
@endsection