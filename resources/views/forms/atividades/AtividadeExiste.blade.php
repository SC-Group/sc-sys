@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    use App\Models\User;
    $userID = session()->get("_auth");
    $user = User::find($userID);
    $fields=[
            $fc->createField(
                'codigo',
                __("view_forms_activity_subscription.ActivityExists.ActivityCode"),
                $fc->shortAns(
                    'text',
                    null,
                    null,
                    null,
                    60
                ),
                true
            )

        ];
    $form = $fc->constructForm(6,__("view_forms_activity_subscription.ActivityExists.Title"),$fields);
@endphp
@section('form')
    @include('forms.components.formgroup',$form)
@endsection