@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    use App\Models\User;
    $userID = session()->get("_auth");
    $user = User::find($userID);
    $username = explode(" ",$user->name)[0];
    $fields=[
            $fc->createField(
                '',
                '',
                $fc->info(__("view_forms_activity_subscription.UserConfirmation.TextInfo",["username"=>$username]))
            ),

            $fc->createField(
                'confirm',
                __("view_forms_activity_subscription.UserConfirmation.Confirmation"),
                $fc->checkBoxes([
                    $fc->option(__("view_forms_activity_subscription.UserConfirmation.ConfirmFieldText"),false,"1"),
                ]),
                true
            )

        ];
    $form = $fc->constructForm(2,__("view_forms_activity_subscription.UserConfirmation.Title"),$fields);
@endphp
@section('form')
    @include('forms.components.formgroup',$form)
@endsection