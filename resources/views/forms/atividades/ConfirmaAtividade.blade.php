@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    use App\Models\Atividade;
    use App\Helpers\IdHelper;
    use App\Models\User;
    $userID = session()->get("_auth");
    $user = User::find($userID);
    $fields=[];
    if(!isset($atividade)){
        $atividade = Atividade::find(  IdHelper::dec( old('atividadeID') )[0]  );
    }

    $fields=[
            $fc->createHiddenField(
                'atividadeID',
                IdHelper::enc([$atividade->id])
            ),
            $fc->createField(
                '',
                '',
                $fc->info(__("view_forms_activity_subscription.Activity.Confirmation.TextInfo"))
            ),

            $fc->createField(
                '',
                __("view_forms_activity_subscription.Activity.Confirmation.ActivityData.FieldName"),
                $fc->info(__("view_forms_activity_subscription.Activity.Confirmation.ActivityData.FieldInfo",["type"=>$atividade->tipo, "name"=>$atividade->nome])),
                true
            )
     ];

    $form = $fc->constructForm(8,__("view_forms_activity_subscription.Activity.Confirmation.Title"),$fields);
@endphp
@section('form')
    @include('forms.components.formgroup',$form)
@endsection