@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    $formDescription = __("view_forms_activity_subscription.Internship.TextInfo");
   $fields=[
            $fc->createField(
                'estagio',
                __("view_forms_activity_subscription.Internship.InternshipFieldQuestion.Title"),
                $fc->choices([
                    $fc->option(__("view_forms_activity_subscription.Internship.InternshipFieldQuestion.OptionYes"),false,'1'),
                    $fc->option(__("view_forms_activity_subscription.Internship.InternshipFieldQuestion.OptionNo"),false,'0')
                ]),
                true
            )
       ];
   $form = $fc->constructForm(9,__("view_forms_activity_subscription.Internship.Title"),$fields,$formDescription);
@endphp

@section('form')
    @include('forms.components.formgroup',$form)
@endsection
