@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    use App\Models\Instituicao;
    $ufrj_id = Instituicao::where('sigla',"UFRJ")->first()->id;
    $formDescription = __("view_forms_activity_subscription.PersonalData.TextInfo");
    $fields=[];


    array_push($fields,$fc->createHiddenField(
                'instituicao',
                $ufrj_id
            ));


    array_push($fields,$fc->createField(
                'name',
                __("view_forms_activity_subscription.PersonalData.FullName"),
                $fc->shortAns(
                    'text',
                    '',
                    '',
                    $fc->createValidation('name')
                ),
                true
            ));


    array_push($fields,$fc->createField(
                'tel',
                __("view_forms_activity_subscription.PersonalData.CelPhone"),
                $fc->shortAns(
                     'tel',
                     "'mask':'(99) 99999-9999', 'removeMaskOnSubmit':true ",
                     ''
                 ),
                 false
            ));


    array_push($fields,$fc->createField(
                 'password',
                 __("view_forms_activity_subscription.PersonalData.Password"),
                 $fc->shortAns(
                     'password',
                     '',
                     '',
                     $fc->createValidation('password')
                 ),
                 true
             ));


    array_push($fields,$fc->createField(
                 'password_confirmation',
                 __("view_forms_activity_subscription.PersonalData.PasswordConfirmation"),
                 $fc->shortAns(
                     'password',
                     '',
                     ''
                 ),
                 true
             ));


    array_push($fields,$fc->createField(
                'category',
                __("view_forms_activity_subscription.PersonalData.Category.Title"),
                $fc->choices([
                    $fc->option(__("view_forms_activity_subscription.PersonalData.Category.Student"),false,"aluno"),
                    $fc->option(__("view_forms_activity_subscription.PersonalData.Category.ExStudent"),false,"ex_aluno"),
                    $fc->option(__("view_forms_activity_subscription.PersonalData.Category.Other"),false,"outro"),
                ]),
                true
            ));


    array_push($fields,$fc->createField(
                'dre',
                __("view_forms_activity_subscription.PersonalData.DRE.Title"),
                $fc->shortAns(
                    'text',
                    '',
                    '',
                    $fc->createValidation('^[1-9][0-9]{8}$','',__("view_forms_activity_subscription.PersonalData.DRE.Validation"))
                ),
                true,
                ["category"=>"^aluno$"]
            ));


    array_push($fields,$fc->createField(
                'curso',
                __("view_forms_activity_subscription.PersonalData.Graduation"),
                $fc->dynamicselect("SelectInstituicao@cursos",["instituicao"=>"instituicao"]),
                true,
                ["category"=>"aluno"]
            ));

    $form = $fc->constructForm(3,__("view_forms_activity_subscription.PersonalData.Title"),$fields,$formDescription);
@endphp

@section('form')
    @include('forms.components.formgroup',$form)
@endsection
