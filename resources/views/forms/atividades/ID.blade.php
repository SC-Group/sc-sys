@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')

@php
    $fields=[];

     array_push($fields,
             $fc->createField(
                 'email',
                 __("view_forms_activity_subscription.ID.EmailQuestion"),
                 $fc->shortAns(
                     'email',
                     '',
                     '',
                     $fc->createValidation('email')
                 ),
                 true
             )
      );

     $form = $fc->constructForm(0,__("view_forms_activity_subscription.ID.Title"),$fields);
@endphp
@section('form')
    @include('forms.components.formgroup',$form)
@endsection