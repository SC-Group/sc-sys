@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    use App\Models\User;
    $userID = session()->get("_auth");
    $user = User::find($userID);
    $username = explode(" ",$user->name)[0];
    $fields=[
            $fc->createField(
                '',
                '',
                $fc->info("Olá $username! certifique se de seus dados estão atualizados. Caso não estejam, acesse o sistema e atualize-os neste link e depois retorne a esta página para o cadastro da atividade. Certifique-se também de que você é um estudante de graduação com matrícula ativa na UFRJ, pois somente estudantes da UFRJ podem ser Staffs")
            ),

            $fc->createField(
                'confirm',
                __("view_forms_activity_subscription.UserConfirmation.Confirmation"),
                $fc->checkBoxes([
                    $fc->option("Confirmo que meus dados estão atualizados e que sou estudante com matrícula ativa da UFRJ",false,"1"),
                ]),
                true
            )

        ];
    $form = $fc->constructForm(2,__("view_forms_activity_subscription.UserConfirmation.Title"),$fields);
@endphp
@section('form')
    @include('forms.components.formgroup',$form)
@endsection