@extends('layouts.main')
@section('container')
    <div style="height: 100vh; background-size: cover;background-repeat: no-repeat; background-position: center"
         class="_BG container-fluid d-flex flex-column justify-content-center">
        <div class="row w-100 d-flex align-self-center justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header bg-sc-primary">
                        Sucesso !!
                    </div>
                    <div class="card-body">
                        <div class="col-12 text-center text-dark">
                            <div class="row">
                                <div class="col">
                                    <strong>OBS: você ja está inscrito automaticamente no evento de {{$year}}. Não se inscreva novamente no formulário de inscrição de participante</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    Abaixo está seu número de inscrição.<br>Guarde-o muito bem, pois ele será usado durante todo o evento:
                                </div>
                            </div>
                            <div class="row pt-3">
                                <div class="col">
                                    <span class="badge badge-danger p-2">
                                        <strong>{{\App\Helpers\SCInscricao::get($inscricao)}}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row p-4  d-flex justify-content-center">
                        <div class="col-4 d-flex justify-content-center flex-wrap">
                            <a class="m-1 btn btn-sc-primary text-center" href="{{route('forminscricao',['year'=>$year])}}">Fazer outra inscrição</a>
                            <a class="m-1 btn btn-link-sc-primary text-center" href="{{route('home')}}">Ir para Página Inicial</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
