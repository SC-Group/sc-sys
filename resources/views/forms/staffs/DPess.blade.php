@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    use App\Models\Instituicao;
    $ufrj_id = Instituicao::where('sigla',"UFRJ")->first()->id;
    $formDescription = __("view_forms_activity_subscription.PersonalData.TextInfo");
    $fields = [];

    array_push($fields,
        $fc->createHiddenField(
                'instituicao',
                $ufrj_id
            )
    );


    array_push($fields, $fc->createField(
                'name',
                __("view_forms_activity_subscription.PersonalData.FullName"),
                $fc->shortAns(
                    'text',
                    '',
                    '',
                    $fc->createValidation('name')
                ),
                true
            )  );


    array_push($fields, $fc->createField(
                'tel',
                __("view_forms_activity_subscription.PersonalData.CelPhone"),
                $fc->shortAns(
                     'tel',
                     "'mask':'(99) 99999-9999', 'removeMaskOnSubmit':true ",
                     ''
                 ),
                 false
            )  );


        if(boolval(env("BOT_ENABLED",false))){

            array_push($fields,
            $fc->createField(
                        '',
                        'A semana da computação é feita com muito carinho para você.',
                        $fc->info(
                            'Por isso não queremos que você perca nada nesse evento. Iremos enviar para seu telefone, através da aplicação '.env('BOT_NAME').', atualizações do evento pra você não perder nenhuma palestra. Basta marcar o campo abaixo.<br>
                            <span style="font-size:10pt">* Ao marcar o campo abaixo, você está concordando em compartilhar com a aplicação '.env('BOT_NAME').' seu
                            <strong>número de inscrição</strong>, <strong>nome</strong>, <strong>e-mail</strong> e <strong>número de telefone</strong>.
                            </span>'
                        ),
                        false,
                        ["tel"=>"[0-9]+"]
                    )
            );
            array_push($fields,
            $fc->createField(
                        'bot',
                        'Atualizações da aplicação '. env("BOT_NAME","Bot"),
                         $fc->checkBoxes([
                            $fc->option("Desejo receber atualizações da aplicação ". env("BOT_NAME","Bot")),
                        ]),
                        true,
                        ["tel"=>"[0-9]+"]
                    )
            );

        }

    array_push($fields, $fc->createField(
                 'password',
                 __("view_forms_activity_subscription.PersonalData.Password"),
                 $fc->shortAns(
                     'password',
                     '',
                     '',
                     $fc->createValidation('password')
                 ),
                 true
             )  );


    array_push($fields, $fc->createField(
                 'password_confirmation',
                 __("view_forms_activity_subscription.PersonalData.PasswordConfirmation"),
                 $fc->shortAns(
                     'password',
                     '',
                     ''
                 ),
                 true
             )  );


    array_push($fields, $fc->createField(
                'dre',
                __("view_forms_activity_subscription.PersonalData.DRE.Title"),
                $fc->shortAns(
                    'text',
                    '',
                    '',
                    $fc->createValidation('^[1-9][0-9]{8}$','',__("view_forms_activity_subscription.PersonalData.DRE.Validation"))
                ),
                true
            )  );


    array_push($fields, $fc->createField(
                'curso',
                __("view_forms_activity_subscription.PersonalData.Graduation"),
                $fc->dynamicselect("SelectInstituicao@cursos",["instituicao"=>"instituicao"]),
                true
            )  );


    array_push($fields, $fc->createField(
                'confirm',
                "Para ser staff da semana da computação, você precisa estar com matrícula ativa e ser aluno de graduação da UFRJ",
                $fc->checkBoxes([
                    $fc->option("Confirmo que sou aluno com matrícula ativa na UFRJ",false,"1"),
                ]),
                true
            )  );


    $form = $fc->constructForm(3,__("view_forms_activity_subscription.PersonalData.Title"),$fields,$formDescription);
@endphp

@section('form')
    @include('forms.components.formgroup',$form)
@endsection
