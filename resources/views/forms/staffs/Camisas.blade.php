@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    $camMascTam = $evt['obj']->camisas()->where('tipo','Masculina')->distinct()->pluck("tamanho")->toArray();
    $camFemTam = $evt['obj']->camisas()->where('tipo','Feminina')->distinct()->pluck("tamanho")->toArray();

    $camMascCor = $evt['obj']->camisas()->where('tipo','Masculina')->distinct()->pluck("cor")->toArray();
    $camFemCor = $evt['obj']->camisas()->where('tipo','Feminina')->distinct()->pluck("cor")->toArray();

    if(count($camMascCor)<=1){
        $camMascCor=[__("view_forms_activity_subscription.NumberOfTShirts")];
    }

    if(count($camFemCor)<=1){
        $camFemCor=[__("view_forms_activity_subscription.NumberOfTShirts")];
    }

    $preco_antes = $evt['obj']->preco_camisa_antes;
    $preco_depois = $evt['obj']->preco_camisa_depois;


    $fields=[];

    $temFem = false;
    $temMasc = false;
    if(count($camFemTam)>0){
        if(filled($evt['obj']->url_img_camisas_fem)){
            array_push($fields,$fc->createField(
                    'camisaFeminina',
                    __("view_forms_activity_subscription.FemaleTShirtTemplate"),
                    $fc->image(
                        asset($evt['obj']->url_img_camisas_fem),
                        "w-100",
                        "col-12 col-sm-9 col-md-7"
                    )
                )
            );
       }
        $temFem=true;
    }

    if(count($camMascTam)>0){
        if(filled($evt['obj']->url_img_camisas_masc)){
            array_push($fields,$fc->createField(
                    'camisaMasculina',
                    ($temFem?
                        __("view_forms_activity_subscription.MaleTShirtTemplate"):
                        __("view_forms_activity_subscription.TShirtTemplate")
                    ),
                    $fc->image(
                        asset($evt['obj']->url_img_camisas_masc),
                        "w-100",
                        "col-12 col-sm-9 col-md-7"
                    )
                )
            );
        }
        $temMasc = true;
    }

    if($temFem){
        array_push($fields,
            $fc->createField(
                'camisas_fem',
                __("view_forms_activity_subscription.FemaleTShirts"),
                $fc->grid(
                    $fc->select([
                        $fc->option('0',true),
                        $fc->option('1'),
                        $fc->option('2'),
                        $fc->option('3'),
                        $fc->option('4'),
                        $fc->option('5'),
                        $fc->option('6'),
                        $fc->option('7'),
                        $fc->option('8'),
                        $fc->option('9'),
                    ]),
                    $camFemTam,
                    $camFemCor
                )
            )
        );
    }

    if($temMasc){
        array_push($fields,
            $fc->createField(
                'camisas_masc',
                __("view_forms_activity_subscription.MaleTShirts"),
                $fc->grid(
                    $fc->select([
                        $fc->option('0',true),
                        $fc->option('1'),
                        $fc->option('2'),
                        $fc->option('3'),
                        $fc->option('4'),
                        $fc->option('5'),
                        $fc->option('6'),
                        $fc->option('7'),
                        $fc->option('8'),
                        $fc->option('9'),
                    ]),
                    $camMascTam,
                    $camMascCor
                )
            )
        );
    }


    if(!$temMasc&&!$temFem){
        $title=__("view_forms_activity_subscription.ThatsAll");
        $formDescription = __("view_forms_activity_subscription.ThatsAllWeNeed");
    }else{
        array_unshift($fields,
           $fc->createField('',
                __("view_forms_activity_subscription.DoYouWantToReserveTShirts.Title"),
                $fc->info("Se você for aprovado como Staff da Semana da computação de $year, você ganhará uma camisa de staff gratuitamente. As camisas oferecidas aqui são camisas para participantes que estarão a venda no evento e sua compra não é obrigatória. ".__("view_forms_activity_subscription.DoYouWantToReserveTShirts.CostBefore",["pricebefore"=>$preco_antes]))
            )
        );
        $title = __("view_forms_activity_subscription.DoYouWantToReserveTShirts.HaveYouSeenTShirt");
        if ($preco_antes === $preco_depois){
            $formDescription = __("view_forms_activity_subscription.DoYouWantToReserveTShirts.TShirtOBS1");
        }else{
            $formDescription = __("view_forms_activity_subscription.DoYouWantToReserveTShirts.TShirtOBS2",["pricebefore"=>$preco_antes,"priceafter"=>$preco_depois]);
        }

    }

   $form = $fc->constructForm(6,$title,$fields,$formDescription,true);
@endphp

@section('form')
    @include('forms.components.formgroup',$form)
@endsection
