@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php

    $emailValue = old("email",isset($emailValue)?$emailValue:null);
    $fields=[

            $fc->createHiddenField("email",$emailValue),
            $fc->createField(
                '',
                '',
                $fc->info(__("view_forms_activity_subscription.Login.TextInfo",["email"=>$emailValue]))
            ),

            $fc->createField(
                'password',
                __("view_forms_activity_subscription.Login.PasswordField"),
                $fc->shortAns(
                    'password',
                    ''
                ),
                true
            )

        ];
    $form = $fc->constructForm(1,__("view_forms_activity_subscription.Login.Title"),$fields);
@endphp
@section('form')
    @include('forms.components.formgroup',$form)
@endsection