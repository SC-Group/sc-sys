<div class="container-fluid">
    @component('forms.components.section',['lastSection'=>$lastSection,'filledData'=>$filledData,'fields'=>$section['fields'],'id'=>$section['id']])
        @slot('title')
            {{$section['title']}}
        @endslot
        @isset($section['description'])
            @slot('description')
                {!! $section['description'] !!}
            @endslot
        @endisset
    @endcomponent
</div>