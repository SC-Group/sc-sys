<div  {{$mandatory?'data-required':''}} data-title="{{$fieldid}}"
      {!! isset($showwhen)?"data-showwhen=".base64_encode(json_encode($showwhen)):""!!}
      {!! isset($loadOptions)?"data-load-options=".base64_encode(json_encode($loadOptions)):""!!}
      class="{{isset($showwhen)?'hideElmForm':''}} input-group mb-5 mt-5">
    @php
        $uid = uniqid();
    @endphp
    <div class="col-sm-12">

        <label for="{{$uid}}" class="text-dark">
            <h4 style="font-family: 'Mukta', sans-serif; font-weight: 300;">
                {{$title}}
                <span class="text-danger">{{$mandatory?'*':''}}</span>
                <div  style="padding-left:15px;text-indent:12pt; color:#aaa;font-size: 12pt">
                    {{$description}}
                </div>
            </h4>
        </label>

        @php $components['mandatory'] = $mandatory @endphp
        @component('forms.components.fields.'.$type,$components)
            @slot('title')
                {{$title}}
            @endslot
            @slot('uid')
                {{$uid}}
            @endslot
            @slot('fieldid')
                {{$fieldid}}
            @endslot
        @endcomponent
    </div>
    <span class="errormsg text-danger col-sm-12">
        <span>{{$errors->has("".$fieldid)? $errors->first("".$fieldid) :''}}</span>
    </span>
</div>