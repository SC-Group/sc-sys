
<form id="survey-form" style="min-height: 100vh" class="row justify-content-center align-items-center" method="post" target="_self">
    {{csrf_field()}}
    <input type="hidden" name="sectionid" value="{{ID::enc([$id])??old('sectionid')}}">
    <div class="col-md-10">
        <div class="card  bg-white border-0  mt-4 mb-4 ">
            <div class="card-header bg-sc-primary fg-sc-primary" style="font-family: 'Mukta', sans-serif; font-weight: 200;font-size: 18pt">{{$title}}</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 text-dark-sc-secondary mb-4" >
                        @isset($description)
                            {!! $description !!}
                        @endisset
                    </div>
                 </div>
                <div class="row">
                    <div class="col-sm-12 text-dark">
                        <strong class="text-danger">*</strong> Campos obrigatórios.
                    </div>
                </div>

                @foreach($fields as $field)
                    @if($field['type']!='hidden')
                        @php
                            $param = [
                                'fieldid'=>$field['fieldid'],
                                'type'=>$field['type'],
                                'mandatory'=> (isset($field['mandatory'])?$field['mandatory']:false)
                                ];
                            if(isset($field['components'])){
                                $param['components']=$field['components'];
                            }
                            $param['showwhen'] = $field['showwhen']??null;
                            $param['loadOptions'] = $field['loadOptions']??null;
                        @endphp
                        @component('forms.components.inputgroup',$param)
                            @slot('title')
                                {{$field['title']}}
                            @endslot
                            @slot('description')
                                {{$field['description']??''}}
                            @endslot

                        @endcomponent
                    @else
                        <input type="hidden" value="{{$field['value']}}" name="{{$field['fieldid']}}">
                    @endif
                @endforeach
            </div>
            <div class="card-footer text-muted">
                @if($id>=1)
                    <input name="buttonSend" formnovalidate="" type="submit" class="btn btn-light btn-outline-dark mr-5" value="{{__("views.FormPreviousButton")}}">
                @endif
                <input name="buttonSend" type="submit" class="btn btn-sc-stress-dark" value="{{$lastSection == true?__("views.FormFinishButton"):__("views.FormNextButton")}}">
            </div>
        </div>
    </div>
</form>
