<div class="row justify-content-center">
    <div class="{{$classcontainer??"col-sm-12"}}">
        <img src="{{$src}}" alt="" class="{{$class}}">
    </div>
</div>