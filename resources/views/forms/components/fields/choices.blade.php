@foreach($options as $option)
    @php
        $uid = uniqid();
        $value = $option['value']??$option['label'];
        if(old(''.$fieldid)==$value || isset($option['checked'])&&$option['checked']) $checkfield = 'checked';
        else  $checkfield = '';
    @endphp
    <div class="custom-control custom-radio mb-3">
        <input type="radio" class="custom-control-input custom-control-input-dark-sc-primary bg-dark-sc-primary" name="{{$fieldid}}" id="{{$uid}}" value="{{$value}}" {{$checkfield}}>
        <label class="custom-control-label custom-control-label-dark-sc-primary text-dark" for="{{$uid}}">{{$option['label']}}</label>
    </div>
@endforeach