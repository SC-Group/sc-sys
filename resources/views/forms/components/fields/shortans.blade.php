<input id="{{$uid}}"
       {!! isset($validation)?"data-validation=".base64_encode(json_encode($validation)):""!!}
       type="{{$type??''}}"
       class="form-control  {{$errors->has("".$title)? 'border-danger' :''}}"
       name='{{$fieldid}}'
       value="{{old(''.$fieldid)??$value}}"
       {{$mandatory?'required':''}}
       aria-label="{{$title}}"
       maxlength="{{$maxlen}}"
       aria-describedby="basic-addon1"
       @if($type!=='password'&&isset($mask)&&$mask!="")
              data-inputmask="{{$mask??''}}"
       @endif
>
