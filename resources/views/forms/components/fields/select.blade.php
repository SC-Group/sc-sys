
<div  class="form-group">
    <select id="{{$uid}}" class="custom-select"  name="{{$fieldid}}">
    @foreach($options as $option)
        <option
                value="{{$option['value']??$option['label']}}"
                {{(isset($option['checked'])&&$option['checked']?'checked':'')}}
        >
            {{$option['label']}}
        </option>
    @endforeach
    </select>
</div>