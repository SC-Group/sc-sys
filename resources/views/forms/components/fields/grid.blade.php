<div class="grid d-flex table-responsive">
    <table class="table text-dark">
        <thead>
            <tr>
                <td></td>
                @foreach($collabels as $k=>$c)
                    <td>
                        {{$c}}
                    </td>
                @endforeach
            </tr>
        </thead>
        <tbody>
        @foreach($rowlabels as $k=>$r)
            <tr>
                <td>
                    {{$r}}
                </td>
                @foreach($collabels as $l=>$c)
                    @php
                        $ncomponents = $components;
                        if($values!=null){
                            if(array_key_exists($k,$values) && array_key_exists($l,$values[$k])){
                                if(array_key_exists("options",$ncomponents)){
                                    foreach ($ncomponents["options"] as $key=>$option){
                                        if($option["value"]!=null){
                                            if($option["value"]==$values[$k][$l]){
                                                $ncomponents["options"][$key]["checked"]=true;
                                            }
                                        }else{
                                            if($option["label"]==$values[$k][$l]){
                                                $ncomponents["options"][$key]["checked"]=true;
                                            }
                                        }
                                    }
                                }else{
                                    $ncomponents["value"]=$values[$k][$l];
                                }
                            }
                        }
                    @endphp
                    <td>
                        @component('forms.components.fields.'.$type,$ncomponents)
                            @slot('title')
                                {{$title}}
                            @endslot
                            @slot('uid')
                                {{$uid}}-{{$r}}-{{$c}}
                            @endslot
                            @slot('fieldid')
                                {{$fieldid."[".$r."][".$c."]"}}
                            @endslot
                        @endcomponent
                    </td>
                @endforeach
            </tr>
        @endforeach
        </tbody>

    </table>
</div>