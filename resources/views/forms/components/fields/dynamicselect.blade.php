
<div  class="form-group">
    <div class="hide formDynamicSelect-option-model">
        <option class="hide" value="">
        </option>
    </div>
    <select data-value="{{$value??old(''.$fieldid)}}" {!! isset($dependency)?"data-dependency=".base64_encode(json_encode($dependency)):""!!}
            id="{{$uid}}" class="custom-select formDynamicSelect"  name="{{$fieldid}}" data-api="{{$api}}" data-js="{{$js}}">
    </select>
</div>