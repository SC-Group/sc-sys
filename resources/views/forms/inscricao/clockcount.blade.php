@extends("layouts.main")
@push('styles')
    <link rel="stylesheet" href="{{asset("css/clockcount_style.css")}}">
@endpush
@section("container")
    <div class="back _BG"></div>
    <div class="text-light container-wrapper container-fluid d-flex content-clock">
       <div class="container-fluid align-self-md-center w-100 content-clock-inner">
           <div class="row clock-title">
               <div class="col-12 text-center">
                   Faltam
               </div>
           </div>
            <div class="row">
                <div class="col-12 flex-wrap d-flex justify-content-center">
                    <div class="d-flex">
                        <div class="circle">
                            <div>
                                0
                            </div>
                            <div>
                                Dias
                            </div>
                        </div>
                        <div class="circle">
                            <div>
                                0
                            </div>
                            <div>
                                Horas
                            </div>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="circle">
                            <div>
                                0
                            </div>
                            <div>
                                Minutos
                            </div>
                        </div>
                        <div class="circle">
                            <div>
                                0
                            </div>
                            <div>
                                Segundos
                            </div>
                        </div>
                    </div>

                </div>
            </div>
           <div class="row">
               <div class="col-12 text-center">
                   <div>Para começarem as inscrições</div>
                   Não recarregue a página! Nós fazemos isso pra você.
               </div>
           </div>
       </div>
    </div>
@endsection
@push('scripts')
    <script>
        window.timeNow = new Date("{{$now}}");
        window.inscStart = new Date("{{$evt['obj']->inicio_insc}}");
    </script>
    <script src="{{asset("/js/form.js")}}"></script>
@endpush