@extends('layouts.main',['setBG'=>true])
@section("container")
    @yield('form')
@endsection
@push('scripts')
    <script src="{{asset('/js/form.js')}}"></script>
@endpush