@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    $ufrj_id = \App\Models\Instituicao::where('sigla','=','UFRJ')->first()->id;
    $formDescription = "";
    $fields=[
             $fc->createHiddenField(
                'instituicao',
                $ufrj_id
            ),
            $fc->createField(
                'exAluno',
                'Eu sou ex-aluno de graduação da UFRJ',
                $fc->choices([
                    $fc->option("sim",false,1),
                    $fc->option("não",false,0)
                ]),
                true
            ),
            $fc->createField(
                'curso',
                'Fiz Graduação de ...',
                $fc->dynamicselect("SelectInstituicao@cursos",["instituicao"=>"instituicao"]),
                true,
                ["exAluno"=>"1"]
            ),
        ];
    $form = $fc->constructForm(5,'Dados Pessoais',$fields,$formDescription);
@endphp

@section('form')
    @include('forms.components.formgroup',$form)
@endsection
