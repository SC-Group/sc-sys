@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    $formDescription = "Bem vindo, Professor! Queremos saber um pouco mais sobre onde você leciona";
    $fields=[

             $fc->createField(
                'instituicaoTipo',
                'Eu sou professor de ...',
                $fc->dynamicselect("SelectInstituicao@tipos",[]),
                true
            ),
             $fc->createField(
                'instituicaoUF',
                'Leciono no estado de  ...',
                $fc->dynamicselect("SelectInstituicao@ufs",["tipo"=>"instituicaoTipo"]),
                true
            ),
             $fc->createField(
                'instituicaoID',
                'Leciono no/na ... (Selecione Abaixo)',
                $fc->dynamicselect("SelectInstituicao@instituicoes",["tipo"=>"instituicaoTipo","uf"=>"instituicaoUF"]),
                true
            ),
            $fc->createField(
                'instituicaoCursoGraduacao',
                'Leciono no curso de graduação de ... (Selecione Abaixo)',
                $fc->dynamicselect(
                    "SelectInstituicao@cursos",
                    [
                        "tipo"=>"instituicaoTipo",
                        "uf"=>"instituicaoUF",
                        "instituicao"=>"instituicaoID"
                    ]
                ),
                true,
                ["instituicaoTipo"=>"Universidade"]
            ),
            $fc->createField(
                'instituicaoCursoTecnico',
                'Leciono no curso técnico de ... (Selecione Abaixo)',
                $fc->dynamicselect("SelectInstituicao@cursos",["tipo"=>"instituicaoTipo","uf"=>"instituicaoUF","instituicao"=>"instituicaoID"]),
                true,
                ["instituicaoTipo"=>"Escola de Ensino Técnico"]
            )
        ];
    $form = $fc->constructForm(4,'Dados Pessoais',$fields,$formDescription);
@endphp

@section('form')
    @include('forms.components.formgroup',$form)
@endsection
