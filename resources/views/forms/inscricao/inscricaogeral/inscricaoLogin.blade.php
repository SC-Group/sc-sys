@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
if(!isset($emailValue)){
    if(session("emailValue")){
        $emailValue=session("emailValue");
    }else{
        $emailValue="definido";
    }
}
    $fields=[
            $fc->createField(
                '',
                '',
                $fc->info(
                    "Parece que o email $emailValue já existe em nosso banco de dados, confirme sua senha para continuarmos"
                )
            ),

            $fc->createHiddenField(
                'email',
                $emailValue
            ),

            $fc->createField(
                'password',
                'Senha',
                $fc->shortAns(
                    'password',
                    ''
                ),
                true
            )

        ];
    $form = $fc->constructForm(1,'Login',$fields);
@endphp
@section('form')
    @include('forms.components.formgroup',$form)
@endsection