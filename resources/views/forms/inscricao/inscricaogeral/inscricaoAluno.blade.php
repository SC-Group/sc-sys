@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    $ufrj_id = \App\Models\Instituicao::where('sigla','=','UFRJ')->first()->id;

    $formDescription = "Olá Aluno! Queremos saber um pouco mais sobre onde você estuda";
    $fields=[

             $fc->createField(
                'instituicaoTipo',
                'Eu sou estudante de ...',
                $fc->dynamicselect("SelectInstituicao@tipos",[]),
                true
            ),
             $fc->createField(
                'instituicaoUF',
                'Estudo no estado de  ...',
                $fc->dynamicselect("SelectInstituicao@ufs",["tipo"=>"instituicaoTipo"]),
                true
            ),
             $fc->createField(
                'instituicaoID',
                'Estudo no/na ... (Selecione Abaixo)',
                $fc->dynamicselect("SelectInstituicao@instituicoes",["tipo"=>"instituicaoTipo","uf"=>"instituicaoUF"]),
                true
            ),
            $fc->createField(
                'instituicaoCursoGraduacao',
                'Faço o curso de graduação de ... (Selecione Abaixo)',
                $fc->dynamicselect(
                    "SelectInstituicao@cursos",
                    [
                        "tipo"=>"instituicaoTipo",
                        "uf"=>"instituicaoUF",
                        "instituicao"=>"instituicaoID"
                    ]
                ),
                true,
                ["instituicaoTipo"=>"Universidade"]
            ),
            $fc->createField(
                'instituicaoCursoTecnico',
                'Faço o curso técnico de ... (Selecione Abaixo)',
                $fc->dynamicselect("SelectInstituicao@cursos",["tipo"=>"instituicaoTipo","uf"=>"instituicaoUF","instituicao"=>"instituicaoID"]),
                true,
                ["instituicaoTipo"=>"Escola de Ensino Técnico"]
            ),
             $fc->createField(
                'DRE',
                'Meu DRE é ... (Insira Abaixo)',
                $fc->shortAns(
                    'text',
                    '',
                    '',
                    $fc->createValidation('^[1-9][0-9]{8}$','',"Esse DRE não é válido")
                ),
                true,
                ["instituicaoID"=>"^".$ufrj_id."$"]
            )

        ];
    $form = $fc->constructForm(3,'Dados Pessoais',$fields,$formDescription);
@endphp

@section('form')
    @include('forms.components.formgroup',$form)
@endsection
