@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    $formDescription = "Por isso, teremos várias empresas à sua disposição, para falar sobre seus processos seletivos e sua cultura.";
   $fields=[
            $fc->createField(
                'estagio',
                'Você gostaria de participar das oportunidades de estágio?',
                $fc->choices([
                    $fc->option("Claro, adoraria!!",false,'1'),
                    $fc->option("Não, obrigado",false,'0')
                ]),
                true
            )
       ];
   $form = $fc->constructForm(6,"Sabemos que estagiar é muito importante",$fields,$formDescription);
@endphp

@section('form')
    @include('forms.components.formgroup',$form)
@endsection
