@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php
    $camMascTam = $evt['obj']->camisas()->where('tipo','Masculina')->distinct()->pluck("tamanho")->toArray();
    $camFemTam = $evt['obj']->camisas()->where('tipo','Feminina')->distinct()->pluck("tamanho")->toArray();

    $camMascCor = $evt['obj']->camisas()->where('tipo','Masculina')->distinct()->pluck("cor")->toArray();
    $camFemCor = $evt['obj']->camisas()->where('tipo','Feminina')->distinct()->pluck("cor")->toArray();

    if(count($camMascCor)<=1){
        $camMascCor=['Número de Camisas'];
    }

    if(count($camFemCor)<=1){
        $camFemCor=['Número de Camisas'];
    }

    $preco_antes = $evt['obj']->preco_camisa_antes;
    $preco_depois = $evt['obj']->preco_camisa_depois;


    $fields=[];

    $temFem = false;
    $temMasc = false;
    if(count($camFemTam)>0){
        if(filled($evt['obj']->url_img_camisas_fem)){
            array_push($fields,$fc->createField(
                    'camisaFeminina',
                    'Modelo de Camisa Feminina',
                    $fc->image(
                        asset($evt['obj']->url_img_camisas_fem),
                        "w-100",
                        "col-12 col-sm-9 col-md-7"
                    )
                )
            );
       }
        $temFem=true;
    }

    if(count($camMascTam)>0){
        if(filled($evt['obj']->url_img_camisas_masc)){
            array_push($fields,$fc->createField(
                    'camisaMasculina',
                    'Modelo de Camisa'.($temFem?" Masculina":""),
                    $fc->image(
                        asset($evt['obj']->url_img_camisas_masc),
                        "w-100",
                        "col-12 col-sm-9 col-md-7"
                    )
                )
            );
        }
        $temMasc = true;
    }

    if($temFem){
        array_push($fields,
            $fc->createField(
                'camisas_fem',
                'Camisas Femininas',
                $fc->grid(
                    $fc->select([
                        $fc->option('0',true),
                        $fc->option('1'),
                        $fc->option('2'),
                        $fc->option('3'),
                        $fc->option('4'),
                        $fc->option('5'),
                        $fc->option('6'),
                        $fc->option('7'),
                        $fc->option('8'),
                        $fc->option('9'),
                    ]),
                    $camFemTam,
                    $camFemCor
                )
            )
        );
    }

    if($temMasc){
        array_push($fields,
            $fc->createField(
                'camisas_masc',
                "Camisas Masculinas",
                $fc->grid(
                    $fc->select([
                        $fc->option('0',true),
                        $fc->option('1'),
                        $fc->option('2'),
                        $fc->option('3'),
                        $fc->option('4'),
                        $fc->option('5'),
                        $fc->option('6'),
                        $fc->option('7'),
                        $fc->option('8'),
                        $fc->option('9'),
                    ]),
                    $camMascTam,
                    $camMascCor
                )
            )
        );
    }


    if(!$temMasc&&!$temFem){
        $title="Isto é Tudo!";
        $formDescription = "Isso é tudo o que precisamos, aguarde para mais novidades";
    }else{
        array_unshift($fields,
           $fc->createField('',
                'Você gostaria de reservar camisas do evento e garantir o seu tamanho ideal?',
                $fc->info("Elas custarão apenas R$".$preco_antes." cada.
                Se quiser, escolha o número de camisas a reservar, os tamanhos e o tipo.")
            )
        );
        $title = 'Você já viu a camisa do evento?';
        if ($preco_antes === $preco_depois){
            $formDescription = "*A reserva da camisa será confirmada apenas se o pagamento for efetuado antes do início do evento.";
        }else{
            $formDescription = "
                *Após o início do evento a camisa custará R$".$preco_depois."
                <br>
                *A reserva da camisa a R$".$preco_antes." será confirmada apenas se o pagamento for efetuado antes do início do evento.";
        }

    }

   $form = $fc->constructForm(7,$title,$fields,$formDescription,true);
@endphp

@section('form')
    @include('forms.components.formgroup',$form)
@endsection
