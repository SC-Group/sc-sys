@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')

@php
    $fields=[];
    if(filled($evt['obj']->url_img_comercial)){

        array_push($fields,$fc->createField(
                 'scimage',
                 '',
                 $fc->image(
                     asset($evt['obj']->url_img_comercial)
                 )
             ));
    }

     array_push($fields,
             $fc->createField(
                 'email',
                 'Qual seu e-mail?',
                 $fc->shortAns(
                     'email',
                     '',
                     '',
                     $fc->createValidation('email')
                 ),
                 true
             )

      );
     $form = $fc->constructForm(0,'Identificação',$fields);
@endphp
@section('form')
    @include('forms.components.formgroup',$form)
@endsection