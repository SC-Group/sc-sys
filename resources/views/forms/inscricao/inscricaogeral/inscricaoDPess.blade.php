@extends('forms.inscricao.inscricao')
@inject('fc', 'App\Http\Services\FormConstructorService')
@php

    $formDescription = "Nesta seção você deve inserir seus dados pessoais.
    Nós usamos estes dados para confeccionar o seu certificado de participação, bem como colher algumas estatísticas e indicadores.
     Nós não iremos de maneira alguma divulgar seus dados pessoais para terceiros, a menos que expressamente autorizado por você.";
    $fields = [];
    array_push($fields,
        $fc->createField(
                'name',
                'Nome Completo',
                $fc->shortAns(
                    'text',
                    '',
                    '',
                    $fc->createValidation('name')
                ),
                true
        )
    );



             
    array_push($fields, 
    $fc->createField(
                'tel',
                'Telefone (Whatsapp)',
                $fc->shortAns(
                     'tel',
                     "'mask':'(99) 99999-9999', 'removeMaskOnSubmit':true ",
                     ''
                 ),
                 false
            )
    );

    if(boolval(env("BOT_ENABLED",false))){

        array_push($fields,
        $fc->createField(
                    '',
                    'A semana da computação é feita com muito carinho para você.',
                    $fc->info(
                        'Por isso não queremos que você perca nada nesse evento. Iremos enviar para seu telefone, através da aplicação '.env('BOT_NAME').', atualizações do evento pra você não perder nenhuma palestra. Basta marcar o campo abaixo.<br>
                        <span style="font-size:10pt">* Ao marcar o campo abaixo, você está concordando em compartilhar com a aplicação '.env('BOT_NAME').' seu
                        <strong>número de inscrição</strong>, <strong>nome</strong>, <strong>e-mail</strong> e <strong>número de telefone</strong>.
                        </span>'
                    ),
                    false,
                    ["tel"=>"[0-9]+"]
                )
        );
        array_push($fields,
        $fc->createField(
                    'bot',
                    'Atualizações da aplicação '. env("BOT_NAME","Bot"),
                     $fc->checkBoxes([
                        $fc->option("Desejo receber atualizações da aplicação ". env("BOT_NAME","Bot")),
                    ]),
                    true,
                    ["tel"=>"[0-9]+"]
                )
        );

    }

    array_push($fields, 
    $fc->createField(
                 'password',
                 'Por favor, crie uma senha para acesso ao sistema',
                 $fc->shortAns(
                     'password',
                     '',
                     '',
                     $fc->createValidation('password')
                 ),
                 true
             )
    );

    array_push($fields, 
    $fc->createField(
                 'password_confirmation',
                 'Por favor, confirme sua senha',
                 $fc->shortAns(
                     'password',
                     '',
                     ''
                 ),
                 true
             )
    );

    array_push($fields, 
    $fc->createField(
                'category',
                'Eu sou... (Selecione abaixo)',
                $fc->choices([
                    $fc->option("Estudante (fundamental, médio, técnico ou graduação)"),
                    $fc->option("Professor"),
                    $fc->option("Outro"),
                ]),
                true
            )
    );

    $form = $fc->constructForm(2,'Dados Pessoais',$fields,$formDescription);
@endphp

@section('form')
    @include('forms.components.formgroup',$form)
@endsection
