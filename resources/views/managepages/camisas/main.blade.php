@extends('layouts.dashboard')
@section("pagename")
    Venda de Camisas
@endsection
@push('styles')
    <link rel="stylesheet" href="{{asset('/css/vendadecamisas.css')}}">
@endpush
@section("content")
    <div  class="container-fluid px-4">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h3 class="card-title text-dark-sc-primary">Camisas do Evento de {{$year}}</h3>
                    </div>
                </div>
                <form id="search-user" method="post">
                    <div class="input-group mb-1">
                        <select name="search-user-type" style="flex-grow: 2;max-width: 100px" class="border-dark-sc-primary col-form-label-sm input-group-prepend custom-select" id="">
                            <option value="inscid" selected>Insc. ID</option>
                            <option value="userid">User ID</option>
                            <option value="nome">Nome</option>
                            <option value="email">E-mail</option>
                            <option value="ninsc">Não Insc.</option>
                        </select>
                        <input id="search-user-input" name="search-user-input" type="text" style="flex-grow: 3" class="border-dark-sc-primary form-control border-md-left-0" aria-label="Exemplo do tamanho do input" aria-describedby="inputGroup-sizing-default">
                        <div id="btn-search" style="flex-grow: 1;max-width: 40px" class="col-1 btn-block col-form-label-md input-group-append p-0">
                            <button type="submit" id="camisa-search-user" class="w-100 btn btn-sc-primary input-group-btn"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
                <div class="row" style="overflow:hidden" id="retrieved-users-box">

                </div>
                <div class="row" id="search-user-navigator">

                </div>
                <div id="open-close-search-users" style="height: 10px;display:none" class="row justify-content-center text-muted">
                    <button class="btn btn-sm btn-link-sc-primary">
                        <i style="display: none" class="fa fa-1x fa-chevron-down"></i>
                        <i style="display: none" class="fa fa-1x fa-chevron-up"></i>
                    </button>
                </div>
            </div>
        </div>

        <div id="vendas-box" style="display: none" class="card text-dark">
            <div class="card-header text-dark-sc-primary">
                <div style="font-size: 9pt" class="col">
                    <h4 id="camisa-user-name"></h4>
                    <div>
                        <strong id="camisa-user-email"></strong>
                    </div>
                    <div>
                        <div>
                            <strong id="user-id-label">User Id</strong>:<span id="camisa-user-id"></span>
                            <strong id="user-insc-label" class="ml-2">Insc Id</strong>:<span id="camisa-user-insc-id"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <div class="btn-group" role="group" aria-label="">
                            <button id="open-status" type="button" class="open-section-camisa active btn btn-sc-primary-dark">Status</button>
                            <button id="open-vende" type="button" class="open-section-camisa  btn btn-sc-primary-dark">Vender</button>
                            <button id="open-troca" type="button" class="open-section-camisa btn btn-sc-primary-dark">Trocar</button>
                            <button id="open-devolve" type="button" class="open-section-camisa btn btn-sc-primary-dark">Devolver</button>
                        </div>
                    </div>
                </div>
                <form class='container-fluid mt-3 text-dark-sc-primary' id="status-camisa">
                    <h5 class="">Status</h5>
                    <div id="status-camisa-container">
                        <div class="row">
                            <div class="col">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-9 col-3 justify-content-end d-flex">
                            <button type="submit" class="btn btn-sc-stress"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                </form>
                <form style="display: none" class='container-fluid mt-3 text-dark-sc-primary' id="vender-camisa">
                    <h5 class="">Vender Camisas</h5>
                    <div id="vende-camisa-container"></div>
                    <div class="row">
                        <div class="col-2">
                            <button type="button" class="btn btn-dark"  id="btn-add-vende"><i class="fa fa-plus"></i> </button>
                        </div>
                        <div class="col-5 offset-5 col-sm-10 offset-sm-0 justify-content-end d-flex flex-wrap">
                            <button type="submit" name="vender" value="venderEentregar" class="mb-2 mb-sm-0 btn btn-warning"><i class="fa fa-save"></i> Vender e Entregar</button>
                            <button type="submit" name="vender" value="vender" class="ml-3 btn btn-sc-stress"><i class="fa fa-save"></i> Vender</button>
                        </div>
                    </div>
                </form>
                <form style="display: none" class='container-fluid text-dark-sc-primary mt-3' id="trocar-camisa">
                    <h5 class="">Trocar Camisas</h5>
                    <div id="troca-camisa-container"></div>
                    <div class="row">
                        <div class="col-2">
                            <button type="button" class="btn btn-dark"  id="btn-add-troca"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-3 offset-7 justify-content-end d-flex">
                            <button type="submit" class="btn btn-sc-stress"  id="btn-save"><i class="fa fa-save"></i> Trocar</button>
                        </div>
                    </div>
                </form>
                <form style="display: none" class='container-fluid text-dark-sc-primary mt-3' id="devolver-camisa">
                    <h5 class="">Devolver Camisas</h5>
                    <div id="devolve-camisa-container"></div>
                    <div class="row">
                        <div class="col-2">
                            <button type="button"  class="btn btn-dark" id="btn-add-devolve"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-3 offset-7 justify-content-end d-flex">
                            <button type="submit" class="btn btn-sc-stress"  id="btn-save"><i class="fa fa-save"></i> Devolver</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
@push("scripts")
    <script>
        __CAMISAS = @json($camisas);
    </script>
    <script src="{{asset("/js/camisas.js")}}"></script>
@endpush

@push('env')
    <meta name="root_event_url" content="{{route("comissao.evento",['year'=>"%%YEAR%%"])}}">
@endpush
