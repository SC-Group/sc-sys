@extends('layouts.dashboard')
@section("pagename")
    Eggs
@endsection
@push('styles')
    <link rel="stylesheet" href="{{asset('/css/credenciamento.css')}}">
@endpush
@section("content")
    <div id="eggs-container" class="container-fluid px-4">
        <div class="card text-dark">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h3 class="card-title text-dark-sc-primary">Eggs do Evento de {{$year}}</h3>
                    </div>
                </div>
                <form class="form-row" id="users" method="post">
                    <div class="col table-responsive" id="retrieved-users-box">
                        <table class="table table-sm">
                            <thead class="bg-light" style="font-weight: bold; font-size: 11pt">
                                <tr>
                                    <th style="width: 60px; vertical-align: middle" scope="row">InscId</th>
                                    <th style="width: 60px; vertical-align: middle" scope="col">UserId</th>
                                    <th style="min-width: 200px; vertical-align: middle" scope="col">Nome</th>
                                    <th style="width: 60px; vertical-align: middle" scope="col">Dias Credenciados</th>
                                    <th style="min-width: 200px; vertical-align: middle" scope="col">Egg Cod</th>
                                    <th style="min-width: 200px; vertical-align: middle" scope="col">Egg Premio</th>
                                    <th style="min-width: 60px; vertical-align: middle" scope="col">Egg Exibido</th>
                                </tr>
                            </thead>
                            <tbody id="cred-user-lines" style="font-size: 10pt !important">
                                <div class="card-group justify-content-center" data-name="eggs-container">

                                    @if($eggs->exists())
                                        @foreach($eggs->get() as $key=>$egg)
                                            @php
                                                $insc = $egg->inscricao()->first();
                                                $user = $insc->user()->first();
                                                $diasCredenciado = $insc->dias()->wherePivot("presenca","Presente")->count();
                                            @endphp
                                            <tr>
                                                <td>
                                                    {{$insc->id}}
                                                </td>
                                                <td>
                                                    {{$user->id}}
                                                </td>
                                                <td>
                                                    {{$user->name}}
                                                </td>
                                                <td>
                                                    {{$diasCredenciado}}
                                                </td>
                                                <td>
                                                    {{$egg->cod}}
                                                </td>
                                                <td>
                                                    {{$egg->premio}}
                                                </td>
                                                <td>
                                                    {{$egg->exibido?"Sim":"Nao"}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </div>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div id="pagescredenc">
                </div>
            </div>
        </div>
    </div>
@endsection
@push("scripts")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
    <script>
        let __socketUrl = `${location.protocol}//${location.hostname}:{{env("NODESERVER_PORT")}}`;
        let __ioService = io(__socketUrl);
    </script>
    <script src="{{asset("/js/credenciamentoScripts/CredenciamentoModels.js")}}"></script>
    <script src="{{asset("/js/credenciamentoScripts/SelectUsers.js")}}"></script>
    <script src="{{asset("/js/credenciamentoScripts/StartCredenciamento.js")}}"></script>
@endpush

@push('env')
    <meta name="root_event_url" content="{{route("comissao.evento",['year'=>"%%YEAR%%"])}}">
@endpush
