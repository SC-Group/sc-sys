@extends('layouts.dashboard')
@section("pagename")
    Credenciamento
@endsection
@push('styles')
    <link rel="stylesheet" href="{{asset('/css/credenciamento.css')}}">
@endpush
@section("content")
    <div id="credenciamento-container" class="container-fluid px-4">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h3 class="card-title text-dark-sc-primary">Credenciamento do Evento de {{$year}}</h3>
                    </div>
                </div>
                <form id="search-user" method="post">
                    <div class="input-group mb-1">
                        <select name="search-user-type" style="flex-grow: 2;max-width: 100px" class="border-dark-sc-primary col-form-label-sm input-group-prepend custom-select" id="">
                            <option value="inscid" selected>Insc. ID</option>
                            <option value="userid">User ID</option>
                            <option value="nome">Nome</option>
                            <option value="email">E-mail</option>
                        </select>
                        <input id="search-user-input" name="search-user-input" type="text" style="flex-grow: 3" class="border-dark-sc-primary form-control border-md-left-0" aria-label="Exemplo do tamanho do input" aria-describedby="inputGroup-sizing-default">
                        <div id="btn-search" style="flex-grow: 1;max-width: 40px" class="col-1 btn-block col-form-label-md input-group-append p-0">
                            <button type="submit" id="camisa-search-user" class="w-100 btn btn-sc-primary input-group-btn"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <div class="card text-dark">
            <div class="card-header">
                Usuários:
            </div>
            <div class="card-body">
                <form class="form-row" id="users" method="post">
                    <div class="col table-responsive" id="retrieved-users-box">
                        <table class="table table-sm">
                            <thead class="bg-light" style="font-size: 12pt !important">
                                <tr>
                                    <th style="width: 60px" scope="row">InscId</th>
                                    <th style="width: 60px" scope="col">UserId</th>
                                    <th style="min-width: 200px" scope="col">Nome</th>
                                    <th style="min-width: 200px" scope="col">E-mail</th>
                                    <th scope="col">Kit</th>
                                    <th style="min-width: 200px" scope="col">

                                        @php
                                            $dias = $evt["obj"]->dias()->get(["data"]);
                                        @endphp
                                        <select class="custom-select custom-select-sm" name="eventdata" id="eventdata">
                                        @foreach($dias as $dia)
                                            <option value="{{($dia->data)}}">{{(\Carbon\Carbon::parse($dia->data)->format('d/m/Y'))}}</option>
                                        @endforeach
                                        </select>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="cred-user-lines" style="font-size: 10pt !important">

                            </tbody>
                        </table>
                    </div>
                </form>
                <div id="pagescredenc">
                </div>
            </div>
        </div>
    </div>
@endsection
@push("scripts")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
    <script>
        let __socketUrl = `${location.protocol}//${location.hostname}:{{env("NODESERVER_PORT")}}`;
        let __ioService = io(__socketUrl);
    </script>
    <script src="{{asset("/js/credenciamento.js")}}"></script>
@endpush

@push('env')
    <meta name="root_event_url" content="{{route("comissao.evento",['year'=>"%%YEAR%%"])}}">
@endpush
