@extends('layouts.dashboard')
@section("pagename")
    Gerência de Staffs
@endsection
@section("content")
    <div  class="container-fluid px-4 text-dark">
        <div class="card">
            <div id="staffsMainContainer" class="card-body">
                <div class="row">
                    <div class="col">
                        <h3 class="card-title text-dark-sc-primary">Staffs do Evento de {{$year}}</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col table-responsive">
                        <table style="font-size: 11pt" id="staffsTable" class="table table-sm">
                            <thead style="font-weight: bold">
                            <tr>
                                <td  style="width: 40px">
                                </td>
                                <td style="min-width: 80px; max-width: 80px">
                                    InscId
                                </td>
                                <td style="min-width: 120px">
                                    Status
                                </td>
                                <td style="min-width: 280px">
                                    Nome
                                </td>
                                <td>
                                    E-mail
                                </td>
                                <td>
                                    DRE
                                </td>
                                <td>
                                    Curso
                                </td>
                                <td style="min-width: 200px">
                                    Comissão
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($staffs as $index=>$staff)
                                    @php
                                        $aluno = $staff->aluno()->first();
                                        $user = $aluno->user()->first();
                                        $inscricao = $user->inscricao()->where("evento_id",$evt["obj"]->id)->first();
                                        $inscId = \App\Helpers\SCInscricao::get($inscricao->id);
                                    @endphp
                                    <tr data-id="{{$staff->id}}">
                                        <td>
                                            <button class="btn btn-sm btn-dark"><i class="fa fa-edit"></i></button></td>
                                        <td>{{$inscId}}</td>
                                        <td>
                                            {{$staff->status != null?$staff->status:"-"}}
                                        </td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$aluno->ufrj_dre}}</td>
                                        <td>{{$aluno->curso()->first()->nome}}</td>
                                        @php
                                            $comissao = $staff->comissao()->first();
                                        @endphp
                                        <td>{{$comissao!=null?$comissao->nome:"-"}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        window.comissoes = JSON.parse('{!! json_encode($comissoes) !!}');
        window.dias = JSON.parse('{!! json_encode($dias) !!}');
        window.horas = JSON.parse('{!! json_encode($horas) !!}');
    </script>
    <script src="{{asset("/js/staffs.js")}}"></script>
@endpush
@push('env')
    <meta name="root_event_url" content="{{route("comissao.evento",['year'=>"%%YEAR%%"])}}">
@endpush
