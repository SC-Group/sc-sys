@extends('layouts.dashboard')
@section("pagename")
    Gerência de Apoios
@endsection
@section("content")
    <div id="gerencia-de-apoios"  class="container-fluid px-4 text-dark">
        <form class="apoios-escolhidos" action="{{route("comissao.apoios.changeStatusApoio",$year)}}" method="post">
            @csrf()
            <input type="hidden" name="year" value="{{$year}}">
            <input type="hidden" name="sendForm" value="">
            <div class="card w-100 ">
                <div class="card-body ">
                    <div class="row">
                        <div class="col">
                            <h3 class="card-title text-dark-sc-primary">Apoios do Evento de {{$year}}</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="optionsbar col rounded p-2">
                            <div class="after-select d-none">
                                <a href="#" class="cancel-btn btn btn-outline-light">Cancelar</a>
                                <button type="button" name="remover" value="remove" class="btn btn-danger">Remover de {{$year}}</button>
                            </div>
                            <div class="before-select">
                                <a href="#" class="select-btn  btn btn-outline-secondary">Selecionar</a>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center flex-wrap">
                        @foreach($apoiosEscolhidos as $key=>$apoio)

                                @php
                                    if($apoio->id == old("organizacaoId")){
                                        $logoOld = $apoio->url_logo;
                                    }
                                @endphp
                                <div class="col" style="min-width: 170px;max-width: 200px">
                                    <div data-name="apoiocard" data-id="{{$apoio->id}}" style="border-width: 3px"  class="card border-white  text-secondary">
                                        <input type="hidden" name="selecionado[{{$apoio->id}}]">
                                        <div class="card-img card-img-top text-center p-1  d-flex justify-content-center align-items-center flex-column" style="height: 100px">
                                            <img draggable="false" style="user-select: none;max-height:100px; max-width:100px; min-width: 60px;" src="{{url($apoio->logo_url)}}"  alt="">
                                        </div>

                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <b style="font-size: 10pt">Nome:</b><br><span style="font-size: 9pt">{{$apoio->nome}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                    </div>
                </div>
            </div>
        </form>
        <form method="post" action="{{route("comissao.apoios.changeStatusOrganizacao",$year)}}" class="todas-as-organizacoes">
            <input type="hidden" name="sendForm" >
            @csrf()
            <input type="hidden" name="year" value="{{$year}}">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title text-secondary">
                                Apoios cadastradas no sistema
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="optionsbar col rounded p-2">
                            <div class="after-select d-none">
                                <button type="reset" class="cancel-btn btn btn-outline-light">Cancelar</button>
                                <button type="button" name="excluir" value="excluir" class="btn btn-danger">Excluir</button>
                                <button type="button" name="adicionar" value="adicionar" class="btn btn-info">Adicionar em {{$year}}</button>
                            </div>
                            <div class="before-select">
                                <a href="#" class="select-btn btn btn-outline-secondary">Selecionar</a>
                                <a href="#" class="add-new btn btn-dark">Adicionar Novo</a>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-center flex-wrap">
                        @php
                            $logoOld = null;
                        @endphp

                        @foreach($apoios as $key=>$apoio)
                            @php
                                if($apoio->id == old("empresaId")){
                                    $logoOld = $apoio->url_logo;
                                }
                            @endphp
                            <div class="col" style="min-width: 170px;max-width: 200px">
                                <div data-name="apoiocard" data-id="{{$apoio->id}}" style="border-width: 3px"  class="card border-white  text-secondary">
                                    <input type="hidden" name="selecionado[{{$apoio->id}}]">
                                    <div class="card-img card-img-top text-center p-1 d-flex justify-content-center flex-column align-items-center" style="height: 100px">
                                        <img draggable="false" style="user-select: none;max-height:100px; max-width:100px; min-width: 60px;" src="{{url($apoio->logo_url)}}"  alt="">
                                    </div>

                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <b style="font-size: 10pt">Nome:</b><br><span style="font-size: 9pt">{{$apoio->nome}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>

                </div>
            </div>
        </form>

    </div>

@endsection
@push('scripts')
    <script src="{{asset("/js/apoios.js")}}"></script>

    @if(!$errors->isEmpty() && $errors->has("modalopen"))
        <script>
            let data = {
                organizacaoId:"{{old("organizacaoId")}}",
                organizacaoNome:"{{old("organizacaoNome")}}",
                organizacaoResponsavel: "{{old("organizacaoResponsavel")}}",
                organizacaoTelefone: "{{old("organizacaoTelefone")}}",
                organizacaoEmail: "{{old("organizacaoEmail")}}",
                organizacaoCargo: "{{old("organizacaoCargo")}}",
                organizacaoSite: "{{old("organizacaoSite")}}",
                organizacaoLogo: "{{$logoOld}}"
            };

            let errors = JSON.parse('{!! json_encode($errors->getMessages()) !!}');

            (function(){
                startApoios.mountApoioModal(data, errors);
            })();
        </script>
    @endif
@endpush
