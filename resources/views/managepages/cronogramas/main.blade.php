@extends('layouts.dashboard')
@section("pagename")
    Cronogramas
@endsection
@push('styles')
    <link rel="stylesheet" href="{{asset('/css/credenciamento.css')}}">
@endpush
@section("content")
    <div id="cronogramas-container" class="container-fluid px-4">
        <div class="card text-muted">
            <div class="card-body">
                <div class="row">
                    <div class="col text-dark-sc-primary">
                        @if($page==="overview")
                            <h3>Cronogramas do evento de {{$year}}</h3>
                        @else
                            @php
                                $d = (new \Carbon\Carbon($dia->data))->format("d/m/Y")
                            @endphp
                            <h3>
                                <a href="{{route("comissao.cronogramas",["year"=>$year])}}" class="btn btn-secondary mr-1"><i class="fa fa-arrow-left"></i></a>Cronograma do dia {{$d}}
                            </h3>
                        @endif

                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        @if($page==="overview")
                            @include("cronogramascomponents.overview")
                        @else
                            @include("cronogramascomponents.cronogramadia")
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('env')
    <meta name="root_event_url" content="{{route("comissao.evento",['year'=>"%%YEAR%%"])}}">
@endpush

@push("scripts")
    <script src="{{asset("/js/cronogramas.js")}}"></script>
@endpush
