@php $name="eventoConfig".(isset($sectionId)?$sectionId:$sectionName); $showCardConfig=isset($showCardConfig)?$showCardConfig:false; @endphp
<form id="{{$name}}Form" name="{{$name}}Form"  action="">
    <div class="card  bg-sc-primary border-0">
        <div id="heading{{$name}}" class="card-header card-header-btn">
            <h5 class="mb-0 fg-sc-primary">
                <a href="javascript:null" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse{{$name}}" aria-expanded="true" aria-controls="collapse{{$name}}">
                    {{$sectionName}}
                </a>
            </h5>
        </div>
        <div id="collapse{{$name}}" class="collapse {{$showCardConfig?"show":""}}" aria-labelledby="heading{{$name}}" data-parent="#accordion">
            <div class="card-body rounded-bottom bg-white">
            {{----}}
                @yield('sectionContent')
            {{----}}
            </div>
        </div>
    </div>
</form>
@hasSection('sectionInjections')
    @yield('sectionInjections')
@endif

