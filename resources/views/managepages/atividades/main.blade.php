@extends('layouts.dashboard')
@section("pagename")
    Gerência de Atividades
@endsection
@section("content")
    <div  class="container-fluid px-4 text-dark">
        <div class="card">
            <div id="atividadesMainContainer" class="card-body">
                <div class="row">
                    <div class="col">
                        <h3 class="card-title text-dark-sc-primary">Atividades do Evento de {{$year}}</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col table-responsive">
                        <table style="font-size: 11pt" id="atividadesTable" class="table table-sm">
                            <thead>
                                <tr>
                                    <td  style="width: 40px">
                                    </td>
                                    <td style="min-width: 350px">
                                        Nome
                                    </td>
                                    <td>
                                        Aprovada?
                                    </td>
                                    <td>
                                        Tipo
                                    </td>
                                    <td>
                                        Organização vinculada
                                    </td>
                                    <td>
                                        Trilha
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col paginationContainer">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{asset("/js/atividades.js")}}"></script>
@endpush
@push('env')
    <meta name="root_event_url" content="{{route("comissao.evento",['year'=>"%%YEAR%%"])}}">
@endpush
