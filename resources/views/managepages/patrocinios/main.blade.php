@extends('layouts.dashboard')
@section("pagename")
    Gerência de Patrocínios
@endsection
@section("content")
    <div id="gerencia-de-patrocinios"  class="container-fluid px-4 text-dark">
        <form class="patrocinios-escolhidos" action="{{route("comissao.patrocinios.changeStatusPatrocinador",$year)}}" method="post">
            @csrf()
            <input type="hidden" name="year" value="{{$year}}">
            <input type="hidden" name="sendForm" value="">
            <div class="card w-100 ">
                <div class="card-body ">
                    <div class="row">
                        <div class="col">
                            <h3 class="card-title text-dark-sc-primary">Patrocínios do Evento de {{$year}}</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="optionsbar col rounded p-2">
                            <div class="after-select d-none">
                                <a href="#" class="cancel-btn btn btn-outline-light">Cancelar</a>
                                <button type="button" name="remover" value="remove" class="btn btn-danger">Remover de {{$year}}</button>
                            </div>
                            <div class="before-select">
                                <a href="#" class="select-btn  btn btn-outline-secondary">Selecionar</a>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center flex-wrap">
                        @php
                            $logoOld = null;
                        @endphp
                        @foreach($patrocinadoresEscolhidos as $key=>$nivel)
                            @foreach($nivel->patrocinadores as $key2=>$patrocinador)
                                @php
                                    if($patrocinador->id == old("empresaId")){
                                        $logoOld = $patrocinador->url_logo;
                                    }
                                @endphp
                                <div class="col" style="min-width: 170px;max-width: 200px">
                                    <div data-name="patrociniocard" data-id="{{$patrocinador->id}}" style="border-width: 3px"  class="card border-white  text-secondary">
                                        <input type="hidden" name="selecionado[{{$patrocinador->id}}]">
                                        <div class="card-img card-img-top text-center p-1  d-flex justify-content-center align-items-center flex-column" style="height: 100px">
                                            <img draggable="false" style="user-select: none;max-height:100px; max-width:100px; min-width: 60px;" src="{{url($patrocinador->url_logo)}}"  alt="">
                                        </div>

                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <b style="font-size: 10pt">Nome:</b><br><span style="font-size: 9pt">{{$patrocinador->nome}}</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <b style="font-size: 10pt">Nivel:</b><br><span style="font-size: 9pt">{{$nivel->nome}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach

                    </div>
                </div>
            </div>
        </form>
        <form method="post" action="{{route("comissao.patrocinios.changeStatusEmpresa",$year)}}" class="todas-as-empresas">
            <input type="hidden" name="nivelId" >
            <input type="hidden" name="sendForm" >
            @csrf()
            <input type="hidden" name="year" value="{{$year}}">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title text-secondary">
                                Empresas cadastradas no sistema
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="optionsbar col rounded p-2">
                            <div class="after-select d-none">
                                <button type="reset" class="cancel-btn btn btn-outline-light">Cancelar</button>
                                <button type="button" name="excluir" value="excluir" class="btn btn-danger">Excluir</button>
                                <button type="button" name="adicionar" value="adicionar" class="btn btn-info">Adicionar em {{$year}}</button>
                            </div>
                            <div class="before-select">
                                <a href="#" class="select-btn btn btn-outline-secondary">Selecionar</a>
                                <a href="#" class="add-new btn btn-dark">Adicionar Novo</a>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-center flex-wrap">
                         @foreach($patrocinadores as $key=>$patrocinador)
                            @php
                                if($patrocinador->id == old("empresaId")){
                                    $logoOld = $patrocinador->url_logo;
                                }
                            @endphp
                            <div class="col" style="min-width: 170px;max-width: 200px">
                                <div data-name="patrociniocard" data-id="{{$patrocinador->id}}" style="border-width: 3px"  class="card border-white  text-secondary">
                                    <input type="hidden" name="selecionado[{{$patrocinador->id}}]">
                                    <div class="card-img card-img-top text-center p-1 d-flex justify-content-center flex-column align-items-center" style="height: 100px">
                                        <img draggable="false" style="user-select: none;max-height:100px; max-width:100px; min-width: 60px;" src="{{url($patrocinador->url_logo)}}"  alt="">
                                    </div>

                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <b style="font-size: 10pt">Nome:</b><br><span style="font-size: 9pt">{{$patrocinador->nome}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>

                </div>
            </div>
        </form>

    </div>

@endsection
@push('scripts')
    <script>
        let niveisPatrocinio = [ @foreach($niveis as $nivel) { "id":parseInt({{$nivel->id}}),"nome":"{{ $nivel->nome }}" }{{$loop->last?"":","}} @endforeach ];
    </script>
    <script src="{{asset("/js/patrocinios.js")}}"></script>
    @if(!$errors->isEmpty() && $errors->has("modalopen"))
        <script>
            let data = {
                empresaId:"{{old("empresaId")}}",
                empresaNome:"{{old("empresaNome")}}",
                empresaResponsavel: "{{old("empresaResponsavel")}}",
                empresaTelefone: "{{old("empresaTelefone")}}",
                empresaEmail: "{{old("empresaEmail")}}",
                empresaCnpj: "{{old("empresaCnpj")}}",
                empresaInscMun: "{{old("empresaInscMun")}}",
                empresaInscEst: "{{old("empresaInscEst")}}",
                empresaSite: "{{old("empresaSite")}}",
                empresaRazaosocial: "{{old("empresaRazaosocial")}}",
                empresaLogo: "{{$logoOld}}",
                empresaNivelId: "{{old("empresaNivelPatrocinioId")}}",
                empresaNiveis:  niveisPatrocinio
            };

            let errors = JSON.parse('{!! json_encode($errors->getMessages()) !!}');

            (function(){
                startPatrocinio.mountPatrocinioModal(data, errors);
            })();
        </script>
    @endif
@endpush
