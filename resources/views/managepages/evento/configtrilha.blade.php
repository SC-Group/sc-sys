@extends('managepages.components.sectioncard',['sectionName'=>'Trilhas'])
@section('sectionContent')
    <div class="card-group justify-content-center trilhasContainer">
            @if($trilhas->count()>0)
                @for($i=0;$i<$trilhas->count();$i++)
                    @component("managepages.evento.components.trilhasrow",["row_id"=>ID::enc([$trilhas[$i]->id]),"nome"=>$trilhas[$i]->nome,"cor"=>$trilhas[$i]->cor,"desc"=>$trilhas[$i]->descricao,"hidden"=>false])
                    @endcomponent
                @endfor
            @endif
    </div>
    <div class="row">
        <div class="col-6  d-flex justify-content-start">
            <a id="addTrilhaButton" href="javascript:nop()" class="btn btn-dark"><i class="fa fa-plus"></i></a>
        </div>
        <div class="col-6  d-flex justify-content-end">
            <input type="submit" class="btn btn-sc-secondary" value="Salvar">
        </div>
    </div>
@endsection

@section('sectionInjections')
    <div class="row-template-trilhas d-none">
        @component("managepages.evento.components.trilhasrow",["hidden"=>false]) @endcomponent
    </div>
@endsection

