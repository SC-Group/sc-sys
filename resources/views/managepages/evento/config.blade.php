@extends('layouts.dashboard')
@section("pagename")
    Configuração do Sistema
@endsection
@push('styles')
    <link rel="stylesheet" href="{{asset('/frameworks/Colorpick/css/colorpick.css')}}">
    <link rel="stylesheet" href="{{asset('/css/assinaturasmanagement.css')}}">
@endpush
@section("content")
<div  class="container-fluid px-4">

    <!--Criar novo Evento-->
    <form  id="eventCreateForm" method="post" action="{{route("comissao.evento.create",["year"=>$year])}}">
        <div data-show="{{count($errors->evento->messages())>0?"true":"false"}}" class="modal fade" id="novoEventoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
            <div class="modal-dialog modal-dialog-centered" role="document" >
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-sc-primary" id="exampleModalLongTitle">Criar Novo Evento</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div   class="modal-body">
                        {{csrf_field()}}
                        <div class="col-md-12">
                            <div class="row">
                                <p class="p-1 text-dark">
                                    O evento de {{empty($eventos)?$year:$eventos->first()->ano+1}} não foi criado ainda. Para criar, preencha abaixo.
                                </p>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="text-dark" for="edicaoEvt">Edição:</label>
                                        <input type="number" min="1" class="form-control" id="edicaoEvt" placeholder="Edição" name="edicaoEvt" value="{{old("edicaoEvt")??(empty($eventos)?1:$eventos->first()->edicao+1)}}">
                                    </div>
                                    <span class="text-danger">{{$errors->evento->first('inscEvtInit')}}</span>
                                </div>
                                <div class='col-md-12'>
                                    <div class="form-group">
                                        <span class="text-dark">
                                                Início das Inscrições:
                                            </span>
                                        <div class="input-group date" id="inicioDasInscPick" data-target-input="nearest">
                                            <input type="text" name="inscEvtInit" value="{{old("inscEvtInit")}}" class="form-control datetimepicker-input" data-target="#inicioDasInscPick" data-toggle="datetimepicker"/>
                                            <div class="input-group-append" data-target="#inicioDasInscPick" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar-alt"></i></div>
                                            </div>
                                        </div>
                                        <span class="text-danger">{{$errors->evento->first('inscEvtInit')}}</span>
                                    </div>
                                </div>
                                <div class='col-md-12'>
                                    <div class="form-group">
                                        <span class="text-dark">
                                            Início do Evento:
                                        </span>
                                        <div class="input-group date" id="inicioDoEventoPick" data-target-input="nearest">
                                            <input name="evtInit" value="{{old("evtInit")}}" type="text" class="form-control datetimepicker-input" data-target="#inicioDoEventoPick" data-toggle="datetimepicker" />
                                            <div class="input-group-append" data-target="#inicioDoEventoPick" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar-alt"></i></div>
                                            </div>
                                        </div>
                                        <span class="text-danger">{{$errors->evento->first('evtInit')}}</span>
                                    </div>
                                </div>
                                <div class='col-md-12'>
                                    <div class="form-group">
                                        <span class="text-dark">
                                            Fim do Evento:
                                        </span>
                                        <div class="input-group date" id="fimDoEventoPick" data-target-input="nearest">
                                            <input type="text" value="{{old("evtEnd")}}" name="evtEnd" class="form-control datetimepicker-input" data-target="#fimDoEventoPick" data-toggle="datetimepicker"/>
                                            <div class="input-group-append" data-target="#fimDoEventoPick" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar-alt"></i></div>
                                            </div>
                                        </div>
                                        <span class="text-danger">{{$errors->evento->first('evtEnd')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sc-secondary" data-dismiss="modal">Close</button>
                        <input class="btn btn-sc-primary" type="submit" value="Criar Evento">
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="card-title text-dark-sc-primary">Eventos do sistema</h3>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row my-2">
                    <div class="col">
                        <p class="text-dark">
                            Você pode criar um novo evento clicando no botão abaixo.
                        </p>
                        <button type="button" class="btn btn-sc-primary" data-toggle="modal" data-target="#novoEventoModal">
                            Criar Novo Evento
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(isset($eventos))
        @include('managepages.evento.configevento')
    @endif
    <div id="accordion">

    </div>
</div>
@endsection
@push("scripts")
    <script src="{{asset('/frameworks/Colorpick/js/css-manager.js')}}"></script>
    <script src="{{asset('/frameworks/Colorpick/js/colors.js')}}"></script>
    <script src="{{asset('/frameworks/Colorpick/js/range.js')}}"></script>
    <script src="{{asset('/frameworks/Colorpick/js/colorpick.js')}}"></script>
    <script src="{{asset("/frameworks/vibrant.min.js")}}"></script>
    <script src="{{asset("/frameworks/vibrant.worker.min.js")}}"></script>
@endpush
@if(isset($eventos))
    @push("scripts")
        <script src="{{asset("/js/evento.js")}}"></script>
    @endpush
@else
    @push('scripts')
        <script src="{{asset("/js/evento.js")}}"></script>
        <script>
            let configEvento = new ConfigEvento();
            configEvento.load();
        </script>
    @endpush
@endif

@push('env')
    <meta name="root_event_url" content="{{route("comissao.evento",['year'=>"%%YEAR%%"])}}">
@endpush
