@extends('managepages.components.sectioncard',['sectionName'=>'Camisas'])
@section('sectionContent')
    <div class="row">
        <div class="col-md-6">
            <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Preço Antes</span>
                </div>
                <input type="text" value="{{$precoAntes}}"  name="precoantes" class="form-control" aria-label="Username" aria-describedby="basic-addon1">
            </div>
            <span class="text-danger"></span>
        </div>
        <div class="col-md-6">
            <div class="input-group-sm input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Preço</span>
                </div>
                <input type="text" value="{{$precoDepois}}" name="precodepois" class="form-control" aria-label="Username" aria-describedby="basic-addon1">
            </div>
            <span class="text-danger"></span>
        </div>
    </div>
    <div class="table-wrap">
        <table class="first-fixed table table-responsive-md text-dark mx-0">
            <thead>
            <tr>
                <th scope="row" style="max-width: 50px">
                    <a href="#" class="add-linha btn-sm btn btn-light border text-dark">
                        <i class="fa fa-plus"></i>
                    </a>
                </th>
                <th scope="col">Tamanho</th>
                <th scope="col" style="min-width: 130px">Tipo</th>
                <th scope="col">Cor</th>
                <th scope="col">Quantidade</th>
            </tr>
            </thead>
            <tbody id="camisas-tbody">

            @if($camisas->count()>0)
                @for($i=0;$i<$camisas->count();$i++)
                    @component("managepages.evento.components.camisarow",["row_id"=>ID::enc([$camisas[$i]->id]),"tam"=>$camisas[$i]->tamanho, "cor"=>$camisas[$i]->cor,"tipo"=>$camisas[$i]->tipo,"qtd"=>$camisas[$i]->qtd_total,"hidden"=>false])
                    @endcomponent
                @endfor
            @endif
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-12  d-flex justify-content-end">
            <input type="submit" class="btn btn-sc-secondary" value="Salvar">
        </div>
    </div>
@endsection

@section('sectionInjections')
<table class="row-template-camisas d-none">
    @component("managepages.evento.components.camisarow",["hidden"=>false]) @endcomponent
</table>
@endsection
