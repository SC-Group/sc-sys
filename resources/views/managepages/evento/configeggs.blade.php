@extends('managepages.components.sectioncard',['sectionName'=>'Eggs'])
@section('sectionContent')
    <div class="row">
        <div class="col-6  d-flex justify-content-start">
            <button type="button" data-name="add-eggs" class="btn btn-dark"><i class="fa fa-plus"></i></button>
        </div>
        <div class="col-6  d-flex justify-content-end">
            <input type="submit" class="btn btn-sc-secondary" value="Salvar">
        </div>
    </div>
    <div class="card-group justify-content-center" data-name="eggs-container">
        @if($eggs->count()>0)
            @for($i=0;$i<$eggs->count();$i++)
                @component("managepages.evento.components.eggsrow",["model"=>false, "row_id"=>ID::enc([$eggs[$i]->id]),"cod"=>$eggs[$i]->cod,"premio"=>$eggs[$i]->premio,"hidden"=>false,"eggvalue"=>$eggs[$i]->url_img])
                @endcomponent
            @endfor
        @endif
    </div>
@endsection

@section('sectionInjections')
    <div class="row-template-eggs d-none">
        @component("managepages.evento.components.eggsrow",["hidden"=>false,"model"=>true]) @endcomponent
    </div>
@endsection

