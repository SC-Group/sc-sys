@extends('managepages.components.sectioncard',['sectionName'=>'Cronograma'])

@section('sectionContent')

    <div class="row text-dark">

        <div class="col table-responsive">
            <table class="table table-sm text-center ">
                <thead>
                    <tr>
                        <td  class="px-0" style="width: 30px">
                            <a href="" data-name="add-chronos" class="btn btn-sm btn-dark">
                                <i class="fa fa-plus"></i>
                            </a>
                        </td>
                        <td class="pl-1" style="min-width:120px;width: 120px;">
                            Horários
                        </td>
                        @foreach($period as $date)

                            <td data-date="{{$date->format("Y-m-d")}}">
                                {{$date->format('d/m')}}
                            </td>
                        @endforeach
                    </tr>
                </thead>
               <tbody>

                    @foreach($horas as $hora)
                        @php
                            $dataId = \App\Helpers\IdHelper::enc([$hora->id])
                        @endphp
                        <tr data-id="{{$dataId}}"  data-name="line-chronos">
                            
                            <td class="px-0 align-middle">
                                <button class="btn btn-sm btn-danger" type="button" data-name="btn-delete-chronos">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </td>

                            <td class="pl-1 align-middle">
                                <div data-name="line-chronos" class="form-group my-1">
                                    <div class="input-group input-group-sm date" data-target-input="nearest">
                                        <input data-name="chronos-inicio"
                                               name="chronos-line[edit][{{$dataId}}][inicio]"
                                               type="text"
                                               placeholder="Inicio"
                                               class="form-control"
                                               value="{{$hora->inicio}}">
                                        <input data-name="chronos-fim"
                                               name="chronos-line[edit][{{$dataId}}][fim]"
                                               type="text"
                                               placeholder="Fim"
                                               class="form-control"
                                               value="{{$hora->fim}}">
                                    </div>
                                </div>
                            </td>


                            @foreach($period as $date)
                                @php
                                    $dia = $evt["obj"]->dias()->whereDate("data",$date);
                                    $value="Atividade";
                                    if($dia->exists()){
                                        $dia = $dia->first();
                                        $cronograma = $dia->cronogramas()->where("hora_id",$hora->id)->has("atividade");
                                        if($cronograma->exists()){
                                            $atividade = $cronograma->first()->atividade()->first();
                                            if($atividade->especial&&$atividade->tipo==="Alimentacao"){
                                                $value=$atividade->nome;
                                            }
                                        }
                                    }

                                @endphp

                                <td class="align-middle">
                                    <input type="hidden" name="chronos-line[edit][{{$dataId}}][status][{{$date->format("Y-m-d")}}]" value="{{$value}}">
                                    <input
                                            style="width: 100px"
                                            type="button"
                                            class="btn btn-sm
                                            @switch($value)
                                                    @case("Almoço")
                                                        btn-danger
                                                    @break
                                                    @case("Coffee-Break")
                                                        btn-warning
                                                    @break
                                                    @default
                                                        btn-light
                                                    @break
                                            @endswitch "
                                            data-name="status-day[{{$dataId}}][{{$date->format("Y-m-d")}}]" value="{{$value}}">
                                </td>

                            @endforeach

                        </tr>
                    @endforeach

                    <!-- CronogramaAtividade Content -->
               </tbody>
            </table>
        </div>
{{--        <div class="col">--}}
{{--            {{$evtInitTime->toDateTimeString()}} :::--}}
{{--            {{$evtEndTime->toDateTimeString()}}--}}
{{--            <br>--}}
{{--            {{$evtInitTime->diffForHumans($evtEndTime)}}--}}
{{--        </div>--}}
    </div>

    <div class="row">
        <div class="col-12  d-flex justify-content-end">
            <input type="submit" class="btn btn-sc-secondary" value="Salvar">
        </div>
    </div>
@endsection

