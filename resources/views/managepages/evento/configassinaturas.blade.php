@extends('managepages.components.sectioncard',['sectionName'=>'Assinaturas'])
@section('sectionContent')
    <div class="row">
        <div class="col-12">
            <h6 class="card-subtitle mb-4 text-dark-sc-primary">
                Faça upload dos dados de assinaturas dos certificados
            </h6>
        </div>
    </div>
    <div class="row">
        @foreach($assinaturas as $key=>$assinatura)
            <div class="col-md-6 mb-4" data-name="assinaturacontainer">
                <h3 class="text-dark">
                    {{ucfirst($key)}} do Departamento
                </h3>

                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Nome</span>
                    </div>
                    <input type="text"
                           name="{{$key}}[name]"
                           class="form-control"
                           value="{{$assinatura["name"]}}"
                           placeholder="Nome do {{ucfirst($key)}} do Departamento"
                           aria-label="{{$key}}Name"
                           aria-describedby="basic-addon1">
                </div>
                <div data-error="{{$key}}.name" style="font-size: 10pt" class="mb-3 text-danger">
                </div>

                <h6 class="text-dark">Escolha o arquivo:</h6>
                <button type="button" class="buttonfile mb-1 btn btn-dark">
                    <i class="fa fa-upload"></i>
                    <input accept="image/*" value="notfilled" type="file" name="{{$key}}[sign]">
                </button>
                <div data-error="{{$key}}.sign" style="font-size: 10pt" class="mb-3 text-danger">
                </div>

                <div  class="mb-1 img-position-wrapper" style="{{$assinatura['sign']==null?"display: none":""}}">
                    <h6 class="text-dark">Arraste a linha para a posição correta da assinatura:</h6>
                    <div class="img-position">
                        <div class="imgsignline">
                            <input type="hidden" name="{{$key}}[pos]" value="{{$assinatura["pos"]??"0.5"}}">
                        </div>
                        <img src="{{$assinatura['sign']}}" alt="">
                    </div>
                </div>
                <div data-error="{{$key}}.pos" style="font-size: 10pt" class="mb-3 text-danger">
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-12  d-flex justify-content-end">
            <input type="submit" class="btn btn-sc-secondary" value="Salvar">
        </div>
    </div>
@endsection

