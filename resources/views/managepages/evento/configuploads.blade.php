@extends('managepages.components.sectioncard',['sectionName'=>'Uploads'])
@section('sectionContent')
    <div class="row">
        <div class="col-12">
            <h6 class="card-subtitle mb-4 text-dark-sc-primary">
                Faça upload das imagens do sistema
            </h6>
        </div>
    </div>
    <div class="row">

        <div class="col-md-3 col-6 col-lg">
            <h6 class="text-dark">Logo:</h6>
            <div class="mb-3">
                <input type="fileimg" name="file[logo]" id="inputGroupFile01" value="{{$evt["obj"]->url_img_logo}}">
            </div>
        </div>

        <div class="col-md-3 col-6 col-lg">
            <h6 class="text-dark">Fundo:</h6>
            <div class="mb-3">
                <input type="fileimg" name="file[bg]" id="inputGroupFile02" value="{{$evt["obj"]->url_img_bg}}">
            </div>
        </div>

        <div class="col-md-3 col-6 col-lg">
            <h6 class="text-dark">Comercial:</h6>
            <div class="mb-3">
                <input type="fileimg" name="file[comercial]" id="inputGroupFile03" value="{{$evt["obj"]->url_img_comercial}}">
            </div>
        </div>

        <div class="col-md-3 col-6 col-lg">
            <h6 class="text-dark">Camisas Femininas:</h6>
            <div class="mb-3">
                <input type="fileimg" name="file[camisas_fem]" id="inputGroupFile04" value="{{$evt["obj"]->url_img_camisas_fem}}">
            </div>
        </div>

        <div class="col-md-3 col-6 col-lg">
            <h6 class="text-dark">Camisas Masculinas:</h6>
            <div class="mb-3">
                <input type="fileimg" name="file[camisas_masc]" id="inputGroupFile05" value="{{$evt["obj"]->url_img_camisas_masc}}">
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-12  d-flex justify-content-end">
            <input type="submit" class="btn btn-sc-secondary" value="Salvar">
        </div>
    </div>
@endsection

