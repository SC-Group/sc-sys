@php
    $uuid = isset($row_id)? $row_id: uniqid("new-");
@endphp
<div data-name="line-egg" class="px-3 pb-2 justify-content-center d-flex" data-id="{{$uuid}}" @if($hidden) style="display:none!important;" @endif>
    <div class="card text-white  bg-sc-primary" style="max-width: 15rem;">
        <div class="card-header">
            <div class="input-group">
                <input class="form-control"
                       data-name="egg-cod"
                       name="egg[{{$uuid}}][cod]"
                       type="text"
                       placeholder="Código"
                       value="{{isset($cod)?$cod:""}}"
                >
            </div>
        </div>
        <div class="card-body bg-white">
            <div class="input-group input-group-sm form-group mb-2">
                <div class="input-group-prepend">
                    <label for="egg-premio-{{$uuid}}" class="input-group-text">
                        Premio
                    </label>
                </div>
                <input class="form-control form-control-sm"
                       data-name="egg-premio"
                       type="text"
                       name="egg[{{$uuid}}][premio]"
                       placeholder="Premio"
                       id="egg-premio-{{$uuid}}"
                       value="{{isset($premio)?$premio:""}}">
            </div>
            <div class="row">
                <div class="col">
                    <input data-style="max-width: 100%; max-height: 50px" data-name="egg-img" type="{{$model?"file":"fileimg"}}" name="egg[{{$uuid}}][img]" id="egg-img-{{$uuid}}" value="{{(isset($eggvalue)&&$eggvalue!==null)?\Illuminate\Support\Facades\Storage::disk("local")->get($eggvalue):""}}">
                </div>
            </div>
        </div>
        <div class="card-footer  bg-white">
            <input type="button" class="btn btn-danger" data-name="btn-delete-eggs" value="Excluir Egg">
        </div>
    </div>
</div>