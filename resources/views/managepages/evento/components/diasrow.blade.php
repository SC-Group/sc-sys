<tr class="lineContainer{{($hidden?" d-none":"")}}" @if(isset($row_id)) data-rowid="{{$row_id}}" @endif>
    <th scope="row">
        <a href="#" class="delete-linha btn btn-danger border">
            <i class="fa fa-minus"></i>
        </a>
    </th>
    <td class=" w-100">
        <div class="input-group py-0" data-cell="date">
            <input style="position:relative; min-width: 100px" type="text" value="{{isset($data)?$data:""}}" class=" form-control datetimepicker-input border-bottom-1 rounded-0 border-right-0 border-top-0 border-left-0 form-control" aria-describedby="basic-addon1">
        </div>
    </td>

    <td>
        <div class="input-group  py-0" data-cell="horariosbtn">
            <a href="#" class="btn btn-light border text-dark">
                <i class="fa fa-pen-square"></i>
            </a>
        </div>
    </td>
</tr>
<tr  @if(isset($row_id)) data-msgrowid="{{$row_id}}" @endif class="msgContainer">
    <td class="msgRow "  style="display:none">
    </td>
</tr>
