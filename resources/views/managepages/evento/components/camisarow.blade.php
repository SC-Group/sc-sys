<tr class="lineContainer" @if(isset($row_id)) data-rowid="{{$row_id}}" @endif @if($hidden == true) style="display:none!important;" @endif>
    <th scope="row">
        <a href="#" class="delete-linha btn-sm btn btn-danger border">
            <i class="fa fa-minus"></i>
        </a>
    </th>
    <td>
        <div class="input-group  py-0 tam">
            <select class="border-bottom-1 rounded-0 border-right-0 border-top-0 border-left-0 custom-select custom-select-sm">
                <option @if(isset($tam)&&$tam=="PP") selected @endif value="PP">PP</option>
                <option @if(isset($tam)&&$tam=="P") selected @endif value="P">P</option>
                <option @if(isset($tam)&&$tam=="M") selected @endif value="M">M</option>
                <option @if(isset($tam)&&$tam=="G") selected @endif value="G">G</option>
                <option @if(isset($tam)&&$tam=="GG") selected @endif value="GG">GG</option>
            </select>
        </div>
    </td>
    <td>
        <div class="input-group  py-0 tipo">
            <select class="border-bottom-1 rounded-0 border-right-0 border-top-0 border-left-0 custom-select custom-select-sm">
                <option @if(isset($tipo)&&$tipo=="Unissex") selected @endif value="Unisex">Unissex</option>
                <option @if(isset($tipo)&&$tipo=="Masculina") selected @endif value="Masculina">Masculina</option>
                <option @if(isset($tipo)&&$tipo=="Feminina") selected @endif value="Feminina">Feminina</option>
            </select>
        </div>
    </td>
    <td>
        <div class="input-group-sm cor py-0">
            <input style="position:relative; min-width: 100px" type="text" value="@if(isset($cor)){{$cor}}@else{{"Branca"}}@endif" class="border-bottom-1 rounded-0 border-right-0 border-top-0 border-left-0 form-control" aria-describedby="basic-addon1">
        </div>
    </td>
    <td>
        <div class="input-group-sm qtd py-0">
            <input style="position:relative" type="text" value="@if(isset($qtd)){{$qtd}}@else{{0}}@endif" class="border-bottom-1 rounded-0 border-right-0 border-top-0 border-left-0 form-control" aria-label="Username" aria-describedby="basic-addon1">
        </div>
    </td>

    
</tr>
<tr  @if(isset($row_id)) data-msgrowid="{{$row_id}}" @endif class="msgContainer">
    <td class="msgRow "  style="display:none">
    </td>
</tr>
