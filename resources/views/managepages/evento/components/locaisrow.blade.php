@php
    $uuid = isset($row_id)? $row_id: uniqid("new-");
@endphp
<div data-name="line-local" class="px-3 pb-2 justify-content-center d-flex" data-id="{{$uuid}}" @if($hidden) style="display:none!important;" @endif>
    <div class="card text-white  bg-sc-primary" style="max-width: 15rem;">
        <div class="card-header">
            <div class="input-group">
                <input class="form-control"
                       data-name="local-nome"
                       name="local[{{$uuid}}][nome]"
                       type="text"
                       placeholder="Nome"
                       value="{{isset($nome)?$nome:""}}"
                >
            </div>
        </div>
        <div class="card-body bg-white">
            <div class="input-group input-group-sm form-group mb-2">
                <div class="input-group-prepend">
                    <label for="local-vagas-{{$uuid}}" class="input-group-text">
                        Vagas
                    </label>
                </div>
                <input class="form-control form-control-sm"
                       data-name="local-vagas"
                       type="number"
                       name="local[{{$uuid}}][vagas]"
                       placeholder="Vagas"
                       id="local-vagas-{{$uuid}}"
                       value="{{isset($vagas)?$vagas:""}}">
            </div>
            <div class="form-group-sm form-group">
                <label class="text-dark">Descrição</label>
                <textarea
                        class="form-control form-control-sm"
                        name="local[{{$uuid}}][descricao]"
                        data-name="local-descricao"
                        rows="3"
                >{{isset($descricao)?$descricao:""}}</textarea>
            </div>
        </div>
        <div class="card-footer  bg-white">
            <input type="button" class="btn btn-danger" data-name="btn-delete-local" value="Excluir Local">
        </div>
    </div>
</div>