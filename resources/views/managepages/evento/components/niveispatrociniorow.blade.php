@php
    $uuid = isset($row_id)? $row_id: uniqid("new-");
@endphp
<div data-name="line-nivelpatrocinio" class="px-3 pb-2 justify-content-center d-flex" data-id="{{$uuid}}" @if($hidden) style="display:none!important;" @endif>
    <div class="card text-white  bg-sc-primary" style="max-width: 15rem;">
        <div class="card-header">
            <div class="input-group">
                <input class="form-control"
                       data-name="nivelpatrocinio-nome"
                       name="nivelpatrocinio[{{$uuid}}][nome]"
                       type="text"
                       placeholder="Nome"
                       value="{{isset($nome)?$nome:""}}"
                >
            </div>
        </div>
        <div class="card-body bg-white">
            <div class="input-group input-group-sm form-group mb-2">
                <div class="input-group-prepend">
                    <label for="nivelpatrocinio-nivel-{{$uuid}}" class="input-group-text">
                        Nível
                    </label>
                </div>
                <input class="form-control form-control-sm"
                       data-name="nivelpatrocinio-nivel"
                       type="number"
                       name="nivelpatrocinio[{{$uuid}}][nivel]"
                       placeholder="Nível"
                       id="nivelpatrocinio-nivel-{{$uuid}}"
                       value="{{isset($nivel)?$nivel:""}}">
            </div>
        </div>
        <div class="card-footer  bg-white">
            <input type="button" class="btn btn-danger" data-name="btn-delete-nivelpatrocinio" value="Excluir Nível">
        </div>
    </div>
</div>