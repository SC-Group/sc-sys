<div class="px-3 pb-2 justify-content-center d-flex" @if(isset($row_id)) data-rowid="{{$row_id}}" @endif @if($hidden == true) style="display:none!important;" @endif>
    <div class="card text-white" style="max-width: 15rem;">
        <div class="card-header">
            <div class="input-group out">
                 <input class="form-control trilhaName" type="text" id="trilha-@if(isset($row_id)){{$row_id}}@endif" placeholder="Nome" value="@if(isset($nome)) {{$nome}} @endif">
                
                <div class="input-group-append ">
                    <span class="input-group-text border-0 text-white" id="basic-addon1"><i class="far fa-edit"></i></span>
                </div>
                
            </div>
            <div class="nomeError text-danger bg-light rounded font-weight-normal" style="font-size:10pt"></div>
                     
        </div>
        <div class="card-body bg-white">
            <div class="input-group mb-2">
                <div data-value="@if(isset($cor)) {{$cor}} @endif" class="colorpick"></div>
            </div>
            <div class="form-group">
                <label class="text-dark" for="exampleFormControlTextarea1">Descrição</label>
                <textarea class="form-control trilhaDesc" rows="3">@if(isset($desc)){{$desc}}@endif</textarea>
            </div>
        </div>
        <div class="card-footer  bg-white">
            <input type="button" class="btn btn-danger delete-trilha" value="Excluir Trilha">
        </div>
    </div>
</div>