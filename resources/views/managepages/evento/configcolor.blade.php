@extends('managepages.components.sectioncard',['sectionName'=>'Cores'])
@section('sectionContent')
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col">
                <h6 class="card-subtitle text-dark">
                    Sugestão de Cores com
                    <a target="_blank" href="https://jariz.github.io/vibrant.js/" class="btn-link text-dark-sc-primary ">Vibrant.js</a>
                </h6>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div id="vibrantLogoSuggestion" class="card">
                    <div style="height: 180px; overflow: hidden" class="justify-content-center d-flex card-img-top">
                        <img class="h-100" style="height: 180px"  src="{{asset($evt['obj']->url_img_logo)}}" alt="Não há um logo carregado para o evento">
                    </div>
                    <div class="card-body">
                        <div class="row d-flex justify-content-center align-content-stretch"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div id="vibrantBGSuggestion" class="card">
                    <div style="height: 180px; overflow: hidden" class="justify-content-center d-flex card-img-top">
                        <img style="height: 180px"  class="h-100"  src="{{asset($evt['obj']->url_img_bg)}}" alt="Não há um background carregado para o evento">
                    </div>
                    <div class="card-body">
                        <div class="row d-flex justify-content-center align-content-stretch"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col">
                <h6 class="card-subtitle text-dark">
                    Seleção de Cores
                </h6>
            </div>
        </div>
            <div class="card">
                <div class="card-body">

                    <div class="row">
                    <div class="p-2 col-md-6">
                        <div class="p-1  text-dark">
                            Cor Primária:
                        </div>
                        <div class="input-group">
                            <div data-value="{{$evt['obj']->primary_color}}" data-color="primary" class="colorpick"></div>
                        </div>
                    </div>
                    <div class="p-2 col-md-6">
                        <div class="p-1  text-dark">
                            Cor Secundária
                        </div>
                        <div class="input-group">
                            <div data-value="{{$evt['obj']->secondary_color}}" data-color="secondary" class="colorpick"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="p-2 col-md-6">
                        <div class="p-1  text-dark">
                            Cor de fundo 1:
                        </div>
                        <div  class="input-group">
                            <div data-value="{{$evt['obj']->bg1_color}}" data-color="bg1" class="colorpick"></div>
                        </div>
                    </div>
                    <div  class="p-2 col-md-6">
                        <div class="p-1  text-dark">
                            Cor de fundo 2:
                        </div>
                        <div class="input-group">
                            <div data-value="{{$evt['obj']->bg2_color}}" data-color="bg2" class="colorpick"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="p-2 col-md-12">
                        <div class="p-1  text-dark">
                            Cor de destaque:
                        </div>
                        <div class="input-group">
                            <div data-value="{{$evt['obj']->stress_color}}" data-color="stress" class="colorpick"></div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
    </div>
</div>
<div class="row">
    <div class="col  d-flex justify-content-end">
        {{--<div class="custom-control custom-checkbox mr-sm-2">--}}
            {{--<input type="checkbox" class="bg-dark-sc-primary custom-control-input custom-control-input-dark-sc-primary" id="customControlAutosizing" name="remember" {{ old('remember') ? 'checked' : '' }}>--}}
            {{--<label class="text-dark-sc-primary custom-control-label custom-control-label-dark-sc-primary" for="customControlAutosizing">Definir Padrão</label>--}}
        {{--</div>--}}
        <input class="btn btn-sc-secondary" type="submit" value="Salvar">
    </div>
</div>
@endsection