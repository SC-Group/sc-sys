@extends('managepages.components.sectioncard',['sectionName'=>'Niveis de Patrocínio', "sectionId"=>"NiveisPatrocinio"])
@section('sectionContent')
    <div class="row">
        <div class="col-6  d-flex justify-content-start">
            <button type="button" data-name="add-niveispatrocinio" class="btn btn-dark"><i class="fa fa-plus"></i></button>
        </div>
        <div class="col-6  d-flex justify-content-end">
            <input type="submit" class="btn btn-sc-secondary" value="Salvar">
        </div>
    </div>
    <div class="card-group justify-content-center" data-name="niveispatrocinio-container">
        @if($niveispatrocinio->count()>0)
            @for($i=0;$i<$niveispatrocinio->count();$i++)
                @component("managepages.evento.components.niveispatrociniorow",["row_id"=>ID::enc([$niveispatrocinio[$i]->id]),"nome"=>$niveispatrocinio[$i]->nome,"nivel"=>$niveispatrocinio[$i]->nivel,"hidden"=>false])
                @endcomponent
            @endfor
        @endif
    </div>
@endsection

@section('sectionInjections')
    <div class="row-template-niveispatrocinio d-none">
        @component("managepages.evento.components.niveispatrociniorow",["hidden"=>false]) @endcomponent
    </div>
@endsection

