<div class="card">
    <div class="card-body">
        <div class="col-md-12">
            <div class="row my-2">
                <div class="col">
                    <p class="text-dark">
                        Ou você pode selecionar um evento abaixo e mudar as informações dele.
                    </p>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-dark-sc-primary border-dark-sc-primary text-light"  id="basic-addon1">Ano do evento</span>
                        </div>


                        <select id="configChangeYear" class="border-dark-sc-primary custom-select">
                            @foreach($eventos as $evento)
                                <option value="{{$evento->ano}}" {{(($evento->ano==$year)?"selected=true":"")}}>{{$evento->ano}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        {{----}}


        <form  id="eventAlterForm" method="post" >
            <div class="col-md-12">
                <div class="row">
                    <div class='col-md-12'>

                        <div class="form-group">
                            <label class="text-dark" for="edicaoEvt">Edição:</label>
                            <input type="number" min="1" class="form-control" id="edicaoEvt" placeholder="Edição" name="edicaoEvt" value="{{old("edicaoEvt")??($evt["edicao"])}}">
                        </div>
                        <span class="text-danger">{{$errors->evento->first('inscEvtInit')}}</span>
                        <div class="form-group">
                            <span class="text-dark">
                                    Início das Inscrições:
                            </span>
                            <div class="input-group date" id="altInicioDasInscPick" data-target-input="nearest">
                                <input type="text" name="inscEvtInit" value="{{$evt["inscInit"]}}" class="form-control datetimepicker-input" data-target="#altInicioDasInscPick" data-toggle="datetimepicker"/>
                                <div class="input-group-append" data-target="#altInicioDasInscPick" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar-alt"></i></div>
                                </div>
                            </div>
                            <span class="text-danger">{{$errors->evento->first('inscEvtInit')}}</span>
                        </div>
                    </div>

                    <div class='col-md-12'>
                        <div class="form-group">
                                            <span class="text-dark">
                                                Início do Evento:
                                            </span>
                            <div class="input-group date" id="altInicioDoEventoPick" data-target-input="nearest">
                                <input name="evtInit" value="{{$evt["evtInit"]}}" type="text" class="form-control datetimepicker-input" data-target="#altInicioDoEventoPick" data-toggle="datetimepicker" />
                                <div class="input-group-append" data-target="#altInicioDoEventoPick" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar-alt"></i></div>
                                </div>
                            </div>
                            <span class="text-danger">{{$errors->evento->first('evtInit')}}</span>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                                            <span class="text-dark">
                                                Fim do Evento:
                                            </span>
                            <div class="input-group date" id="altFimDoEventoPick" data-target-input="nearest">
                                <input type="text" value="{{$evt["evtEnd"]}}" name="evtEnd" class="form-control datetimepicker-input" data-target="#altFimDoEventoPick" data-toggle="datetimepicker"/>
                                <div class="input-group-append" data-target="#altFimDoEventoPick" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar-alt"></i></div>
                                </div>
                            </div>
                            <span class="text-danger">{{$errors->evento->first('evtEnd')}}</span>
                        </div>

                    </div>
                    <div class='col-md-12 d-flex justify-content-end'>
                        <div class="form-group">
                            <input id="alterEventFormSubmit" type="submit" value="Salvar" class="btn btn-sc-secondary">
                        </div>

                    </div>
                </div>
            </div>
        </form>
        {{----}}


    </div>
</div>



