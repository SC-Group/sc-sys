@extends('managepages.components.sectioncard',['sectionName'=>'Locais'])
@section('sectionContent')
    <div class="row">
        <div class="col-6  d-flex justify-content-start">
            <button type="button" data-name="add-locais" class="btn btn-dark"><i class="fa fa-plus"></i></button>
        </div>
        <div class="col-6  d-flex justify-content-end">
            <input type="submit" class="btn btn-sc-secondary" value="Salvar">
        </div>
    </div>
    <div class="card-group justify-content-center" data-name="locais-container">
        @if($locais->count()>0)
            @for($i=0;$i<$locais->count();$i++)
                @component("managepages.evento.components.locaisrow",["row_id"=>ID::enc([$locais[$i]->id]),"nome"=>$locais[$i]->nome,"vagas"=>$locais[$i]->vagas,"descricao"=>$locais[$i]->descricao,"hidden"=>false])
                @endcomponent
            @endfor
        @endif
    </div>
@endsection

@section('sectionInjections')
    <div class="row-template-locais d-none">
        @component("managepages.evento.components.locaisrow",["hidden"=>false]) @endcomponent
    </div>
@endsection

