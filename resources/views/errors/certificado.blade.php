@extends('layouts.main')
@section('container')

    <div style="height: 65vh" class="container d-flex flex-column justify-content-center">
        <div class="row w-100 d-flex align-self-center justify-content-center">
            <div class="col-lg-6">
                <div class="card p-4">
                    <div class="header w-100 d-flex justify-content-center">
                        <div class="col-md-6 justify-content-center d-flex">
                            <div style="min-height: 140px; width: 115px; background-size: contain; background-repeat: no-repeat; background-position: center" class="_LOGO">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="mt-3 mb-5 col-12 text-center text-dark-sc-primary">
                            <strong>
                            {{$msg}}
                            </strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
