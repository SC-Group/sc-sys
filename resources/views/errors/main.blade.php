@extends('layouts.main')
@section('container')
    <div class="header w-100 d-flex justify-content-center">
        <div class="col-md-6 justify-content-center d-flex pt-4">
            <div style="height: 20vh; min-height: 140px; width: 115px; background-size: contain; background-repeat: no-repeat" class="_LOGO">
            </div>
        </div>
    </div>
    <div style="height: 65vh" class="container d-flex flex-column justify-content-center">
        <div class="row w-100 d-flex align-self-center justify-content-center">
            <div class="col-lg-6">
                <div class="card p-4">
                    <div class="row">
                        <div class="col-12 text-center text-dark-sc-primary">
                            {{$msg}}
                        </div>
                    </div>
                    <div class="row p-4  d-flex justify-content-center">
                        <div class="col-4 d-flex justify-content-center">
                            <a class="btn btn-sc-primary text-center" href="{{url()->previous()}}">{{__("views.Back")}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
