@extends('layouts.main',['setBG'=>true])

    @push("styles")
       <style>
           body{
               background-repeat: no-repeat;
               background-position: center;
               background-size: cover;
           }
           ._LOGO{
               height: 130px;
               width: 100%;
               background-size: contain;
               background-repeat: no-repeat;
               background-position: center;
           }
       </style>
    @endpush




@section('container')
    <div class="container-fluid">
        <div style="min-height: 100vh" class="row justify-content-center align-items-center">
            <div class="col-md-9" style="max-width: 700px !important;">
                <div class="card text-dark m-3" >
                    <form  class="   form-horizontal" method="POST" action="{{ route('login') }}">
                        <div class="card-body">
                            <div class="row justify-content-center mb-4">
                                <div class="_LOGO"></div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-5 d-flex justify-content-center align-content-center">
                                    <div class="d-flex flex-column h-100 mb-4" style="justify-content: center; align-items: center">
                                        <p>{{__("views.NotRegisteredLink")}}</p>
                                        <p>
                                            <a class="btn btn-outline-sc-primary" href="{{route("forminscricao",["year"=>$year])}}">{{__("views.SubscriptionButton")}}</a>
                                        </p>
                                    </div>
                                </div>
                                <div class="login-col col-12 col-md-7">
                                    <div class="row">
                                        <div class="col mb-2">
                                            <h5>
                                                {{__("views.LoginTitle")}}:
                                            </h5>
                                        </div>
                                    </div>
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{$year}}" name="year">
                                    <div class="row mb-2">
                                        <div class="col">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <label for="email" class="input-group-text">{{__("views.Email")}}</label>
                                                </div>
                                                <input id="email" type="email" class="form-control" name="email" placeholder="{{__("views.Email")}}" value="{{ old('email') }}" required autofocus aria-label="E-mail" aria-describedby="basic-addon1">
                                            </div>
                                            @if ($errors->has('email'))
                                                <div style="font-size: 10pt" class="text-danger help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <label for="password" class="input-group-text">{{__("views.Password")}}</label>
                                                </div>
                                                <input id="password" type="password" class="form-control" name="password" required placeholder="{{__("views.Password")}}" required autofocus aria-describedby="basic-addon1">
                                            </div>
                                            @if ($errors->has('password'))
                                                <div style="font-size: 10pt" class="help-block text-danger">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row mb-2">
                                        <div class="col-4 ">
                                            <button type="submit" class="btn btn-sc-secondary">
                                                {{__("views.LoginButton")}}
                                            </button>
                                        </div>
                                        <div class="col-8 d-flex justify-content-end align-items-center">
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="bg-dark-sc-primary custom-control-input custom-control-input-dark-sc-primary" id="customControlAutosizing" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label class="text-dark-sc-primary custom-control-label custom-control-label-dark-sc-primary" for="customControlAutosizing">{{__("views.RememberMe")}}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col d-flex justify-content-center">
                                            <a class="btn btn-link text-dark-sc-primary" href="{{ route('password.request') }}">
                                                {{__("views.ForgotPasswordLink")}}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
