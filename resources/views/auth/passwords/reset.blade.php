@extends('layouts.main')

@section('container')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div style="height: 100vh" class=" text-sc-primary row justify-content-center align-content-center">
            <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                <div class="card bg-sc-primary">
                    <div class="card-header">
                        <div class="card-title mb-0 text-white">
                            {{__("views.PasswordRecoveryTitle")}}
                        </div>
                    </div>
                    <div class="card-body bg-light rounded-bottom">

                        <div class="row mb-3">
                            <div class="col">
                                {{ csrf_field() }}

                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="mb-3 w-100">
                                    <div class="input-group {{ $errors->has('email') ? 'has-error' :''}}">
                                        <div class="input-group-prepend">
                                            <label for="email" class="input-group-text">{{__("views.Email")}}</label>
                                        </div>
                                        <input id="email" placeholder="{{__("views.Email")}}" type="email" class="form-control" name="email" value="{{old('email')}}" required autofocus>
                                    </div>
                                    @if ($errors->has('email'))
                                    <div>
                                        <span style="font-size: 10pt" class="help-block text-danger">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                </div>
                                <div class="mb-3 w-100">
                                    <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <div class="input-group-prepend">
                                            <label for="password" class="input-group-text">{{__("views.Password")}}</label>
                                        </div>
                                        <input id="password" placeholder="{{__("views.Password")}}" type="password" class="form-control" name="password" required>
                                    </div>
                                    @if ($errors->has('password'))
                                    <div>
                                        <span style="font-size: 10pt" class="help-block text-danger">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                </div>
                                <div class="w-100 mb-3">
                                    <div class="input-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <div class="input-group-prepend">
                                            <label for="password-confirm" class="input-group-text">{{__("views.PasswordConfirmation")}}</label>
                                        </div>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                    @if ($errors->has('password_confirmation'))
                                    <div>
                                        <span style="font-size: 10pt" class="text-danger help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="input-group">
                                    <button type="submit" class="btn btn-sc-primary">
                                        {{__("views.UpdatePasswordButton")}}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
