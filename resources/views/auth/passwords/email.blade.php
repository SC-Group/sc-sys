@extends('layouts.main')

@section('container')
    <div class="container">
        @if (session('status'))
            <div class="alert mt-4 alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div style="height: 100vh" class=" text-sc-primary row justify-content-center align-content-center">
            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                <div style="width: 350px" class="card bg-sc-primary">
                    <div class="card-header">
                        <div class="card-title mb-0 text-white">
                            {{__("views.PasswordRecoveryTitle")}}
                        </div>
                    </div>
                    <div class="card-body bg-light rounded-bottom">

                        <div class="row mb-3">
                            <div class="col">
                                {{ csrf_field() }}

                                <div class="input-group {{ $errors->has('email') ? 'has-error' :''}}">
                                    <div class="input-group-prepend">
                                        <label for="email" class="input-group-text">{{__("views.Email")}}</label>
                                    </div>
                                    <input id="email" type="email" class="form-control" placeholder="{{__("views.Email")}}" name="email" value="{{old("email")}}" required>
                                </div>
                                @if ($errors->has('email'))
                                    <div style="font-size: 10pt" class="text-danger help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="input-group">
                                    <button type="submit" class="btn btn-sc-primary">
                                        {{__("views.SendEmailWithLinkButton")}}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
