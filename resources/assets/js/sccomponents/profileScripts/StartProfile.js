import './../formScripts/formActions';
import {ProfileModel} from "./ProfileModel";

class StartProfile{
    constructor(){
        this.form = $("#profile-form");
    
        this.alunoProfile = this.form.find("#aluno-profile");
        this.professorProfile=this.form.find("#professor-profile");
        this.exufrjProfile=this.form.find("#exufrj-profile");
        this.loadEvents();
    }

    loadEvents(){
        const _this=this;
        this.form.find("#tipouser").change(function(){
            _this.alunoProfile.addClass("d-none");
            _this.professorProfile.addClass("d-none");
            _this.exufrjProfile.addClass("d-none");
            switch($(this).val()){
                case "exufrj": _this.exufrjProfile.removeClass("d-none");break;
                case "professor": _this.professorProfile.removeClass("d-none");break;
                case "aluno": _this.alunoProfile.removeClass("d-none");break;
            }
        });

        this.alunoProfile.find("#instituicao-Aluno-id").change(function(){
            let drefield = _this.alunoProfile.find("#aluno-dre");
            if($(this).val()==="1"){
                drefield.removeClass("d-none");
            }else{
                drefield.addClass("d-none");
            }
        });


        let tipo = this.form.find("input[name=tiposalvamento]");

        this.form.find("#inscrever").click(function(e){
           tipo.val("inscrever");
            let modalCamisas = $("#modal-inscricao");

            if(modalCamisas.length===0){
                _this.form.submit();
            }else{
                modalCamisas.modal("show");
            }
            e.preventDefault();
            e.stopPropagation();
        });

        this.form.find("[name=inscrever-com-camisas]").click(function(e){
            tipo.val("inscrever_com_camisas");
            _this.form.submit();
            e.stopPropagation();
            e.preventDefault();
        });

        this.form.find("#salvar").click(function(){
            tipo.val("salvar");
            _this.form.submit();
        });



        this.form.find("#tipouser").trigger("change");
    }
}

window.loadPostInfo = function(data){
    ModalAlert.load(ProfileModel(data));
    ModalAlert.show();
};

window.startProfile = new StartProfile();
export {StartProfile};