function ProfileModel(data) {

    return $(`
<div class="text-center text-muted">

    <div class="row">
        <div class="col">
            <div class="alert alert-${data.status}" role="alert">
                Inscrição realizada com sucesso <br>
                ${data.msgBot}
            </div>
        </div>
    </div>                     
    <div class="row">
        <div class="col">
            Abaixo está seu número de inscrição.<br>Guarde-o muito bem, pois ele será usado durante todo o evento:
        </div>
    </div>
    <div class="row pt-3">
        <div class="col">
            <span class="badge badge-danger p-2">
                <strong>${data.inscnum}</strong>
            </span>
        </div>
    </div>
                      
</div> 
                    
`);
}

export {ProfileModel};