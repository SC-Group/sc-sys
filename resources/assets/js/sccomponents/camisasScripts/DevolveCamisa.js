import {ComercioCamisa} from './ComercioCamisa'
import {modelDevolve} from './ModelosComponentesCamisa'

export class DevolveCamisa extends ComercioCamisa{
    constructor(user, container){
        super(user,container,"devolve");
        this.modelo = $(modelDevolve);
    }

    addAcao(e){
        let id = `${new Date().valueOf()}`;
        let line = this.modelo.clone();
        line.attr("id","l"+id);
        this.container.append(line[0]);
        line.find(".close-camisa-line").on("click",e=>line.remove());
        this.createLineParts(line,id);
        return [line,id];
    }

    createLineParts(lineElm,lineElmId){
        lineElmId = `l[${lineElmId}]`;
        let tamSelect = lineElm.find("[name=tamanho]");
        tamSelect.attr("name",`${lineElmId}[tamanho]`);
        let tipoSelect = lineElm.find("[name=tipo]");
        tipoSelect.attr("name",`${lineElmId}[tipo]`);
        let corSelect = lineElm.find("[name=cor]");
        corSelect.attr("name",`${lineElmId}[cor]`);

        let qtdInput = lineElm.find("[name=qtd]");
        let idInput = lineElm.find("[name=camisaid]");
        tamSelect.on("change",(e)=>{
            this.addOptionsTipo(tipoSelect,tamSelect);
            tipoSelect.trigger("change");
        });
        tipoSelect.on("change",(e)=>{
            this.addOptionsCor(corSelect,tamSelect,tipoSelect);
            corSelect.trigger("change");
        });
        corSelect.on("change",e=>{
            let dataqtd = this.constructQuantidade(__CAMISAS,tamSelect.val(),tipoSelect.val(),corSelect.val(),true);
            qtdInput.attr("min",0);
            qtdInput.attr("max",dataqtd.maxVal);
            qtdInput.attr("name",`${lineElmId}[qtd]`);
            idInput.attr("name",`${lineElmId}[camisaid]`);
            idInput.val(dataqtd.id);
        });
        this.addOptionsTamanho(tamSelect);
        tamSelect.trigger("change");
    }

    mountAlert(response){
        let devolvidas = "";
        for(let i in response.camisasDevolvidas){
            if(response.camisasDevolvidas.hasOwnProperty(i)){
                let camisa = response.camisas[i-1];
                let tamanho = camisa.tamanho;
                let tipo = camisa.tipo;
                let cor = camisa.cor;
                let val = response.camisasDevolvidas[i];
                devolvidas+=`<div>${tamanho}.${tipo},${cor}: ${val}</div>`;
            }
        }

        let message = `<div>
            <h5>Camisas físicas a serem devolvidas </h5>
            <p>${devolvidas}</p>
            <strong>Valor total devolvido para o cliente: R$ ${response.precoTotal}</strong>
        </div>`;

        ModalAlert.load(message);
        setTimeout(()=>ModalAlert.show(),500);
    }

    save(elm,e) {

        let formData = new FormData(elm);

        formData.append("entidadeId", this.entidade.id);
        formData.append("entidadeTipo",this.entidade.tipo);

        let confirmed = ()=>{
            let comm = new Comm(window.location.getUrl() + "/devolvercamisas", false, formData, {
                contentType: false,
                processData: false,
            });

            comm.success(response => {
                __CAMISAS = response.camisas;
                this.container.empty();
                this.mountAlert(response);
            });

            comm.error(response => {
                __CAMISAS = response.responseJSON.camisas;
                this.container.empty();
            });
            comm.send();
        };

        Confirm.load(`Tem certeza de que deseja fazer a devolução destes items?`, confirmed);
        Confirm.show();

    }


}