import {ComercioCamisa} from './ComercioCamisa'
import {modelStatus} from "./ModelosComponentesCamisa";

export class StatusCamisa extends ComercioCamisa{
    constructor(user,container){
        super(user,container,"status");
        this.modelo = $(modelStatus);
    }


    createLineParts(lineElm,lineElmId){
        lineElmId = `l[${lineElmId}]`;
        let camisaInput = lineElm.find("[name=camisa]");
        camisaInput.attr("name",`${lineElmId}[camisa]`);
        let reservadasInput = lineElm.find("[name=reservadas]");
        reservadasInput.attr("name",`${lineElmId}[reservadas]`);
        let pagasInput = lineElm.find("[name=pagas]");
        pagasInput.attr("name",`${lineElmId}[pagas]`);
        let entreguesInput = lineElm.find("[name=entregues]");
        entreguesInput.attr("name",`${lineElmId}[entregues]`);
        let idInput = lineElm.find("[name=camisaid]");
        idInput.attr("name",`${lineElmId}[camisaid]`);
    }


    addAcao(e){
        let id = `${new Date().valueOf()}`;
        let line = this.modelo.clone();
        line.attr("id","l"+id);
        this.container.append(line[0]);
        this.createLineParts(line,id);
        return [line,id];
    }

    constructCamisasFromServer(response){
        let errors = response.errors || {};

        for(let i=0;i<response[`entidadeCamisas`].length;i++){
            let entidadeCamisa = response[`entidadeCamisas`][i];
            let camisa = response.camisas.find(camisa=>camisa.id===entidadeCamisa.id);
            let tamanho = camisa.tamanho;
            let tipo = camisa.tipo;
            let cor = camisa.cor;
            let id = camisa.id;
            let qtdEntregue = entidadeCamisa.entregues;
            let qtdreserv = entidadeCamisa.reservadas;
            let qtdpg = entidadeCamisa.pgAntes+entidadeCamisa.pgDepois;

            let line = $(this.addAcao()[0]);
            line.find(`[name*=camisa]`).val(`${tamanho}.${tipo},${cor}`);
            line.find(`[name*=reservadas]`).val(qtdreserv);
            line.find(`[name*=pagas]`).val(qtdpg);
            line.find(`[name*=entregues]`).val(qtdEntregue);
            line.find(`[name*=camisaid]`).val(id);
            if(errors[id]!==undefined){
                line.find(".errorContainer").html(errors[id]);
            }
        }
    }

    save(elm,e) {

        let formData = new FormData(elm);
        formData.append("entidadeId", this.entidade.id);
        formData.append("entidadeTipo",this.entidade.tipo);

        let confirmed = ()=>{
            let comm = new Comm(window.location.getUrl() + "/statuscamisas", false, formData, {
                contentType: false,
                processData: false
            });

            comm.success(response => {
                __CAMISAS = response.camisas;
                this.container.empty();
                this.constructCamisasFromServer(response);
            });

            comm.error(response => {
                __CAMISAS = response.responseJSON.camisas;
                this.container.empty();
                this.constructCamisasFromServer(response.responseJSON);
            });
            comm.send();
        };

        Confirm.load(`Tem certeza de que deseja alterar os status destes items?`, confirmed);
        Confirm.show();

    }


    loadFromServer(){
        let _this=this;
        let comm = new Comm(window.location.getUrl()+"/entidadecamisas",false,{
            entidadeId:this.entidade.id,
            entidadeTipo: this.entidade.tipo
        });
        comm.success(response=>{
            __CAMISAS = response.camisas;
            _this.constructCamisasFromServer(response);
        });
        comm.error(response=>{
            console.log(response);
        });
        comm.send();
    }

}