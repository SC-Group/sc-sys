import {DevolveCamisa} from './DevolveCamisa'
import {StatusCamisa} from './StatusCamisa'
import {TrocaCamisa} from './TrocaCamisa'
import {VendeCamisa} from './VendeCamisa'

class StartCamisa{

    constructor(){
        this.vendasBox = document.querySelector("#vendas-box");
        this.openCloseSearchUsers = document.querySelector("#open-close-search-users")
        this.openVendeBtn = document.querySelector("#open-vende");
        this.openTrocaBtn = document.querySelector("#open-troca");
        this.openStatusBtn = document.querySelector("#open-status");
        this.openDevolveBtn = document.querySelector("#open-devolve");
        this.vender = document.querySelector("#vender-camisa");
        this.trocar = document.querySelector("#trocar-camisa");
        this.devolver = document.querySelector("#devolver-camisa");
        this.status = document.querySelector("#status-camisa");
        this.searchForm = document.querySelector("#search-user");
        this.retrievedUsersBox = document.querySelector("#retrieved-users-box");
        this.searchFormSelect = this.searchForm.querySelector("[name=search-user-type]");
        this.searchFormInput = this.searchForm.querySelector("#search-user-input");
        this.searchFormBtn = this.searchForm.querySelector("#btn-search");
        this.searchFormNavigator = document.querySelector("#search-user-navigator");
        this.selectedUser=null;
        this.openCloseSearchUsers.querySelector(".fa-chevron-down");
        this.searchArrup = this.openCloseSearchUsers.querySelector(".fa-chevron-up");
        this.searchArrdown = this.openCloseSearchUsers.querySelector(".fa-chevron-down");
        this.searchForm.addEventListener("submit",e=>{
           e.preventDefault();
           this.searchdata = new FormData(this.searchForm);
           this.searchUser(1,this.searchdata);
           return false;
        });

        let _this=this;

        this.sectionVende = new VendeCamisa(this.selectedUser,this.vender);
        this.sectionStatus = new StatusCamisa(this.selectedUser,this.status);
        this.sectionTroca = new TrocaCamisa(this.selectedUser,this.trocar);
        this.sectionDevolve = new DevolveCamisa(this.selectedUser,this.devolver);

        this.openCloseSearchUsers.children[0].addEventListener("click",function(e){_this.toggleSearchUsers(e)});
        this.openCloseSearchUsers.children[0].addEventListener("touch",function(e){_this.toggleSearchUsers(e)});

        this.searchFormSelect.addEventListener("change",e=>{
            this.openCloseSearchUsers.style.display="none";
            this.vendasBox.style.display="none";
            this.retrievedUsersBox.innerHTML="";
            this.searchFormInput.value="";
            this.searchFormNavigator.innerHTML="";

            this.searchArrup.style.display="none";
            this.searchArrdown.style.display="none";
            this.searchFormSelect.style.borderTopRightRadius="";
            this.searchFormSelect.style.borderBottomRightRadius="";
            this.searchFormBtn.style.display='block';
            this.searchFormInput.style.display='block';
        });
        let evt = new Event("change");
        this.searchFormSelect.dispatchEvent(evt);
        this.openDevolveBtn.addEventListener("click",(e)=>this.openDevolve(e));
        this.openTrocaBtn.addEventListener("click",(e)=>this.openTroca(e));
        this.openVendeBtn.addEventListener("click",(e)=>this.openVende(e));
        this.openStatusBtn.addEventListener("click",(e)=>this.openStatus(e));


    }

    toggleSearchUsers(e){
        let arrdown = this.searchArrdown;
        let arrup = this.searchArrup;
        let hidden = this.retrievedUsersBox.style.display === "none";
        if(hidden){
            this.retrievedUsersBox.style.display="";
            this.searchFormNavigator.style.display="";
            arrdown.style.display="none";
            arrup.style.display="";
        }else{
            arrdown.style.display="";
            arrup.style.display="none";
            this.searchFormNavigator.style.display="none";
            this.retrievedUsersBox.style.display="none";
        }
    }
    searchNextPage(page){
        this.searchUser(page,this.searchdata);
    }

    selectUser(elm,data){
        if(elm){
            elm.parentElement.querySelectorAll(".card").forEach((e)=>e.style.backgroundColor="");
            elm.querySelector(".card").style.backgroundColor="rgba(0,0,0,0.1)";
        }

        this.selectedUser = data;
        this.sectionVende.reload(data);
        this.sectionStatus.reload(data);
        this.sectionTroca.reload(data);
        this.sectionDevolve.reload(data);
        this.toggleSearchUsers();
        this.vendasBox.style.display="flex";
        let idLabel = $(this.vendasBox).find("#user-id-label");
        let inscLabel = $(this.vendasBox).find("#user-insc-label");
        idLabel.html(data.tipo==="card"?"Card Id":"User Id");
        inscLabel.html(data.tipo==="card"?"Code":"Insc Id");
        this.vendasBox.querySelector("#camisa-user-name").innerHTML=data.name;
        this.vendasBox.querySelector("#camisa-user-email").innerHTML=data.email;
        this.vendasBox.querySelector("#camisa-user-id").innerHTML=data.id;
        this.vendasBox.querySelector("#camisa-user-insc-id").innerHTML=(data.inscId!=undefined?data.inscId:"N/A");
    }

    searchUser(page,data){
        let _this=this;
        let card = $(this.searchFormSelect).val()==="ninsc";
        let comm;

        if(!card){
            comm = new Comm(`${Env.__LOCAL}search/users?page=${page}`,false,data,{
                processData: false,
                contentType: false
            });
        }else{
            comm = new Comm(`${Env.__LOCAL}search/card`,false,data,{
                processData: false,
                contentType: false
            });
        }

        comm.success(function (response) {
            _this.retrievedUsersBox.style.display = "";
            _this.searchFormNavigator.style.display = "";
            _this.retrievedUsersBox.innerHTML = "";
            _this.searchFormInput.value = "";
            _this.searchFormNavigator.innerHTML = "";
            _this.searchArrup.style.display = "";
            _this.openCloseSearchUsers.style.display = "";
            for (let i = 0; i < response.user.length; i++) {
                let user = document.createElement("div");
                user.setAttribute("class", "mb-1 mt-1 col-lg-3 col-md-6 col-sm-12");
                user.innerHTML = `
                    <div class="card card-clickable w-100 h-100 text-dark-sc-primary mb-0 mt-0">
                      <div class="card-body pt-1 pb-1 pl-2 pr-2">
                        <div class="mb-1 text-muted font-weight-light" style="font-size: 14pt !important;">
                            ${response.user[i].name}
                        </div>
                        <div class="mb-1 text-muted font-weight-light" style="font-size: 10pt !important;">
                            <div class="row">
                                <div class="col">
                                    <span>${response.user[i].email}</span>
                                </div>                            
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <span class="font-weight-bold">${card?"Card":"User"} Id:</span> <span>${response.user[i].id}</span>
                                </div>
                                <div class="col-6">
                                    <span class="font-weight-bold">${card?"Code":"Insc. Id"}:</span> <span>${response.user[i].inscId !== undefined ? response.user[i].inscId : "N/A"}</span>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>                    
                `;
                user.addEventListener("click", () => _this.selectUser(user, response.user[i]));
                user.addEventListener("touch", () => _this.selectUser(user, response.user[i]));
                _this.retrievedUsersBox.appendChild(user);

            }

            if (!card){
                let p = new Pagination();
                let elmP = p.createPaginationObject(response.pages.current,response.pages.last,function(page){
                    _this.searchNextPage(page);
                });
                $(_this.searchFormNavigator).html(elmP);
            }
        });

        comm.error(function (response) {
            console.log(response);
        });

        comm.send();
    }


    resetAllContainers(){
        this.sectionStatus.resetContainer();
        this.sectionVende.resetContainer();
        this.sectionTroca.resetContainer();
        this.sectionDevolve.resetContainer();
    }

    openStatus(e){
        $(".open-section-camisa").removeClass("active");
        $(this.openStatusBtn).addClass("active");
        this.status.style.display="block";
        this.vender.style.display="none";
        this.trocar.style.display="none";
        this.devolver.style.display="none";
        this.sectionStatus.reload(this.selectedUser);
    }

    openVende(e){
        $(".open-section-camisa").removeClass("active");
        $(this.openVendeBtn).addClass("active");
        this.status.style.display="none";
        this.vender.style.display="block";
        this.trocar.style.display="none";
        this.devolver.style.display="none";
        this.sectionVende.reload(this.selectedUser);
    }

    openTroca(e){
        $(".open-section-camisa").removeClass("active");
        $(this.openTrocaBtn).addClass("active");
        this.status.style.display="none";
        this.vender.style.display="none";
        this.trocar.style.display="block";
        this.devolver.style.display="none";
        this.sectionTroca.resetContainer();
    }

    openDevolve(e){
        $(".open-section-camisa").removeClass("active");
        $(this.openDevolveBtn).addClass("active");
        this.status.style.display="none";
        this.vender.style.display="none";
        this.trocar.style.display="none";
        this.devolver.style.display="block";
        this.sectionDevolve.resetContainer();
    }

}
let startCamisa = new StartCamisa();
export {startCamisa};