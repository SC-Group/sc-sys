export class ComercioCamisa{
    constructor(entidade,form,acao){
        this.entidade = entidade;
        this.acao = acao;
        this.form=$(form);
        this.resetContainer(acao);
        this.btnAdd = this.form.find(`#btn-add-${acao}`);
        if(this.btnAdd.length>0)
            this.btnAdd.click(e=>this.addAcao(e));
        this.container = this.form.find(`#${acao}-camisa-container`);

        let _this=this;
        this.form.submit(function(e){
            e.preventDefault();
            _this.save(this,e);
            return false;
        });
    }

    resetContainer(){
        document.querySelector(`#${this.acao}-camisa-container`).innerHTML="";
    }

    addAcao(e){}

    reload(entidadeData){
        this.entidade = entidadeData;
        this.loadFromServer();
        this.resetContainer();
    }

    loadFromServer(){

    }

    constructOptions(obj){
        let options = "";
        for (let i in obj){
            if(obj.hasOwnProperty(i))
                options+=`<option ${obj[i]?"selected":""} value="${i}">${i}</option>`;
        }
        return options;
    }

    constructTamanho(data, val){
        let tam = {};
        for(let i =0; i< data.length;i++){
            tam[data[i].tamanho]=(val===data[i].tamanho);
        }
        return tam;
    }

    constructTipo(data, val, tamVal){
        let tipo = {};
        for(let i =0; i < data.length;i++){
            if(data[i].tamanho===tamVal)
                tipo[data[i].tipo]=(val===data[i].tipo);
        }
        return tipo;
    }

    constructCor(data, val, tamVal, tipoVal){
        let cor = {};
        for(let i =0; i < data.length;i++){
            if(data[i].tamanho===tamVal && data[i].tipo===tipoVal)
                cor[data[i].cor]=(val===data[i].cor);
        }
        return cor;
    }

    constructQuantidade(data, tamVal, tipoVal, corVal,devolve){
        for(let i =0; i < data.length;i++){
            if(data[i].tamanho===tamVal && data[i].tipo===tipoVal && data[i].cor===corVal)
                return {id:data[i].id,maxVal:(devolve?(data[i].qtdComprada):(data[i].qtdTotal - data[i].qtdComprada))};
        }
    }


    addOptionsTamanho(elmTam){
        let tamList = this.constructTamanho(__CAMISAS,elmTam.val());
        let options = this.constructOptions(tamList);
        elmTam.html(options);
    }

    addOptionsTipo(elmTipo,elmTam){
        let tamList = this.constructTipo(__CAMISAS,elmTipo.val(),elmTam.val());
        let options = this.constructOptions(tamList);
        elmTipo.html(options);
    }

    addOptionsCor(elmCor,elmTam,elmTipo){
        let tamList = this.constructCor(__CAMISAS,elmCor.val(),elmTam.val(),elmTipo.val());
        let options = this.constructOptions(tamList);
        elmCor.html(options);
    }


    createLineParts(lineElm,lineElmId,lineElmName,devolve){
        let hasName = lineElmName!==undefined;
        lineElmName = lineElmName?`[${lineElmName}]`:"";
        lineElmId = `l[${lineElmId}]`;
        let tamSelect = lineElm.find("[name=tamanho]");
        tamSelect.attr("name",`${lineElmId}${lineElmName}[tamanho]`);
        let tipoSelect = lineElm.find("[name=tipo]");
        tipoSelect.attr("name",`${lineElmId}${lineElmName}[tipo]`);
        let corSelect = lineElm.find("[name=cor]");
        corSelect.attr("name",`${lineElmId}${lineElmName}[cor]`);
        let qtdInput;
        if(hasName){
            qtdInput = lineElm.closest(".trocaLineContainer").find("[name=qtd]");
        }else {
            qtdInput = lineElm.find("[name=qtd]");
        }
        let idInput = lineElm.find("[name=camisaid]");
        tamSelect.on("change",(e)=>{
            this.addOptionsTipo(tipoSelect,tamSelect);
            tipoSelect.trigger("change");
        });
        tipoSelect.on("change",(e)=>{
            this.addOptionsCor(corSelect,tamSelect,tipoSelect);
            corSelect.trigger("change");
        });
        corSelect.on("change",e=>{
           let dataqtd = this.constructQuantidade(__CAMISAS,tamSelect.val(),tipoSelect.val(),corSelect.val(),devolve);
           qtdInput.attr("min",0);
           qtdInput.attr("max",dataqtd.maxVal);
           qtdInput.attr("name",`${lineElmId}[qtd]`);
           idInput.attr("name",`${lineElmId}${lineElmName}[camisaid]`);
           idInput.val(dataqtd.id);
        });
        this.addOptionsTamanho(tamSelect);
        tamSelect.trigger("change");
    }


}