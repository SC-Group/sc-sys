let modelVenda='',modelDevolve='';
modelVenda = modelDevolve = `
<div class="row">
    <div class="mb-3 pt-2 bg-light pb-2 col border rounded border-dark-sc-primary">
        <div class="row">
            <div class="col-2 col-md-1">
                <div class="form-row">
                    <div class="col btn-group-sm ">
                        <button class="close-camisa-line mt-1 btn btn-danger">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-10 col-md-3">
                <div class="form-row">
                    <div class=" col mb-1 input-group-sm form-group input-group mt-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Qtd</span>
                        </div>
                        <input type="number" class="form-control" placeholder="Qtd" aria-label="qtd" name="qtd" aria-describedby="basic-addon1">
                    </div>
                </div>
            </div>
            <div class="col-md-8  col-sm-12">
            
                <div class="form-row">
                    <div class="col-12 input-group-sm mb-1 form-group input-group mt-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Camisa</span>
                        </div>
                        <select name="tamanho" class="custom-select form-control" aria-label="Tam" aria-describedby="basic-addon1">
                    
                        </select> 
                        <select name="tipo" class="custom-select form-control" aria-label="Tipo" aria-describedby="basic-addon1">
                        
                        </select>
                        <select name="cor" class="custom-select form-control" aria-label="Cor" aria-describedby="basic-addon1">
                        
                        </select>
                    </div>
                    <input type="hidden" name="camisaid" value="">
                </div>
            </div>
        </div>
        <div style="font-size: 10pt" class="text-center text-danger errorContainer">
            
        </div>
    </div>
</div>

`;

let modelTroca = `
<div class="row trocaLineContainer">
    <div  class="mb-3 bg-light pt-2 pb-2 col border rounded border-dark-sc-primary">
        <div class="form-row">
            <div class="col-md-1 col-2 pl-2 pr-2">
                <div class="d-md-flex d-none row">
                    <h6 class="col">&nbsp;</h6>
                </div>
                <div class="btn-group-sm ">
                    <button class="close-camisa-line mb-1 btn btn-danger">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="col-md-1 col-10">
                <div class="d-md-flex d-none row">
                    <h6 class="col">Qtd</h6>
                </div>
                <div class="lineQtd form-row">
                    <div class="col-12 input-group-sm mb-1 form-group input-group">
                         <div class="d-flex d-md-none input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Qtd</span>
                        </div>
                        <input type="number" name="qtd" class="form-control" placeholder="Qtd" aria-label="qtd" aria-describedby="basic-addon1">
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-12">
                <div class="row">
                    <h6 class="col">Troca:</h6>
                </div>
                <div class="lineTroca form-row">
                    <div class="col-12 input-group-sm mb-1 form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Camisa</span>
                        </div>
                        <select name="tamanho" class="custom-select form-control" aria-label="Tam" aria-describedby="basic-addon1">
                    
                        </select> 
                        <select name="tipo" class="custom-select form-control" aria-label="Tipo" aria-describedby="basic-addon1">
                        
                        </select>
                        <select name="cor" class="custom-select form-control" aria-label="Cor" aria-describedby="basic-addon1">
                        
                        </select>
                    </div>
                    <input type="hidden" name="camisaid" value="">
                </div>
            </div>
            <div class="col-md-5 col-sm-12">
                <div class="row">
                    <h6 class="col">Por:</h6>
                </div>
                <div class="linePor form-row">
                    <div class="col-12 input-group-sm mb-1 form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Camisa</span>
                        </div>
                        <select name="tamanho" class="custom-select form-control" aria-label="Tam" aria-describedby="basic-addon1">
                    
                        </select> 
                        <select name="tipo" class="custom-select form-control" aria-label="Tipo" aria-describedby="basic-addon1">
                        
                        </select>
                        <select name="cor" class="custom-select form-control" aria-label="Cor" aria-describedby="basic-addon1">
                        
                        </select>
                    </div>
                    <input type="hidden" name="camisaid" value="">
                </div>
            </div>
        <div style="font-size: 10pt" class="text-danger text-center errorContainer">
            
        </div>
    </div>
</div>
`;

let modelStatus=`
<div class="row">
    <div class="mb-3 pt-2 bg-light pb-2 col border rounded border-dark-sc-primary">
        <div class="row">
            <div class="col-12">
                <div class="form-row">
                    <div class=" col-md-3 mb-1 col-sm-6 input-group-sm form-group input-group mt-1">
                       <input type="text" class="form-control" placeholder="Camisa" aria-label="camisa" name="camisa" aria-describedby="basic-addon1" readonly>
                    </div>
                    <div class="col-6 col-md-3 mb-1 col-sm-6 input-group-sm form-group input-group mt-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Reserv.</span>
                        </div>
                        <input type="number" class="form-control" placeholder="Reservadas" aria-label="reservadas" name="reservadas" aria-describedby="basic-addon1" readonly>
                    </div>
                    <div class="col-6 col-md-3 mb-1 col-sm-6 input-group-sm form-group input-group mt-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Pagas</span>
                        </div>
                        <input type="number" class="form-control" placeholder="Pagas" aria-label="pagas" name="pagas" aria-describedby="basic-addon1" readonly>
                    </div>
                    <div class=" col-md-3 mb-1 col-sm-6 input-group-sm form-group input-group mt-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Entregues</span>
                        </div>
                        <input type="number" class="form-control" placeholder="Entregues" aria-label="entregues" name="entregues" aria-describedby="basic-addon1">
                    </div>
                    <input type="hidden" name="camisaid" value="">
                </div>
            </div>
        </div>
        <div style="font-size: 10pt" class="text-center text-danger errorContainer">
            
        </div>
    </div>
</div>

`;

export {modelTroca,modelStatus,modelDevolve,modelVenda};