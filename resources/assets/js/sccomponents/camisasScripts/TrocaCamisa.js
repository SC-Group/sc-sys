import {ComercioCamisa} from './ComercioCamisa'
import {modelTroca} from "./ModelosComponentesCamisa";

export class TrocaCamisa extends ComercioCamisa{
    constructor(user,container){
        super(user,container,"troca");
        this.modelo = $(modelTroca);
    }

    addAcao(e){
        let id = `${new Date().valueOf()}`;
        let line = this.modelo.clone();
        line.attr("id","l"+id);
        this.container.append(line[0]);
        line.find(".close-camisa-line").on("click",e=>line.remove());

        let lineTroca = line.find(".lineTroca");
        let linePor = line.find(".linePor");
        let lineQtd = line.find(".lineQtd");
        this.createLineParts(lineQtd,id,'lineQtd',true);
        this.createLineParts(lineTroca,id,"lineTroca",true);
        this.createLineParts(linePor,id,"linePor",true);

        return [line,id];

    }

    constructLines(response){
        let errors = response.errors || {};

        for(let i in errors){
            if(errors.hasOwnProperty(i)){
                let _idTroca = i.split("-");
                let _idPor = _idTroca[1];
                _idTroca = _idTroca[0];
                let line = $(this.addAcao()[0]);
                let lineTroca = line.find(".lineTroca");
                let linePor = line.find(".linePor");

                let camisaTroca = response.camisas[_idTroca-1];
                let camisaPor = response.camisas[_idPor-1];

                let tamanhoTroca = camisaTroca.tamanho;
                let tamanhoPor = camisaPor.tamanho;
                let tipoTroca = camisaTroca.tipo;
                let tipoPor = camisaPor.tipo;
                let corTroca = camisaTroca.cor;
                let corPor = camisaPor.cor;
                let idTroca = camisaTroca.id;
                let idPor = camisaPor.id;

                let selectTamTroca = lineTroca.find("[name*=tamanho]");
                let selectTamPor = linePor.find("[name*=tamanho]");
                selectTamTroca.val(tamanhoTroca);
                selectTamPor.val(tamanhoPor);
                lineTroca.find("[name*=tipo]").val(tipoTroca);
                linePor.find("[name*=tipo]").val(tipoPor);
                lineTroca.find("[name*=cor]").val(corTroca);
                linePor.find("[name*=cor]").val(corPor);
                lineTroca.find("[name*=camisaid]").val(idTroca);
                linePor.find("[name*=camisaid]").val(idPor);
                line.find(".errorContainer").html(errors[i]);
                selectTamTroca.trigger("change");
                selectTamPor.trigger("change");
            }
        }
    }

    mountAlert(response){
        let trocadas = "";
        for(let i in response.camisasTrocadas){
            if(response.camisasTrocadas.hasOwnProperty(i)){
                let _idTroca = i.split("-");
                let _idPor = _idTroca[1];
                _idTroca = _idTroca[0];

                let camisaTroca = response.camisas[_idTroca-1];
                let camisaPor = response.camisas[_idPor-1];

                let tamanhoTroca = camisaTroca.tamanho;
                let tamanhoPor = camisaPor.tamanho;
                let tipoTroca = camisaTroca.tipo;
                let tipoPor = camisaPor.tipo;
                let corTroca = camisaTroca.cor;
                let corPor = camisaPor.cor;

                let qtd = response.camisasTrocadas[i];
                trocadas+=`<div class="mb-3">
                        <div><strong>Recebe do cliente:</strong> ${tamanhoTroca}.${tipoTroca},${corTroca}: ${qtd}</div>
                        <div><strong>Entrega para cliente:</strong> ${tamanhoPor}.${tipoPor},${corPor}: ${qtd}</div>
                    </div>`;
            }
        }

        let message = `<div>
            <h5>Camisas físicas a serem trocadas </h5>
            <p>${trocadas}</p>
        </div>`;

        ModalAlert.load(message);
        setTimeout(()=>ModalAlert.show(),500);
    }

    save(elm,e) {

        let formData = new FormData(elm);
        formData.append("entidadeId", this.entidade.id);
        formData.append("entidadeTipo",this.entidade.tipo);

        let confirmed = ()=>{
            let comm = new Comm(window.location.getUrl() + "/trocarcamisas", false, formData, {
                contentType: false,
                processData: false
            });

            comm.success(response => {
                __CAMISAS = response.camisas;
                this.container.empty();
                this.constructLines(response);
                this.mountAlert(response);
            });

            comm.error(response => {
                __CAMISAS = response.responseJSON.camisas;
                this.container.empty();
            });
            comm.send();
        };

        Confirm.load(`Tem certeza de que deseja trocar estes items?`, confirmed);
        Confirm.show();

    }

}