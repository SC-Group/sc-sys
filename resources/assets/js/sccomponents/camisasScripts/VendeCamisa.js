import {ComercioCamisa} from './ComercioCamisa'
import {modelVenda} from "./ModelosComponentesCamisa";

export class VendeCamisa extends ComercioCamisa{
    constructor(entidade, container){
        super(entidade,container,"vende");
        this.modelo = $(modelVenda);
    }


    addAcao(e){
        let id = `${new Date().valueOf()}`;
        let line = this.modelo.clone();
        line.attr("id","l"+id);
        this.container.append(line[0]);
        line.find(".close-camisa-line").on("click",e=>line.remove());
        this.createLineParts(line,id);
        return [line,id];
    }

    mountAlert(response){
        let message = `<div>
            <strong>Valor total a cobrar do cliente: R$ ${response.precoTotal}</strong>
        </div>`;

        ModalAlert.load(message);
        setTimeout(()=>ModalAlert.show(),500);
    }


    constructCamisasFromServer(response){
        let errors = response.errors || {};

        for(let i=0;i<response[`entidadeCamisas`].length;i++){
            let entidadeCamisa = response[`entidadeCamisas`][i];
            let camisa = response.camisas[entidadeCamisa.id-1];
            let tamanho = camisa.tamanho;
            let tipo = camisa.tipo;
            let cor = camisa.cor;
            let id = camisa.id;
            let qtdreserv = entidadeCamisa.reservadas;
            let qtdpg = entidadeCamisa.pgAntes+entidadeCamisa.pgDepois;
            let reservadoNpg = qtdreserv-qtdpg;
            if(reservadoNpg>0){
                let line = $(this.addAcao()[0]);
                let selectTam = line.find(`select[name*=tamanho]`);
                selectTam.val(tamanho);
                line.find(`select[name*=tipo]`).val(tipo);
                line.find(`select[name*=cor]`).val(cor);
                line.find(`[name*=qtd]`).val(reservadoNpg);
                line.find(`[name*=camisaid]`).val(id);
                if(errors[id]!==undefined){
                    line.find(".errorContainer").html(errors[id]);
                }
                selectTam.trigger("change");
            }
        }
    }

    loadFromServer(){
        let comm = new Comm(window.location.getUrl()+"/entidadecamisas",false,{
            entidadeTipo:this.entidade.tipo,
            entidadeId:this.entidade.id
        });
        comm.success(response=>{
            __CAMISAS = response.camisas;
            this.container.empty();
            this.constructCamisasFromServer(response);
        });
        comm.error(response=>{
            console.log(response);
        });
        comm.send();
    }

    save(elm,e) {

        let btn = $(elm).find("button[type=submit]:focus");
        let formData = new FormData(elm);
        formData.append("vender", btn.val());
        formData.append("entidadeId", this.entidade.id);
        formData.append("entidadeTipo",this.entidade.tipo);


        let entregar = btn.val()==="venderEentregar";
        let confirmed = ()=>{
            let comm = new Comm(window.location.getUrl() + "/vendecamisas", false, formData, {
                contentType: false,
                processData: false
            });

            comm.success(response => {
                __CAMISAS = response.camisas;
                this.container.empty();
                this.constructCamisasFromServer(response);
                this.mountAlert(response);
            });

            comm.error(response => {
                __CAMISAS = response.responseJSON.camisas;
                this.container.empty();
                this.constructCamisasFromServer(response.responseJSON);
            });
            comm.send();
        };

        Confirm.load(`Tem certeza de que deseja vender ${entregar?"e <strong>entregar</strong> ":""}estes items?`, confirmed);
        Confirm.show();

    }

}