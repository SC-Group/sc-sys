import {StaffEditModel} from "./StaffsEditModel";

class StaffsEdit {

    constructor(line,comissoes, dias, horas) {
        this.comissoes = comissoes;
        this.dias =dias;
        this.horas = horas;
       this.staffId=parseInt(line.attr("data-id"));
       this.loadStaffData();
    }

    loadStaffData(){
        let _this= this;
        let comm = new Comm(window.location.getUrl() + "/staffdata",false, {"staff_id":this.staffId});
        comm.success(function(response){
            Comm.hideLoader();
            _this.loadModel(response);
        });
        comm.send();
    }
    loadModel(response){
        this.model = StaffEditModel(response, this.comissoes, dias, horas);
        Modal.resetAll();
        Modal.setBody(this.model);
        Modal.addFooterButton("Cancelar","btn btn-secondary",null,true);
        Modal.addFooterButton("Salvar","btn btn-sc-primary",()=>{
            this.model.submit();
        });
        Modal.setWidth("lg");
        Modal.show();
    }
}

export {StaffsEdit};