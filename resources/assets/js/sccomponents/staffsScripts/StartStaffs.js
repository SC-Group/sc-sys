import {StaffsEdit} from "./StaffsEdit";

class StartStaffs{
    constructor() {
        this.tableContent=$("#staffsTable");
        this.comissoes=window.comissoes || [];
        this.dias = window.dias || [];
        this.horas = window.horas || [];
        this.setEvents();
        this.staffEdit = null;
    }

    setEvents(){
        const _this=this;
        this.tableContent.find("button").click(function(){
            let line = $(this).closest("[data-id]");
            _this.openStaffEdit(line);
        })
    }

    loadComissoes(){

    }

    openStaffEdit(line){
        this.staffEdit = new StaffsEdit(line,this.comissoes, this.dias, this.horas);
    }

}

window.StartStaffs= new StartStaffs();