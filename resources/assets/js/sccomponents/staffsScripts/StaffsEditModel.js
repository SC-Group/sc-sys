function StaffEditModel(values, comissoes, dias, horas){

        let comissoesOptions = comissoes.reduce((acc,curr)=>{
            return acc+`<option value="${curr.id}" ${values.comissao?(values.comissao.id===curr.id?"selected":""):""}>${curr.nome}</option>`;
        },"");


        let tableHeader = dias.reduce((acc,curr)=>{
            let date = new Date(curr.data+"T00:00:00");
            return acc+`<td style="font-weight: bold"class="align-middle text-dark text-center bg-light">${date.getDay()+1}/${date.getMonth()+1}</td>`;
        },"");

        let disponibilidade = horas.reduce((acc, curr)=>{
            let inicio = new Date('1970-01-01T'+curr.inicio);
            let fim = new Date('1970-01-01T'+curr.fim);
            let line =`<tr><td class="bg-light text-dark align-middle text-center" style="width: 120px !important">
                    ${inicio.getHours()}:${(inicio.getMinutes() < 10 ? '0' : '')}${inicio.getMinutes()} / ${fim.getHours()}:${(fim.getMinutes() < 10 ? '0' : '')}${fim.getMinutes()}
            </td>`;
            line+=dias.reduce((acc,curr2)=>{
                let found = values.disponibilidades.find((elm)=>(elm.dia_id===curr2.id && elm.hora_id === curr.id));
                return acc+(found?`<td class="text-white align-middle text-center bg-success"><i class="fa fa-check"></i></td>`:`<td></td>`);
            },"");
            return acc+line+`</tr>`;
        },"");

    return $(`
        <form method="post" action="${location.getUrl()}/saveStaff">
            <input type="hidden" name="year" value="${Env.__YEAR}">
            <input type="hidden" name="_token" value="${$("meta[name=csrf-token]").attr("content")}">
           
            <div data-colapse="staffinfo">
                <input name="staffid" type="hidden" value="${values.id}">
                
                <div class="mb-3">
                    <strong>Nome:</strong> ${values.aluno.user.name}
                </div>
                 <div class="mb-3">
                    <strong>E-mail:</strong> ${values.aluno.user.email||""}
                </div>
                <div class="mb-3">
                    <strong>DRE:</strong> ${values.aluno.ufrj_dre||""}
                </div>
                <div class="mb-3">
                    <strong>Curso:</strong> ${values.aluno.curso.nome||""}
                </div>
                <div class="mb-3">
                    <strong>Status:</strong>
                     <select name="status" class="custom-select custom-select-sm">
                        <option value="">Selecione...</option>
                        <option value="Em processo" ${values.status === "Em processo" ? "selected" : ""} >Em processo</option>
                        <option value="Aprovado" ${values.status === "Aprovado" ? "selected" : ""}>Aprovado</option>
                        <option value="Dispensado" ${values.status === "Dispensado" ? "selected" : ""}>Dispensado</option>
                    </select>
                </div>
                
                <div class="mb-3">
                    <strong>Comissão:</strong> 
                    <select name="comissao" class="custom-select custom-select-sm">
                        <option value="">Selecione...</option>
                        ${comissoesOptions}
                    </select>
                </div>
                
                <div class="mb-3">
                    <strong>Disponibilidade:</strong>
                    <div class="table-responsive-sm">
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                <td class="bg-light"></td>
                                ${tableHeader}
                                </tr>
                            
                            </thead>
                            <tbody>
                            ${disponibilidade}
                            </tbody>
                        </table>
                    </div>
                </div>
                                         
            </div>
        </form>
    `);
}
export {StaffEditModel};