import {CronogramaDetails} from "./CronogramaDetails";

class StartCronogramas{
    constructor() {
        this.setEvents();
        this.hideEmpty();
    }

    hideEmpty(){
        $("")
    }

    openDetails(elm){
        this.detais = new CronogramaDetails(elm);
    }

    setEvents(){
        const _this=this;
        let cellAdd = $(".cronogramas .cellAdd");
        let cellEdit = $(".cronogramas .cellEdit");
        let cellShow = $(".cronogramas .cellShow");

        cellAdd.click(function(){
            _this.openDetails(this,true);
        });
        cellEdit.click(function(){
           _this.openDetails(this);
        });
        cellShow.click(function(){
            _this.openDetails(this);
        });

    }
}

$(window).resize(function() {
    let h = $("table")[0].offsetHeight;
    $(".overCol").css("height", (h - 2) + "px");
});
$(window).trigger("resize");

window.startCronogramas = new StartCronogramas();
export {StartCronogramas};