function CronogramaDetailModel(dados,dadosGerais, dia, horainicio, horafim, local, dataId) {
    let atividadesNomes = "";
    dados = dados||{};

    if(dadosGerais){
        atividadesNomes=dadosGerais.reduce(function(acc,curr){
            return acc+= `<option data-value = "${curr.nome}" value="${curr.id}" ${dados.nome===curr.nome?'selected':''}>${curr.nome}</option>`;
        },"");
        atividadesNomes = `
            <div class="mb-3">
                Atividade:
                <div class="">
                    <select name="atividade" class="custom-select custom-select">
                         <option value="">Nenhuma</option>
                        ${atividadesNomes}
                    </select>
                </div>
                <div class="error-message text-danger" id="error-message-tipo"></div>
            </div>
        `;
    }else{
        atividadesNomes = `<h3 class="text-dark-sc-primary mb-4">${dados.nome}</h3>`;
    }

    let palestrantes = (dados.palestrantes!=null?dados.palestrantes.reduce(function(acc,curr){
       return acc+=`<li style="font-size: 10pt" class="list-group-item p-1 text-muted">${curr.nome}</li>`;
    },""):"");

    let atividadeInfo = dados.id!=null?`
        <div class="mb-3">
            Palestrante${(dados.palestrantes.length>1)?"s":""}:
            <ul class="list-group list-group-horizontal">
            ${palestrantes}
            </ul>
        </div>
        ${dados.trilha!=null?
            `<div class="mb-3">
            Trilha:
            <div class="p-1" style="font-size:10pt;border-radius:5px;color: ${dados.trilha.cor.l>50?"#333":"#fefefe"}; background-color: hsl(${dados.trilha.cor.h},${dados.trilha.cor.s}%,${dados.trilha.cor.l}%)">
                ${dados.trilha.nome}
            </div>
            </div>`:""
        }
        <div class="mb-3">
            Tipo de Atividade:
            <div class="p-1" style="font-size:10pt;border-radius:5px;color: #333;">
                ${dados.tipo}
            </div>
        </div>
    `:"";

    return $(`
         <form style="font-size: 12pt" method="post" action="#">
                    <input type="hidden" name="year" value="${Env.__YEAR}">
                    <input type="hidden" name="_token" value="${$("meta[name=csrf-token]").attr("content")}">
                    <div data-colapse="atividadeinfo">
                        <input name="dataid" type="hidden" value="${dataId}">
                        <h6 class="text-muted">${dia} - de  ${horainicio} a ${horafim} - ${local}</h6>
                      
                        ${atividadesNomes}
                        
                        
                        
                        ${atividadeInfo}
                        
                    </div>
                                     
                </form>
    `);
}

export {CronogramaDetailModel}