import {CronogramaDetailModel} from "./CronogramaDetailModel";

class CronogramaDetails{
    constructor(elm){
        this.cellBtn = $(elm);
        this.generalData = window.atividades;
        let dataString = this.cellBtn.attr("data-atividade");
        this.dataId = this.cellBtn.attr("data-id");
        this.dataDia = this.cellBtn.attr("data-dia");
        this.dataHoraInicio=this.cellBtn.attr("data-horainicio");
        this.dataHoraFim=this.cellBtn.attr("data-horafim");
        this.dataLocal=this.cellBtn.attr("data-local");
        this.data = {};
        if(dataString != null && dataString!=="") {
            this.data = JSON.parse(this.cellBtn.attr("data-atividade"));
        }
        this.model = CronogramaDetailModel(this.data, this.generalData , this.dataDia, this.dataHoraInicio, this.dataHoraFim, this.dataLocal, this.dataId);
        this.setEvents(this.model);
        this.openModal(this.model);
    }

    openModal(contentForm){
        Modal.resetAll();
        Modal.setWidth("lg");
        Modal.setBody(contentForm);
        if(this.generalData) {
            Modal.addFooterButton("Salvar", "btn-sc-primary", () => {
                contentForm.submit()
            })
        }
        Modal.addFooterButton("Fechar","btn-secondary", null,true);
        Modal.show();
    }

    changeContentModal(contentForm){
        Modal.setBody(contentForm);
        Modal.resetFooter(contentForm);
        if(this.generalData) {
            Modal.addFooterButton("Salvar", "btn-sc-primary", () => {
                contentForm.submit()
            })
        }
        Modal.addFooterButton("Fechar","btn-secondary", null,true);
    }

    setEvents(contentForm){
        const _this=this;
        contentForm.find("[name=atividade]").change(function(e){
            let data = _this.generalData.find((elm)=>{
                return (''+elm.nome) === $(this).find(":selected").attr("data-value");
            });
            data = CronogramaDetailModel(data, _this.generalData , _this.dataDia, _this.dataHoraInicio, _this.dataHoraFim, _this.dataLocal, _this.dataId);
            _this.setEvents(data);
            _this.changeContentModal(data);
        });
    }

}

export {CronogramaDetails};