/**
 * @return {string}
 */
function EmpresaEditModel(data, errors, edicao) {
    let nivel="";
    errors = errors || {};
    if(data && data.empresaNivelId!==undefined && data.empresaNivelId!=null){
        let options = data.empresaNiveis.reduce((acc,curr)=>{
           return  acc+`<option ${data.empresaNivelId===curr.id?"selected":""} value="${curr.id}">${curr.nome}</option>`;
        },"");
        nivel = `
        <div class="form-group">
            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Nivel de patrocínio</label>
            <select name="empresaNivelPatrocinioId" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
                <option value="">Escolha</option>
                ${options}
            </select>
            <span style="font-size: 9pt" class="text-danger">${errors.empresaNivelPatrocinioId?errors.empresaNivelPatrocinioId.join("; <br>"):""}</span>
        </div>
        `;
    }else{
        nivel=`<input type="hidden" name="empresaNivelPatrocinioId" value="0">`;
    }

    return `
    <form enctype="multipart/form-data" method="post" action="${location.getUrl()}/saveEmpresa">
        <input type="hidden" name="year" value="${Env.__YEAR}">
        <input type="hidden" name="_token" value="${$("meta[name=csrf-token]").attr("content")}">
        <input type="hidden" value="${data&&data.empresaId?data.empresaId:""}" name="empresaId">
        <input type="hidden" name="tipoCadastro" value="${edicao?"edicao":"novo"}">
        <div class="form-group">
            <label class="" for="customFile">Logo</label>
            <div style="border-color: #ced4da" class="border mb-0 rounded">
                <div class="d-flex justify-content-center p-2">
                    <img style="max-width: 100%; max-height: 80px" src="${data&&data.empresaLogo?data.empresaLogo:""}" alt="">
                </div>
                <div>
                    <input type="file" name="empresaLogoFile" class="d-none" id="customFile">
                    <label style="border-radius: 0 0 4px 4px" class="btn btn-dark w-100 mb-0" for="customFile">
                        <i class="fa fa-upload"></i>
                    </label>
                </div>
            </div>  
            <span style="font-size: 9pt" class="text-danger">${errors.empresaLogo?errors.empresaLogo.join("; <br>"):""}</span>
        </div>
        
        
        <div class="form-group">
            <label for="empresaModalNome">Nome</label>
            <input type="text" name="empresaNome" class="form-control" id="empresaModalNome" value="${data&&data.empresaNome?data.empresaNome:""}">
            <span style="font-size: 9pt" class="text-danger">${errors.empresaNome?errors.empresaNome.join("; <br>"):""}</span>
        </div>
        
        <div class="form-group">
            <label for="empresaModalRazaoSocial">Razão Social</label>
            <input type="text" name="empresaRazaosocial" class="form-control" id="empresaModalRazaoSocial" value="${data&&data.empresaRazaosocial?data.empresaRazaosocial:""}">
            <span style="font-size: 9pt"  class="text-danger">${errors.empresaRazaosocial?errors.empresaRazaosocial.join("; <br>"):""}</span>
        </div>
        
        ${nivel}
        
        <div class="form-group">
            <label for="empresaModalResponsavel">Responsável</label>
            <input type="text" name="empresaResponsavel" class="form-control" id="empresaModalResponsavel" value="${data&&data.empresaResponsavel?data.empresaResponsavel:""}">
            <span style="font-size: 9pt" class="text-danger">${errors.empresaResponsavel?errors.empresaResponsavel.join("; <br>"):""}</span>
        </div>
        <div class="form-group">
            <label for="empresaModalTelefone">Telefone</label>
            <input type="text" data-inputmask="'mask': '(99) 99999-9999', 'removeMaskOnSubmit': true" name="empresaTelefone" class="form-control" id="empresaModalTelefone"  value="${data&&data.empresaTelefone?data.empresaTelefone:""}" placeholder="(21) 98888-8888">
            <span style="font-size: 9pt" class="text-danger">${errors.empresaTelefone?errors.empresaTelefone.join("; <br>"):""}</span>
        </div>
        <div class="form-group">
            <label for="empresaModalEmail">E-mail</label>
            <input type="email"  name="empresaEmail" class="form-control" id="empresaModalEmail" value="${data&&data.empresaEmail?data.empresaEmail:""}" placeholder="name@example.com">
            <span style="font-size: 9pt" class="text-danger">${errors.empresaEmail?errors.empresaEmail.join("; <br>"):""}</span>
        </div>
        <div class="form-group">
            <label for="empresaModalCnpj" >CNPJ</label>
            <input type="text" data-inputmask="'mask': '99.999.999/9999-99' , 'removeMaskOnSubmit': true" name="empresaCnpj" class="form-control" id="empresaModalCnpj" value="${data&&data.empresaCnpj?data.empresaCnpj:""}" placeholder="99.999.999/9999-99">
            <span style="font-size: 9pt" class="text-danger">${errors.empresaCnpj?errors.empresaCnpj.join("; <br>"):""}</span>
        </div>
        <div class="form-group">
            <label for="empresaModalInscMun">Inscrição Municipal</label>
            <input type="text" name="empresaInscMun" data-inputmask="'mask':'99999'" class="form-control" value="${data&&data.empresaInscMun?data.empresaInscMun:""}" id="empresaModalInscMun" >
            <span style="font-size: 9pt" class="text-danger">${errors.empresaInscMun?errors.empresaInscMun.join("; <br>"):""}</span>
        </div>
        <div class="form-group">
            <label for="empresaModalInscEst">Inscrição Estadual</label>
            <input type="text" name="empresaInscEst" data-inputmask="'mask':'99999999'" class="form-control" value="${data&&data.empresaInscEst?data.empresaInscEst:""}" id="empresaModalInscEst" >
            <span style="font-size: 9pt" class="text-danger">${errors.empresaInscEst?errors.empresaInscEst.join("; <br>"):""}</span>
        </div>
        <div class="form-group">
            <label for="empresaModalSite">Site</label>
            <input type="text" name="empresaSite" data-inputmask="'mask':'http:\\a'" class="form-control" id="empresaModalSite" value="${data&&data.empresaSite?data.empresaSite:""}" placeholder="www.example.com">
            <span style="font-size: 9pt" class="text-danger">${errors.empresaSite?errors.empresaSite.join("; <br>"):""}</span>
        </div>
       
    </form>

    `;
}

export {EmpresaEditModel};