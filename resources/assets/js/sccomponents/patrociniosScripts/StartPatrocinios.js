import {EmpresaEditModel} from "./EmpresaEditModel";

class StartPatrocinios{

    constructor() {
        this.container = $("#gerencia-de-patrocinios");
        this.escolhidos = this.container.find(".patrocinios-escolhidos");
        this.todos = this.container.find(".todas-as-empresas");

        this.patrociniocard = this.container.find("[data-name=patrociniocard]");
        this.optionsbar = this.container.find(".optionsbar");


        this.setEvents();

    }

    setAreaEvents(area){
        const _this=this;
        let optionsbar =  area.find(".optionsbar");
        let selectionActivated = false;
        let cards = area.find("[data-name=patrociniocard]");
        let inputs =  cards.find("input[name^=selecionado]");
        optionsbar.find(".select-btn").click(function(){
            optionsbar.addClass("bg-secondary");
            optionsbar.find(".after-select").removeClass("d-none");
            optionsbar.find(".before-select").addClass("d-none");
            selectionActivated = true;
        });

       optionsbar.find(".cancel-btn").click(function () {
           optionsbar.removeClass("bg-secondary");
           optionsbar.find(".after-select").addClass("d-none");
           optionsbar.find(".before-select").removeClass("d-none");
           selectionActivated = false;
           inputs.val(false);
           cards.removeClass("border-sc-primary")
       });

        area.find("[data-name=patrociniocard]").click(function(e){
            let elm = $(this);
            if(selectionActivated) {
                let input = elm.find("input[name^=selecionado]");
                if(input.val() === "true"){
                    input.val(null);
                    elm.removeClass("border-sc-primary")
                }else{
                    input.val(true);
                    elm.addClass("border-sc-primary")
                }
            }else{
                _this.cardClickAction(elm,area);
            }
            e.preventDefault();
            e.stopPropagation();
        });
    }

    fileUploadEvent(form){
        form.find('input[type=file]').on('change', function() {
            if (this.files && this.files[0]) {
                let imgcontainer = $(this).closest(".form-group");
                let img = imgcontainer.find("img");
                let reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
                reader.addEventListener("load",function (e) {
                    img[0].src = reader.result;
                });

            }
        });
    }

    cardClickActionEmpresa(elm){
        let id = elm.attr("data-id");
        const _this = this;
        const comm = new Comm(`${location.getUrl()}/getEmpresa`,false, {id:id});
        comm.success(function (response) {
            Comm.hideLoader();
            let data = {
                empresaId:response.data.id,
                empresaNome:response.data.nome,
                empresaResponsavel: response.data.responsavel,
                empresaTelefone: response.data.telefone,
                empresaEmail: response.data.email,
                empresaCnpj:response.data.cnpj,
                empresaInscMun: response.data.inscmun,
                empresaInscEst: response.data.inscest,
                empresaSite: response.data.site,
                empresaRazaosocial: response.data.razaosocial,
                empresaLogo: response.data.url_logo
            };

            _this.mountPatrocinioModal(data);
        });
        comm.send();
    }

    cardClickActionPatrocinio(elm){
        let id = elm.attr("data-id");
        const _this = this;
        const comm = new Comm(`${location.getUrl()}/getPatrocinador`,false, {id:id});
        comm.success(function (response) {
            Comm.hideLoader();

            let data = {
                empresaId:response.data.id,
                empresaNome:response.data.nome,
                empresaResponsavel: response.data.responsavel,
                empresaTelefone: response.data.telefone,
                empresaEmail: response.data.email,
                empresaNivelId: response.data.nivel_patrocinio[0].id,
                empresaNiveis:response.niveis,
                empresaCnpj:response.data.cnpj,
                empresaInscMun: response.data.inscmun,
                empresaInscEst: response.data.inscest,
                empresaSite: response.data.site,
                empresaRazaosocial: response.data.razaosocial,
                empresaLogo: response.data.url_logo
            };
            _this.mountPatrocinioModal(data,null,true);
        });
        comm.send();
    }

    mountPatrocinioModal(content, errors, edicao){
        edicao = (edicao===true);
        let contentelm = $(EmpresaEditModel(content, errors, edicao));
        this.fileUploadEvent(contentelm);
        Modal.resetAll();
        Modal.setWidth("lg");
        Modal.setBody(contentelm);
        Modal.addFooterButton("Cancelar","btn btn-secondary",null,true);
        Modal.addFooterButton("Salvar","btn btn-sc-primary",()=>{
            contentelm.submit();
        });
        Modal.show();
        contentelm.find(":input").inputmask();

    }


    cardClickAction(elm, area){
        if(area.hasClass("patrocinios-escolhidos")){
            this.cardClickActionPatrocinio(elm);
        }else{
            this.cardClickActionEmpresa(elm);
        }

    }

    adicionarAEvento(){
        if(niveisPatrocinio.length===0){
            Messages.error("Primeiro cadastre níveis de patrocínio para este evento.");
            return;
        }
        if(this.todos.find("[name^=selecionado][value=true]").length===0){
            Messages.error("Primeiro selecione clicando nas organizações");
            return;
        }
        let nivel = $(`
                <div class="form-group">
                    <label>Escolha o nível de patrocínio para adicionar</label>
                    <select class="custom-select my-1 mr-sm-2">
                        <option value="">Escolha</option>
                        ${
            niveisPatrocinio.reduce((acc,curr)=>{
                return acc+`<option value="${curr.id}">${curr.nome}</option>`;
            },"")
        }
                    </select>
                </div>
            `);
        Modal.resetAll();
        Modal.setWidth("md");
        Modal.setBody(nivel);
        Modal.addFooterButton("Cancelar","btn btn-secondary",null,true);
        Modal.addFooterButton("Salvar","btn btn-sc-primary",()=>{
            let nivelEscolhido = nivel.find("select").val();
            $(this.todos).find("input[name=sendForm]").val("adicionar");
            $(this.todos).find("input[name=nivelId]").val(nivelEscolhido);
            this.todos.submit();
        });
        Modal.show();

    }

    excluirEmpresa(){
        if(this.todos.find("[name^=selecionado][value=true]").length===0){
            Messages.error("Primeiro selecione clicando nas organizações");
            return;
        }

        Confirm.resetAll();
        Confirm.load(
            "Tem certeza que gostaria de excluir permanentemente este(s) item(s)?",
            ()=>{
                this.todos.find("input[name=sendForm]").val("excluir");
                this.todos.submit();
            },
            ()=>{},
            "Sim",
            "Cancelar"
        );

        Modal.show();
    }

    removerDeEvento(){
        if(this.escolhidos.find("[name^=selecionado][value=true]").length===0){
            Messages.error("Primeiro selecione clicando nas organizações");
            return;
        }
        Confirm.resetAll();
        Confirm.load(
            "Tem certeza que gostaria de remover este(s) item(s) deste evento?",
            ()=>{
                this.escolhidos.find("input[name=sendForm]").val("remover");
                this.escolhidos.submit();
            },
            ()=>{},
            "Sim",
            "Cancelar"
        );

        Modal.show();
    }

    setEvents(){
        const _this=this;
        let optionstodos = this.todos.find(".optionsbar");
        optionstodos.find(".add-new").click(()=>this.mountPatrocinioModal());

        this.escolhidos.find("[name=remover]").click((e)=>{e.preventDefault(); this.removerDeEvento() });
        this.todos.find("[name=adicionar]").click((e)=>{e.preventDefault(); this.adicionarAEvento() });
        this.todos.find("[name=excluir]").click((e)=>{e.preventDefault(); this.excluirEmpresa() });
        this.setAreaEvents(this.escolhidos);
        this.setAreaEvents(this.todos);
    }

    optionsBarEvents(){



    }

}

let startPatrocinio=new StartPatrocinios();
window.startPatrocinio = startPatrocinio;
export {startPatrocinio};