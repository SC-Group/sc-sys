import tableAtividadesLineModel from './TableAtividadesLineModel'
import AtividadesEdit from './AtividadesEdit'

class StartAtividades{
    constructor(){
        this.atividadesMainContainer = $(`#atividadesMainContainer`);
        this.table = $("#atividadesTable");
        this.atividadesPage = 1;
        this.orderedTable = new window.OrderTable(this.table);
        this.trilhas = [];
        this.cronogramas = [];
        this.loadData(this.atividadesPage);
        this.data = {};
        this.loadTrilhas();
        this.loadCronogramas();
        this.pagination = new Pagination();
        this.dataIsReady = 0;

    }
    loadEdit(isreload){
        if(this.dataIsReady>=3) {
            if (!isreload) {
                this.atividadesEdit = new AtividadesEdit(this, this.cronogramas, this.trilhas, this.data);
            } else {
                this.atividadesEdit.cronogramas = this.cronogramas;
                this.atividadesEdit.trilhas = this.trilhas;
                this.atividadesEdit.values = this.data;
            }
        }
    }
    constructAtividades(response){
        let _this=this;
        const tbody = this.table.find(`tbody`);
        let body = response.data.reduce((acc, elm)=>{
            return acc + tableAtividadesLineModel(elm)
        },'');
        tbody.html(body);

        tbody.find(".atividade-edit").click(function(e){
            let error = false;
            if(_this.data===undefined || _this.data.length===0){
                Messages.error("Dados da atividade não existem");
                error=true;
            }
            if(!error){
                _this.atividadesEdit.openAtividade(e,this);
            }

        });

        this.orderedTable.setOriginalOrder();

        let pagination = this.pagination.createPaginationObject(response.current_page,response.last_page, (page)=>this.loadData(page));
        this.atividadesMainContainer.find('.paginationContainer').html(pagination);
    }

   loadTrilhas(isreload){
        const _this = this;
        const comm = new Comm(`${location.getUrl()}/trilhas`);
        comm.success(function (response) {
            _this.trilhas = response;
            _this.dataIsReady++;
            _this.loadEdit(isreload);
        });
        comm.error(()=>{
            _this.dataIsReady++;
            _this.loadEdit(isreload);
        });
        comm.send();
    }

   
    loadCronogramas(isreload){
        const _this = this;
        const comm = new Comm(`${location.getUrl()}/disponibilidades`);
        comm.success(function (response) {
            _this.cronogramas = response;
            _this.dataIsReady++;
            _this.loadEdit(isreload);
        });
        comm.error(()=>{
            _this.dataIsReady++;
            _this.loadEdit(isreload);
        });
        comm.send();
    }

    loadData(page,isreload){
        const _this=this;
        const comm = new Comm(`${location.getUrl()}/all?page=${page}`);
        comm.success(function (response) {
            _this.data = response.data;
            _this.constructAtividades(response,isreload);
            _this.dataIsReady++;
            _this.loadEdit(isreload);
        });
        comm.send();
    }


}

window.StartAtividades = StartAtividades;
window.a = new StartAtividades();