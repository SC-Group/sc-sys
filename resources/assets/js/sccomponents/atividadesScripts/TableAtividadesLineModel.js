export default function tableAtividadesLineModel(item) {
    let cor;
    let lightTxt;
    let trilha = "-";
    if(item.trilha){
        cor = JSON.parse(item.trilha.cor);
        lightTxt = parseFloat(cor.l)<=60;
        trilha = `
            <div style="
                    word-break: keep-all;
                    display: inline-block;
                    border: 1px solid ${ lightTxt? `hsl(${cor.h}, ${cor.s}%, ${cor.l}%) ` :"#333"};
                    padding: 3px;
                    border-radius: 3px;
                    background-color: hsl(${cor.h}, ${cor.s}%, ${cor.l}%);
                    color:${ lightTxt? "#fff"  :"#333"}
                    ">
                ${item.trilha.nome}
            </div>
        `
    }


    let line = `
    <tr data-id="${item.id}">
        <td>
            <button class="atividade-edit btn btn-sm btn-dark"><i class="fa fa-edit"></i></button>
        </td>
        <td data-col="nome">
            ${item.nome}
        </td>
        <td data-col="aprovada">
            ${item.aprovada ?'Sim':'Não'}
        </td>
        <td data-col="tipo">
            ${item.tipo}
        </td>
        <td data-col="organizacao_vinculada">
            ${item.organizacao_vinculada?item.organizacao_vinculada:"-"}
        </td>
        <td data-col="trilha">
           ${trilha}
        </td>
    </tr>
`;
return line;
}