import {AtividadesEditModel} from "./AtividadesEditModel";
export default class AtividadesEdit{
    constructor(start,cronogramas, trilhas,data){
        this.start = start;
        this.trilhas = trilhas;
        this.values = data;
        this.cronogramas = cronogramas;
        this.cronogramasSelecionados = {};
    }

    saveAtividade(index){
        const _this = this;
        let formData = new FormData(this.form[0]);
        if( Object.keys(this.cronogramasSelecionados).length>0)
             formData.append("cronogramas_selecionados",JSON.stringify(this.cronogramasSelecionados));
        let comm = new Comm(window.location.getUrl() + "/saveatividade",false,formData,{
            contentType: false,
            processData: false
        });

        comm.success(function(response){
            $(".error-message").html("");
            _this.start.dataIsReady = 0;
            _this.start.loadTrilhas(true);
            _this.start.loadCronogramas(true);
            _this.start.loadData(_this.start.atividadesPage,true)
        });

        comm.error((response)=>{
           let errors = response.responseJSON.errors;
           $(".error-message").html("");
           for(let i in errors){
               if(errors.hasOwnProperty(i)){
                   $("#error-message-"+i).text(errors[i].join("<br>"));
               }
           }
        });

        comm.send();
    }

    openAtividade(e,btn){
        btn = $(btn);
        let row = btn.closest('tr');
        let rowId = parseInt(row.attr("data-id"));
        let index = this.values.findIndex(function(element) {
            return element.id === rowId;
        });

        let value = this.values[index];

        this.form = AtividadesEditModel(this.cronogramas, this.trilhas,value);

        Modal.resetAll();
        Modal.setWidth('lg');
        Modal.setBody(this.form);
        Modal.setTitle('Editar atividade');
        Modal.addFooterButton('Fechar','btn-sc-secondary',()=>{},true);
        Modal.addFooterButton('Salvar','btn-sc-primary',()=>{this.saveAtividade(index)},false);
        Modal.show();
        this.setFormEvents();
    }

    setFormEvents(){
        const _this = this;
        let selectTrilha = this.form.find('[name=trilha]');
        let selectTrilhaWrapper = selectTrilha.parent();
        let abaBtns = this.form.find('a[data-target]');

        abaBtns.click(function (e) {
            $(`[data-colapse]`).css(`display`,'none');
            abaBtns.removeClass('active');
            let target = $(this).attr("data-target");
            $(this).addClass('active');
            $(`[data-colapse=${target}]`).css("display","");
        });

        selectTrilha.change(function(e){
            let option = $(this).find("option:selected");
            let bg = option.css('background-color');
            let txt = option.css('color');
            selectTrilhaWrapper.css('background-color',bg);
            selectTrilha.css('color',txt);
        });

        this.form.find("a[data-cronograma]:not(.disabled)").click(function(){
            let cronograma = JSON.parse($(this).attr("data-cronograma"));
            if($(this).hasClass("active")){
                $(this).removeClass("active");
                $(this).removeClass("btn-secondary");
                $(this).addClass("btn-light");
                delete _this.cronogramasSelecionados[`${cronograma[0]}-${cronograma[1]}`];
            }else{
                $(this).addClass("active");
                $(this).addClass("btn-secondary");
                $(this).removeClass("btn-light");
                _this.cronogramasSelecionados[`${cronograma[0]}-${cronograma[1]}`] = cronograma;
            }
        });

         $('[data-toggle="tooltip"]').tooltip();

        selectTrilha.trigger('change');
    }


}