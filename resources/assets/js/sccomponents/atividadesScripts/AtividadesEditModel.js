function AtividadesEditModel(cronogramas, trilhas,values) {

    let trilhasOptions =  trilhas.reduce((acc,cur)=>{
        let cor = JSON.parse(cur.cor);
        let lightTxt = parseFloat(cor.l)<=60;
        return acc += `
            <option style='
                background-color: hsl(${cor.h}, ${cor.s}%, ${cor.l}%);
                color:${ lightTxt? "#fff"  :"#333"}' 
                value="${cur.id}" ${(values.trilha && values.trilha.id===cur.id)?'selected':''}>
                    ${cur.nome}          
            </option>
        `;
    },'');

    let cronogramasDias = cronogramas.dias.reduce((acc,curr)=>{
            let date = new Date(curr.data);
            return acc+=`<td style="min-width: 200px; max-width: 200px; width: 200px">
                ${date.getDate()+1}/${date.getMonth()+1}
            </td>`;
    },'');


    let cronogramasHoras = cronogramas.horas.reduce((acc,curr)=>{
        let cronogramasValues = cronogramas.dias.reduce((acc2,curr2)=>{
            let horaId = curr.id;
            let diaId = curr2.id;
        
            let palestrantes = values.palestrantes.reduce((acc3, curr3)=>{
                let isPlaced = curr3.disponibilidades.findIndex((element)=>(element.hora_id === horaId && element.dia_id === diaId))>=0;
                return acc3+=isPlaced?`<span class="badge">${curr3.user.name}</span><br>`:'';
            },'');
            return acc2+=`<td>
                <a data-cronograma="[${diaId},${horaId}]" 
                style="overflow: hidden; word-wrap:break-word; width: 100%; height: 100%; min-height: 60px" 
                class="btn-light btn disabled border-secondary">
                 ${palestrantes}
                </a>
            </td>`;
        },'');
        let hour1 = new Date("1997-01-01T"+curr.inicio);
        let h1min = hour1.getMinutes();
        h1min = (h1min<10?"0":"")+h1min;
        let hour2 = new Date("1997-01-01T"+curr.fim);
        let h2min = hour1.getMinutes();
        h2min = (h2min<10?"0":"")+h2min;
        return acc+=`<tr>
                <td class="bg-sc-primary py-3">
                    ${hour1.getHours()}:${h1min} a ${hour2.getHours()}:${h2min}
                </td>
                ${cronogramasValues}
            </tr>`;
    },'');

    return $(`
                <form style="font-size: 12pt">
                    <ul class="nav nav-tabs mb-4">
                      <li class="nav-item">
                        <a data-target="atividadeinfo" class="nav-link active" href="#">Atividade</a>
                      </li>
                      <li class="nav-item">
                        <a data-target="atividadecronograma" class="nav-link" href="#">Disponibilidades</a>
                      </li>
                    </ul>
                    <div data-colapse="atividadeinfo">
                        <input name="id" type="hidden" value="${values.id}">
                        <div class="mb-3">
                            Codigo: <spam class="badge badge-pill badge-info">${values.codigo}</spam>
                        </div>
                        <div class=" mb-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-sm-nome">Nome</span>
                                </div>
                                <input name="nome" type="text" class="form-control" value="${values.nome}">
                            </div>
                            <div class="error-message text-danger" id="error-message-nome"></div>
                        </div>
                        
                        
                        <div class="mb-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-sm-capacidade">Capacidade</span>
                                </div>
                                <input name="capacidade" type="number" class="form-control" value="${values.capacidade || 0}">
                            </div>
                            <div class="error-message text-danger" id="error-message-capacidade"></div>
                        </div>
                         
                        
                        <div class="form-group sm mb-3">
                            <label for="edit-atividade-descricao">Descrição</label>
                            <textarea name="descricao" style="font-size: 10pt" id="edit-atividade-descricao" class="form-control" rows="5">${values.descricao||""}</textarea>
                            <div class="error-message text-danger" id="error-message-descricao"></div>
                        </div>
                        
                         <div class="form-group sm mb-3">
                            <label for="edit-atividade-recursos">Recursos</label>
                            <textarea name="recursos" style="font-size: 10pt" id="edit-atividade-recursos" class="form-control"  rows="5">${values.recursos||""}</textarea>
                            <div class="error-message text-danger" id="error-message-recursos"></div>
                        </div>
                        <div class="mb-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-sm">Tipo</span>
                                </div>
                                <select name="tipo" class="custom-select custom-select-sm">
                                    <option value="Minicurso" ${values.tipo==='Minicurso'?'selected':''}>Minicurso</option>
                                    <option value="Workshop" ${values.tipo==='Workshop'?'selected':''}>Workshop</option>
                                    <option value="Palestra" ${values.tipo==='Palestra'?'selected':''}>Palestra</option>
                                </select>
                            </div>
                            <div class="error-message text-danger" id="error-message-tipo"></div>
                        </div>
    
                        <div class="row mb-3">
                            <div class="col flex-nowrap">
                                <label class="d-block" for="inlineFormCustomSelectPref">Aprovação:</label>
                                <div class="mx-2 my-2 custom-control d-inline custom-checkbox">
                                    <input ${values.aprovada?"checked":""} type="checkbox" value="1" name="aprovacao"  class="custom-control-input" id="edit-atividade-aprovacao">
                                    <label class="custom-control-label" for="edit-atividade-aprovacao">Aprovado</label>
                                </div>
                                <div class="error-message text-danger" id="error-message-aprovacao"></div>
                            </div>
                        </div>
    
                        <div class="mb-3">
                            <div class="input-group input-group-sm d-inline-flex">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-sm">Trilha</span>
                                </div>
                                <div style="flex: 1 1 auto;" class="border rounded-right" >
                                    <select name="trilha" style='background-color: transparent' class=" border-0 w-100 custom-select custom-select-sm" id="inputGroupSelect01">
                                        <option style="color: rgb(102, 102, 102);" ${!values.trilha?'selected':''} value="">Selecione...</option>
                                       ${trilhasOptions}
                                    </select>
                                </div>
                            </div>
                            <div class="error-message text-danger" id="error-message-trilha"></div>
                        </div>
                        
                        <div class="row mb-3">
                            <div class="col flex-xl-nowrap">
                                <label class="d-block" for="inlineFormCustomSelectPref">Gera certificado específico:</label>
                                <div class="mx-2 my-2 custom-control d-inline custom-radio">
                                    <input type="radio" ${values.gera_certificado?"checked":""} value="1"  name="certificado"  class="custom-control-input" id="customatividadesim">
                                    <label class="custom-control-label" for="customatividadesim">Sim</label>
                                </div>
                                <div class="mx-2 my-2 custom-control d-inline custom-radio">
                                    <input type="radio" ${values.gera_certificado?"":"checked"} value="0" name="certificado" class="custom-control-input" id="customatividadenao">
                                    <label class="custom-control-label" for="customatividadenao">Não</label>
                                </div>
                                <div class="error-message text-danger" id="
                                certificado"></div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col flex-nowrap">
                                <label class="d-block" for="inlineFormCustomSelectPref">Inscrição específica necessária:</label>
                                <div class="mx-2 my-2 custom-control d-inline custom-radio">
                                    <input ${values.precisainscrever?"checked":""} type="radio" value="1" name="inscricao"  class="custom-control-input" id="customatividadesim2">
                                    <label class="custom-control-label" for="customatividadesim2">Sim</label>
                                </div>
                                <div class="mx-2 my-2 custom-control d-inline custom-radio">
                                    <input type="radio" value="0" ${values.precisainscrever?"":"checked"} name="inscricao" class="custom-control-input" id="customatividadenao2">
                                    <label class="custom-control-label" for="customatividadenao2">Não</label>
                                </div>
                                <div class="error-message text-danger" id="error-message-inscricao"></div>
                            </div>
                        </div>
                    </div>
                    <div style="display: none" data-colapse="atividadecronograma">
                        <div class="container table-responsive">
                            <table class="table-sm w-100 rounded text-center">
                                <thead class="bg-sc-primary">
                                    <tr>
                                        <td style="min-width: 120px; max-width: 120px; width:120px">                                        
                                        </td>              
                                        ${cronogramasDias}
                                    </tr>
                                </thead>
                                <tbody>
                                   ${cronogramasHoras}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </form>
    `);
}


export {AtividadesEditModel};