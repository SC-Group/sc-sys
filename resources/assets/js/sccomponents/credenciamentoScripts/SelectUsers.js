export default class SelectUsers{
    constructor(container){
        let _this=this;
        this.searchUsers = container.find("#search-user");
        this.selectType = this.searchUsers.find("[name=search-user-type]");
        this.inputSearch = this.searchUsers.find("[name=search-user-input]");
        this.userDataReady=()=>{};
        this.searchUsers.submit(function(e){
            e.preventDefault();
            _this.formData = new FormData(this);
            _this.search(_this.formData,1);
            return false;
        });
        __ioService.on("userStatus",function(data){
            _this.socketDataParse(JSON.parse(data));
        });
    }

    getPage(page){
        this.search(this.formData,page);
    }

    parseDias(userData,dias){
        userData.dias={};
        for(let i=0;i<dias.length;i++){
            userData.dias[dias[i].data] = dias[i].pivot.presenca;
        }
    }

    socketDataParse(data){
        if(this.usersData!==undefined &&
            this.usersData[`${data.inscid}`]!==undefined &&
            this.usersData[`${data.inscid}`].dias!==undefined
        ){
            this.usersData[`${data.inscid}`].dias[data.data] = data.status;
            this.usersData[`${data.inscid}`].kit = (data.kit===true||data.kit===1||data.kit==='true');
            this.userDataReady({pagesData:this.pagesData, usersData:this.usersData,userData:data});
        }else{
            Messages.warning("Dados foram atualizados mas sistema não pode se adequar. Recarregue a página");
        }
    }

    parseUsers(response){
        this.usersData={};
        this.pagesData=response.pagesData;
        for(let i=0; i < response.users.length;i++){
            let inscId = response.users[i].inscricao[0].id;
            let userData={
                email:response.users[i].email,
                userid:response.users[i].id,
                name:response.users[i].name,
                inscid:response.users[i].inscricao[0].id,
                kit:response.users[i].inscricao[0].kit===1,
            };
            this.parseDias(userData,response.users[i].inscricao[0].dias);
            this.usersData[`${inscId}`]=userData;
        }
        this.userDataReady({pagesData:this.pagesData,usersData:this.usersData});
    }

    search(data,page){
        let _this=this;
        let comm = new Comm(`${location.getUrl()}/getusers?page=${page}`,false,data,{
            processData: false,
            contentType: false
        });
        comm.success(function (response) {
            _this.parseUsers(response);
        });
        comm.send();
    }
}
