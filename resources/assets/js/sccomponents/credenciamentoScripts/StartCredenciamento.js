import {userLine} from "./CredenciamentoModels";
import SelectUsers from './SelectUsers'

class StartCredenciamento{
    constructor() {
        let _this=this;
        let today = new Date();
        let todayString = today.toISOString().slice(0,10);
        this.container = $("#credenciamento-container");
        this.modelo = $(userLine);
        this.selectDate = this.container.find("[name=eventdata]");
        let todayOption  = this.selectDate.find(`[value=${todayString}]`);
        if(todayOption[0]!==undefined){
            this.selectDate.val(todayString);
            this.selectDate.trigger("change");
        }
        this.searchUsers = new SelectUsers(this.container);
        this.dateSelected = this.selectDate.val();
        this.pages = this.container.find("#pagescredenc");
        this.usersLines = this.container.find("#cred-user-lines");

        this.searchUsers.userDataReady = function(data){
            _this.usersData = data.usersData;
            _this.pagesData = data.pagesData;

            if(data.userData!==undefined){
                _this.updateUserLine(data.userData);
            }else{
                _this.constructPagesLink(data.pagesData);
                _this.constructUsers(data.usersData);
            }
        };

        this.selectDate.change(function(){
            _this.dateSelected = $(this).val();
            _this.constructUsers(_this.usersData);
            if(_this.pagesData!==undefined)
                _this.constructPagesLink(_this.pagesData);
        });
    }

    constructPagesLink(pagesData){
        let _this=this;
        let p = new Pagination();
        let elmP = p.createPaginationObject(pagesData.current,pagesData.last,function(page){
            _this.searchUsers.getPage(page);
        });
        this.pages.html(elmP);
    }

    setStatusColor(val,statuscell){
        let select = statuscell.find("select");
        statuscell.removeClass("table-success");
        select.removeClass("text-success");
        statuscell.removeClass("table-danger");
        select.removeClass("text-danger");
        if(val==="Presente"){
            statuscell.addClass("table-success");
            select.addClass("text-success");
        }else if(val==="Faltou"){
            statuscell.addClass("table-danger");
            select.addClass("text-danger");
        }
    }

    constructUserLine(i,userData,date){
        let _this=this;
        let line = this.modelo.clone();
        line.attr("id",`insc-${i}`);
        let insccell =  line.find("[data-name=inscid]");
        let usercell =  line.find("[data-name=userid]");
        let namecell =  line.find("[data-name=name]");
        let emailcell =  line.find("[data-name=email]");
        let statuscell =  line.find("[data-name=status]");
        let kitcell =  line.find("[data-name=kit]");
        insccell.html(i);
        let inputId = uuid();
        let kitInput = kitcell.find("input");
        let kitLabel = kitcell.find("label");
        kitInput.attr("id",inputId);
        kitLabel.attr("for",inputId);
        kitInput[0].checked = userData.kit;
        usercell.html(userData.userid);
        namecell.html(userData.name);
        emailcell.html(userData.email);
        let statusSelect = statuscell.find("select");
        let status = userData.dias[date];
        if(status!==undefined && status!==null)
            statusSelect.val(status);
        else
            statusSelect.val("Selecione");


        function update(){
            let kitVal = kitInput[0].checked;
            let statusVal = statusSelect.val();
            _this.setStatusColor(statusVal,statuscell);
            _this.saveStatus(userData.inscid,_this.dateSelected,kitVal,statusVal);
        }

        kitInput.change(function(){
            update();
        });

        statusSelect.change(function(){
            update();
        });

        _this.setStatusColor(statusSelect.val(),statuscell);

        return line;
    }

    saveStatus(inscid,date,kitVal,status){
        let _this = this;
        let data = {inscid:inscid,kit:kitVal, status:status,  data:date};
        let comm = new Comm(`${location.getUrl()}/alterstatus`,false,data);
        comm.success(function (response) {
            let user = response.user;
            _this.searchUsers.socketDataParse(user);
        });
        comm.send();
    }


    constructUsers(usersData){
        this.usersLines.empty();
        if(usersData!==undefined){
            for(let i in usersData){
                if(usersData.hasOwnProperty(i)){
                    let userLine = this.constructUserLine(i,usersData[i],this.dateSelected);
                    this.usersLines.append(userLine);
                }
            }
        }
    }

    updateUserLine(userData){
        let line = this.usersLines.find(`#insc-${userData.inscid}`);
        let kitcell =  line.find("[data-name=kit]");
        let kitInput = kitcell.find("input");
        kitInput[0].checked = userData.kit;

        if(userData.data === this.dateSelected){
            let statuscell =  line.find("[data-name=status]");
            let statusSelect = statuscell.find("select");
            let status = userData.status==null?"Selecione":userData.status;
            statusSelect.val(status);
            this.setStatusColor(status,statuscell);
        }
    }
}


let startCredenciamento=new StartCredenciamento();
export {startCredenciamento};