let userLine = `
    <tr>
        <th class="text-center" data-name="inscid" scope="row"></th>
        <td class="text-center" data-name="userid" ></td>
        <td data-name="name" scope="col"></td>
        <td data-name="email" scope="col"></td>
        <td data-name="kit" scope="col">
            <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input">
                <label class="custom-control-label" ></label>
            </div>
        </td>
        <td data-name="status" class="" style="min-width: 200px; font-size: 12pt" scope="col">
            <select style="background-color: rgba(255,255,255,0.5);" class="custom-select custom-select-sm" name="status" id="status">
                <option value="Selecione">Selecione...</option>
                <option value="Presente">Presente</option>
                <option value="Faltou">Faltou</option>
            </select>
        </td>
    </tr>
`;
export {userLine};