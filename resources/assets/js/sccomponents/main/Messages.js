class Messages{
    static show(msg,status,log){
        if(log)Messages[status+"Log"](msg);
        let _st=status;
        status = status=="error"?"danger":status;
        var alert = $('.alert-model').clone();
        $('.alert-container').append(alert);

        alert.removeClass('alert-model');
        alert.removeClass('d-none');
        alert.addClass('show alert-'+status);
        alert.find('.status>span').html(_st.charAt(0).toUpperCase() + _st.slice(1));
        alert.find('.msg').html(msg);
        alert.alert();
        setTimeout(function(){alert.alert('close')},5000);

    }

    static log(msg,color){
        console.log('%c '+msg, 'color:'+color);
    }

    static success(msg,log){Messages.show(msg,'success',log||false)};
    static warning(msg,log){Messages.show(msg,"warning",log||false)};
    static info(msg,log){Messages.show(msg,"info",log||false)};
    static error(msg,log){Messages.show(msg,"error",log||false)};
    static successLog(msg){Messages.log(msg,'#00FF44');};
    static warningLog(msg){Messages.log(msg,'#FFDD33');};
    static infoLog(msg){Messages.log(msg,'#0044FF')};
    static errorLog(msg){Messages.log(msg,'#FF1122')};
}

window.Messages = Messages;