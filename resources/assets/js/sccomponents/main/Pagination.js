class Pagination{
    createContainer(content){
        return `<nav>
                <ul class="pagination pagination-sm">
                    ${content}
                </ul>
            </nav>`
    }

    createMainItem(label,value,disabled){
        return `
            <li class="page-item ${disabled ? "disabled" : ""}">
                <${disabled ? "span" :`a href="#"`} data-value="${value}" class="page-link">
                    ${label}
                </${disabled ? "span" : "a"} >
            </li>
            `
    }

    createItem(i,current){
        return current?`
               <li class="page-item active">
                    <span class="page-link">
                        ${i}
                        <span class="sr-only">(current)</span>
                    </span>
               </li>`
            :
            `<li class="page-item">
                <a class="page-link" data-value="${i}" href="#">
                    ${i}
                </a>
            </li>`;
    }

    createPaginationString(current,last){
        let first = (current-3)<1?1:current-3;
        let nlast = (current+3)>last?last:current+3;
        let firstDisabled = first===1;
        let lastDisabled = nlast===last;

        let itens=``;
        itens+=this.createMainItem("<i class=\"fas fa-angle-double-left\"></i>",1,firstDisabled);
        for(let i = first;i<=nlast;i++){
            itens+=this.createItem(i,i===current);
        }
        itens+=this.createMainItem("<i class=\"fas fa-angle-double-right\"></i>",last,lastDisabled);

        return this.createContainer(itens);
    }

    createPaginationObject(current,last,callback){
        let elm = $(this.createPaginationString(current,last));
        elm.find("a[data-value]").click(function(e){
            e.preventDefault();
            let page = $(this).attr("data-value");
            callback(page);
            return false;
        });
        return elm;
    }

}

window.Pagination = Pagination;