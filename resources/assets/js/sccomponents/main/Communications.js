$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
/*  url: Url de destino da requisição;
*   log: se vai ou não logar (true| false)
*   param: o corpo que vai ser enviado (default: json)
*/
class Communications{
    constructor(url, log, param, options){
        options = options || {};
        if(options.contentType===false&& options.processData===false){
            if(param===undefined)param = new FormData();
            param.set("year",Env.__YEAR);
        }else{
            param = param ||{};
            param.year = Env.__YEAR;
        }
        this.param = param;
        this.log = log || false;
        this.showError=options.showError===undefined?true:options.showError;
        this.showMsg=options.showMsg===undefined?true:options.showMsg;
        this._fail = ()=>{};
        this._success = ()=>{};
        this.options=options;
        this._showLoader = !options.hideLoader;
        this.options.url = url;
        this.options.data = this.param;
        this.options.type = options.type || "post";
        this.options.dataType = options.dataType || "json";
        this.loaderTime = setTimeout(()=>{},0);
    }

    static showLoader(){
        $('.loading-box').addClass("show");
    }

    static hideLoader(){
        $('.loading-box').removeClass("show");
    }

    addParam(key,value){
        this.param[key]=value;
    }

    error(action){
        this._fail=(response)=>{action(response)};
    }
    success(action){
        this._success=(response)=>{action(response)};
    }

    async send(){
        if(this._showLoader){
            clearTimeout(this.loaderTime);
            Communications.showLoader();
        }

        let _this = this;

        await $.ajax(_this.options)
            .done(function(response) {
                clearTimeout(this.loaderTime);
                response = response || {};
                if(response.msg!==undefined && response.status !== undefined && _this.showMsg ) Messages[response.status](response.msg,_this.log);
                _this._success(response);

            })
            .fail(function(response) {
                clearTimeout(this.loaderTime);
                let msg = "Ocorreu um erro!";
                let status = 'error';

                if(response.responseJSON!==undefined && response.responseJSON.msg!==undefined ){
                    let resp = response.responseJSON;
                    msg = resp.msg;
                    status = resp.status === undefined? status: resp.status ;
                }

                if(_this.showError)Messages[status](msg,_this.log);
                _this._fail(response);
            })
            .always(function() {
                _this.loaderTime = setTimeout(()=>Communications.hideLoader(),1000);
            });
    }
}

window.Communications = Communications;
window.Comm = window.Communications; //Alias;