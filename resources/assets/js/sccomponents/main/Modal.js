class _Modal{
    constructor(){
        this.modal = $("#Modal");
        this.header = this.modal.find(".modal-header");
        this.modalTitle = this.modal.find(".modal-title");
        this.footer= this.modal.find(".modal-footer");
        this.body = this.modal.find(".modal-body");
    }

    addFooterButton(label,classes,action,dismiss){
        classes = classes || "";
        dismiss = dismiss || false;
        this.footer.removeClass("d-none");
        let button =  $(`<button type="button" class="btn ${classes}" ${dismiss?`data-dismiss="modal"`:""}>${label}</button>`);
        if(action){
           button.click(action);
        }

        this.footer.append(button);
    }

    resetHeader(){
        this.modalTitle.html("");
        this.header.addClass("d-none");
    }

    resetFooter(){
        this.footer.addClass("d-none");
        this.footer.html("");
    }

    resetWidth(){
        this.modal.find('.modal-dialog').addClass('modal-'+this.width);
    }

    setWidth(width){
        this.width = width;
        this.modal.find('.modal-dialog').addClass('modal-'+this.width);
    }

    resetBody(){
        this.body.html("");
    }

    setBody(body){
        this.body.html(body);
    }

    setTitle(title){
        this.modalTitle.html(title);
        this.header.removeClass("d-none");
    }

    show(){
        return this.modal.modal();
    }

    dispose(){
        return this.modal.modal('dispose');
    }

    hide(){
        return this.modal.modal('hide');
    }

    resetAll(){
        this.hide();
        this.resetWidth();
        this.resetHeader();
        this.resetFooter();
        this.resetBody();
    }

}

window.Modal = new _Modal();

class _Confirm extends _Modal{
    constructor(){
        super();
    }

    load(message,actionY,actionN,labelY,labelN){
        labelY = labelY || "Sim";
        labelN = labelN || "Não";
        this.resetAll();
        let _this = this;
        this.setTitle("Confirmação");
        this.setBody(message);
        actionN = actionN || function(){};
        actionY = actionY || function(){};
        let _actionN = function(e){
            actionN(e);
            _this.resetAll();
            _this.hide();
        };

        let _actionY = function(e){
            actionY(e);
            _this.resetAll();
            _this.hide();
        };

        this.addFooterButton(labelN,"mr-3 btn-dark",_actionN);
        this.addFooterButton(labelY,"ml-3 btn-sc-primary",_actionY);
    }
}

window.Confirm = new _Confirm();

class _ModalAlert extends _Modal{
    constructor(){
        super();
    }

    load(message){
        this.resetAll();
        this.setTitle("Informação");
        this.setBody(message);
        this.addFooterButton("Ok","btn-sc-primary",()=>{},true);
    }
}

window.ModalAlert = new _ModalAlert();
