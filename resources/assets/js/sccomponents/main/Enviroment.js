class Enviroment{
    static get(key){
        let value =  document.querySelector(`meta[name=${key}]`).getAttribute("content");
        try {
            return JSON.parse(value);
        }catch(e){
            if(typeof value === "string" && value.length === 0) return null;
            else return value;
        }
    }
    static set(key,value){
        if(typeof value !== "string")value = JSON.stringify(value);
        return document.querySelector(`meta[name=${key}]`).setAttribute("content",value);
    }
}
let Env = Enviroment;
Env.__LOCAL = Env.get('local');
Env.__YEAR= Env.get("year") || "";

window.Env = Env;
window.Enviroment = Env;