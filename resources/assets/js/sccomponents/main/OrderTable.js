class OrderTable {
    constructor(table){
        this.table = $(table);
        this.colLabels = this.table.find("thead td, thead th");
        this.setOriginalOrder();
        this.toggleSequence = [`up`,`reset`,`down`];
        this.constructOrderButtons();

    }

    setOriginalOrder(){
        this.table.find("tbody>tr").each((idx,elm)=>{
            $(elm).attr("data-original-order",idx);
        });
    }

    orderButton(label){
        return $(`
            <a data-sort-type="reset" class="text-dark-sc-primary w-100 d-flex text-left btn btn-light" href="#">
                <div class="w-100">
                    ${label}
                </div>
                <div style="width: 10px" class="text-center">
                    <i class="fa d-none"></i>
                </div>
             </a>
            `)
    }
    setSort(elm,sortType){
        elm = $(elm);
        const icon = elm.find("i");
        icon.attr("class","fa");
        elm.attr("data-sort-type",sortType);
        switch (sortType) {
            case "up": icon.addClass("fa-arrow-up"); break;
            case "down": icon.addClass("fa-arrow-down"); break;
            case "reset": icon.addClass("d-none"); break;
            default: icon.addClass("d-none"); break;
        }
    }

    getSort(elm){
        return $(elm).attr("data-sort-type");
    }

    sortRowsUp(numCol){
        const tbody = $(this.table).find("tbody");
        const rows = tbody.find("tr");
        rows.sort((a,b)=>{
            return -1*$(a).find(`td:nth-child(${numCol+1})`).text().localeCompare($(b).find(`td:nth-child(${numCol+1})`).text());
        }).appendTo(tbody);
    }

    sortRowsDown(numCol){
        const tbody = $(this.table).find("tbody");
        const rows = tbody.find("tr");
        rows.sort((a,b)=>{
            return $(a).find(`td:nth-child(${numCol+1})`).text().localeCompare($(b).find(`td:nth-child(${numCol+1})`).text());
        }).appendTo(tbody);
    }

    sortRowsReset(){
        const tbody = $(this.table).find("tbody");
        const rows = tbody.find("tr");
        rows.sort((a,b)=>{
            return $(a).attr("data-original-order").localeCompare($(b).attr("data-original-order"));
        }).appendTo(tbody);
    }

    nextTypeSort(typeSort){
        return (this.toggleSequence.lastIndexOf(typeSort)+1)%this.toggleSequence.length;
    }

    toggleSort(elm){
        elm = $(elm);
        let typeSort = this.getSort(elm);
        this.sort(elm,this.toggleSequence[this.nextTypeSort(typeSort)]);
    }

    sort(elm,typeSort){
        elm = $(elm);
        const idx = elm.closest(`td`).index();

        this.setSort(this.colLabels.find("[data-sort-type]"),"reset");

        if(typeSort==="up"){
            this.setSort(elm,"up");
            this.sortRowsUp(idx);
        }else if(typeSort==="down"){
            this.setSort(elm, "down");
            this.sortRowsDown(idx);
        }else if(typeSort==="reset" || typeSort===null){
            this.setSort(elm, "reset");
            this.sortRowsReset();
        }
    }

    constructOrderButtons(){
        const _this=this;
        this.colLabels.each((idx,elm)=>{
            let txt = $(elm).text();
            if(!isBlank(txt)){
                let orderBtn = this.orderButton(txt);
                $(elm).html(orderBtn);
                orderBtn.click(function(e){_this.toggleSort(this);});
            }
        })
    }
}

window.OrderTable = OrderTable;