window.nop=()=>{}
class DynamicSelect{
   static _mountOption(label,value){
      return `<option value="${value||label}">${label}</option>`;
   }
}

window.DynamicSelect = DynamicSelect;

window.filename = function(file){
   var fullPath = file.value;
   if (fullPath) {
      var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
      var filename = fullPath.substring(startIndex);
      if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
         filename = filename.substring(1);
      }
      return filename;
   }
};

window.setInputRule = function(){
   $("input.custom-file-input[type=file]").on("change",function(e){
      var label = $(this).parent().find("label");
      label.html(filename(this));
   });
};
window.setInputRule();


window.location.__proto__.getUrl=function(){
   return window.location.origin+window.location.pathname;
};

window.uuid = ()=>{
   return Math.random().toString(36).substr(2, 16);
};

function resizeHeader(){
   let header = $(".page-header-wrapper");
   let menu = $(".menu-open-wrapper a");
   let scroll = window.scrollTop||window.scrollY;
   let min = 60;
   let max = 110;
   scroll = scroll<(max-min)?max-scroll:min;
   header.css("height",scroll);
   menu.css("height",scroll);
}
$(window).on("scroll",function(e){
   resizeHeader();
});

$(window).on("load",()=>resizeHeader());




var toggled=false;
$("#menu-toggle").click(function(e) {
   e.preventDefault();
   var w =$("#dashboard-wrapper");
   var s = $(this).find("span");

   if(!toggled){
      w.addClass("toggled");
      s.addClass("iconbg-sc-primary").removeClass("bg-sc-stress");
      $(window).on("resize",function () {
         setPageHeight();
      });
      setPageHeight();
      toggled = true;
   }else{
      w.removeClass("toggled");
      s.removeClass("iconbg-sc-primary").addClass("bg-sc-stress");
      unsetPageHeight();
      $(window).off("resize");
      toggled = false;
   }
});

$(".page-wrapper").click(function (e) {
   $("#dashboard-wrapper").removeClass("toggled");
   $("#menu-toggle").find("span").removeClass("iconbg-sc-primary").addClass("bg-sc-stress");
   unsetPageHeight();
   $(window).off("resize");
   toggled = false;
});

window.toggleMenuSections= function(elm){
   let newElm = $(elm).parent();
   let oldElm = $("[menu-opened]");
   let iconCaretNew = newElm.find('i');
   let iconCaretOld = oldElm.find('i');
   iconCaretNew.addClass("fa-caret-down").removeClass('fa-caret-right');
   iconCaretOld.removeClass("fa-caret-down").addClass('fa-caret-right');
   oldElm.removeAttr("menu-opened");
   if(newElm[0] !== oldElm[0]){
      newElm.attr("menu-opened","");
   }
   setTimeout(()=>setPageHeight(),400);
};

window.isBlank = function(string){
   return string===undefined || string === null || string.replace(/^\s+|\s+$/g, "").length===0;
};


window.setPageHeight = function (){
   let sidebar = $("#dashboard-wrapper .sidebar-content");
   let h = sidebar.innerHeight();
   $(".page-wrapper").css("height",h+"px");
};
window.unsetPageHeight = function (){
   $(".page-wrapper").css("height","");
};
