import './dynamicSelectScripts/SelectInstituicao';
class FormActions{
    load(){
        let _this = this;
        let elms = $('[data-showwhen]');
        elms.each((index)=>{
            let elm = $(elms[index]);
            _this.showWhen(elm,elm.attr('data-showwhen'));
        });

        elms = $('[data-validation]');
        elms.each((index)=>{
            let elm = $(elms[index]);
            _this.validation(elm,elm.attr('data-validation'));
        });

        elms = $('[data-load-options]');
        elms.each((index)=>{
            let elm = $(elms[index]);
            _this.loadOptions(elm,elm.attr('data-load-options'));
        });

        elms=$(".formDynamicSelect");
        elms.each((index)=>{
            let elm = $(elms[index]);
            _this.loadDynamicSelect(elm, elm.attr('data-dependency'));
        });
    }

    loadOptions(){}
    showWhen(elm,watch){
        watch = JSON.parse(atob(watch));
        for(let i in watch){
            if(watch.hasOwnProperty(i)){
                let element = $(`[name='${i}']`);

                let eventHandler=function(){

                    let regex = RegExp(watch[i]);
                    let value = element.val();


                    if(element.is(":radio")){
                        value = element.parent().find("input[type=radio]:checked").val() || '';
                    }

                    let isValid = value!==null?value.match(regex):false;
                    if(element.is(':checkbox')){
                        value ="";
                        let cb = $(`[name='${i}']:checked`);
                        cb.each((index)=>{
                            value += cb.eq(index).val()+";";
                        });
                        isValid = value.match(regex);
                    }

                    if(isValid){
                        elm = $(elm);
                        elm.removeClass("hideElmForm");
                        if(elm.is('[data-required]')){
                            $(elm).find(":input").attr('required','');
                        }

                    }else{
                        $(elm).addClass("hideElmForm");
                        if(elm.is('[data-required]')){
                            $(elm).find(':input').removeAttr('required');
                        }
                    }
                };
                element.on('change',function(){eventHandler()});
                eventHandler();
            }
        }
    }

    validation(elm,watch){
        watch = atob(watch);
        watch = JSON.parse(watch);
        elm.on('change',(e)=>{
            let regex = RegExp(watch.pattern,watch.patternOptions);
            let isValid = elm.val().match(regex);
            let c = elm.closest('.input-group')
            if(! isValid){
                elm.addClass('border-danger');
               c.find('.errormsg').html(watch.msg);
            }
            else{
                elm.removeClass('border-danger');
                c.find('.errormsg').html('');
            }
        });
    }

    async loadDynamicSelect(elm,watch){
        watch = JSON.parse(atob(watch));
        let value = elm.attr('data-value');
        let selectWatch = "";
        for(let i in watch){
            selectWatch += `[name='${watch[i]}'],`;
        }
        selectWatch = selectWatch.substring(0, selectWatch.length - 1);
        let js = elm.attr("data-js").split("@");
        let api = elm.attr("data-api");
        let watchElements = $(selectWatch);
        let _this=this;
        watchElements.change(function(){
            _this.fillValue(elm,watch,watchElements,value,js, api);
        });
        this.fillValue(elm,watch,watchElements,value,js, api);

    }

    async fillValue(elm,watch,watchElements,value,js, api){
            await DynamicSelect[js[0]][(js[1]!==undefined?js[1]:'load')](elm,watch,watchElements,api);
            if(typeof value=="string" && value.length>0){
                await new Promise((resolve => setTimeout(()=>{
                    if(elm.find(`[value='${value}']`).length>0)
                        elm.val(value);
                    resolve();
                },300)));
                elm.trigger('change');
            }
    }

}

$(document).ready(()=>{
    new FormActions().load();
});