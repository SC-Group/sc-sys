try {
    if (window.timeNow) {
        function getHumanPeriod(milliseconds){
            return [
                Math.floor((milliseconds / 1000) % 60),
                Math.floor((milliseconds / (1000 * 60)) % 60),
                Math.floor((milliseconds / (1000 * 60 * 60)) % 24),
                Math.floor(milliseconds / (1000 * 60 * 60 * 24))
            ];
        }

        setInterval(()=>{
            window.timeNow.setTime(window.timeNow.getTime() + 1000);
        },1000);


        function updateInsc(){
            let url = "insctime";
            let comm = new Comm(Env.__LOCAL+url,false,null,{hideLoader:true});
            comm.success((response)=>{
                window.inscStart= new Date(response.timeInsc);
                window.timeNow = new Date(response.timeNow);
            });
            comm.error((response)=>{
                Messages.error(response.msg);
            });
            comm.send();

        }

        setInterval(()=>updateInsc(),60000);

        let openInsc = setTimeout(()=>{},0);
        function checkOpenInsc(){
            clearTimeout(openInsc);
            openInsc = setTimeout(()=>{
                let t = window.inscStart.getTime()-window.timeNow.getTime();
                let th = getHumanPeriod(t);
                if(t>0){
                    document.querySelectorAll(".circle div:first-child").forEach((elm,index)=>{
                        let value = th[th.length-index-1];
                        if(value>100){
                            elm.innerHTML = "+ de 100";
                        }else{
                            elm.innerHTML = `${value}`;
                        }
                    });
                    checkOpenInsc();
                }else{
                    setTimeout(()=>{
                        clearTimeout(openInsc);
                        window.location.reload();
                    },1000);
                }
            },100);
        }
        checkOpenInsc();
    }
}catch (e) {}
