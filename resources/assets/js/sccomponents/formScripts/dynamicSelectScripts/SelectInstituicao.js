class InstituicoesQuery{
    static getUF(tipo){
        let url = "instituicoes/uf";
        let comm = new Comm(Env.__LOCAL+url,false, {tipo:tipo});
        return new Promise((resolve,reject)=>{
            comm.success((response)=>{
                resolve(response);
            });
            comm.error((response)=>{
                reject(response);
            });
            comm.send();
        });

    }

    static getTipos(){
        let url = "instituicoes/tipos";
        let comm = new Comm(Env.__LOCAL+url,false);
        return new Promise((resolve,reject)=>{
            comm.success((response)=>{
                let out = [];
                response.forEach((elm)=>{
                    out.push(elm.tipo);
                });
                resolve(out);
            });
            comm.error((response)=>{
                reject(response);
            });
            comm.send();
        });
    }

    static get(tipo,uf_sigla){
        let url = "instituicoes";
        let comm = new Comm(Env.__LOCAL+url,false, {tipo:tipo,uf_sigla:uf_sigla});
        return new Promise((resolve,reject)=>{
            comm.success((response)=>{
                resolve(response);
            });
            comm.error((response)=>{
                reject(response);
            });
            comm.send();
        });
    }

    static getCursos(instituicaoId){
        let url = "instituicoes/cursos";
        let comm = new Comm(Env.__LOCAL+url,false, {id:instituicaoId});
        return new Promise((resolve,reject)=>{
            comm.success((response)=>{
                resolve(response);
            });
            comm.error((response)=>{
                reject(response);
            });
            comm.send();
        });

    }


}


DynamicSelect.SelectInstituicao = {
    tipos: async function(elm){
        let tipos = await InstituicoesQuery.getTipos();
        let html = '<option value="">Selecione...</option>';
        tipos.forEach((val,index,arr)=>{
            html+=DynamicSelect._mountOption(val);
        });
        elm[0].innerHTML=html;
    },
    ufs: async function(elm,watch){
        let tipo = $(`[name=${watch['tipo']}]`).val();
        let html = '<option value="">Selecione...</option>';
        if(tipo){
            let uf = await InstituicoesQuery.getUF(tipo);
            uf.forEach((val,index,arr)=>{
                html+=DynamicSelect._mountOption(val.uf_nome,val.uf_sigla);
            });
        }
        elm[0].innerHTML=html;
    },
    instituicoes: async function(elm,watch){
        let tipo = $(`[name=${watch['tipo']}]`).val();
        let uf = $(`[name=${watch['uf']}]`).val();
        let html = '<option value="">Selecione...</option>';
        if(tipo&&uf){
            let instituicao = await InstituicoesQuery.get(tipo,uf);
            instituicao.forEach((val,index,arr)=>{
                html+=DynamicSelect._mountOption(`${val.nome} (${val.sigla})`,val.id);
            });
        }
        elm[0].innerHTML=html;
    },
    cursos: async function(elm,watch){
        let instituicao = $(`[name=${watch['instituicao']}]`).val();
        let html = '<option value="">Selecione...</option>';
        if(instituicao&&instituicao!=="Selecione..."){
            let cursos = await InstituicoesQuery.getCursos(instituicao);
            cursos.forEach((val,index,arr)=>{
                html+=DynamicSelect._mountOption(val.nome,val.id);
            });
        }

        elm[0].innerHTML=html;
    }
};