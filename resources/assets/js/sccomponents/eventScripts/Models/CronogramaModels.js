let modelCronograma= (type,startHour,endHour, statusDays, dataId)=>{

    let line = `
    <tr data-id="${type}-${dataId}" data-name="line-chronos">
        <td class="px-0 align-middle">
           <button class="btn btn-sm btn-danger" type="button" data-name="btn-delete-chronos">
               <i class="fa fa-minus"></i>
           </button>
        </td>
        <td class="pl-1 align-middle">
           <div data-name="line-chronos" class="form-group my-1">
               <div class="input-group input-group-sm date" data-target-input="nearest">
                   <input data-name="chronos-inicio"
                          name="chronos-line[${type}][${dataId}][inicio]"
                          type="text"
                          placeholder="Inicio"
                          class="form-control"
                          value="${startHour}">
                   <input data-name="chronos-fim"
                          name="chronos-line[${type}][${dataId}][fim]"
                          type="text"
                          placeholder="Fim"
                          class="form-control"
                          value="${endHour}">
               </div>
           </div>
        </td>
`;

    for(let i = 0; i< statusDays.length; i++ ){
        line+=`
            <td class="align-middle">
                <input type="hidden" name="chronos-line[${type}][${dataId}][status][${$("[data-date]").eq(i).attr("data-date")}]" value="${statusDays[i]||"Atividade"}">
                <input style="width: 100px" type="button" class="btn btn-sm btn-light" data-name="status-day[${dataId}][${$("[data-date]").eq(i).attr("data-date")}]" value="${statusDays[i]||"Atividade"}">
            </td>
        `;
    }
    line+="</tr>";
    return line;
};

export {modelCronograma};