export class ConfigNiveisPatrocinio {
    constructor(){
        this.form=$("#eventoConfigNiveisPatrocinioForm");
        this.modelNiveisPatrocinio = $(".row-template-niveispatrocinio");
        this.addNiveisPatrocinio=this.form.find('[data-name=add-niveispatrocinio]');
        this.niveisPatrocinioContainer=this.form.find("[data-name=niveispatrocinio-container]");
        this.deleted=[];
    }

    async load(i){
        this.next = (++i);
        await this.setUpNiveisPatrocinio();
        if(window.configEventoVariables.hasLoaded(this.next)===false) window.configEventoVariables.loadClasses(this.next);
    }

    async setUpNiveisPatrocinio(){
        let _this = this;
        this.form.remove();
        let datasend = new Comm(window.location.getUrl() + "/niveispatrocinio", true);
        datasend.success((response) => {
            _this.form = $(response.view);
            $("#accordion").append(_this.form);
            _this.modelNiveisPatrocinio = $(".row-template-niveispatrocinio");
            _this.addNiveisPatrocinio=_this.form.find('[data-name=add-niveispatrocinio]');
            _this.niveisPatrocinioContainer=_this.form.find("[data-name=niveispatrocinio-container]");
            _this.setUpNiveisPatrocinioConfig();
        });
        await datasend.send();
    }

    setupEvents(lines){
        let _this = this;
        let btnDelete = lines.find("[data-name=btn-delete-nivelpatrocinio]");
        btnDelete.click(function (e) {
            e.preventDefault();
            let removed = $(this).closest("[data-name=line-nivelpatrocinio]");
            let dataId = removed.attr("data-id");
            if(!dataId.includes('new-')) {
                _this.deleted.push(dataId);
            }
            removed.remove();
            return false;
        });
    }

    setupID(line,lineId){
        let nome = line.find("[data-name=nivelpatrocinio-nome]");
        let nivel=line.find("[data-name=nivelpatrocinio-nivel]");
        let lineNivelPatrocinio = line.find("[data-name=line-nivelpatrocinio]");
        lineNivelPatrocinio.attr("data-id",lineId);
        nome.attr("name",`nivelpatrocinio[${lineId}][nome]`);
        nivel.attr("name",`nivelpatrocinio[${lineId}][nivel]`);
    }

    setupLine(line){
        let lineId = "new-"+uuid();
        this.setupID(line,lineId);
        this.setupEvents(line);
    }
    setUpNiveisPatrocinioConfig(){
        let _this=this;

        this.addNiveisPatrocinio = this.form.find("[data-name=add-niveispatrocinio]");
        this.addNiveisPatrocinio.click(function(e){
            e.preventDefault();
            let line = _this.modelNiveisPatrocinio.clone();

            line.removeClass("d-none");
            _this.niveisPatrocinioContainer.append(line);
            _this.setupLine(line);
            return false;
        });
        this.form.submit(function(e){
            e.preventDefault();
            _this.save(this,e);
            return false;
        });
        this.setupEvents(this.form);
    }

    save(elm,e){
        let formData = new FormData(elm);
        let _this=this;
        formData.append("deleted",JSON.stringify(this.deleted));
        let comm = new Comm(window.location.getUrl() + "/saveniveispatrocinio",false,formData,{
            contentType: false,
            processData: false
        });

        comm.success(function(response){
            _this.deleted=[];
            let newIds = response.ids;
            for(let i in newIds){
                if(newIds.hasOwnProperty(i)){
                    let line = $(`[data-id=${i}]`);
                    line.attr("data-id",newIds[i]);
                    _this.setupID(line,newIds[i]);
                }
            }
        });

        comm.send();
    }

}