//Uploads de Imagens do sistema;
export class ConfigAssinaturas {
    async load(i){
        this.next=i+1;
        let _this=this;
        let retorno = false;
        $("#eventoConfigAssinaturasForm").remove();
        let datasend = new Comm(window.location.getUrl()+"/signs",true);
        datasend.success((response)=>{
            $("#accordion").append(response.view);
            $("#eventoConfigAssinaturasForm").on("submit",function(e){e.preventDefault(); return _this.saveAssinaturas(e,this);});
            this.loadAssinaturasEvent($("#eventoConfigAssinaturasForm"));
            retorno = true;
        });
        await datasend.send();
        return retorno;
    }

    moveAssinaturaLine(elm,percentPos){
        elm.css('top',`${100*percentPos}%`);
    }

    loadAssinaturasEvent(formElem){
        let _this=this;
        formElem.find('input[type=file]').on('change', function() {
            if (this.files && this.files[0]) {
                let container = $(this).closest('[data-name=assinaturacontainer]');
                let imgcontainer = container.find('.img-position');
                let img = imgcontainer.find("img");
                let reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
                reader.addEventListener("load",function (e) {
                    img[0].src = reader.result;
                    img[0].onload = function(e){
                        imgcontainer.closest('.img-position-wrapper').css('display','block');
                        _this.signImageLoaded(e,imgcontainer,reader.result);
                    };
                });

            }
        });

        let elmClicked;
        let posstart;
        let moveSign = function(e){
            let mouseRelPos = e.pageY - elmClicked.parent().offset().top - posstart;
            let h = elmClicked[0].parentElement.offsetHeight;
            let percentPos = mouseRelPos/h;
            if(percentPos>=-1 && percentPos<=0) {
                $(elmClicked).find("input[type=hidden]").val(-1 * percentPos);
                _this.moveAssinaturaLine(elmClicked, percentPos);
            }
        };

        let imgline = formElem.find('.img-position  .imgsignline' );

        imgline.each((i,elm)=>{
            let val = $(elm).find("input[type=hidden]").val();
            val = parseFloat(val)*-1;
            _this.moveAssinaturaLine($(elm),val);
        });

        imgline.on("mousedown touchstart",function (e) {
            elmClicked = $(this);
            posstart = e.offsetY;
           $('body').on("mousemove touchmove",moveSign);
        });
        $('body').on("mouseup mouseleave touchend",function (e) {
            if(elmClicked!=null && elmClicked.length>0)
                $('body').off("mousemove touchmove",moveSign);
        });
        return false;
    }



    signImageLoaded(e,elm,base64){
        let imgline = elm.find('.imgsignline' );
        let val = $(imgline).find("input[type=hidden]").val();
        val = parseFloat(val)*-1;
        this.moveAssinaturaLine(imgline,val);
    }

    saveAssinaturas(e,elm){
        let _this=this;
        let form = new FormData(elm);

        let datasend = new Comm(window.location.getUrl()+"/savesigns",false,form, {
            contentType:false,
            processData:false
        });
        datasend.success((response)=>{
            let errorContainers = $(elm).find("[data-error]");
            errorContainers.each((i,elm)=>{
                $(elm).empty();
            });
        });
        datasend.error((response)=>{
            let errorContainers = $(elm).find("[data-error]");
            errorContainers.each((i,elm)=>$(elm).empty());
            let messages = response.responseJSON;
            for(let i in messages ){
                if(messages.hasOwnProperty(i)){
                    let msgContainer = $(elm).find(`[data-error="${i}"]`);
                    msgContainer.append(messages[i].join(";&nbsp;&nbsp;&nbsp;"));
                }
            }
        });
        datasend.send();
        return false;
    }
}