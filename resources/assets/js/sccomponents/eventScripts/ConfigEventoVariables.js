import {ConfigEvento} from "./ConfigEvento";
import {ConfigEggs} from "./ConfigEggs";
import {ConfigLocais} from "./ConfigLocais";
import {ConfigCamisas} from "./ConfigCamisas";
import {ConfigColors} from "./ConfigColors";
import {ConfigCronograma} from "./ConfigCronograma";
import {ConfigTrilhas} from "./ConfigTrilhas";
import {ConfigUploads} from "./ConfigUploads";
import {ConfigAssinaturas} from "./ConfigAssinaturas";
import {ConfigNiveisPatrocinio} from "./ConfigNiveisPatrocinio";

class ConfigEventoVariables{
    constructor(order) {
        this.loaded = {};
        this.orderToOpen = order;
    }
    async loadClasses (j){
        for(let i=(j||0);i<this.orderToOpen.length;i++){
            let obj = new this.orderToOpen[i]["class"]();
            this.loaded[this.orderToOpen[i]["name"]] = obj;

            if(! (await obj.load(i))){
                break;
            }
        }
    }
    hasLoaded(index){
        if(index<this.orderToOpen.length)
            return this.loaded[this.orderToOpen[index]["name"]]!==undefined;
        else return -1;
    }
}

window.configEventoVariables = new ConfigEventoVariables([
    {name:"ConfigEvento",class:ConfigEvento},
    {name:"ConfigCronograma",class:ConfigCronograma},
    {name:"ConfigAssinaturas",class:ConfigAssinaturas},
    {name:"ConfigUploads",class:ConfigUploads},
    {name:"ConfigColors",class:ConfigColors},
    {name:"ConfigCamisas",class:ConfigCamisas},
    {name:"ConfigTrilhas",class:ConfigTrilhas},
    {name:"ConfigLocais",class:ConfigLocais},
    {name:"ConfigEggs",class:ConfigEggs},
    {name:"ConfigNiveisPatrocinio",class:ConfigNiveisPatrocinio}
]);


$(document).ready(()=> {
    window.configEventoVariables.loadClasses();
});