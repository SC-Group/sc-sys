//Uploads de Imagens do sistema;
export class ConfigUploads {
    async load(i){
        this.next=i+1;
        let _this=this;
        let retorno = false;
        $("#eventoConfigUploadsForm").remove();
        let datasend = new Comm(window.location.getUrl()+"/uploads",true);
        datasend.success((response)=>{
            $("#accordion").append(response.view);
            $("#eventoConfigUploadsForm").on("submit",function(e){e.preventDefault(); return _this.uploadConfigSetup(e,this);});
            retorno = true;
            $("#eventoConfigUploadsForm").find("input[type=fileimg]").setUploadImg();
        });
        await datasend.send();
        return retorno;
    }

    uploadConfigSetup(e,elm){
        let _this=this;
        let form = new FormData(elm);

        let datasend = new Comm(window.location.getUrl()+"/saveuploads",false,form, {
            contentType:false,
            processData:false,
            showError:false,
        });
        datasend.success((response)=>{
            let responsePasses = true;
            if(response.urls&&response.urls.logo){
                $("img[data-image=main-logo]").each(function () {
                    $(this).attr("src",Env.__LOCAL+response.urls.logo);
                });
                $("div[data-image=main-logo]").each(function () {
                    $(this).css("background-image","url('"+Env.__LOCAL+response.urls.logo+"')");
                });
            }else{
                responsePasses=false;
            }

            if(response.urls&&response.urls.bg){
                $("img[data-image=main-bg]").each(function () {
                    $(this).attr("src",Env.__LOCAL+response.urls.bg);
                });
                $("div[data-image=main-bg]").each(function () {
                    $(this).css("background-image","url('"+Env.__LOCAL+response.urls.bg+"')");
                });
            }else{
                responsePasses=false;
            }

            if(response.urls&&response.urls.camisas_fem){
                $("img[data-image=main-camisas_fem]").each(function () {
                    $(this).attr("src",Env.__LOCAL+response.urls.camisas_fem);
                });
                $("div[data-image=main-camisas_fem]").each(function () {
                    $(this).css("background-image","url('"+Env.__LOCAL+response.urls.camisas_fem+"')");
                });
            }else{
                responsePasses=false;
            }

            if(response.urls&&response.urls.camisas_masc){
                $("img[data-image=main-camisas_masc]").each(function () {
                    $(this).attr("src",Env.__LOCAL+response.urls.camisas_masc);
                });
                $("div[data-image=main-camisas_masc]").each(function () {
                    $(this).css("background-image","url('"+Env.__LOCAL+response.urls.camisas_masc+"')");
                });
            }else{
                responsePasses=false;
            }

            if(responsePasses && window.configEventoVariables.hasLoaded(_this.next)===false){
                window.configEventoVariables.loadClasses(_this.next);
            }
        });
        datasend.error((response)=>{
            let msg = response.responseJSON;
            if(msg.error!==undefined){
                Messages.error(msg.error);
            }
            if(msg.year!==undefined){
                for(let i =0;i< msg.year.length;i++)
                Messages.error(msg.year[i]);
            }
            if(msg.logo!==undefined){
                for(let i =0;i< msg.logo.length;i++)
                    Messages.error(msg.logo[i]);
            }
            if(msg.bg!==undefined){
                for(let i =0;i< msg.bg.length;i++)
                    Messages.error(msg.bg[i]);
            }

        });
        datasend.send();
        return false;
    }
}