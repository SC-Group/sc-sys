export class ConfigTrilhas{
    constructor(){
        this.form=$("#eventoConfigTrilhasForm");
        this.deletedTrilhasIds = [];
    }

    async load(i){
        this.next = (++i);
        await this.setUpTrilhas();
        if(window.configEventoVariables.hasLoaded(this.next)===false) window.configEventoVariables.loadClasses(this.next);
    }
    async setUpTrilhas(){
        let _this = this;
        this.form.remove();
        let datasend = new Comm(window.location.getUrl() + "/trilhas", true);
        datasend.success((response) => {
            $("#accordion").append(response.view);
            this.form = $("#eventoConfigTrilhasForm");
            _this.setUpTrilhasConfig();

        });
        await datasend.send();
    }

    setTrilhaAlert(id,text,trilha){
        trilha.find(".nomeError").html(text);
    }
    resetTrilhaAlert(id,newid,trilha){
        trilha.find(".nomeError").html("");
    }

    async addTrilha(e) {
        let _this=this;
        let trilha = $(".row-template-trilhas").children().clone();
        let trilhaCont =  $('.trilhasContainer');
        let numtrilhas = trilhaCont.children().length;
        let rowId = "new-trilha-"+Math.trunc(Math.random()*100)+""+numtrilhas;
        trilha.attr("data-rowid",rowId);
        trilha.find(".delete-trilha").on("click",function(){_this.deleteTrilha(this)});
        trilhaCont.append(trilha);
        let cp = trilha.find(".colorpick");
        await this.startColorPick(cp);
    }

    startColorPick(cp){
        let value = cp.attr("data-value");
        value = value===''?{h:180,s:50,l:50}:JSON.parse(value);
        value = [parseFloat(value.h), parseFloat(value.s), parseFloat(value.l)];
        let colorPick = new Colorpick(cp[0],value);
        colorPick.ctype = cp.attr("data-color");
        colorPick.onChange=function(e){
            let card = $(e.outbox).parent().parent().parent();
            let color = `hsl(${e.value[0]},${e.value[1]}%,${e.value[2]}%)`;
            card.css("background-color",color);
        };
        cp[0].colorpick=colorPick;
    }

    deleteTrilha(elm) {
        let trilha = $(elm).parent().parent().parent();
        let trilhaId = trilha.attr("data-rowid");
        if(trilhaId.search("new") === (-1)){
            this.deletedTrilhasIds.push(trilhaId);
        }
        trilha.remove();
    }

    setTrilhasId(response){
        for(let id in response.trilhas){
            if(response.trilhas.hasOwnProperty(id)){
                let trilha = $("[data-rowid="+id+"]");

                if(response.trilhas[id].error===undefined){
                    this.resetTrilhaAlert(id,response.trilhas[id]['newid'],trilha);
                    trilha.attr("data-rowid",response.trilhas[id]['newid']);
                }else{
                    let text = (response.trilhas[id].error);
                    this.setTrilhaAlert(id,text,trilha);
                }
            }
        }
    }

    getTrilhasValues(form){
        let rows={};
        form.find("[data-rowid]").each(function (index) {
            let id = $(this).attr("data-rowid");
            let nome = $(this).find(".trilhaName").val();
            let cor = $(this).find(".colorpick")[0].colorpick.value;
            cor = {h:cor[0],s:cor[1],l:cor[2]};
            let desc = $(this).find(".trilhaDesc").val();
            rows[id]={nome:nome,cor:cor,desc:desc};
        });
        return rows;
    }

    formSubmit(e){
        let _this = this;
        let data={};
        data.trilhas = this.getTrilhasValues(_this.form);
        data.deletedTrilhasIds = _this.deletedTrilhasIds;
        data.ano = Env.__YEAR;

        let datasend = new Comm(window.location.getUrl() + "/savetrilhas",false,data);
        datasend.success((response) => {
            _this.setTrilhasId(response);
            _this.deletedTrilhasIds=[];
        });

        datasend.send();

    }

    setUpTrilhasConfig(){
        let _this=this;
        setTimeout(function () {
            $('#addTrilhaButton').click(function(e){_this.addTrilha(e)});
            _this.form.find(".trilhasContainer").find(".delete-trilha").on("click",function (){_this.deleteTrilha(this)});
            let cp = _this.form.find(".colorpick");
            _this.form.on("submit",function(e){
                e.preventDefault();
                _this.formSubmit(e);
                return false;
            });
            cp.each(function(i){
                _this.startColorPick($(this));
            });
        },500);
    }
}
