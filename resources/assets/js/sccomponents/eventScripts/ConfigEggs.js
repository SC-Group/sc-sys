export class ConfigEggs {
    constructor(){
        this.form=$("#eventoConfigEggsForm");
        this.modelEggs = $(".row-template-eggs");
        this.addEggs=this.form.find('[data-name=add-eggs]');
        this.eggsContainer=this.form.find("[data-name=eggs-container]");
        this.deleted=[];
    }

    async load(i){
        this.next = (++i);
        await this.setUpEggs();
        if(window.configEventoVariables.hasLoaded(this.next)===false) window.configEventoVariables.loadClasses(this.next);
    }

    async setUpEggs(){
        let _this = this;
        this.form.remove();
        let datasend = new Comm(window.location.getUrl() + "/eggs", true);
        datasend.success((response) => {
            _this.form = $(response.view);
            $("#accordion").append(_this.form);
            _this.modelEggs = $(".row-template-eggs");
            _this.addEggs=_this.form.find('[data-name=add-eggs]');
            _this.eggsContainer=_this.form.find("[data-name=eggs-container]");
            _this.setUpEggsConfig();
            _this.form.find('input[type=fileimg]').setUploadImg();
        });
        await datasend.send();
    }

    setupEvents(lines){
        let _this = this;
        let btnDelete = lines.find("[data-name=btn-delete-eggs]");
        btnDelete.click(function (e) {
            e.preventDefault();
            let removed = $(this).closest("[data-name=line-egg]");
            let dataId = removed.attr("data-id");
            if(!dataId.includes('new-')) {
                _this.deleted.push(dataId);
            }
            removed.remove();
            return false;
        });
    }

    setupID(line,lineId){
        let cod = line.find("[data-name=egg-cod]");
        let premio=line.find("[data-name=egg-premio]");
        let img=line.find("[data-name=egg-img]");
        let lineLocal = line.find("[data-name=line-egg]");
        lineLocal.attr("data-id",lineId);
        cod.attr("name",`egg[${lineId}][cod]`);
        premio.attr("name",`egg[${lineId}][premio]`);
        img.attr("name",`egg[${lineId}][img]`);
        img.attr("type",`fileimg`);

    }

    setupLine(line){
        let lineId = "new-"+uuid();
        this.setupID(line,lineId);
        this.setupEvents(line);
        this.form.find("input[type=fileimg]").setUploadImg();
    }
    setUpEggsConfig(){
        let _this=this;

        this.addEggs = this.form.find("[data-name=add-eggs]");
        this.addEggs.click(function(e){
            e.preventDefault();
            let line = _this.modelEggs.clone();

            line.removeClass("d-none");
            _this.eggsContainer.append(line);
            _this.setupLine(line);
            return false;
        });
        this.form.submit(function(e){
            e.preventDefault();
            _this.save(this,e);
            return false;
        });
        this.setupEvents(this.form);
    }

    save(elm,e){
        let formData = new FormData(elm);
        let _this=this;
        formData.append("deleted",JSON.stringify(this.deleted));
        let comm = new Comm(window.location.getUrl() + "/saveeggs",false,formData,{
            contentType: false,
            processData: false
        });

        comm.success(function(response){
            _this.deleted=[];
            let newIds = response.ids;
            for(let i in newIds){
                if(newIds.hasOwnProperty(i)){
                    let line = $(`[data-id=${i}]`);
                    line.attr("data-id",newIds[i]);
                    _this.setupID(line,newIds[i]);
                }
            }
        });

        comm.send();
    }

}