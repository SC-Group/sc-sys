export class ConfigEvento{
    async load(){
        $("#eventCreateForm input[type=text]").inputmask("99/99/9999 99:99:99");
         $("#eventAlterForm input[type=text]").inputmask("99/99/9999 99:99:99");

        await $("#eventCreateForm .modal").modal();

        await this.setTimeFormats("#inicioDoEventoPick","#fimDoEventoPick");
        await this.setTimeFormats("#inicioDasInscPick","#inicioDoEventoPick");

        await this.setTimeFormats("#altInicioDoEventoPick","#altFimDoEventoPick");
        await this.setTimeFormats("#altInicioDasInscPick","#altInicioDoEventoPick");
        await this.setEvents();
        return true;
    }

    setTimeFormats(t,linkto){
        t = $(t);
        let options = {
            format: "DD/MM/YYYY HH:mm:ss",
            useCurrent:false,
            icons: {
                time: "fa fa-clock",
                date: "fa fa-calendar-alt",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }};
        t.datetimepicker(options);
        if(linkto!==undefined){
            linkto = $(linkto);
            linkto.datetimepicker(options);
            this.linkTimeFormats(t,linkto);
        }
    }
    setEvents(){
        let _this=this;
        $("#configChangeYear").on("change",function(){ConfigEvento.changeYear($(this).val())});
        $("#eventAlterForm").on("submit",function (e) {
            e.preventDefault();
            let _this=$(this);
            let data={
                edicaoEvt: _this.find("input[name=edicaoEvt]").val(),
                inscEvtInit:_this.find("input[name=inscEvtInit]").val(),
                evtInit:_this.find("input[name=evtInit]").val(),
                evtEnd:_this.find("input[name=evtEnd]").val()
            };
            let datasend = new Comm(window.location.getUrl()+"/alter",true,data,{showMsg:false});
            datasend.success(async (e)=>{

                if( window.configEventoVariables.loaded["ConfigCronograma"]) {
                    await window.configEventoVariables.loaded["ConfigCronograma"].setUpCronograma();
                    window.configEventoVariables.loaded["ConfigCronograma"].save(null,null,"Evento salvo e cronogramas atualizados com sucesso");
                }else{
                    Messages.warning("Evento salvo mas não pôde atualizar cronogramas. Recarregue a página");
                }

            });
            datasend.send();

            return false;
        });
    }

    static changeYear(year) {
        window.location.href = Env.get('root_event_url').replace("%%YEAR%%", year);
    }

    linkTimeFormats(t1,t2){
        t1.on("change.datetimepicker", function (e) { t2.datetimepicker('minDate', e.date); });
        t2.on("change.datetimepicker", function (e) { t1.datetimepicker('maxDate', e.date); });
    }
}

window.ConfigEvento=ConfigEvento;