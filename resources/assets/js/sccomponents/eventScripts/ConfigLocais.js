export class ConfigLocais {
    constructor(){
        this.form=$("#eventoConfigLocaisForm");
        this.modelLocais = $(".row-template-locais");
        this.addLocais=this.form.find('[data-name=add-locais]');
        this.locaisContainer=this.form.find("[data-name=locais-container]");
        this.deleted=[];
    }

    async load(i){
        this.next = (++i);
        await this.setUpLocais();
        if(window.configEventoVariables.hasLoaded(this.next)===false) window.configEventoVariables.loadClasses(this.next);
    }

    async setUpLocais(){
        let _this = this;
        this.form.remove();
        let datasend = new Comm(window.location.getUrl() + "/locais", true);
        datasend.success((response) => {
            _this.form = $(response.view);
            $("#accordion").append(_this.form);
            _this.modelLocais = $(".row-template-locais");
            _this.addLocais=_this.form.find('[data-name=add-locais]');
            _this.locaisContainer=_this.form.find("[data-name=locais-container]");
            _this.setUpLocaisConfig();
        });
        await datasend.send();
    }

    setupEvents(lines){
        let _this = this;
        let btnDelete = lines.find("[data-name=btn-delete-local]");
        btnDelete.click(function (e) {
            e.preventDefault();
            let removed = $(this).closest("[data-name=line-local]");
            let dataId = removed.attr("data-id");
            if(!dataId.includes('new-')) {
                _this.deleted.push(dataId);
            }
            removed.remove();
            return false;
        });
    }

    setupID(line,lineId){
        let nome = line.find("[data-name=local-nome]");
        let vagas=line.find("[data-name=local-vagas]");
        let descricao=line.find("[data-name=local-descricao]");
        let lineLocal = line.find("[data-name=line-local]");
        lineLocal.attr("data-id",lineId);
        nome.attr("name",`local[${lineId}][nome]`);
        vagas.attr("name",`local[${lineId}][vagas]`);
        descricao.attr("name",`local[${lineId}][descricao]`);
    }

    setupLine(line){
        let lineId = "new-"+uuid();
        this.setupID(line,lineId);
        this.setupEvents(line);
    }
    setUpLocaisConfig(){
        let _this=this;

        this.addLocais = this.form.find("[data-name=add-locais]");
        this.addLocais.click(function(e){
            e.preventDefault();
            let line = _this.modelLocais.clone();

            line.removeClass("d-none");
            _this.locaisContainer.append(line);
            _this.setupLine(line);
            return false;
        });
        this.form.submit(function(e){
            e.preventDefault();
            _this.save(this,e);
            return false;
        });
        this.setupEvents(this.form);
    }

    save(elm,e){
        let formData = new FormData(elm);
        let _this=this;
        formData.append("deleted",JSON.stringify(this.deleted));
        let comm = new Comm(window.location.getUrl() + "/savelocais",false,formData,{
            contentType: false,
            processData: false
        });

        comm.success(function(response){
            _this.deleted=[];
            let newIds = response.ids;
            for(let i in newIds){
                if(newIds.hasOwnProperty(i)){
                    let line = $(`[data-id=${i}]`);
                    line.attr("data-id",newIds[i]);
                    _this.setupID(line,newIds[i]);
                }
            }
        });

        comm.send();
    }

}