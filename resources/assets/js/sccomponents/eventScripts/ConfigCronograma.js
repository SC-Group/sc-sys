import {modelCronograma} from './Models/CronogramaModels';
export class ConfigCronograma {
    constructor(){
        this.form=$("#eventoConfigCronogramaForm");
        this.addChronos=this.form.find('[data-name=add-chronos]');
        this.tableBody = this.form.find('table > tbody');
        this.deleted=[];
    }

    async load(i){
        this.next = (++i);
        await this.setUpCronograma();
        if(window.configEventoVariables.hasLoaded(this.next)===false) window.configEventoVariables.loadClasses(this.next);
    }

    updateIds(newIds){
        for(let id in newIds){
            if(newIds.hasOwnProperty(id)) {
                let newId = newIds[id];
                let line = this.form.find(`tr[data-id=new-${id}]`);
                line.attr(`data-id`,`${newId}`);
                let elms = line.find(`[name*=${id}]`);
                elms.each((i, elm)=>{
                    let elmAttr = $(elm).attr("name");
                    $(elm).attr("name", elmAttr.replace(`${id}`,`${newId}`).replace("new","edit"));
                });

                elms = line.find(`[data-name*=${id}]`);
                elms.each((i, elm)=>{
                    let elmAttr = $(elm).attr("data-name");
                    $(elm).attr("data-name", elmAttr.replace(`${id}`,`${newId}`).replace("new","edit"));
                });
            }
        }
    }
    save(elm,e,msg){
        const _this = this;
        let formData = new FormData($("#eventoConfigCronogramaForm")[0]);

        if(this.deleted.length>0)
            formData.append("chronos-line[deleted]",JSON.stringify(this.deleted));
        let comm = new Comm(window.location.getUrl() + "/savecronograma",false,formData,{
            showMsg: msg===undefined,
            showError: msg===undefined,
            contentType: false,
            processData: false
        });

        comm.success(function(response){
            if(msg){
                Messages.success(msg);
            }
            if(response.data && response.data.newIds){
                _this.updateIds(response.data.newIds);
            }
        });
        if(msg){
            comm.error(()=>{
                Messages.success(msg);
            });
        }
        comm.send();

    }

    setupEvents(lines){
        let _this = this;
        let btnDelete = lines.find("[data-name=btn-delete-chronos]");

        btnDelete.click(function (e) {
            e.preventDefault();
            let removed = $(this).closest("[data-name=line-chronos]");
            let dataId = removed.attr("data-id");
            let isNew = dataId.includes('new-');

            if (!isNew) {
                _this.deleted.push(dataId);
             }
            removed.remove();

            return false;
        });
    }

    setButonContent(elm,value,btnClass){
        const input = $(elm).parent().find("[type=hidden]");
        $(elm).removeClass("btn-warning").removeClass("btn-danger").removeClass("btn-light").addClass("btn-"+btnClass);
        $(elm).val(value || "Atividade");
        input.val(value);
    }

    setupLine(lines){
        const _this = this;
        let inicio = lines.find("[data-name^=chronos-inicio]");
        let fim = lines.find("[data-name^=chronos-fim]");
        let status = lines.find("[data-name^=status-day]");

        status.click(function(e){
            let value = this.value;
            switch (value) {
                case "Atividade":
                    _this.setButonContent(this,"Coffee-Break", "warning");
                break;
                case "Coffee-Break":
                    _this.setButonContent(this,"Almoço", "danger");
                    break;
                case "Almoço":
                    _this.setButonContent(this,"Atividade", "light");
                break;
            }
        });

        this.setupEvents(lines);

        inicio.inputmask("datetime",{
            inputFormat:"HH:MM",
            placeholder:"__:__"
        });

        fim.inputmask("datetime",{
            inputFormat:"HH:MM",
            placeholder:"__:__"
        });

    }

    addLine(start,end,values){
        const table = this.form.find(`table`);
        const numdays = table.find('thead>tr td').length - 2;
        const model= $(modelCronograma("new",start||"",end||"",values||Array(numdays).fill("Atividade"),uuid()));
        this.tableBody.append(model);
        this.setupLine(model)
    }

    setUpCronogramaConfig(){
        let _this=this;
        this.form=$("#eventoConfigCronogramaForm");
        this.tableBody = this.form.find('table > tbody');
        this.addChronos = this.form.find("[data-name=add-chronos]");
        this.addChronos.click((e)=>{e.preventDefault();this.addLine();return false;});
        this.form.submit(function(e){
            e.preventDefault();
            _this.save(this,e);
            return false;
        });
        this.setupLine(this.form);
    }

    async setUpCronograma(){
        let _this = this;
        this.form.remove();
        let datasend = new Comm(window.location.getUrl() + "/cronograma", true);
        datasend.success((response) => {
            _this.form = $(response.view);
            $("#accordion").prepend(_this.form);
            _this.setUpCronogramaConfig();
        });
        await datasend.send();
    }

}