export class ConfigColors {
    constructor() {
        this.colorsPick=[];
        this.form=null;
        this.selectedColorPick=0;
    }

    async load(i) {
        this.next = (++i);
        let _this = this;
        let retorno = false;
        let form = document.querySelector("#eventoConfigCoresForm");
        if(form!=null)form.remove();
        let datasend = new Comm(window.location.getUrl() + "/colors", true);
        datasend.success((response) => {
            $("#accordion").append(response.view);
            _this.form = $("#eventoConfigCoresForm");
            _this.form.on("submit", function (e) {
                e.preventDefault();
                return _this.colorsSendConfig(e);
            });
            setTimeout(()=>{
                this.colorSuggestions($("#vibrantLogoSuggestion"));
                this.colorSuggestions($("#vibrantBGSuggestion"));
            },1000);

            _this.setupColorPick();
            this.selectColorPick();

         //   if(window.configEventoVariables.hasLoaded(this.next)===false) window.configEventoVariables.loadClasses(this.next);
            retorno = true;
        });
        await datasend.send();
        return retorno;
    }

    colorSuggestions(card) {
        let _this = this;
        let img = card.find("img")[0];

        let cardbody = card.find('.card-body').find("div");
        if((img===undefined)||(img.naturalHeight<=0)){
            Messages.error("Não há imagem carregada para sugestões",true);
            return false;
        }

        Vibrant.from(img.src).getPalette((err, palette) => {

            for (let color in palette){
                if (palette.hasOwnProperty(color) && palette[color]) {
                    let colorHex = palette[color].getHex();
                    let div = document.createElement("div");
                    div.setAttribute("class","btn border m-1 p-3 col-3");
                    div.setAttribute("style","background-color:"+ colorHex);
                    div.addEventListener('click',(e)=>{
                        let cor = Colors.hex2hsl(colorHex);
                        cor[0] = Math.round(cor[0]);
                        cor[1] = Math.round(cor[1]);
                        cor[2] = Math.round(cor[2]);
                        _this.setColorPick(cor);
                    });
                    cardbody[0].appendChild(div);
                }
            }
            card.css({'background-color':palette['Vibrant'].getHex()})
            cardbody.find("div").css({"border-color":palette['DarkVibrant'].getHex()})
        });
    };

    setColorPick(color){
        let cc = this.colorsPick[this.selectedColorPick];
        cc.setRangesValues(color);
        this.selectedColorPick = (++this.selectedColorPick)%this.colorsPick.length;
        this.selectColorPick();
    }

    selectColorPick(){
        this.colorsPick.forEach((elm)=>{
            elm.outercontent.style.boxShadow = '';
        });
        this.colorsPick[this.selectedColorPick].outercontent.style.boxShadow = '0 3px 20px rgba(0,0,0,0.7)';
    }

    setupColorPick(){
        let _this = this;
            let elements = this.form.find('.colorpick');
            elements.each((elm) => {
                let value = elements.eq(elm).attr("data-value");
                value = value===''?{h:180,s:50,l:50}:JSON.parse(value);
                value = [parseFloat(value.h), parseFloat(value.s), parseFloat(value.l)];
                _this.colorsPick[elm] = new Colorpick(elements[elm],value);
                _this.colorsPick[elm].ctype = elements[elm].getAttribute("data-color");
                _this.colorsPick[elm].outercontent.addEventListener('click', (e) => {
                    _this.selectedColorPick = elm;
                    this.selectColorPick();
                });
            });
    }

    updateColors(resp){
        document.querySelector("#customCSS").href = Env.__LOCAL+resp.css;
    }

    colorsSendConfig(){
        let _this=this;
        let param = {};
        for (let i =0;i<this.colorsPick.length;i++)
        {
            let val = this.colorsPick[i].value;
            param[this.colorsPick[i].ctype] = {h:val[0],s:val[1],l:val[2]};
        }

        let senddata = new Comm(window.location.getUrl() + "/savecolors", true,{colors:param});
        senddata.success(resp => _this.updateColors(resp));
        senddata.send();
    }
}