export class ConfigCamisas {
    constructor(){
        this.deletedCamisasRowIds = [];
        this.form=$("#eventoConfigCamisasForm");
    }
    async load(i){
        this.next = (++i);
        await this.setUpCamisas();
        if(window.configEventoVariables.hasLoaded(this.next)===false) window.configEventoVariables.loadClasses(this.next);
    }
    async setUpCamisas(){
        let _this = this;
        this.form.remove();
        let datasend = new Comm(window.location.getUrl() + "/camisas", true);
        datasend.success((response) => {
            $("#accordion").append(response.view);
            _this.setUpCamisasConfig();

        });
        await datasend.send();
    }

    formSubmit(e){
        let _this=this;
        let data={};
        data.rows = _this.getRowsValues(_this.form);
        data.deletedRowsIds = _this.deletedCamisasRowIds;
        data.precos=_this.getPrecos(_this.form);
        data.ano = Env.__YEAR;
        let datasend = new Comm(window.location.getUrl()+"/savecamisas",true,data);

        datasend.success((response)=>{
            _this.setRowsId(response);
            _this.deletedCamisasRowIds=[];
            if(window.configEventoVariables.hasLoaded(this.next)===false) window.configEventoVariables.loadClasses(this.next);
        });

        datasend.send();

    }

    setUpCamisasConfig(){
        let _this = this;
        setTimeout(function () {
            _this.form = $("#eventoConfigCamisasForm");
            _this.form.find("[name=precoantes]").inputmask("numeric",{autoUnmask:true,rightAlign:false,numericInput: true,prefix:"R$" });  //static mask with dynamic syntax
            _this.form.find("[name=precodepois]").inputmask("numeric",{autoUnmask:true,rightAlign:false,numericInput: true,prefix:"R$" });  //static mask with dynamic syntax
            _this.form.find("tr").find(".delete-linha").on("click",(e)=>{e.preventDefault(); _this.deleteLinha(e); return false;});

            _this.form.on("submit",(e)=>{
                e.preventDefault();
                _this.formSubmit(e);
                return false;
            });

            _this.form.find("table .add-linha").on('click',e=>{
                e.preventDefault();
                _this.addLinha(e);
                return false;
            });
        },1000);
    }

    getRowsValues(){
        let rows={};
        this.form.find("[data-rowid]").each(function (index) {
            let id = $(this).attr("data-rowid");
            let tam = $(this).find(".tam select").val();
            let tipo = $(this).find(".tipo select").val();
            let qtd = $(this).find(".qtd input").val();
            let cor = $(this).find(".cor input").val();
            rows[id]={tam:tam,tipo:tipo,cor:cor,qtd:qtd};
        });
        return rows;
    }

    addLinha(e) {
        let _this = this;
        let tbody = $(_this.form).find("#camisas-tbody");
        let row = $(".row-template-camisas .lineContainer").clone();
        let rowAlert = $(".row-template-camisas .msgContainer").clone();
        let rowId = "new-"+(new Date().valueOf());
        row.attr("data-rowid",rowId);
        rowAlert.attr("data-msgrowid",rowId);
        tbody.append(row);
        tbody.append(rowAlert);
        row.find(".delete-linha").on("click",(e)=>{e.preventDefault(); _this.deleteLinha(e); return false;});
    }

    deleteLinha(e) {
        let row = $(e.target).closest("tr");
        let _this = this;
        let rowId = row.attr("data-rowid");
        if(rowId.search("new") === (-1)){
            _this.deletedCamisasRowIds.push(rowId);
        }
        row.remove();
        $("[data-msgrowid="+rowId+"]").remove();
    }



    setRowsId(response){
        for(let id in response.rows){
            if(response.rows.hasOwnProperty(id)){
                let row = $("[data-rowid="+id+"]");
                if(response.rows[id].error === undefined){
                    this.resetRowAlert(id,response.rows[id].newid);
                    row.attr("data-rowid",response.rows[id].newid);
                }else{
                    let text = (response.rows[id].error);
                    this.setRowAlert(id,text);
                }
            }
        }
    }

    getPrecos(){
        let form = this.form;
        let preco_antes = form.find("[name=precoantes]").val();
        let preco_depois = form.find("[name=precodepois]").val();
        return {antes:parseFloat(preco_antes),depois:parseFloat(preco_depois)};
    }

    setRowAlert(id,msg){
        let row = $("[data-rowid="+id+"]");
        let placement = $("[data-msgrowid="+id+"]").find(".msgRow");
        row.css('background-color','#ffaaaa');
        placement.html(msg);

        row.on("mouseover",function(e){
            placement.fadeIn(300);
        });
        row.on("mouseleave",function(e){
            e.preventDefault();
            e.stopPropagation();
            placement.fadeOut(300);
            return false;
        });
    }

    resetRowAlert(id,newId){
        let row = $("[data-rowid="+id+"]");
        let placement = $("[data-msgrowid="+id+"]");
        let pl = placement.find(".msgRow")
        if(newId!==undefined){
            placement.attr("data-msgrowid",newId)
        }
        row.css('background-color','');
        pl.html('');
        row.off("mouseover");
        row.off("mouseout");
    }

}