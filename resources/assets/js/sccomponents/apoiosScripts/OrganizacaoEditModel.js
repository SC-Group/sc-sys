/**
 * @return {string}
 */
function OrganizacaoEditModel(data, errors, edicao) {

    errors = errors || {};

    return `
    <form enctype="multipart/form-data" method="post" action="${location.getUrl()}/saveOrganizacao">
        <input type="hidden" name="_token" value="${$("meta[name=csrf-token]").attr("content")}">
        <input type="hidden" value="${data&&data.organizacaoId?data.organizacaoId:""}" name="organizacaoId">
        <input type="hidden" name="tipoCadastro" value="${edicao?"edicao":"novo"}">
        <div class="form-group">
            <label class="" for="customFile">Logo</label>
            <div style="border-color: #ced4da" class="border mb-0 rounded">
                <div class="d-flex justify-content-center p-2">
                    <img style="max-width: 100%; max-height: 80px" src="${data&&data.organizacaoLogo?data.organizacaoLogo:""}" alt="">
                </div>
                <div>
                    <input type="file" name="organizacaoLogoFile" class="d-none" id="customFile">
                    <label style="border-radius: 0 0 4px 4px" class="btn btn-dark w-100 mb-0" for="customFile">
                        <i class="fa fa-upload"></i>
                    </label>
                </div>
            </div>  
            <span style="font-size: 9pt" class="text-danger">${errors.organizacaoLogo?errors.organizacaoLogo.join("; <br>"):""}</span>
        </div>
        
        
        <div class="form-group">
            <label for="organizacaoModalNome">Nome</label>
            <input type="text" name="organizacaoNome" class="form-control" id="organizacaoModalNome" value="${data&&data.organizacaoNome?data.organizacaoNome:""}">
            <span style="font-size: 9pt" class="text-danger">${errors.organizacaoNome?errors.organizacaoNome.join("; <br>"):""}</span>
        </div>
        
        <div class="form-group">
            <label for="organizacaoModalResponsavel">Responsável</label>
            <input type="text" name="organizacaoResponsavel" class="form-control" id="organizacaoModalResponsavel" value="${data&&data.organizacaoResponsavel?data.organizacaoResponsavel:""}">
            <span style="font-size: 9pt" class="text-danger">${errors.organizacaoResponsavel?errors.organizacaoResponsavel.join("; <br>"):""}</span>
        </div>
        <div class="form-group">
            <label for="organizacaoModalTelefone">Telefone</label>
            <input type="text" data-inputmask="'mask': '(99) 99999-9999', 'removeMaskOnSubmit': true" name="organizacaoTelefone" class="form-control" id="organizacaoModalTelefone"  value="${data&&data.organizacaoTelefone?data.organizacaoTelefone:""}" placeholder="(21) 98888-8888">
            <span style="font-size: 9pt" class="text-danger">${errors.organizacaoTelefone?errors.organizacaoTelefone.join("; <br>"):""}</span>
        </div>
        <div class="form-group">
            <label for="organizacaoModalEmail">E-mail</label>
            <input type="email"  name="organizacaoEmail" class="form-control" id="organizacaoModalEmail" value="${data&&data.organizacaoEmail?data.organizacaoEmail:""}" placeholder="name@example.com">
            <span style="font-size: 9pt" class="text-danger">${errors.organizacaoEmail?errors.organizacaoEmail.join("; <br>"):""}</span>
        </div>
        <div class="form-group">
            <label for="organizacaoModalCargo">Cargo do Responsável</label>
            <input type="text"  name="organizacaoCargo" class="form-control" id="organizacaoModalCargo" value="${data&&data.organizacaoCargo?data.organizacaoCargo:""}">
            <span style="font-size: 9pt" class="text-danger">${errors.organizacaoCargo?errors.organizacaoCargo.join("; <br>"):""}</span>
        </div>
        <div class="form-group">
            <label for="organizacaoModalSite">Site</label>
            <input type="text" name="organizacaoSite" class="form-control" id="organizacaoModalSite" value="${data&&data.organizacaoSite?data.organizacaoSite:""}" placeholder="www.example.com">
            <span style="font-size: 9pt" class="text-danger">${errors.organizacaoSite?errors.organizacaoSite.join("; <br>"):""}</span>
        </div>
       
    </form>

    `;
}

export {OrganizacaoEditModel};