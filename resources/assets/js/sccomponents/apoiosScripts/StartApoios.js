import {OrganizacaoEditModel} from "./OrganizacaoEditModel";
class StartApoios{

    constructor() {
        this.container = $("#gerencia-de-apoios");
        this.escolhidos = this.container.find(".apoios-escolhidos");
        this.todos = this.container.find(".todas-as-organizacoes");

        this.apoiocard = this.container.find("[data-name=apoiocard]");
        this.optionsbar = this.container.find(".optionsbar");


        this.setEvents();

    }

    setAreaEvents(area){

        const _this=this;
        let optionsbar =  area.find(".optionsbar");
        let selectionActivated = false;
        let cards = area.find("[data-name=apoiocard]");
        let inputs =  cards.find("input[name^=selecionado]");
        optionsbar.find(".select-btn").click(function(){
            optionsbar.addClass("bg-secondary");
            optionsbar.find(".after-select").removeClass("d-none");
            optionsbar.find(".before-select").addClass("d-none");
            selectionActivated = true;
        });

       optionsbar.find(".cancel-btn").click(function () {
           optionsbar.removeClass("bg-secondary");
           optionsbar.find(".after-select").addClass("d-none");
           optionsbar.find(".before-select").removeClass("d-none");
           selectionActivated = false;
           inputs.val(false);
           cards.removeClass("border-sc-primary")
       });

        area.find("[data-name=apoiocard]").click(function(e){
            let elm = $(this);
            if(selectionActivated) {
                let input = elm.find("input[name^=selecionado]");
                if(input.val() === "true"){
                    input.val(null);
                    elm.removeClass("border-sc-primary")
                }else{
                    input.val(true);
                    elm.addClass("border-sc-primary")
                }
            }else{
                _this.cardClickAction(elm,area);
            }
            e.preventDefault();
            e.stopPropagation();
        });
    }

    fileUploadEvent(form){
        form.find('input[type=file]').on('change', function() {
            if (this.files && this.files[0]) {
                let imgcontainer = $(this).closest(".form-group");
                let img = imgcontainer.find("img");
                let reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
                reader.addEventListener("load",function (e) {
                    img[0].src = reader.result;
                });

            }
        });
    }

    cardClickActionOrganizacao(elm){
        let id = elm.attr("data-id");
        const _this = this;
        const comm = new Comm(`${location.getUrl()}/getOrganizacao`,false, {id:id});
        comm.success(function (response) {
            Comm.hideLoader();
            console.log(response);
            let data = {
                organizacaoId:response.data.id,
                organizacaoNome:response.data.nome,
                organizacaoResponsavel: response.data.responsavel,
                organizacaoTelefone: response.data.tel_responsavel,
                organizacaoEmail: response.data.email_responsavel,
                organizacaoCargo:response.data.cargo_responsavel,
                organizacaoSite: response.data.site,
                organizacaoLogo: response.data.logo_url
            };

            _this.mountApoioModal(data);
        });
        comm.send();
    }

    cardClickActionApoio(elm){
        let id = elm.attr("data-id");
        const _this = this;
        const comm = new Comm(`${location.getUrl()}/getApoio`,false, {id:id});
        comm.success(function (response) {
            Comm.hideLoader();

            let data = {
                organizacaoId:response.data.id,
                organizacaoNome:response.data.nome,
                organizacaoResponsavel: response.data.responsavel,
                organizacaoTelefone: response.data.tel_responsavel,
                organizacaoEmail: response.data.email_responsavel,
                organizacaoCargo:response.data.cargo_responsavel,
                organizacaoSite: response.data.site,
                organizacaoLogo: response.data.logo_url
            };

            _this.mountApoioModal(data,null,true);
        });
        comm.send();
    }

    mountApoioModal(content, errors, edicao){
        edicao = (edicao===true);
        let contentelm = $(OrganizacaoEditModel(content, errors, edicao));
        this.fileUploadEvent(contentelm);
        Modal.resetAll();
        Modal.setWidth("lg");
        Modal.setBody(contentelm);
        Modal.addFooterButton("Cancelar","btn btn-secondary",null,true);
        Modal.addFooterButton("Salvar","btn btn-sc-primary",()=>{
            contentelm.submit();
        });
        Modal.show();
        contentelm.find(":input").inputmask();

    }


    cardClickAction(elm, area){
        if(area.hasClass("apoios-escolhidos")){
            this.cardClickActionApoio(elm);
        }else{
            this.cardClickActionOrganizacao(elm);
        }

    }

    adicionarAEvento(){
        if(this.todos.find("[name^=selecionado][value=true]").length===0){
            Messages.error("Primeiro selecione clicando nas organizações");
            return;
        }

        $(this.todos).find("input[name=sendForm]").val("adicionar");
        this.todos.submit();


    }

    excluirOrganizacao(){
        if(this.todos.find("[name^=selecionado][value=true]").length===0){
            Messages.error("Primeiro selecione clicando nas organizações");
            return;
        }

        Confirm.resetAll();
        Confirm.load(
            "Tem certeza que gostaria de excluir permanentemente este(s) item(s)?",
            ()=>{
                this.todos.find("input[name=sendForm]").val("excluir");
                this.todos.submit();
            },
            ()=>{},
            "Sim",
            "Cancelar"
        );

        Modal.show();
    }

    removerDeEvento(){
        if(this.escolhidos.find("[name^=selecionado][value=true]").length===0){
            Messages.error("Primeiro selecione clicando nas organizações");
            return;
        }
        Confirm.resetAll();
        Confirm.load(
            "Tem certeza que gostaria de remover este(s) item(s) deste evento?",
            ()=>{
                this.escolhidos.find("input[name=sendForm]").val("remover");
                this.escolhidos.submit();
            },
            ()=>{},
            "Sim",
            "Cancelar"
        );

        Modal.show();
    }

    setEvents(){
        const _this=this;
        let optionstodos = this.todos.find(".optionsbar");
        optionstodos.find(".add-new").click(()=>this.mountApoioModal());

        this.escolhidos.find("[name=remover]").click((e)=>{e.preventDefault(); this.removerDeEvento() });
        this.todos.find("[name=adicionar]").click((e)=>{e.preventDefault(); this.adicionarAEvento() });
        this.todos.find("[name=excluir]").click((e)=>{e.preventDefault(); this.excluirOrganizacao() });
        this.setAreaEvents(this.escolhidos);
        this.setAreaEvents(this.todos);
    }

}

let startApoios=new StartApoios();
window.startApoios = startApoios;
export {startApoios};