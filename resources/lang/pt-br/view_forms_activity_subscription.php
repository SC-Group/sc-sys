<?php
return [
    "Choose"=>"Selecione",
    "ChooseOption"=>"Selecione...",
    "ChoiceNewActivity"=>"Quero cadastrar uma nova atividade",
    "ChoiceExistentActivity"=>"Quero me associar a uma atividade já existente",
    "ActivityTitle"=>"Atividade",
    "NumberOfTShirts"=>"Número de camisas",
    "FemaleTShirtTemplate"=>"Modelo de camisa feminina",
    "MaleTShirtTemplate"=>"Modelo de camisa masculina",
    "TShirtTemplate"=>"Modelo de camisa",
    "FemaleTShirts"=>"Camisas femininas",
    "MaleTShirts"=>"Camisas masculinas",
    "ThatsAll"=>"Isto é tudo!",
    "ThatsAllWeNeed"=> "Isso é tudo o que precisamos, aguarde para mais novidades",
    "DoYouWantToReserveTShirts"=>[
        "Title"=>'Você gostaria de reservar camisas do evento e garantir o seu tamanho ideal?',
        "CostBefore"=>"Elas custarão apenas R$:pricebefore cada. Se quiser, escolha o número de camisas a reservar, os tamanhos e o tipo.",
        "HaveYouSeenTShirt"=>'Você já viu a camisa do evento?',
        "TShirtOBS1"=>"*A reserva da camisa será confirmada apenas se o pagamento for efetuado antes do início do evento.",
        "TShirtOBS2"=>"*Após o início do evento a camisa custará R$:pricebefore.<br>*A reserva da camisa a R$:priceafter será confirmada apenas se o pagamento for efetuado antes do início do evento.",

    ],
    "ActivityConfirmation"=>[
        "Title"=>'Confirmação de Atividade',
        "TextInfo"=>"Certifique se de que você está se associando a atividade correta conferindo os dados abaixo. 
                Caso estejam corretos, clique em \"Próximo\". Caso não estejam corretos, clique em \"Anterior\" e tente novamente.",
        "ActivityData"=>[
            "FieldName"=>"Dados da Atividade:",
            "FieldInfo"=>"<strong>Tipo de Atividade:</strong> :type<br><strong>Título da atividade:</strong> :name<br>",
        ],
    ],
    "UserConfirmation"=>[
        "Title"=>'Confirmação de dados atualizados',
        "Confirmation"=>"Confirmação",
        "TextInfo"=>"Olá :username! certifique se de seus dados estão atualizados. Caso não estejam, acesse o sistema e
             atualize-os neste <a target=\"_blank\" href=\"".route("home")."\">link</a> e depois retorne a esta página para o cadastro da atividade.",
        "ConfirmFieldText"=>"Confirmo que meus dados estão atualizados",
    ],
    "Availability"=>[
        "AvailabolityFieldTitle"=>'Disponibilidade',
        "TextInfo"=>"Por favor, preencha abaixo a sua disponibilidade durante os dias da Semana da Computação para que
        possamos avaliar o melhor dia e horário a ser alocado para sua(s) atividade(s) caso seja(m) selecionada(s).",

    ],
    "PersonalData"=>[
        "Title"=>"Dados Pessoais",
        "TextInfo"=>"Nesta seção você deve inserir seus dados pessoais.
    Nós usamos estes dados para confeccionar o seu certificado de participação, bem como colher algumas estatísticas e indicadores.
     Nós não iremos de maneira alguma divulgar seus dados pessoais para terceiros, a menos que expressamente autorizado por você.",
        "FullName"=>"Nome Completo",
        "CelPhone"=>'Telefone Celular (Whatsapp)',
        "Password"=>"Por favor crie uma senha para acesso ao sistema",
        "PasswordConfirmation"=>'Por favor, confirme sua senha',
        "Category"=>[
            "Title"=>"Eu sou... (Selecione abaixo)",
            "ExStudent"=>"Ex-aluno de graduação da UFRJ",
            "Student"=>"Aluno de graduação da UFRJ",
            "Other"=>"Outro"
        ],
        "DRE"=>[
            "Title"=>"Meu DRE é ... (Insira abaixo)",
            "Validation"=>"Esse DRE não é valido",
        ],
        "Graduation"=>"Graduação de ..."
    ],
    "End"=>[
        "CopyButton"=>"Copiar",
        "SpeakerNumberText"=>"Abaixo está seu número de palestrante.<br>Guarde-o muito bem, pois ele será usado durante todo o evento:",
        "ActivitySubscriptionSuccess"=>"Sucesso ao cadastrar ou associar atividade.",
        "ActivityCode"=>"Abaixo está seu código da atividade.<br>Copie-o e peça para os outros palestrantes desta mesma atividade (se houver algum além de você), 
                        se cadastrarem no formulário de submissão de atividades usando este código.",
        "OtherActivitySubscriptionButton"=>"Fazer outra submissão de atividade",
        "GoToStartPage"=>"Ir para Página Inicial",
        "CopiedToClipboard"=>"Copiado para área de transferência"
    ],
    "Internship"=>[
        "Title"=>"Sabemos que estagiar é muito importante",
        "TextInfo"=>"Por isso, teremos várias empresas à sua disposição, para falar sobre seus processos seletivos e sua cultura.",
        "InternshipFieldQuestion"=>[
            "Title"=>"Você gostaria de participar das oportunidades de estágio?",
            "OptionYes"=>"Claro, adoraria!!",
            "OptionNo"=>"Não, obrigado",
        ]
    ],
    "ID"=>[
        "Title"=>"Identificação",
        "EmailQuestion"=>'Qual o seu e-mail?'
    ],
    "TESTE"=>":teste",
    "Login"=>[
        "Title"=>"Login",
        "TextInfo"=> "Parece que o e-mail :email já existe em nosso banco de dados, preencha abaixo para continuarmos",
        "PasswordField"=>"Senha"
    ],
    "NewActivity"=>[
        "Title"=>'Cadastrar nova atividade',
        "TextInfo"=>"",
        "ActivityTypeField"=>[
            "Title"=>'Tipo de atividade',
            "WorkshopField"=>"Workshop",
            "LectureField"=>"Palestra"
        ],
        "ActivityTitleField"=>'Título da atividade',

        "IsAttachedOrganizationField"=>[
            "Title"=>'A atividade é vinculada a uma empresa/organização?',
            "YesOption"=>"Sim",
            "NoOption"=>"Não"
        ],
        "OrganizationField"=>'Nome da organização vinculada',

        "OrganizationTypeField"=>[
            "Title"=>'Tipo de organização Vinculada',
            "GroupOption"=>"Grupo de interesse/extensão",
            "CompanyOption"=>"Empresa"
        ],
        "DescriptionField"=>"Descrição sucinta da atividade",
        "ResourcesField"=>'Recursos necessários para a atividade',
    ],
    "ActivityExists"=>[
        "Title"=>'Associar a Atividade Existente',
        "ActivityCode"=>'Código da atividade',
    ]

];