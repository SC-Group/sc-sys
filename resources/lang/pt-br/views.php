<?php
return [
    "SubscriptionButton"=>"Inscreva-se",
    "NotRegisteredLink"=>"Não está cadastrado?",
    "Password"=>"Senha",
    "RememberMe"=>"Continuar logado",
    "LoginTitle"=>"Entrar",
    "ForgotPasswordLink"=>"Esqueceu sua senha?",
    "Email"=>"E-mail",
    "LoginButton"=>"Login",
    "PasswordRecoveryTitle"=>"Recuperação de senha",
    "SendEmailWithLinkButton"=>"Enviar e-mail com link",
    "PasswordConfirmation"=>"Confirmação de senha",
    "UpdatePasswordButton"=>"Atualizar a senha",
    "Back"=>"Voltar",
    "FormNextButton"=>"Próximo",
    "FormPreviousButton"=>"Anterior",
    "FormFinishButton"=>"Terminar e enviar"
];