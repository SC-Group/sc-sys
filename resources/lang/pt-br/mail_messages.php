<?php
return [
    "AuthMail"=>[
        "subject"=>"Semana da Computação:Recuperar Senha",
        "greetings"=>"Olá!",
        "lineMessage"=>'Você está recebendo este e-mail porque nós identificamos uma solicitação para recuperação de senha',
        "action"=>"Recuperar Senha",
        "linePS"=>'Se você não deseja ou não solicitou recuperação de senha, por favor ignore esta mensagem.',
    ]
];