<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Senhas devem ter pelo menos seis caracteres e combinarem com a confirmação.',
    'reset' => 'Sua senha foi recuperada!',
    'sent' => 'Nós enviamos um link de recuperação para seu e-mail!',
    'token' => 'Este token de recuperação de senha é inválido.',
    'user' => "Não conseguimos encontrar um usuário com este e-mail.",

];
