<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'O campo :attribute precisa ser aceito.',
    'active_url'           => 'O campo :attribute não é uma url válida.',
    'after'                => 'O campo :attribute precisa se uma data depois de :date.',
    'after_or_equal'       => 'O campo :attribute precisa ser uma data depois ou igual a :date.',
    'alpha'                => 'O campo :attribute precisa conter somente letras.',
    'alpha_dash'           => 'O campo :attribute precisa conter somente letras, números e traços.',
    'alpha_num'            => 'O campo :attribute precisa conter somente letras e números.',
    'array'                => 'O campo :attribute tem que ser um array.',
    'before'               => 'O campo :attribute precisa estar antes de :date.',
    'before_or_equal'      => 'O campo :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'O campo :attribute precisa ser um número entre :min e :max.',
        'file'    => 'O campo :attribute precisa ter entre :min e :max KB.',
        'string'  => 'O campo :attribute precisa ter entre :min e :max caracteres.',
        'array'   => 'O campo :attribute precisa ter entre :min e :max itens.',
    ],
    'boolean'              => 'o campo :attribute precisa ser booleano.',
    'confirmed'            => 'A confirmação do campo :attribute não corresponde.',
    'date'                 => 'O campo :attribute não é uma data válida.',
    'date_format'          => 'O campo :attribute não corresponde ao formato :format.',
    'different'            => 'O campos :attribute e :other precisam ser diferentes.',
    'digits'               => 'Os campo :attribute precisa ter :digits dígitos.',
    'digits_between'       => 'O campo :attribute precisa ter entre :min e :max dígitos.',
    'dimensions'           => 'O campo :attribute tem dimensões inválidas de imagem.',
    'distinct'             => 'O campo :attribute tem valor duplicado.',
    'email'                => 'O campo :attribute precisa ser um e-mail válido.',
    'exists'               => 'O campo :attribute selecionado é inválido.',
    'file'                 => 'O campo :attribute precisa ser um arquivo.',
    'filled'               => 'O campo :attribute precisa conter um valor.',
    'image'                => 'O campo :attribute precisa ser uma imagem.',
    'in'                   => 'O campo :attribute selecionado é inválido.',
    'in_array'             => 'O campo :attribute precisa estar em :other.',
    'integer'              => 'O campo :attribute precisa ser inteiro.',
    'ip'                   => 'O campo :attribute tem que ser um endereço IP válido.',
    'ipv4'                 => 'O campo :attribute tem que ser um endereço IPv4 válido.',
    'ipv6'                 => 'O campo :attribute tem que ser um endereço IPv6 válido.',
    'json'                 => 'O campo :attribute precisa ser uma string JSON válida.',
    'max'                  => [
        'numeric' => 'O campo :attribute precisa ser no máximo :max.',
        'file'    => 'O campo :attribute precisa ter no máximo :max kilobytes.',
        'string'  => 'O campo :attribute precisa ter no máximo :max caracteres.',
        'array'   => 'O campo :attribute precisa ter no máximo :max itens.',
    ],
    'mimes'                => 'O campo :attribute precisa ser um arquivo do tipo: :values.',
    'mimetypes'            => 'O campo :attribute precisa ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => 'O campo :attribute precisa ser pelo menos :min.',
        'file'    => 'O campo :attribute precisa ter pelo menos :min kilobytes.',
        'string'  => 'O campo :attribute precisa ter pelo menos :min characteres.',
        'array'   => 'O campo :attribute precisa ter pelo menos :min itens.',
    ],
    'not_in'               => 'O campo :attribute selecionado é inválido.',
    'numeric'              => 'O campo :attribute precisa ser um número.',
    'present'              => 'O campo :attribute precisa estar presente.',
    'regex'                => 'O formato do campo :attribute é inválido.',
    'required'             => 'O campo :attribute é necessário.',
    'required_if'          => 'O campo :attribute é necessário quando :other tem o valor :value.',
    'required_unless'      => 'O campo :attribute é necessário a menos que :other seja :values.',
    'required_with'        => 'O campo :attribute é necessário quando :values está presente.',
    'required_with_all'    => 'O campo :attribute é necessário quando :values estão presentes.',
    'required_without'     => 'O campo :attribute é necessário quando :values não está presente.',
    'required_without_all' => 'O campo :attribute é necessário quando nenhum dos valores :values estão presentes.',
    'same'                 => 'Os campos :attribute e :other precisam corresponder.',
    'size'                 => [
        'numeric' => 'O campo :attribute precisa ter o tamanho :size.',
        'file'    => 'O campo :attribute precisa ter :size kilobytes.',
        'string'  => 'O campo :attribute precisa ter :size characters.',
        'array'   => 'O campo :attribute precisa conter :size itens.',
    ],
    'string'               => 'O campo :attribute precisa ser uma string.',
    'timezone'             => 'O campo :attribute precisa ser uma zona válida.',
    'unique'               => 'O valor do campo :attribute já foi usado.',
    'uploaded'             => 'Falhou ao tentar realizar o upload de :attribute.',
    'url'                  => 'O formato de :attribute é inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'password'=>"senha",
        'email'=>"e-mail",
        'password_confirmation'=>'confirmação de senha',
        "name"=>"nome",
        "year"=>"ano",
        "camisas_masc"=>"camisas masculinas",
        "camisas_fem"=>"camisas femininas",
        "qtd"=>"quantidade",
        "eggcod"=>"Código",
        "tipouser"=>"Tipo",
        "ufrj-dre"=>"DRE"
    ],

];
