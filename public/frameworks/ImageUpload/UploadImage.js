(function($) {
    const imgUploadModel = (name, id, imgSrc, styleImg, attr)=>{
        const ID = ()=>'_img_' + Math.random().toString(36).substr(2, 9);
        id=id||ID();
        styleImg = styleImg||"max-width: 100%; max-height: 80px";

        return $(`
        <div style="border-color: #ced4da" class="fileUploadImg border mb-0 rounded">
            <div class="d-flex justify-content-center p-2">
                <img style="${styleImg}" src="${imgSrc?imgSrc:""}" alt="">
            </div>
            <div>
                <input type="file" name="${name}" class="d-none" id="${id}" >
                <label style="border-radius: 0 0 4px 4px" class="btn btn-dark w-100 mb-0" for="${id}">
                    <i class="fa fa-upload"></i>
                </label>
            </div>
        </div>  
        `);
    };

    function fileUploadEvent(imgUpload){
        imgUpload.find('input[type=file]').on('change', function() {

            if (this.files && this.files[0]) {
                let imgcontainer = $(this).closest(".fileUploadImg");
                let img = imgcontainer.find("img");
                let reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
                reader.addEventListener("load",function (e) {
                    img[0].src = reader.result;
                });
            }
        });
    }

    $.fn.extend({
        setUploadImg: function() {
            $(this).each((index, elm)=> {
                elm = $(elm);
                if (elm.attr("type") === "fileimg") {
                    let name = elm.attr("name");
                    let id = elm.attr("id");
                    let imgSrc = elm.val();
                    let styleimg =elm.attr("data-style");
                    let attr = elm.prop("attributes");
                    let newElement = imgUploadModel(name, id, imgSrc, styleimg);
                    let inputElm = newElement.find("input");
                    $.each(attr, function() {
                        if(this.name==="id" ||this.name==="data-style" || this.name==="value" ||  this.name==="type" || this.name ==="name" ) return "";
                       inputElm.attr(this.name, this.value);
                    });
                    elm.replaceWith(newElement);
                    fileUploadEvent(newElement);
                    return newElement;
                } else {
                    console.error("can't find input of type fileimg");
                }

            });
        },

    });
    loadUploadImg();
})(jQuery);
function loadUploadImg(){
    $("input[type=fileimg]").setUploadImg();
}
