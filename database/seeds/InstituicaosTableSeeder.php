<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Filesystem\Filesystem;
class InstituicaosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filesystem = new Filesystem;
        $filesystem->cleanDirectory(public_path("images/EventsImages"));
        $filesystem->cleanDirectory(public_path("css/EventsColors"));
        $filesystem->cleanDirectory(storage_path("app"));

        Model::unguard();
        $path = __DIR__."/instituicoes.sql";
        DB::unprepared(file_get_contents($path));
        $this->command->info('Instituições seeded!');
    }
}
