<?php
$cursos_UVA=[
    "ARQUITETURA E URBANISMO",
    "BIOMEDICINA",
    "CIÊNCIAS BIOLÓGICAS",
    "CIÊNCIAS DA COMPUTAÇÃO",
    "DESIGN DE MODA",
    "DIREITO",
    "ENFERMAGEM",
    "ENGENHARIA AMBIENTAL",
    "ENGENHARIA CIVIL",
    "ENGENHARIA DA COMPUTAÇÃO",
    "ENGENHARIA DE PETRÓLEO E GÁS",
    "ENGENHARIA ELÉTRICA",
    "ENGENHARIA MECÂNICA",
    "FISIOTERAPIA",
    "FONOAUDIOLOGIA",
    "JORNALISMO",
    "NUTRIÇÃO",
    "ODONTOLOGIA",
    "PSICOLOGIA",
    "PUBLICIDADE E PROPAGANDA",
    "RELAÇÕES INTERNACIONAIS",
    "SERVIÇO SOCIAL",
    "TURISMO",
    "ENGENHARIA DE PRODUÇÃO",
    "FILOSOFIA",
    "GEOGRAFIA",
    "HISTÓRIA",
    "LETRAS",
    "MATEMÁTICA, COMPUTAÇÃO E SUAS TECNOLOGIAS",
    "PEDAGOGIA",
    "EDUCAÇÃO FÍSICA",
    "CIÊNCIAS ECONÔMICAS",
    "FARMÁCIA",
    "TEOLOGIA",
    "CIÊNCIAS CONTÁBEIS",
    "ADMINISTRAÇÃO",
    "SISTEMAS DE INFORMAÇÃO",
];