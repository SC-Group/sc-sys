<?php 
$cursos_UFRJ = ["ADMINISTRAÇÃO",
"ARQUITETURA E URBANISMO",
"ARTES CÊNICAS",
"ARTES VISUAIS - ESCULTURA",
"ASTRONOMIA",
"CIÊNCIA DA COMPUTAÇÃO",
"LETRAS",
"PSICOLOGIA",
"BIBLIOTECONOMIA E GESTÃO DE UNID INFORMAÇÃO",
"CIÊNCIAS ATUARIAIS",
"CIÊNCIAS ATUARIAIS / ESTATÍSTICA",
"CIÊNCIAS BIOLÓGICAS",
"CIÊNCIAS BIOLÓGICAS - MODALIDADE MÉDICA",
"BIOFÍSICA",
"BIOTECNOLOGIA",
"MICROBIOL E IMUNOLOGIA",
"CIÊNCIAS CONTÁBEIS",
"CIÊNCIAS ECONÔMICAS",
"CIÊNCIAS MATEMÁTICAS E DA TERRA",
"CIÊNCIAS SOCIAIS",
"COMPOSIÇÃO DE INTERIOR",
"COMPOSIÇÃO PAISAGÍSTICA",
"COMUNICAÇÃO SOCIAL",
"COMUNICAÇÃO VISUAL DESIGN",
"CONSERVAÇÃO E RESTAURAÇÃO",
"DANÇA",
"DEFESA E GESTÃO ESTRATÉGICA INTERNACIONAL",
"DESENHO INDUSTRIAL",
"DIREITO",
"EDUCAÇÃO FÍSICA",
"ENFERMAGEM E OBSTETRÍCIA",
"ENGENHARIA (CICLO BÁSICO)",
"ENGENHARIA AMBIENTAL",
"ENGENHARIA DE COMPUTAÇÃO E INFORMAÇÃO",
"ENGENHARIA DE CONTROLE E AUTOMAÇÃO",
"ENGENHARIA DE PETRÓLEO",
"ENGENHARIA NAVAL",
"ENGENHARIA MECÂNICA",
"ENGENHARIA CIVIL",
"ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO",
"ENGENHARIA DE PRODUÇÃO",
"ENGENHARIA ELÉTRICA",
"ENGENHARIA QUÍMICA",
"ESCULTURA",
"ESTATÍSTICA",
"FARMÁCIA",
"FILOSOFIA",
"FÍSICA",
"FÍSICA MÉDICA",
"FISIOTERAPIA",
"FONOAUDIOLOGIA",
"GASTRONOMIA",
"GEOGRAFIA",
"GEOLOGIA",
"GESTÃO PÚBLICA DESENV ECONÔMICO E SOCIAL",
"GRAVURA",
"HISTÓRIA",
"HISTÓRIA DA ARTE",
"JORNALISMO",
"LETRAS",
"ED ARTÍSTICA",
"MATEMÁTICA",
"MATEMÁTICA APLICADA",
"MEDICINA",
"METEOROLOGIA",
"MÚSICA",
"MUSICOTERAPIA",
"NANOTECNOLOGIA",
"NUTRIÇÃO",
"ODONTOLOGIA",
"PEDAGOGIA",
"PINTURA",
"QUÍMICA",
"RELAÇÕES INTERNACIONAIS",
"SAÚDE COLETIVA",
"SERVIÇO SOCIAL",
"TEORIA DA DANÇA",
"TERAPIA OCUPACIONAL"];
