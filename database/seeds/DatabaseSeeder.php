<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(InstituicaosTableSeeder::class);
        $this->call(CursosTableSeeder::class);
        $this->call(ComissaosTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
