<?php

use App\Models\Comissao;
use Illuminate\Database\Seeder;

class ComissaosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comissao = new Comissao();
        $comissao->nome = "Inscrições e Credenciamento";
        $comissao->shortname = "insccred";
        $comissao->save();

        $comissao = new Comissao();
        $comissao->nome = "Administrativo e Financeiro";
        $comissao->shortname = "admfin";
        $comissao->save();

        $comissao = new Comissao();
        $comissao->nome = "Patrocínio";
        $comissao->shortname = "patrocinio";
        $comissao->save();

        $comissao = new Comissao();
        $comissao->nome = "Comunicação";
        $comissao->shortname = "comm";
        $comissao->save();

        $comissao = new Comissao();
        $comissao->nome = "Staffs";
        $comissao->shortname = "staffs";
        $comissao->save();

        $comissao = new Comissao();
        $comissao->nome = "Infraestrutura";
        $comissao->shortname = "infra";
        $comissao->save();

        $comissao = new Comissao();
        $comissao->nome = "Programação e Atividades";
        $comissao->shortname = "proativ";
        $comissao->save();

        $this->command->info('Comissões seeded!');
    }
}
