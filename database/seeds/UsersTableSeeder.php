<?php

use App\Models\Aluno;
use App\Models\Comissao;
use App\Models\Instituicao;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Http\Request;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $email=env("STANDARD_APP_LOGIN","semanadacomputacao@dcc.ufrj.br");
        $name=env("STANDARD_APP_NAME","Semana da Computação");

        $dir = Comissao::where('shortname',env("STANDARD_APP_DIRETORIA","insccred"))->first();

        $ufrj = Instituicao::where('sigla','UFRJ')->first();
        $cc = $ufrj->cursos()->where('nome',"CIÊNCIA DA COMPUTAÇÃO")->first();

        $user = new User;
        $aluno = new Aluno();

        $aluno->estagio= boolval(env("STANDARD_APP_ESTAGIO",true));
        $aluno->ufrj_dre = env("STANDARD_APP_DRE",999999999);
        $aluno->instituicao()->associate($ufrj);
        $aluno->curso()->associate($cc);

        $aluno->comissao()->associate($dir);

        $user->name = $name;
        $user->email = $email;
        $passwd = "scadmin";
        $user->password=bcrypt($passwd);
        $user->save();
        $aluno->user()->associate($user);
        $aluno->save();

        echo "Usuário administrador criado.\n";
        echo "Usuário: $email\n";
        echo "Senha: $passwd\n";

//        $pass = new App\Http\Controllers\Auth\ForgotPasswordController();
//        $response = $pass->broker()->sendResetLink(["email"=>$user->email]);
//
//       if($response == Password::RESET_LINK_SENT) {
//        echo "Email enviado";
//       }else {
//            echo "Erro ao enviar email";
//       }

    }
}
