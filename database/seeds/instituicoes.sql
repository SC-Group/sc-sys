INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Universidade Federal do Rio de Janeiro','UFRJ','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Universidade Estadual do Rio de Janeiro','UERJ','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Universidade Federal do Estado do Rio de Janeiro','UNIRIO','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Centro Federal de Educação Tecnológica Celso Suckow da Fonseca','CEFET','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Centro Federal de Educação Tecnológica Celso Suckow da Fonseca','CEFET','RIO DE JANEIRO','RJ','Escola de Ensino Técnico');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Fundação de Apoio a Escola Técnica','FAETEC','RIO DE JANEIRO','RJ','Escola de Ensino Técnico');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Fundação de Apoio a Escola Técnica','FAETEC','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Pontíficia Universidade Católica do Rio de Janeiro','PUC','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Universidade Veiga de Almeida','UVA','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Universidade Castelo Branco','UCB','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Universidade Santa Úrsula','USU','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Universidade Estácio de Sá','ESTACIO','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Universidade Federal Fluminense','UFF','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Universidade do Grande Rio','UNIGRANRIO','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Colégio Militar do Rio de Janeiro','CM','RIO DE JANEIRO','RJ','Escola de Ensino Médio');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Colégio Pedro II','CPII','RIO DE JANEIRO','RJ','Escola de Ensino Médio');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Colégio Pedro II','CPII','RIO DE JANEIRO','RJ','Escola de Ensino Técnico');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Instituto Federal do Rio de Janeiro','IFRJ','RIO DE JANEIRO','RJ','Escola de Ensino Técnico');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Instituto Federal do Rio de Janeiro','IFRJ','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Escola Politécnica de Saúde Joaquim Venâncio','FIOCRUZ','RIO DE JANEIRO','RJ','Escola de Ensino Técnico');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Ensino Médio Regular','Ensino Médio','RIO DE JANEIRO','RJ','Escola de Ensino Médio');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Ensino Fundamental Regular','Ensino Fundamental','RIO DE JANEIRO','RJ','Escola de Ensino Fundamental');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Colégio Estadual José Leite Lopes','NAVE','RIO DE JANEIRO','RJ','Escola de Ensino Técnico');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Universidade Federal Rural do Rio de Janeiro','UFRRJ','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Fundação Getúlio Vargas','FGV','RIO DE JANEIRO','RJ','Universidade');

INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Outra','-','RIO DE JANEIRO','RJ','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Outra','-','RIO DE JANEIRO','RJ','Escola de Ensino Técnico');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Outra','-','RIO DE JANEIRO','RJ','Escola de Ensino Fundamental');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Outra','-','RIO DE JANEIRO','RJ','Escola de Ensino Médio');

-- São Paulo
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Universidade Federal de São Paulo','UNIFESP','SÃO PAULO','SP','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Universidade de São Paulo','USP','SÃO PAULO','SP','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Fundação Getúlio Vargas','FGV','SÃO PAULO','SP','Universidade');

INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Outra','-','SÃO PAULO','SP','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Outra','-','SÃO PAULO','SP','Escola de Ensino Técnico');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Outra','-','SÃO PAULO','SP','Escola de Ensino Fundamental');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Outra','-','SÃO PAULO','SP','Escola de Ensino Médio');



INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Outra','-','Outro','-','Universidade');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Outra','-','Outro','-','Escola de Ensino Técnico');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Outra','-','Outro','-','Escola de Ensino Fundamental');
INSERT INTO instituicaos(nome,sigla,uf_nome,uf_sigla,tipo)
VALUES ('Outra','-','Outro','-','Escola de Ensino Médio');

