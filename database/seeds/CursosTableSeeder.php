<?php

use Illuminate\Database\Seeder;
use App\Models\Curso;
use App\Models\Instituicao;
class CursosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private function load($inst,$cursos){
        foreach ($cursos as $strcurso){
            $curso = Curso::firstOrCreate(["nome"=>$strcurso]);
            $curso->nome = $strcurso;
            $curso->instituicoes()->attach($inst);
            $curso->save();
        }
    }

    public function run()
    {
        $tipos = ["ens_fund","ens_med","ens_sup","ens_tec"];
        $N=[];
        $path = __DIR__."/InstituicaoCursos";
        foreach ($tipos as $tipo){
            $N[$tipo] = [];
            foreach (glob($path."/".$tipo."/*",GLOB_ONLYDIR) as $dir) {
                $dirname = explode("/",$dir);
                $dirname = ($dirname[count($dirname)-1]);
                $N[$tipo][strtoupper($dirname)] = [];
                foreach (glob($path."/".$tipo."/".$dirname."/*.php") as $file) {
                    $filename = explode("/",$file);
                    $filename = ($filename[count($filename)-1]);
                    $instituicao = strtoupper(explode("_",$filename)[0]);
                    require $file;
                    $N[$tipo][strtoupper($dirname)][$instituicao] = ${"cursos_".$instituicao};
                }
            }
        }



        foreach ($N["ens_sup"] as $est_name=>$estado){
            foreach ($estado as $strinst=>$cursos){
                array_push($cursos,"OUTRO");
                $inst = Instituicao::where("uf_sigla",$est_name)->where("sigla",$strinst)->where("tipo","Universidade")->first();
                if($inst!=null){
                    $this->load($inst,$cursos);
                }
            }
        }

        foreach ($N["ens_med"] as $est_name=>$estado){
            foreach ($estado as $strinst=>$cursos){
                array_push($cursos,"OUTRO");
                $inst = Instituicao::where("uf_sigla",$est_name)->where("sigla",$strinst)->where("tipo","Escola de Ensino Médio")->first();
                if($inst!=null){
                    $this->load($inst,$cursos);
                }
            }
        }

        foreach ($N["ens_tec"] as $est_name=>$estado){
            foreach ($estado as $strinst=>$cursos){
                array_push($cursos,"OUTRO");
                $inst = Instituicao::where("uf_sigla",$est_name)->where("sigla",$strinst)->where("tipo","Escola de Ensino Técnico")->first();
                if($inst!=null){
                    $this->load($inst,$cursos);
                }
            }
        }

        foreach ($N["ens_fund"] as $est_name=>$estado){
            foreach ($estado as $strinst=>$cursos){
                array_push($cursos,"OUTRO");
                $inst = Instituicao::where("uf_sigla",$est_name)->where("sigla",$strinst)->where("tipo","Escola de Ensino Fundamental")->first();
                if($inst!=null){
                    $this->load($inst,$cursos);
                }
            }
        }

        $instituicoes = Instituicao::where("nome","Outra")->get();
        foreach ($instituicoes as $inst){
            $cursos = ["CIÊNCIA DA COMPUTAÇÃO", "SISTEMAS DE INFORMAÇÃO","ENGENHARIA DA COMPUTAÇÃO","OUTRO"];
            $this->load($inst,$cursos);
        }

        $this->command->info('Cursos seeded!');
    }
}

