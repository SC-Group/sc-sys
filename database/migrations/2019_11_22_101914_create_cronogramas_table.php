<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronogramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cronogramas', function (Blueprint $table) {
            $table->increments("id");

            $table->integer("atividade_id")->unsigned()->nullable();
            $table->integer("local_id")->unsigned()->nullable();

            $table->integer("hora_id")->unsigned()->nullable();
            $table->integer("dia_id")->unsigned()->nullable();

            $table->timestamps();

            $table->foreign("atividade_id")->references("id")->on("atividades")->onDelete("cascade");
            $table->foreign("local_id")->references("id")->on("locals")->onDelete("cascade");

            $table->foreign("hora_id")->references("id")->on("horas")->onDelete("cascade");
            $table->foreign("dia_id")->references("id")->on("dias")->onDelete("cascade");

            $table->unique(["hora_id", "dia_id", "local_id", "atividade_id"],"hora_local_ativ");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cronogramas');
    }
}
