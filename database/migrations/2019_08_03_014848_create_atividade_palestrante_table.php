<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtividadePalestranteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atividade_palestrante', function (Blueprint $table) {
            $table->integer('atividade_id')->unsigned();
            $table->integer('palestrante_id')->unsigned();
            $table->boolean("presente")->default(false);
            $table->timestamps();
            $table->foreign("atividade_id")->references("id")->on("atividades")->onDelete("cascade");
            $table->foreign("palestrante_id")->references("id")->on("palestrantes")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atividade_palestrante');
    }
}
