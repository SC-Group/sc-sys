<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("ano");
            $table->dateTime("inicio");
            $table->dateTime("fim");
            $table->dateTime("inicio_insc");
            $table->string("coordenador_name")->nullable();
            $table->string("coordenador_sign")->nullable();
            $table->float("coordenador_sign_pos")->nullable()->default(0);
            $table->string("chefe_dept_name")->nullable();
            $table->string("chefe_dept_sign")->nullable();
            $table->float("chefe_dept_sign_pos")->nullable()->default(0);
            $table->string("url_img_logo")->nullable();
            $table->string("url_img_bg")->nullable();
            $table->string("url_img_comercial")->nullable();
            $table->string("url_img_camisas_fem")->nullable();
            $table->string("url_img_camisas_masc")->nullable();
            $table->string("css_url")->nullable();
            $table->string("primary_color")->nullable();
            $table->string("secondary_color")->nullable();
            $table->string("bg1_color")->nullable();
            $table->string("bg2_color")->nullable();
            $table->string("stress_color")->nullable();
            $table->integer('edicao')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
