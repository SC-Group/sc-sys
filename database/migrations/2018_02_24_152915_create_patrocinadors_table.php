<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatrocinadorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patrocinadors', function (Blueprint $table) {
            $table->increments('id');
            $table->string("nome");
            $table->string("razaosocial");
            $table->string("responsavel");
            $table->string("telefone");
            $table->string("email");
            $table->string("url_logo");
            $table->string("cnpj")->nullable();
            $table->string("inscmun")->nullable();
            $table->string("inscest")->nullable();
            $table->string("site")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patrocinadors');
    }
}
