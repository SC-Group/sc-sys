<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNivelPatrocinioPatrocinadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nivel_patrocinio_patrocinador', function (Blueprint $table) {
            $table->integer('nivel_patrocinio_id')->unsigned();
            $table->integer('patrocinador_id')->unsigned();
            $table->timestamps();

            $table->foreign("nivel_patrocinio_id")->references("id")->on("nivel_patrocinios")->onDelete("cascade");
            $table->foreign("patrocinador_id")->references("id")->on("patrocinadors")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nivel_patrocinio_patrocinador');
    }
}
