<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePalestrantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('palestrantes', function (Blueprint $table) {
            $table->increments('id');

            $table->string("tel")->nullable();
            $table->string("empresa")->nullable();
            $table->integer("evento_id")->unsigned();
            $table->integer("user_id")->unsigned();
            $table->timestamps();


            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("evento_id")->references("id")->on("eventos")->onDelete("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('palestrantes');
    }
}
