<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAtividadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("atividades",function (Blueprint $table){
            $table->boolean("gera_certificado")->default(false);
            $table->boolean("aprovada")->default(false);
            $table->enum("tipo_organizacao_vinculada",["Grupo de Interesse","Empresa"])->nullable();
            $table->string("organizacao_vinculada")->nullable();
            $table->string("codigo")->nullable();
            $table->integer("evento_id")->unsigned();
            $table->text("recursos")->nullable();
            $table->boolean('especial')->default(false);

            $table->foreign("evento_id")->references("id")->on("eventos")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("atividades",function (Blueprint $table){
            $table->dropColumn("gera_certificado");
            $table->dropColumn("aprovada");
            $table->dropColumn("tipo_organizacao_vinculada");
            $table->dropColumn("organizacao_vinculada");
            $table->dropColumn("codigo");
            $table->dropColumn("recursos");
            $table->dropColumn('especial');
            $table->dropForeign("atividades_evento_id_foreign");
            $table->dropColumn("evento_id");
        });
    }
}
