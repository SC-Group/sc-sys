<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer("curso_id_ex_ufrj")->unsigned()->nullable();
            $table->foreign("curso_id_ex_ufrj")->references("id")->on("cursos")->onDelete("set null");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['curso_id_ex_ufrj']);
            $table->dropColumn(["curso_id_ex_ufrj"]);
        });
    }
}
