<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNivelPatrociniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nivel_patrocinios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("nivel");
            $table->string("nome");
            $table->integer("evento_id")->unsigned();
            $table->timestamps();

            $table->foreign("evento_id")->references("id")->on("eventos")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nivel_patrocinios');
    }
}
