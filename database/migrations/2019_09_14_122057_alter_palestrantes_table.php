<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPalestrantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("palestrantes",function(Blueprint $table){
            $table->dropColumn("tel");
            $table->dropColumn("empresa");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("palestrantes",function(Blueprint $table){
            $table->string("empresa")->nullable();
            $table->string("tel")->nullable();
        });
    }
}
