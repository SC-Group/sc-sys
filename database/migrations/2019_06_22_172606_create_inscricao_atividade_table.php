<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscricaoAtividadeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscricao_atividade', function (Blueprint $table) {
            $table->integer('inscricao_id')->unsigned();
            $table->integer('atividade_id')->unsigned();
            $table->boolean("preinscrito")->default(false);
            $table->boolean("presente")->default(false);
            $table->timestamps();

            $table->foreign("inscricao_id")->references("id")->on("inscricaos")->onDelete("cascade");
            $table->foreign("atividade_id")->references("id")->on("atividades")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscricao_atividade');
    }
}
