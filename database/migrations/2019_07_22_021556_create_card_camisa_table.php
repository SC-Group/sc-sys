<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardCamisaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_camisa', function (Blueprint $table) {
            $table->integer('card_id')->unsigned();
            $table->integer('camisa_id')->unsigned();
            $table->integer("pgantes")->default(0);
            $table->integer("pgdepois")->default(0);
            $table->integer("entregues")->default(0);
            $table->integer("reservadas")->default(0);
            $table->timestamps();

            $table->foreign("card_id")->references("id")->on("cards")->onDelete("cascade");
            $table->foreign("camisa_id")->references("id")->on("camisas")->onDelete("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_camisa');
    }
}
