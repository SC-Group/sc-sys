<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventoApoioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evento_apoio', function (Blueprint $table) {
            $table->integer('evento_id')->unsigned();
            $table->integer('apoio_id')->unsigned();
            $table->timestamps();

            $table->foreign("evento_id")->references("id")->on("eventos")->onDelete("cascade");
            $table->foreign("apoio_id")->references("id")->on("apoios")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evento_apoio');
    }
}
