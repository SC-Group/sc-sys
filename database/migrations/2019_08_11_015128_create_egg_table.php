<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEggTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eggs', function (Blueprint $table) {
            $table->increments("id");
            $table->string("cod");
            $table->string('premio');
            $table->string("url_img");
            $table->boolean("exibido")->default(false);
            $table->integer("evento_id")->unsigned();
            $table->integer("inscricao_id")->unsigned()->nullable();
            $table->timestamps();

            $table->foreign("evento_id")->references("id")->on("eventos")->onDelete("cascade");
            $table->foreign("inscricao_id")->references("id")->on("inscricaos")->onDelete("set null");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eggs");
    }
}
