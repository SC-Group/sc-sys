<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisponibilidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disponibilidades', function (Blueprint $table) {
            $table->increments("id");

            $table->integer("staff_id")->unsigned()->nullable();
            $table->integer("palestrante_id")->unsigned()->nullable();

            $table->integer("hora_id")->unsigned()->nullable();
            $table->integer("dia_id")->unsigned()->nullable();

            $table->timestamps();

            $table->foreign("staff_id")->references("id")->on("staffs")->onDelete("cascade");
            $table->foreign("palestrante_id")->references("id")->on("palestrantes")->onDelete("cascade");

            $table->foreign("hora_id")->references("id")->on("horas")->onDelete("cascade");
            $table->foreign("dia_id")->references("id")->on("dias")->onDelete("cascade");

            $table->unique(["hora_id", "dia_id", "palestrante_id", "staff_id"],"hora_palest_staff");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disponibilidades');
    }
}
