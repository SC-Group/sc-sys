<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApoiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apoios', function (Blueprint $table) {
            $table->increments('id');
            $table->string("nome");
            $table->string("responsavel");
            $table->string("tel_responsavel")->nullable();
            $table->string("email_responsavel");
            $table->string("cargo_responsavel")->nullable();
            $table->string("logo_url");
            $table->string("site")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apoios');
    }
}
