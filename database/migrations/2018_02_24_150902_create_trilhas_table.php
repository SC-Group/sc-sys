<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrilhasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trilhas', function (Blueprint $table) {
            $table->increments('id');
            $table->string("nome");
            $table->string("cor");
            $table->text("descricao")->nullable();
            $table->integer("evento_id")->unsigned();
            $table->timestamps();

            $table->foreign("evento_id")->references("id")->on("eventos")->onDelete("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trilhas');
    }
}
