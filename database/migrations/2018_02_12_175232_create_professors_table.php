

<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professors', function (Blueprint $table) {
            $table->integer('id')->unsigned();


            $table->string("area_atuacao")->nullable();

            $table->integer("instituicao_id")->unsigned();

            $table->integer("curso_id")->unsigned()->nullable();


            $table->timestamps();

            $table->foreign("instituicao_id")->references("id")->on("instituicaos")->onDelete("cascade");


            $table->foreign("id")->references("id")->on("users")->onDelete("cascade");

            $table->foreign("curso_id")->references("id")->on("cursos")->onDelete("set null");
            $table->primary('id');


        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professors');
    }
}
