<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstituicaoCursoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instituicao_curso', function (Blueprint $table) {
            $table->integer('instituicao_id')->unsigned();
            $table->integer('curso_id')->unsigned();
            $table->timestamps();

            $table->foreign("instituicao_id")->references("id")->on("instituicaos")->onDelete("cascade");
            $table->foreign("curso_id")->references("id")->on("cursos")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instituicao_curso');
    }
}
