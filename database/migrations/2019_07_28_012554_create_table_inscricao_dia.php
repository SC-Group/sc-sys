<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInscricaoDia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscricao_dia', function (Blueprint $table) {
            $table->integer('inscricao_id')->unsigned();
            $table->integer('dia_id')->unsigned();
            $table->enum("presenca",["Presente","Faltou"])->nullable();

            $table->timestamps();

            $table->foreign("inscricao_id")->references("id")->on("inscricaos")->onDelete("cascade");
            $table->foreign("dia_id")->references("id")->on("dias")->onDelete("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscricao_dia');
    }
}
