<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffAtividadeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_atividade', function (Blueprint $table) {
            $table->integer('staff_id')->unsigned();
            $table->integer('atividade_id')->unsigned();
            $table->timestamps();

            $table->foreign("staff_id")->references("id")->on("staffs")->onDelete("cascade");
            $table->foreign("atividade_id")->references("id")->on("atividades")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_atividade');
    }
}
