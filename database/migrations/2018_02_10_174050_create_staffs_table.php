<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffs', function (Blueprint $table) {
            $table->increments("id");

            $table->string("status")->nullable();

            $table->integer("comissao_id")->unsigned()->nullable();
            $table->integer('aluno_id')->unsigned();
            $table->integer('evento_id')->unsigned();
            $table->timestamps();

            $table->foreign("evento_id")->references("id")->on("eventos")->onDelete("cascade");
            $table->foreign("aluno_id")->references("id")->on("alunos")->onDelete("cascade");
            $table->foreign("comissao_id")->references("id")->on("comissaos")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staffs');
    }
}
