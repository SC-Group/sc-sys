<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCamisas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('camisas', function (Blueprint $table) {
            $table->dropColumn(["preco_antes", "preco_durante"]);
            $table->renameColumn("qtd_restante", "qtd_comprada");
            $table->string("cor");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
