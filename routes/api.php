<?php

use App\Models\Inscricao;
use Illuminate\Http\Request;
use \App\Http\Services\SearchUserService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('client:be-staff')->post('/getuser', function (Request $request) {
    $data = json_decode($request->getContent());

    if(isset($data->inscIds)) {
        $insc = DB::table('users')
            ->join('inscricaos', 'users.id', '=', 'inscricaos.user_id')
            ->whereIn("inscricaos.id",$data->inscIds);
        if ($insc->exists()) {
            return response()->json($insc->get(["inscricaos.id","name", "email"]));
        }
    }
    return response()->json(["msg"=>"falhou em achar usuario","status"=>"error"],400);
});

Route::post('/auth','Auth\Api\AuthorizationController@login');