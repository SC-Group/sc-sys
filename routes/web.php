<?php
use Illuminate\Support\Facades\Request;
use App\Models\Evento;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//*******************************************
Route::name("cronogramas")->namespace("Cronogramas")->prefix("/{year}/cronogramas")->group(function() {
    Route::get('/', "CronogramasController@getCronogramasOverview");
    Route::get('/{dia}/{mes}', "CronogramasController@getCronograma")->name(".getcronograma");
});
Route::post('instituicoes/uf','Forms\DynamicDataController@getInstituicaoEstados');
Route::post('instituicoes','Forms\DynamicDataController@getInstituicoes');
Route::post('instituicoes/tipos','Forms\DynamicDataController@getInstituicaoTipos');
Route::post('instituicoes/cursos','Forms\DynamicDataController@getInstituicaoCursos');

Route::any('insctime','Forms\ClockController@getInscTime');

Route::post('search/users',"SearchUserController@searchUser")->middleware(["auth","can:be-staff"]);

Route::get("/error",function(){
    return App\Http\Services\SCService::view("errors.main", ["msg"=>session("msg")]);
})->name("errorpage");
// Authentication Routes...
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::any('/logout', 'Auth\LoginController@logout')->name('logout');
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
//*******************************************************

Route::get('/{year}/inscricao', 'Forms\InscricaoController@getInscricao')->name('forminscricao');
Route::post('/{year}/inscricao', 'Forms\InscricaoController@postInscricao');
Route::get('/{year}/inscricao/sucesso','Forms\InscricaoController@getInscricaoEnd')->name('inscsuccess');

Route::get('/{year}/inscricaoatividade', 'Forms\SubmissaoAtividadesController@getInscricao')->name('formatividade');
Route::post('/{year}/inscricaoatividade', 'Forms\SubmissaoAtividadesController@postInscricao');
Route::get('/{year}/inscricaoatividades/sucesso','Forms\SubmissaoAtividadesController@getInscricaoEnd')->name('inscatividadesuccess');

Route::get('/{year}/inscricaostaff', 'Forms\StaffController@getInscricao')->name('formatividade');
Route::post('/{year}/inscricaostaff', 'Forms\StaffController@postInscricao');
Route::get('/{year}/inscricaostaff/sucesso','Forms\StaffController@getInscricaoEnd')->name('inscstaffsuccess');

Route::name("certificados")->namespace("Certificados")->prefix("/{year}/certificados")->middleware(["auth"])->group(function() {
    Route::any('/participante','CertificadoParticipanteController@getCertificado')->name(".participante");
    Route::any('/palestrante/{cod}','CertificadoPalestranteController@getCertificado')->name(".palestrante");
});

Route::name("profile")->prefix("/{year}/profile")->middleware(["auth"])->group(function () {
    Route::get('/', 'ProfileController@getProfile');
    Route::post('/saveprofile', 'ProfileController@saveProfile')->name(".saveprofile");

    Route::post('/savestaff', 'ProfileController@saveStaffProfile')->name(".savestaff");
    Route::post('/savepalestrante', 'ProfileController@savePalestranteProfile')->name(".savepalestrante");
});
Route::get('/{year?}', 'HomeController@index')->name('home');

Route::post('/{year}/inserteggcod', 'HomeController@validateEgg')->name('inserteggcod');

Route::post('search/users',"SearchUserController@searchUser")->name(".search-users")->middleware(["auth",'can:be-staff']);
Route::post('search/card',"SearchNaoInscritoController@searchCard")->name(".search-card")->middleware(["auth",'can:be-staff']);

Route::name("staff")->namespace("Staff")->prefix("/{year}/staff")->middleware(["auth",'can:be-staff'])->group(function () {

    Route::name(".venda-camisa")->namespace("Camisas")->prefix("vendacamisa")->group(function() {
        Route::get('/','CamisasController@getCamisas');
        Route::post("/entidadecamisas","CamisasController@getCamisasDaEntidade")->name(".getCamisasDaEntidade");
        Route::post("/vendecamisas","VenderCamisasController@venderCamisas")->name(".venderCamisas");
        Route::post("/trocarcamisas","TrocarCamisasController@trocarCamisas")->name(".trocarCamisas");
        Route::post("/devolvercamisas","DevolverCamisasController@devolverCamisas")->name(".devolverCamisas");
        Route::post("/statuscamisas","StatusCamisasController@statusCamisas")->name(".statusCamisas");
    });
    Route::name(".credenciamento")->namespace("Credenciamento")->prefix("credenciamento")->group(function() {
        Route::get('/','CredenciamentoController@getCredenciamento');
        Route::post('/getusers','CredenciamentoController@getUsers')->name(".getUsers");
        Route::post('/alterstatus','CredenciamentoController@alterStatusUser')->name(".alterStatus");
    });

});

Route::name("comissao")->namespace("Comissao")->prefix("/{year}/comissao")->middleware(["auth",'can:be-comissao'])->group(function () {

    Route::name(".evento")->namespace("Evento")->prefix("evento")->group(function(){
        Route::get('/',"EventoController@getConfig");
        Route::post('/create',"EventoController@createEvent")->name(".create");
        Route::post('/alter',"EventoController@alterEvent")->name(".alter");

        Route::post('/colors',"ColorsController@getColorsConfig")->name(".colors");
        Route::post('/savecolors',"ColorsController@setCssColors")->name(".savecolors");

        Route::post('/uploads',"UploadsController@getUploadsConfig")->name(".uploads");
        Route::post("/saveuploads","UploadsController@sendFiles")->name(".saveuploads");

        Route::post('/camisas',"CamisasController@getCamisasConfig")->name(".camisas");
        Route::post('/savecamisas',"CamisasController@sendCamisasConfig")->name(".savecamisas");

        Route::post('/cronograma',"CronogramaController@getConfig")->name(".cronograma");
        Route::post('/savecronograma',"CronogramaController@sendConfig")->name(".savecronograma");

        Route::post('/locais',"LocaisController@getLocaisConfig")->name(".locais");
        Route::post('/savelocais',"LocaisController@saveLocaisConfig")->name(".savelocais");

        Route::post('/trilhas',"TrilhasController@getTrilhasConfig")->name(".trilhas");
        Route::post('/savetrilhas',"TrilhasController@saveTrilhasConfig")->name(".savetrilhas");

        Route::post('/eggs',"EggsController@getEggsConfig")->name(".eggs");
        Route::post('/saveeggs',"EggsController@saveEggsConfig")->name(".saveeggs");

        Route::post('/signs',"AssinaturasController@getAssinaturasConfig")->name(".signs");
        Route::post('/savesigns',"AssinaturasController@saveAssinaturasConfig")->name(".savesigns");

        Route::post('/niveispatrocinio',"NiveisPatrocinioController@getConfig")->name(".niveispatrocinio");
        Route::post('/saveniveispatrocinio',"NiveisPatrocinioController@saveConfig")->name(".saveniveispatrocinio");
    });

    Route::name(".eggs")->namespace("Eggs")->prefix("eggs")->group(function(){
        Route::get('/',"EggsController@getEggsConfig");
    });

    Route::name(".staffs")->namespace("Staffs")->prefix("staffs")->group(function(){
        Route::get('/',"StaffsController@getStaffsConfig");
        Route::post("/staffdata","StaffsController@getStaffData")->name("staffdata");
        Route::post("/saveStaff","StaffsController@saveStaff")->name("savestaff");
    });

    Route::name(".atividades")->namespace("Atividades")->prefix("atividades")->group(function() {
        Route::get('/', "AtividadesController@getAtividadesConfig");
        Route::post('/all', "AtividadesController@getAtividades")->name('.all');
        Route::post('/trilhas', "AtividadesController@getTrilhas")->name('.allTrilhas');
        Route::post('/disponibilidades', "AtividadesController@getDisponibilidades")->name('.allDisponibilidades');
        Route::post("/saveatividade","AtividadesController@saveAtividade")->name(".saveAtividade");
    });

    Route::name(".patrocinios")->namespace("Patrocinios")->prefix("patrocinios")->group(function() {
        Route::get('/', "PatrociniosController@getPatrociniosConfig");
        Route::post('/getEmpresa', "PatrociniosController@getEmpresaDataEdit")->name(".empresa");
        Route::post('/saveEmpresa', "PatrociniosController@saveEmpresaDataEdit")->name(".saveEmpresa");
        Route::post('/getPatrocinador', "PatrociniosController@getPatrocinadorDataEdit")->name(".getPatrocinador");
        Route::post('/savePatrocinador', "PatrociniosController@savePatrocinadorDataEdit")->name(".savePatrocinador");
        Route::post('/changeStatusPatrocinador',"PatrociniosController@changeStatusPatrocinador")->name(".changeStatusPatrocinador");
        Route::post('/changeStatusEmpresa',"PatrociniosController@changeStatusEmpresa")->name(".changeStatusEmpresa");
        Route::post("/getNiveis","PatrociniosController@getNiveis")->name("getNiveis");

    });

    Route::name(".apoios")->namespace("Apoios")->prefix("apoios")->group(function() {
        Route::get('/', "ApoiosController@getApoiosConfig");
        Route::post('/getOrganizacao', "ApoiosController@getOrganizacaoDataEdit")->name(".organizacao");
        Route::post('/saveOrganizacao', "ApoiosController@saveOrganizacaoDataEdit")->name(".saveOrganizacao");
        Route::post('/getApoio', "ApoiosController@getApoioDataEdit")->name(".getApoio");
        Route::post('/saveApoio', "ApoiosController@saveApoioDataEdit")->name(".saveApoio");
        Route::post('/changeStatusOrganizacao',"ApoiosController@changeStatusOrganizacao")->name(".changeStatusOrganizacao");
        Route::post('/changeStatusApoio',"ApoiosController@changeStatusApoio")->name(".changeStatusApoio");

    });

    Route::name(".cronogramas")->namespace("Cronogramas")->prefix("cronogramas")->group(function() {
        Route::get('/', "CronogramasController@getCronogramasOverview");
        Route::get('/{dia}/{mes}', "CronogramasController@getCronograma")->name(".getcronograma");
        Route::post('/{dia}/{mes}', "CronogramasController@saveCronograma")->name(".savecronograma");
    });

});

//ViewRoutes



/*// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
*/